// AutoTest.cpp: implementation of the CAutoTest class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
#include "AutoTest.h"
#include "Mainfrm.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAutoTest::CAutoTest(CMainFrame &mf) :
			m_mf4AT(mf)
{

}


CAutoTest::~CAutoTest()
{

}



void CAutoTest::OnXdDispatch(int code, void *pContext)
{
	switch(code) {
	case TEST_GRAB_CONTINUOUS:
		TestGrabContinuous();
		break;
	case XD_GRAB_CONTINUOUS:
//		OnXdGrabContinuous(0);
		break;
	default:
		break;
	}
}
void CAutoTest::OnXdGrabContinuous(int idx)
{
	CTreeViewCtrl &t =  m_mf4AT.m_xdItemsView.m_tvcXdItems;
	
	HTREEITEM hItem = t.GetFirstVisibleItem();
	hItem = t.GetChildItem(hItem);
	int n = 0;
	do {
		if(n<idx) 
		{
			n++;
		}
		else
		{
			t.SelectItem(hItem);
			m_mf4AT.SendMessage(WM_COMMAND, ID_GRAB_CONTINUOUS, 0);
			return;
		}
	}while (hItem = t.GetNextItem(hItem, TVGN_NEXT));
}

void CAutoTest::TestGrabContinuous()
{
	CTreeViewCtrl	&t		= m_mf4AT.m_xdItemsView.m_tvcXdItems;
	
	HTREEITEM		hItem	= t.GetFirstVisibleItem();
	hItem = t.GetChildItem(hItem);
	do {
		t.SelectItem(hItem);
		m_mf4AT.SendMessage(WM_COMMAND, ID_GRAB_CONTINUOUS, 0);
	}while (hItem = t.GetNextItem(hItem, TVGN_NEXT));

}