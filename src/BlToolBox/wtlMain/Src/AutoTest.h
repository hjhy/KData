// AutoTest.h: interface for the CAutoTest class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AUTOTEST_H__BCB15A32_F666_4B9D_A71C_56EFE771D417__INCLUDED_)
#define AFX_AUTOTEST_H__BCB15A32_F666_4B9D_A71C_56EFE771D417__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMainFrame;

#define	TEST_GRAB_CONTINUOUS	0
#define	XD_GRAB_CONTINUOUS	1


class CAutoTest  
{
public:
	CAutoTest(CMainFrame &MainFrame);
	virtual ~CAutoTest();

	void OnXdDispatch(int code, void *pContext);
	void OnXdGrabContinuous(int idx);

private:
	void TestGrabContinuous();

	CMainFrame &m_mf4AT;
};

#endif // !defined(AFX_AUTOTEST_H__BCB15A32_F666_4B9D_A71C_56EFE771D417__INCLUDED_)
