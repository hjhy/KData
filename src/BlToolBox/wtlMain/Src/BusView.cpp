// BusView.cpp: implementation of the CXdSubItemsView class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DeviceManager.h"
#include "BusView.h"
#include "device.h"
#include "mainfrm.h"
#include "utility.h"



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


//------------------------------------------------------------------------------
// CXdSubItemsView::CXdSubItemsView(CDeviceManager& DeviceManager, CMainFrame& MainFrame) 
//------------------------------------------------------------------------------
/**
* Constructs a new bus viewer window
*
* \param     DeviceManager  Reference to the camera manager 
* \param     MainFrame Reference to the main frame
*/
//------------------------------------------------------------------------------
CXdSubItemsView::CXdSubItemsView(CDeviceManager& dm, CMainFrame& mf) 
: m_refXdDM(dm), m_xdMainFrm(mf), 
 UNABLE_TO_OPEN("<Cannot open device>") 
{
}



LRESULT CXdSubItemsView::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	CTreeViewCtrl &t = m_tvcXdItems;
	t.Attach(GetDlgItem(IDC_BUSTREE));
	
	// Create Image List
	m_ImageList.Create(16, 16, ILC_MASK, 4, 4);
	CBitmap img;
	CBitmap mask;
	// insert image for "This PC"
	img.LoadBitmap(IDB_BITMAP1);
	mask.LoadBitmap(IDB_BITMAP2);
	m_ImageList.Add(img, mask);
	// insert image for "BCAM device"
	img.Detach();
	mask.Detach();
	img.LoadBitmap(IDB_BITMAP3);
	mask.LoadBitmap(IDB_BITMAP4);
	m_ImageList.Add(img, mask);
	//xdTest_004:
	img.Detach();
	mask.Detach();
	img.LoadBitmap(IDB_BITMAP5);
	mask.LoadBitmap(IDB_BITMAP6);
	m_ImageList.Add(img, mask);
	//xdTest_004:  3,3
	img.Detach();
	mask.Detach();
	img.LoadBitmap(IDB_BITMAP_KLINES);
	mask.LoadBitmap(IDB_BITMAP_KLINES1);
	m_ImageList.Add(img, mask);
	
	//xdTest_004:  4,4
	img.Detach();
	mask.Detach();
	img.LoadBitmap(IDB_BITMAP_SONG);
	mask.LoadBitmap(IDB_BITMAP_SONG1);
	m_ImageList.Add(img, mask);

	t.SetImageList(m_ImageList, TVSIL_NORMAL);
	
	// Insert root item
	m_RootItem = t.InsertItem("Ư������԰", 0, 0, TVI_ROOT, TVI_LAST); 
    
	int nTotal = m_xdMainFrm.GetDeviceNum(); 	
	
	for(int i = 1; i <= nTotal; i++){
		xdAddNode(i);
	}
	
	t.Expand(m_RootItem);
	
	return 0;
}




LRESULT CXdSubItemsView::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	NodeMap_t::iterator pEntry;
	for (pEntry = m_NodeMap.begin(); pEntry != m_NodeMap.end(); ++pEntry){
		delete pEntry->second;
	}
	
	bHandled = FALSE;
	return 0;
}




LRESULT CXdSubItemsView::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	RECT rcClient;
	GetWindowRect(&rcClient);
	
	HWND hwnd;
	RECT rc;
	hwnd = GetDlgItem(IDC_BUSTREE);
	::GetWindowRect(hwnd, &rc);
	int w=rcClient.right - rc.left;
	int h=rcClient.bottom - rc.top;

	::SetWindowPos(hwnd, HWND_TOP, 
		0, 0, 
		w, h,
		SWP_NOMOVE|SWP_NOACTIVATE|SWP_NOZORDER);
	
	return 0;
}




LRESULT CXdSubItemsView::OnContextMenu(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
	UINT Flags;
	CPoint point = CPoint(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
	CTreeViewCtrl &t = m_tvcXdItems;
	t.ScreenToClient(&point);
	HTREEITEM hItem = t.HitTest(point, &Flags);
	if (hItem && (Flags & TVHT_ONITEM)){
		t.SelectItem(hItem);
		if (m_NodeMap.find(hItem) != m_NodeMap.end()) {
			CBusNode *pNode = m_NodeMap[hItem];
			//			m_MainFrame->UpdateUI();
			CMenu menuContext;
			menuContext.LoadMenu(IDR_BUSVIEW_CONTEXT_MENU);
			CMenuHandle menuPop(menuContext.GetSubMenu(0));
			t.ClientToScreen(&point);
			m_xdMainFrm.m_CmdBar.TrackPopupMenu(menuPop, TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y);
		}
	}
	
	return 0;
}





//------------------------------------------------------------------------------
// void CXdSubItemsView::CurrentDeviceChanged(CDevice* pDevice)
// Author: 
//------------------------------------------------------------------------------
/**
* To be called if another device is to be selected (e.g. the user actiates an image 
* child window associated with a camera device which isn't selected in the tree view
*
* \param     pDevice Pointer to the camera device
* \return    void
*
*/
//------------------------------------------------------------------------------
void CXdSubItemsView::CurrentDeviceChanged(CDevice* pDevice)
{
	if ( ! pDevice )
		return;
	int DeviceNumber = pDevice->m_nNumber;// pDevice->m_pInfo->DeviceNumber();
	
	NodeMap_t::iterator it;
	for ( it = m_NodeMap.begin(); it != m_NodeMap.end(); ++it )
	{
		if ( it->second->GetDeviceNumber() == DeviceNumber )
		{
			m_tvcXdItems.SelectItem(it->first);
			break;
		}
	}
}




//------------------------------------------------------------------------------
// LRESULT CXdSubItemsView::OnLButtonDblClk(int idCtrl, LPNMHDR pnmh, BOOL& bHandled)
//------------------------------------------------------------------------------
/**
* Called if the user performs a double click on one of the tree view's nodes
*
* \param     idCtrl
* \param     pnmh
* \param     bHandled
* \return    0
*
*/
//------------------------------------------------------------------------------

LRESULT CXdSubItemsView::OnLButtonDblClk(int idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	try
	{
		CTreeViewCtrl &t = m_tvcXdItems;
		HTREEITEM hItem = t.GetSelectedItem();
		if ( m_NodeMap.find(hItem) != m_NodeMap.end() )
		{
			CBusNode* pNode = m_NodeMap[hItem];
			
			// open new MDI child
			if ( m_refXdDM.GetCurrentDevice() != NULL  && ! m_refXdDM.ExistMDIChild() )
				m_refXdDM.AddMDIChild();
			
		}
	} CATCH_REPORT();
	return 0;
}

LRESULT CXdSubItemsView::OnSelChanged(int idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	try{
		LPNMTREEVIEW pnmtv = (LPNMTREEVIEW) pnmh;
		HTREEITEM hItem = pnmtv->itemNew.hItem;
		if ( m_NodeMap.find(hItem) != m_NodeMap.end() ){
			CBusNode* pNode = m_NodeMap[hItem];
			int n = pNode->GetDeviceNumber();
			DWORD error = m_refXdDM.CurrentDeviceChanged(n);
		}
		else{
			// inform the camera manager that currently now device is selected
			m_refXdDM.CurrentDeviceChanged(0);
		}
	}
	CATCH_REPORT();
	
	
	return 0;
}

 
void CXdSubItemsView::xdAddNode(int nNumber)
{
	CBusNode *pNode = NULL;
	CString ModelName = UNABLE_TO_OPEN;
	HVTYPE type;
	
	if ( ContainsNode(nNumber) ){
		RemoveNode(nNumber);
	}
	
	try
	{
		// add a camera device
		CDevice *pDevice = NULL;
		m_refXdDM.AddDevice(nNumber, m_hWnd);
		pDevice = m_refXdDM.GetDevice(nNumber);
		assert ( pDevice != NULL );
		
		try
		{
			ModelName = pDevice->m_strModelName;// pDevice->m_pInfo->DeviceName();
			type = pDevice->m_Type;// pDevice->m_pInfo->DeviceType();
			
			pDevice->OnXdActivate(false);
			try
			{
				pDevice->Set_Default_AOI();
				pDevice->RestoreConfiguration(xdHVDevice::CRegistryPropertyBag::Open(APPLICATION_KEY + ModelName/*pDevice->m_pInfo->DeviceName()*/ + "\\Device"));
			}
			catch ( HVBaseException& e)
			{
				if ( e.Error() != STATUS_INTERNAL_ERROR )
					ReportError(e);
			}
			pDevice->Deactivate();	
		}
		CATCH_REPORT();
	
		
		pNode = new CBusNode(ModelName, nNumber, type);
		if ( pNode == NULL )
			throw HVBaseException(ERROR_OUTOFMEMORY, "CXdSubItemsView::AddNode()");
	}
	CATCH_REPORT();

	CTreeViewCtrl &t = m_tvcXdItems;
	HTREEITEM hItem;
	//xdTest_004:
	if(-1 != ModelName.Find("Klines",0))
	{
		 hItem= t.InsertItem(ModelName, 3, 3, m_RootItem, TVI_LAST);
	}
	else if(-1 != ModelName.Find("Tools",0))
	{
		 hItem= t.InsertItem(ModelName, 4, 4, m_RootItem, TVI_LAST);
	}
	else
	{
		 hItem= t.InsertItem(ModelName, 1, 1, m_RootItem, TVI_LAST);
	}
	t.Expand(m_RootItem);
	m_NodeMap[hItem] = pNode;
}

bool CXdSubItemsView::ContainsNode(int nNumber)
{
	NodeMap_t::iterator pEntry;
	for ( pEntry = m_NodeMap.begin(); pEntry != m_NodeMap.end(); ++pEntry )
	{
		if ( pEntry->second->GetDeviceName() == nNumber )
		{
			return true;
		}
	}
	return false;
}
 
void CXdSubItemsView::RemoveNode(int nNumber)
{
	NodeMap_t::iterator pEntry;
	for ( pEntry = m_NodeMap.begin(); pEntry != m_NodeMap.end(); ++pEntry )
	{
		if ( pEntry->second->GetDeviceNumber() == nNumber )
		{
			m_tvcXdItems.DeleteItem(pEntry->first);
			// destroy node
			delete pEntry->second;
			// remove node from map
			m_NodeMap.erase(pEntry);
			if ( m_tvcXdItems.GetCount() == 0 )
			{ 
			} 
			
			break;
		}
	}
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Protected / private member functions
//
 
void CXdSubItemsView::ReportError(HVBaseException& e) 
{
	m_xdMainFrm.ReportError(e);
}
