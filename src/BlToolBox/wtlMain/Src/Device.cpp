// Device.cpp: implementation of the CDevice class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Device.h"
#include "ChildFrm.h"
#include "mainfrm.h"

#define NEARLYINFINITE 10000

// initialize static members
long CDevice::s_cGrabsActive = 0;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDevice::CDevice(int nNumber, int nIndex, HWND hWndNotify, CMainFrame& MainFrame) : 
m_nNumber(nNumber), 

m_nIndex(nIndex),
m_hWndNotify(hWndNotify), 
m_ppXdBuffers(NULL),
m_pBitmap(NULL),
m_pCurrentBuffer(NULL),
m_pNextBuffer(NULL),
m_fBitmapValid(false),
m_SensorSize(0,0),
m_ImageSize(400,300), ///must have default value
m_Origin(0,0),
m_xdClCode(Color_Mono8),
m_DisplayMode(1),
m_Resolution(Res_Mode0),
m_pInfo(NULL),  
m_pSnapMode(NULL), 

m_fContinuousGrabActive(false),
m_fSingleGrabActive(false), 
m_pChildFrame(NULL),  

m_MainFrame(MainFrame)
{  

	m_bXdAOI = false;
}

CDevice::~CDevice()
{
	try
	{
		// If we are still grabbing, cancel the grabbing
		if ( IsGrabActive() )
		{
			GrabCancel();
		}
	}
	catch ( HVBaseException& e)
	{
		m_MainFrame.ReportError(e);
	}
	
	// Free memory buffers 
	xdReleaseBuffers();

	if ( m_pBitmap != NULL )
		delete m_pBitmap;

	Deactivate();
}	


 
void CDevice::OnXdActivate(bool bInit)
{
	if ( ! IsOpen() )
	{
		CHVBase::Open(m_nNumber, m_hWndNotify);
		if ( bInit)
			xdInitDevice(); 
	}
}


void CDevice::Deactivate()
{
	if ( IsOpen() ){
		CHVBase::Close();
	}
}

void CDevice::xdInitDevice()
{
	m_bXdAOI = m_btflCmd.OnXdIsCanSetAOI(GetHandle());
	SetResolution(m_Resolution);
	m_SensorSize = xd_Get_Sensor_Size(m_Resolution);
	m_ImageSize = m_SensorSize;
	xdSetOutputWindow(m_Origin.x, m_Origin.y, m_ImageSize.cx, m_ImageSize.cy);
} 

std::list<CString> CDevice::DeviceModeList()
{
	std::list<CString> DeviceModes;
	BYTE buffer[256] = {0};
	int size = 0;
	GetDeviceInfo(DESC_RESOLUTION, NULL, &size);
	GetDeviceInfo(DESC_RESOLUTION, buffer, &size);
	int count = size /  (2 * sizeof(DWORD));
	DeviceModes.clear();
	CString strDesc;
	for (int i = 0; i < count; ++i)
	{
		strDesc.Format("mode %d (%d * %d)", 
			i, *((DWORD *)buffer + 2 * i), *((DWORD *)buffer + 2 * i + 1));
		DeviceModes.push_back(strDesc);
	}
	
	return DeviceModes;
}



CDevice::Idx2Mode_t CDevice::DeviceModeMap()
{
	CDevice::Idx2Mode_t DeviceModes;
	BYTE buffer[256] = {0};
	int size = 0;
	GetDeviceInfo(DESC_RESOLUTION, NULL, &size);
	int count = size /  (2 * sizeof(DWORD));
	DeviceModes.clear();
	for (int i = 0; i < count; ++i)
		DeviceModes.insert(Idx2Mode_t::value_type(i, (HVResolution)i));
	
	return DeviceModes;
}



CSize CDevice::xd_Get_Sensor_Size(HVResolution mode) 
{
	CSize sz;
	BYTE buffer[256] = {0};
	int size = 0;
	GetDeviceInfo(DESC_RESOLUTION, NULL, &size);
	GetDeviceInfo(DESC_RESOLUTION, buffer, &size);
	int count = size /  (2 * sizeof(DWORD));
	if ( mode >= count )
		throw HVBaseException(STATUS_PARAMETER_INVALID, "CDevice::SensorSize()");
	sz.cx = *((DWORD *)buffer + 2 * mode);
	sz.cy = *((DWORD *)buffer + 2 * mode + 1);
	
	return sz;
}
 
//------------------------------------------------------------------------------
void CDevice::SetMDIChild(CChildFrame* pChild)
{ 
	m_pChildFrame = pChild; 
	if ( pChild != NULL )
	{
		// Inform the the image view about the current video format and AOI dimensions
		m_pChildFrame->v.xdConfigurationChanged(m_Resolution, 
			m_SensorSize, m_ImageSize, m_Origin); 
	}
}




//------------------------------------------------------------------------------
// void CDevice::GrabSingle()
// Author: 
//------------------------------------------------------------------------------
/**
* Grab single frame
*
* \return    void
*
*/
//------------------------------------------------------------------------------

void CDevice::GrabSingle()
{
	assert ( !m_fContinuousGrabActive );
	assert ( m_pChildFrame != NULL );
	
	m_fSuspendGrab		= false;
	m_pNextBuffer		= NULL;
	m_pCurrentBuffer	= NULL;
	m_fBitmapValid		= false;

	// Setup associated image window
	m_pChildFrame->v.xdConfigurationChanged(m_Resolution, m_SensorSize, m_ImageSize, m_Origin); 
  
	xdCreateNewBuffers(1); 
	
	m_fSingleGrabActive = true;  // the display thread will reset this flag and will free the resources

	// Notify plugins 
	CImageInfo info = {0};
	GetImageInfo(info);
	m_pChildFrame->m_xdPlxMng.OnStartGrabbing(info);
	
	try
	{
		BYTE *ppBuffer[1];
		ppBuffer[0] = *m_ppXdBuffers[0];
		// Grab Image
		xdSnapShot(ppBuffer, 1);  // issue one shot command
	}
	catch(HVBaseException &e)
	{
		m_fSingleGrabActive = false;
		throw e;
	}
	
	m_pNextBuffer = m_ppXdBuffers[0];

	OnXdShowFrame();

	// Notify plugins
	CImageData data = {0};
	GetImageData(data);	
	m_pChildFrame->m_xdPlxMng.OnStopGrabbing(info, data);
}

 
void CDevice::GrabFromFile(CString FileName)
{
	assert ( ! IsGrabActive() );
	assert ( m_pChildFrame != NULL );
	
	// release image buffers and current bitmap
	xdReleaseBuffers();
	if ( m_pBitmap != NULL )
	{
		delete m_pBitmap;
		m_pBitmap = NULL;
		m_fBitmapValid = false;
	}
	
	// Load bitmap from file
	m_pBitmap = new CHVBitmap(FileName);
	if ( m_pBitmap != NULL )
	{
		m_fBitmapValid = true;
 
		m_pChildFrame->v.xdConfigurationChanged((HVResolution)-1,
			m_pBitmap->GetSensorSize(),
			m_pBitmap->GetSize(), 
			m_pBitmap->GetOrigin());
	}
}



//------------------------------------------------------------------------------
// void CDevice::GrabContinuous()
// Author: 
//------------------------------------------------------------------------------
/**
* Activate continuous grabbing
*
* \return void 
*
*/
//------------------------------------------------------------------------------
void CDevice::GrabContinuous()
{
	assert ( ! m_fContinuousGrabActive );
	assert ( m_pChildFrame != NULL );
	
	// Allocate bandwidth, enable Device and queue in buffers, ISO_ENABLE=true
	PrepareContinuousGrab();
	
	m_fContinuousGrabActive = true;  // signals that we are grabbing

	// Notify plugins
	CImageInfo info = {0};
	GetImageInfo(info);
	m_pChildFrame->m_xdPlxMng.OnStartGrabbing(info);
	
	// book keeping about number of active grabs
	s_cGrabsActive ++;
	if ( s_cGrabsActive == 1 )
	{   // Start timer for the frame rate display
		m_MainFrame.SetTimer(TIMERID, TIMERINTERVAL);
	}
	
	// start watches to measure fps
	m_DisplayWatch.Start();
	 
}




//------------------------------------------------------------------------------
// void CDevice::GrabCancel()
// Author: 
//------------------------------------------------------------------------------
/**
* Cancel continuous grabbing.
*  * kill timer to display framerates ( if the last grabbing instance is to be cancelled )
*  * suspend the display thread ( to prevent it from accessing image buffers )
*  * cancel pending i/o requests
*
* \return void
*/
//------------------------------------------------------------------------------

void CDevice::GrabCancel()
{
	DWORD error = 0;
	
	if ( m_fContinuousGrabActive )
	{
		assert(! m_fSingleGrabActive);
		s_cGrabsActive --;   
		if ( s_cGrabsActive == 0 )
			m_MainFrame.KillTimer(TIMERID);
		m_fContinuousGrabActive = false;  // this will signal the thread not to enque further buffers 
		error = CancelContinuousGrab();
		ATLTRACE("m_MainFrame.m_bToClose=%d",m_MainFrame.m_bToClose);
		if(!m_MainFrame.m_bToClose)		m_MainFrame.m_DeviceManager.XD_Show_All_FPS();
	}
	else
	{
		assert(m_fSingleGrabActive);
		assert(! m_fContinuousGrabActive);
		m_fSingleGrabActive = false;
	}
	
	m_CritSect.Lock();
	m_pNextBuffer = NULL;
	m_CritSect.Unlock();

	m_MainFrame.UpdateUI();
    
//	ATLASSERT( m_pCurrentBuffer != NULL);
	
	if ( m_pCurrentBuffer ) 
	{
		CImageInfo info = {0};	
		GetImageInfo(info);		//
		CImageData data = {0};
		data.pRawBuffer		= *m_pCurrentBuffer;
		assert( m_pBitmap != NULL);
		data.pImageBuffer	= *m_pBitmap;
		m_pChildFrame->m_xdPlxMng.OnStopGrabbing(info, data);
	}
}


 
unsigned int CDevice::OnXd_GetFrameRate(double& displayed)
{
	int n = 0;
	assert ( m_fContinuousGrabActive );
	
	double avg = m_DisplayAvg.Avg();
	n	= m_DisplayAvg.All();
	displayed = avg == 0 ? 0.0 : 1.0 / avg;
	m_DisplayAvg.Reset();
	return n;
}


unsigned int CDevice::XD_Get_FPS(double& fps,bool b1)
{ 
	unsigned int n = 0;
	if ( !m_fContinuousGrabActive ) 
	{ 
		n = 0;
		return n;
	}
	else{ 
		double avg = m_DisplayAvg.Avg();
		n	= m_DisplayAvg.All();
		fps = avg == 0 ? 0.0 : 1.0 / avg;
		if(this!=m_MainFrame.m_DeviceManager.GetCurrentDevice())
			m_DisplayAvg.Reset();
		n++;
		return n;
	}
}

//inline 
void CDevice::GetImageInfo(CImageInfo &info)
{
	if ( ( m_xdClCode == Color_Mono8 && ! m_DisplayMode ) || m_xdClCode == Color_RGB8 ){
		info.ImageBitDepth = info.ImageBitDepthReal = 8;
	}
	else{
		info.ImageBitDepth = info.ImageBitDepthReal = 24;
	}

	info.ImageFormat	= (IMAGEFORMAT)m_xdClCode;
	info.ImageWidth		= m_ImageSize.cx;
	info.ImageHeight	= m_ImageSize.cy;
	info.ImageSizeBytes = info.ImageWidth * info.ImageHeight * info.ImageBitDepth / 8;
}



inline void CDevice::GetImageData(CImageData &data)
{
	ATLASSERT( m_pBitmap != NULL);
	ATLASSERT( m_pCurrentBuffer != NULL);
	data.pRawBuffer		= *m_pCurrentBuffer;
	data.pImageBuffer	= *m_pBitmap;	
}

 
bool CDevice::ReconfigurationRequest()
{
	ATLTRACE("ReconfigurationReqest()\n");
	if ( ! IsGrabActive() )
		return true;
	
	// a pending single grab will be terminated
	if ( IsSingleGrabActive() )
	{
		m_fSingleGrabActive = false;
	}
	
	m_fSuspendGrab = true;  // the threads will no longer queue in buffers
	
	if ( IsContinuousGrabActive() ){
		// cancel I/O requests 
		StopSnap();
		CloseSnap();
	}
	
	// now we are ready for a reconfiguration
	
	return true;
	
}


 

void CDevice::OnContinuousGrabError(DWORD err)
{
	HVBaseException e(err, "Image acquisition");
	m_MainFrame.ReportError(e);
	if ( m_fContinuousGrabActive )
		GrabCancel();
}




//------------------------------------------------------------------------------
// void CDevice::CancelContinuousGrab()
// Author: 
//------------------------------------------------------------------------------
/**
* Cancel an active continuous grab. The acquisition thread will be terminated.
*
* \return   error code. 0 indicates success
*/
//------------------------------------------------------------------------------

DWORD CDevice::CancelContinuousGrab()
{
	DWORD error = 0;
	
	m_fSuspendGrab = true;  // the acquisition thread will no longer queue in buffers
	
	try
	{
		StopSnap();
		CloseSnap();             // cancel I/O requests 
	}
	catch ( HVBaseException& e )
	{
		if ( e.Error() != STATUS_NOT_OPEN_SNAP )
			throw e;
	} 
	
	return error;
}




 

void CDevice::PrepareContinuousGrab()
{
	m_fBitmapValid = false;
	if ( m_pChildFrame != NULL ){
		m_pChildFrame->v.xdConfigurationChanged(m_Resolution, m_SensorSize, m_ImageSize, m_Origin); 
	} 
	xdCreateNewBuffers(1);  
	
	m_pNextBuffer		= NULL;
	m_pCurrentBuffer	= NULL;
	
	m_DisplayAvg.Reset(0);
	
	m_fSuspendGrab = false;
	
	BYTE *pBuffer[1];
	pBuffer[0] = *m_ppXdBuffers[0];
	try{
		xdOpenSnap(CDevice::SnapCallbackProc, this);
		xdStartSnap(pBuffer, 1);
	}
	catch ( HVBaseException& e){
		CloseSnap();
		throw e;
	}
}
 
void CDevice::xdCreateNewBuffers(unsigned long n)
{
	try
	{ 
		xdReleaseBuffers();
		
		m_ppXdBuffers = new CHVBitmap*[n];// m_ppBuffers = new BYTE[w*h];
		if ( m_ppXdBuffers == NULL )
		{
			throw HVBaseException(E_OUTOFMEMORY, "CDevice::CreateNewBuffers()");
		}
		ZeroMemory(m_ppXdBuffers, n * sizeof(CHVBitmap*));
		m_cBuffers = n;
	 
		m_xdClCode = Color_Mono8;
		for ( unsigned long i = 0; i < n; i++)
		{
			m_ppXdBuffers[i] = new CHVBitmap(m_ImageSize,
				                             m_SensorSize, 
											 m_Origin,  
											 Color_RGB8//m_xdClCode
											 );
			if ( m_ppXdBuffers[i] == NULL )
				throw HVBaseException(E_OUTOFMEMORY, "CDevice::CreateNewBuffers()");
		}
		
		// Create new bitmap for display purposes		
		// first free an existing one
		if ( m_pBitmap != NULL )
		{
			delete m_pBitmap;
			m_pBitmap = NULL;
			m_fBitmapValid = false;
		}
		
		// We will only allocate an extra bitmap to display, if the Device sends image data
		// in a format we have explicetely to convert. If no conversion is needed, no extra memory 
		// for the display bitmap will be allocated. Instead the grab functions have to ensure that 
		// m_pBitmap points to one of the buffers stored in m_ppBuffers.
		if ( ( m_xdClCode != Color_Mono8 || m_DisplayMode ) && m_xdClCode != Color_RGB8 ) {
			// create a new Bitmap to display
			m_pBitmap = new CHVBitmap(m_ImageSize,
				                      m_SensorSize, 
									  m_Origin, 
				                      Color_RGB8);
		}
		else{
			m_pBitmap = new CHVBitmap(m_ImageSize, m_SensorSize, m_Origin, Color_Mono8);
		}
		if ( m_pBitmap == NULL )
				throw HVBaseException(E_OUTOFMEMORY, "CDevice::CreateBewBuffers()");
	}
	catch (HVBaseException& e)
	{
		xdReleaseBuffers();
		throw e;
	}  
}

void CDevice::xdReleaseBuffers()
{
	if ( m_ppXdBuffers != NULL )
	{
		for ( int i = 0; i < m_cBuffers; i ++ )
		{
			if ( m_ppXdBuffers[i] != NULL && m_ppXdBuffers[i] != m_pBitmap )
			{
				delete m_ppXdBuffers[i];
			}
		}
		delete[] m_ppXdBuffers;
		m_ppXdBuffers = NULL;
		m_cBuffers = 0;
	}
} 
void CDevice::OnXdShowFrame()
{
	static int n = 0;
	n++;
	m_pCurrentBuffer = m_pNextBuffer;
	m_pNextBuffer = NULL;

	if ( m_pCurrentBuffer == NULL ) 
		return ; // no new buffer available, sleep until a new one arrives
	
	try
	{
		CImageInfo info = {0};
		CImageData data = {0};
		
		GetImageInfo(info);
		data.pRawBuffer		= *m_pCurrentBuffer;
		data.pImageBuffer	= *m_pBitmap; 
		int w=info.ImageWidth;
		int h=info.ImageHeight;  
		m_pChildFrame->m_xdPlxMng.OnShowFrame(info, data); 
		m_fBitmapValid = true; 
		if ( m_fSingleGrabActive ){
			m_fSingleGrabActive = false;  
			m_MainFrame.UpdateUI();
		} 
		HDC DC = m_pChildFrame->v.GetDC();
		if(NULL!=DC)
		{
			m_pChildFrame->v.xdBitBlt(DC, m_pBitmap);
			m_pChildFrame->v.ReleaseDC(DC);
			//printf("n=%d\n",n); //test4
		}
		else
		{
			//printf("NULL==DC n=%d\n",n); //test1: n=9859 down!
		}

		
		if ( m_fContinuousGrabActive )
			// actualize moving average ( for fps display purposes)  
			m_DisplayAvg.Add(m_DisplayWatch.Stop(true));
		
	}  //end try

	catch (HVBaseException&  e )
	{
		PostMessage(m_MainFrame, WM_ERROR, e.Error(), (long) this); 
	}
}



//------------------------------------------------------------------------------
// void CDevice::ConfigurationChanged()
// Author: 
//------------------------------------------------------------------------------
/**
* The client informs the Device object that the reconfiguration is done. 
* A suspended grab will be resumed.
*
* \return void
*
*/
//------------------------------------------------------------------------------

void CDevice::ConfigurationChanged()
{
	if ( m_pChildFrame ){
		CImageInfo info = {0};
		GetImageInfo(info);
		m_pChildFrame->m_xdPlxMng.OnManagerNotify(info);
	}

	if ( m_fContinuousGrabActive ){
		try
		{
			PrepareContinuousGrab();
			m_fSuspendGrab = false; 
		}
		catch ( HVBaseException& e )
		{
			m_MainFrame.ReportError(e);
			GrabCancel();
		}
	}
}
 
void CDevice::SetAOI(CRect AOI)
{	
	if ( ReconfigurationRequest() )
	{
		int l=AOI.left;
		int t=AOI.top;
		int w=AOI.Width();
		int h=AOI.Height();
		switch(m_Resolution) {//修正小分辨率小窗口的Bug HYL 2006.9.11
		case Res_Mode1:
			l=l*2;
			t=t*2;
			break;
		case Res_Mode2:
			l=l*4;
			t=t*4;
			break;
		case Res_Mode3:
			l=l*8;
			t=t*8;
			break;
		case Res_Mode4:
			l=l*16;
			t=t*16;
			break;
		default:
			break;
		}
		
		CHVBase::xdSetOutputWindow(l,t,w,h);
		
		m_Origin.x = AOI.left;
		m_Origin.y = AOI.top;
		m_ImageSize.cx = AOI.Width();
		m_ImageSize.cy = AOI.Height(); 
		ConfigurationChanged();
	}
} 

/// Set Video mode
void CDevice::SetVideoMode(HVResolution mode)
{
	if ( ReconfigurationRequest() )
	{
		CHVBase::SetResolution(mode);
		m_Resolution = mode;
		m_SensorSize = xd_Get_Sensor_Size(mode);
				
		m_Origin.SetPoint(0, 0);
		m_ImageSize = m_SensorSize;
		CHVBase::xdSetOutputWindow(m_Origin.x, m_Origin.y,
			m_ImageSize.cx, m_ImageSize.cy);
 
		ConfigurationChanged();
		MaximizeAOI();

	}
}

 
void CDevice::MaximizeAOI()
{
	
	SetAOI(CRect(CPoint(0, 0), m_xdFormat.OnXdGetSensorSize())); 
}

void CDevice::Set_Default_AOI()
{
	m_Resolution = Res_Mode0;
	m_ImageSize = xd_Get_Sensor_Size(Res_Mode0);
	m_Origin.SetPoint(0, 0);
}




//------------------------------------------------------------------------------
// void CDevice::SetBayerToRGBConversion( int mode )
// Author: 
//------------------------------------------------------------------------------
/**
* Enable or disable the Bayer to RGB conversion
*
* \param     mode
* \return    
*
* void
* 
*/
//------------------------------------------------------------------------------

void CDevice::SetDisplayMode( int mode )
{ 
	assert ( mode >= 0 && mode < 5); 
	if ( ReconfigurationRequest() )
	{
		m_DisplayMode = mode;
	}
	ConfigurationChanged();
 	
}



//------------------------------------------------------------------------------
// void CDevice::ParametrizeDevice(DCSVideoMode mode, DCSColorCode code, CPoint position, CSize size, unsigned long bpp)
// Author: 
//------------------------------------------------------------------------------
/**
*  Parametrize camera ( format 7 )
*
* \param     mode
* \param     code
* \param     position
* \param     size
* \param     bpp
* \return    void
*
*/
//------------------------------------------------------------------------------
void CDevice::ParametrizeDevice(HVResolution mode, HVColorCode code, CPoint position, CSize size)
{

}


 
int CALLBACK CDevice::C1XdBtflCMD::xdAddPRP(BTFL_ADD_PRP_INFO *pInfo)
{ 
	int nRet = 0;
	CPropertyListCtrl *p = (CPropertyListCtrl *)(pInfo->pxd1Param); 
   
	p->OnAddItem(*pInfo);

	return nRet;
}

int CALLBACK CDevice::SnapCallbackProc(HV_SNAP_INFO *pInfo)
{
	CDevice *This = (CDevice *)(pInfo->pParam);
	assert(This);
	
	if ( ! This->m_fSuspendGrab )
	{
		This->m_pNextBuffer = This->m_ppXdBuffers[pInfo->nIndex];
		This->OnXdShowFrame(); 
	} 
	return 1;
}

 void CDevice::SaveConfiguration(const CPropertyBagPtr ptrBag) 
{
	 ptrBag->WriteLong("VideoMode", m_Resolution);
	 ptrBag->WriteLong("ImageCx", m_ImageSize.cx);
	 ptrBag->WriteLong("ImageCy", m_ImageSize.cy);
	 ptrBag->WriteLong("OriginX", m_Origin.x);
	 ptrBag->WriteLong("OriginY", m_Origin.y);
	 
	 if ( m_pSnapMode->IsSupported() )
		 m_pSnapMode->Save(ptrBag->CreateBag("SnapMode")); 
	 ptrBag->WriteString("xdstr","str..sdafsdf");
 }
		
void CDevice::RestoreConfiguration(const CPropertyBagPtr ptrBag)
{
	if ( ReconfigurationRequest() )
	{
		if (m_Type==SV1410GCTYPE||m_Type==SV2000GCTYPE)
			return;
		// restore the video mode
		try
		{
			m_Resolution = (HVResolution) ptrBag->ReadLong("VideoMode");
			m_ImageSize.cx = ptrBag->ReadLong("ImageCx");
			m_ImageSize.cy = ptrBag->ReadLong("ImageCy");
			m_Origin.x = ptrBag->ReadLong("OriginX");
			m_Origin.y = ptrBag->ReadLong("OriginY");
		}
		catch ( HVBaseException& )
		{
			m_Resolution = Res_Mode0;
			m_ImageSize = xd_Get_Sensor_Size(Res_Mode0);
			m_Origin.SetPoint(0, 0);
		}
		 
		// restore snap mode settings
		if ( m_pSnapMode->IsSupported() )
			m_pSnapMode->Restore(ptrBag->GetBag("SnapMode"));
 		
  
		try
		{
			CPropertyBagPtr ptrBcamViewerBag = ptrBag->GetBag("BeautifulloverBag");
			m_DisplayMode = ptrBcamViewerBag->ReadLong("DisplayMode");
			
			SetBlank(ptrBcamViewerBag->ReadLong("HBlank"),ptrBcamViewerBag->ReadLong("VBlank"));
		}
		catch ( HVBaseException& )
		{
			// Additional information for bcam viewer not found.
			// Nothing to be done;
		}

		xdInitDevice(); 
		ConfigurationChanged();
	}
}
			
void CDevice::OnPrpScalarChanged(BTFL_PRP_ID id,int v)
{ 
	 m_btflCmd.OnXdPrpScalarChanged(m_hXdHhv,id,v); 
}
			
void CDevice::OnXdPrpClickItem(BTFL_PRP_ID id,int v)
{  
	m_btflCmd.OnXdPrpClickItem(m_hXdHhv,id,v);
	
}		
void CDevice::OnXdPrpListItemChanged(BTFL_PRP_ID id,int v)
{  
	m_btflCmd.OnXdPrpListChanged(m_hXdHhv,id,v);
	
}
void CDevice::_OnAddPRP(CPropertyListCtrl &xdlv)
{ 
	m_btflCmd.OnXdAddPrp(m_hXdHhv,xdlv);
}
BTFLSTATUS CDevice::OnXdCmd(HWND hWnd,BEAUTIFULLOVER_COMMAND_CODE c,CMD_BEAUTIFULLOVER_CONTEXT		&btfCtx)
{ 	
	BTFLSTATUS status = BTFL_STATUS_NOT_SUPPORT_INTERFACE; 
	
	return status;
} 

bool CDevice::C1XdBtflCMD::OnXdIsCanSetAOI(HHV h)
{
	bool							b		= false;
	BTFLSTATUS						r		= BTFL_STATUS_NOT_SUPPORT_INTERFACE; 
	BEAUTIFULLOVER_COMMAND_CODE		c		= CMD_BTFL_AOI;
	CMD_BEAUTIFULLOVER_CONTEXT		ctx		= xdMakeContext1(c,1);	  
	r = BtflCommand(h,c,&ctx);
 
	if( BTFL_STATUS_OK == r)
	{
		b = ctx.BTFL_PARAM.AOI.b;
	}
	else
	{
		XdM(c);
	}
	
	return b;
}


void CDevice::C1XdBtflCMD::XdM(BEAUTIFULLOVER_COMMAND_CODE c)
{
	CString s = "xdUT_001: \n Not finished Good with CODE: ";
	switch(c)
	{
	case CMD_BTFL_AOI:
		s += "CMD_BTFL_AOI";  
		break;
	case CMD_BTFL_BEA01000____________:
		s += "CMD_BTFL_BEA01000____________";  
		break;
	case CMD_BEAUTIFULLOVER_0001:
		s += "CMD_BEAUTIFULLOVER_0001";  
		break;
	case CMD_BTFL_ADD_PRP:
		s += "CMD_BTFL_ADD_PRP";  
		break;
	case CMD_BTFL_PRP_CHANGED:
		s += "CMD_BTFL_PRP_CHANGED";  
		break;
	case CMD_BTFL_0004:
		s += "CMD_BTFL_0004";  
		break;
	case CMD_BTFL_0005:
		s += "CMD_BTFL_0005";  
		break;
	default:
		break;
	}

	::MessageBox(NULL,s,"XdM",IDOK);
}

CMD_BEAUTIFULLOVER_CONTEXT CDevice::C1XdBtflCMD::xdMakeContext1(
																BEAUTIFULLOVER_COMMAND_CODE c,
																int id)
{
	CMD_BEAUTIFULLOVER_CONTEXT		r; 
	r.code = c;

	strcpy(r.szVerify,"BEAUTIFULLOVER");   
	r.BTFL_PARAM.CallBack.pFun		= NULL;
	r.BTFL_PARAM.CallBack.pParam	= NULL;
	r.BTFL_PARAM.AOI.id				= id;
	return r;
}

void CDevice::C1XdBtflCMD::OnXdPrpListChanged(HHV h,BTFL_PRP_ID id, int v)
{
	BTFLSTATUS r						= BTFL_STATUS_NOT_SUPPORT_INTERFACE;
	BEAUTIFULLOVER_COMMAND_CODE	c		= CMD_BTFL_PRP_CHANGED;
	CMD_BEAUTIFULLOVER_CONTEXT		btfCtx;
	btfCtx.code							= c;
	strcpy(btfCtx.szVerify,"BEAUTIFULLOVER");   
	
	btfCtx.BTFL_PARAM.CallBack.pFun		= NULL;
	btfCtx.BTFL_PARAM.CallBack.pParam	= NULL;
	btfCtx.BTFL_PARAM.PrpChanged.id		= id;
	btfCtx.BTFL_PARAM.PrpChanged.v		= v;
	
	r = BtflCommand(h,c,&btfCtx); 
	
	if( BTFL_STATUS_OK != r)
	{  
//		XdM(c);
	} 
}
void CDevice::C1XdBtflCMD::OnXdPrpClickItem(HHV h,BTFL_PRP_ID id, int v)
{
	BTFLSTATUS r						= BTFL_STATUS_NOT_SUPPORT_INTERFACE;
	BEAUTIFULLOVER_COMMAND_CODE		c	= CMD_BTFL_PRP_HIT;
	CMD_BEAUTIFULLOVER_CONTEXT		btfCtx;
	btfCtx.code							= c;
	strcpy(btfCtx.szVerify,"BEAUTIFULLOVER");   
	
	btfCtx.BTFL_PARAM.CallBack.pFun		= NULL;
	btfCtx.BTFL_PARAM.CallBack.pParam	= NULL;
	btfCtx.BTFL_PARAM.PrpChanged.id		= id;
	btfCtx.BTFL_PARAM.PrpChanged.v		= v;
	
	r = BtflCommand(h,c,&btfCtx); 
	
	if( BTFL_STATUS_OK != r)
	{  
//		XdM(c);
	} 
}
void CDevice::C1XdBtflCMD::OnXdPrpScalarChanged(HHV h,BTFL_PRP_ID id, int v)
{
	BTFLSTATUS r = BTFL_STATUS_NOT_SUPPORT_INTERFACE;
	BEAUTIFULLOVER_COMMAND_CODE		c		= CMD_BTFL_PRP_CHANGED;
	CMD_BEAUTIFULLOVER_CONTEXT		btfCtx;
	btfCtx.code = c;
	strcpy(btfCtx.szVerify,"BEAUTIFULLOVER");   
	
	btfCtx.BTFL_PARAM.CallBack.pFun		= NULL;
	btfCtx.BTFL_PARAM.CallBack.pParam	= NULL;
	btfCtx.BTFL_PARAM.PrpChanged.id		= id;
	btfCtx.BTFL_PARAM.PrpChanged.v		= v;
	 
	r = BtflCommand(h,c,&btfCtx);
	
	if( BTFL_STATUS_OK != r)
	{  
//		XdM(c);
	} 
}

void CDevice::C1XdBtflCMD::OnXdAddPrp(HHV h,CPropertyListCtrl &lc)
{
	
	BTFLSTATUS r = BTFL_STATUS_NOT_SUPPORT_INTERFACE;
	CPropertyListCtrl *pLV = &lc;

	 BEAUTIFULLOVER_COMMAND_CODE	c		= CMD_BTFL_ADD_PRP;
	 CMD_BEAUTIFULLOVER_CONTEXT		btfCtx;
	 btfCtx.code = c;
	 strcpy(btfCtx.szVerify,"BEAUTIFULLOVER");   

	 btfCtx.BTFL_PARAM.CallBack.pFun	= xdAddPRP;
	 btfCtx.BTFL_PARAM.CallBack.pParam	= pLV;

	 r = BtflCommand(h,c,&btfCtx); 
	 if(BTFL_STATUS_OK != r)
	 {
	//	 XdM(c);
	 }

}
