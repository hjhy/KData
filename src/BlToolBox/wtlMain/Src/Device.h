// Device.h: interface for the CDevice class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEVICE_H__A6B47285_FBD5_4E5C_8939_56369F355777__INCLUDED_)
#define AFX_DEVICE_H__A6B47285_FBD5_4E5C_8939_56369F355777__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define WM_ERROR WM_USER + 42   // user defined message, sent by the acquisistion thread to the main frame if 
								// an error occured in the thread's run method


class CChildFrame;
class CMainFrame;
struct CImageInfo;
struct CImageData;

#include "image.h"
#include "resource.h"
#include "math.h"
#include "HVBase.h"
#include "..\inc\HVDef.h" 
#include "Controls\PropertyList.h"

const unsigned long TIMERID = 0xa7a3;
const unsigned long TIMERINTERVAL = 1000;


class CDevice : public CHVBase
{ 
public:
	CDevice(int nNumber, int nIndex, HWND hWndNotify, CMainFrame& MainFrame);
	virtual ~CDevice();
	
public:	
	void OnXdPreTranslateMsg(MSG* pMsg)
	{m_btflCmd.OnXdPreTranslateMsg(m_hXdHhv,pMsg);}
	void OnXdPrpClickItem(BTFL_PRP_ID id,int v);
	void OnPrpScalarChanged(BTFL_PRP_ID id,int v);
	void OnXdPrpListItemChanged(BTFL_PRP_ID id,int v);
	virtual void _OnAddPRP(CPropertyListCtrl &lv);
	virtual BTFLSTATUS OnXdCmd(HWND							hWnd,
									BEAUTIFULLOVER_COMMAND_CODE		c,
									CMD_BEAUTIFULLOVER_CONTEXT		&btfCtx);

	/// open the device driver and do some initialization
	void OnXdActivate(bool bInit = true);
	/// close the device driver and do some cleanup
	void Deactivate();
	/// Tell the camera object which is its associated MDI child window
	void SetMDIChild(CChildFrame* pChild);
	
	/// Grab single frame
	void GrabSingle();
	/// "Grab" image from file
	void GrabFromFile(CString FileName);
	/// Activate continuous grabbing
	void GrabContinuous();
	/// Cancel continuous grabbing
	void GrabCancel();  
	/// Are we currently grabbing ?
	bool IsGrabActive() { return m_fContinuousGrabActive || m_fSingleGrabActive; }
	bool IsContinuousGrabActive() { return m_fContinuousGrabActive; }
	bool IsSingleGrabActive() { return m_fSingleGrabActive; }
	
	/// Get current acquisition information
	void GetImageInfo(CImageInfo &info);
	void GetImageData(CImageData &data);
	
	/// Retrieve actual frame rate
	unsigned int OnXd_GetFrameRate(double& displayed); 
	
	unsigned int XD_Get_FPS(double& fps,bool b=0);

	/// Return the bitmap to display  ( The image view will use this function in its OnPaint method)
	CHVBitmap* GetBitmap() 
	{ 
		return m_fBitmapValid ? m_pBitmap : NULL;
	}
	
	/// Return the current color code
	HVColorCode OnXdGetColorCode() { return m_xdClCode; }
	
	/// Inform the camera object that it is to be reconfigured (e.g. by the properties dialog or by changing the AOI)
	/// An active grabbed will be suspended.
	bool ReconfigurationRequest();
	
	/// Inform the camera object that the reconfiguration is done. A suspended grab will be resumed.
	void ConfigurationChanged();
	
	void Set_Default_AOI();
	/// SetAOI
	void SetAOI(CRect AOI);
	/// Maximize the AOI
	void MaximizeAOI();
	/// Parametrize Device 
	void ParametrizeDevice(HVResolution mode, HVColorCode code, CPoint position, CSize size);
	/// Has the camera a scalable AOI ?
	bool IsScalable() {return m_bXdAOI;}
	
	/// Set Video mode
	void SetVideoMode(HVResolution mode);
	
	/// Set Bayer to RGB conversion mode ( 0: no conversion, 1 .. 4 enable conversion)
	void SetDisplayMode( int mode );
	/// Get Bayer to RGB conversion mode ( 0: no conversion, 1 .. 4 enable conversion)
	int GetDisplayMode() { return m_DisplayMode; }
	
	void SaveConfiguration(const CPropertyBagPtr ptrBag);
	/// save the current configuration to a file
	void SaveConfiguration(const CString fileName) { 
		SaveConfiguration(CIniFilePropertyBag::Create(fileName, "HVDevice")); }
	/// restore configuration from a CPropertyBag
	void RestoreConfiguration(const CPropertyBagPtr ptrBag);
	/// restore configuration from a file
	void RestoreConfiguration(const CString fileName) {  
		RestoreConfiguration(CIniFilePropertyBag::Open(fileName, "HVDevice"));}
   
 
    CSize GetImageSize() {
        return m_ImageSize;
    }
   
	/// Supply information about the camera and the driver
	class CInfo
	{
	protected:
		CDevice * const m_pDevice;            ///< Backpointer for data exchange

	public:
		CInfo(CDevice *pDevice) : m_pDevice(pDevice)
		{}
		CString VendorName() 
		{ 
			char lpTemp[256];		  
			LoadString(NULL,IDS_STRING_VENDOR,lpTemp,MAX_PATH);		  
			CString str;
			str.Format("%s", lpTemp);
			return str;
		} 
		int DeviceNumber() {return m_pDevice->m_nNumber;}
		CString NodeId() 
		{
			char info[256] = {0};
			int size = 0;
			m_pDevice->GetDeviceInfo(DESC_DEVICE_SERIESNUM, NULL, &size);
			m_pDevice->GetDeviceInfo(DESC_DEVICE_SERIESNUM, info, &size);
			return info;
		}
 
		virtual CString DeviceName() = 0;
		virtual CString ModelName() = 0;
		virtual HVTYPE DeviceType() = 0;
		
	} ; ///< Device information object
	friend class CInfo;
	
	CInfo *m_pInfo;
	
	class CFormat{
		IMPLEMENT_THIS( CDevice, m_xdFormat ); ///< This() returns pointer to outer class
	public:	
		HVColorCode		ColorMode()			{return This()->m_xdClCode;}
		HVResolution	Resolution()		{return This()->m_Resolution;}
		CSize			ImageSize()			{return This()->m_ImageSize;}
		CPoint			ImagePosition()		{return This()->m_Origin;}
		CSize			OnXdGetSensorSize()	{return This()->m_SensorSize;} 
	} m_xdFormat;
	friend class CFormat;
	
	 
	class CSnapMode
	{
	public:
		CDevice * const m_pDevice;            ///< Backpointer for data exchange
		
		CSnapMode(int Value, CDevice* pDevice ) :
			m_SnapMode(Value),
			m_pDevice(pDevice)
		{
			m_strXDTest1 = "xdtest111";
		}
		
		virtual bool IsSupported() = 0;
		int Value() {return m_SnapMode;}
		void Set(int Value)
		{
			m_pDevice->SetSnapMode((HV_SNAP_MODE)Value);
			m_SnapMode = Value; 
		}
		
		virtual void Refresh()
		{
			Set(Value());
		}
		
	protected:
		void Save(const CPropertyBagPtr ptrBag)
		{
			if ( IsSupported() )
			{
				ptrBag->WriteLong("Value", m_SnapMode);
			  ptrBag->WriteString("xdstr1", m_strXDTest1);
			}
		}
		
		void Restore(const CPropertyBagPtr ptrBag)
		{
			if ( IsSupported() )
			{
				m_SnapMode = ptrBag->ReadLong("Value");
			}
		}
		
	protected:
		CString		m_strXDTest1;
		int			m_SnapMode;
		friend class CDevice;
	};
	friend class CSnapMode;
	
	CSnapMode *m_pSnapMode;
	
	private:
		class C1XdBtflCMD
		{ 
		public:
			void OnXdPreTranslateMsg(HHV h,MSG *pMsg){
				
				BTFLSTATUS r = BTFL_STATUS_NOT_SUPPORT_INTERFACE; 
				BEAUTIFULLOVER_COMMAND_CODE	c		= CMD_BTFL_G_PRETRASLATEMSG;
				CMD_BEAUTIFULLOVER_CONTEXT		btfCtx;
				btfCtx.code = c;
				strcpy(btfCtx.szVerify,"BEAUTIFULLOVER"); 
				btfCtx.BTFL_PARAM.MSG.pMsg = pMsg;
				
				r = BtflCommand(h,c,&btfCtx); 
				if(BTFL_STATUS_OK != r)
				{
					//	 XdM(c);
				}
			};
		void OnXdAddPrp(HHV h,CPropertyListCtrl &lc);
		void OnXdPrpClickItem(HHV h,BTFL_PRP_ID id,int v);
		void OnXdPrpListChanged(HHV h,BTFL_PRP_ID id,int v);
		void OnXdPrpScalarChanged(HHV h,BTFL_PRP_ID id,int v);
		bool OnXdIsCanSetAOI(HHV h);
		C1XdBtflCMD()	{};
		~C1XdBtflCMD()	{};
	private:
		
		static int CALLBACK xdAddPRP(BTFL_ADD_PRP_INFO *Param);
		void XdM(BEAUTIFULLOVER_COMMAND_CODE c);
		CMD_BEAUTIFULLOVER_CONTEXT		xdMakeContext1(BEAUTIFULLOVER_COMMAND_CODE c,int id);
	}m_btflCmd;
 
 

	//
	typedef map<int, HVResolution>         Idx2Mode_t;
	Idx2Mode_t DeviceModeMap();
	std::list<CString> DeviceModeList();
	
	protected:
		CSize xd_Get_Sensor_Size(HVResolution mode);	
		 
	private:
		static int CALLBACK SnapCallbackProc(HV_SNAP_INFO *pInfo);
		// hide copy constructor and assignment operator
		CDevice( const CDevice& );
		CDevice& operator=( const CDevice& );
		
		/// Initialize Device 
		void xdInitDevice(); 
		/// To be called before a continuous grab is started
		void PrepareContinuousGrab();
		/// Allocate n new buffers
		void xdCreateNewBuffers(unsigned long n);
		/// Release Buffers
		void xdReleaseBuffers(); 
		/// Callback. It is called if an error occured in one of the threads
		void OnContinuousGrabError(DWORD err);
		/// Cancel an active continuous grab
		DWORD CancelContinuousGrab();
		
		// Refresh the cached information and notify clients about changes
	private: 
		void OnXdShowFrame(); 
		
		
	public:
		/// Device number
		int		            m_nNumber;
		//////////////////////////////////////////////////////////////////////////
protected:		
	    int					m_nIndex;
		/// Window handle to receive device notifications
		HWND                m_hWndNotify;
		
		/// number of buffers we use for image acquisition
		int                 m_cBuffers;       
		/// array of buffers to be filled by the DMA
		CHVBitmap**			m_ppXdBuffers;    
		/// bitmap to display. 
		CHVBitmap*			m_pBitmap; 
		/// 
		CHVBitmap*			m_pCurrentBuffer;
		/// the next buffer to be processed by the display thread
		CHVBitmap*		    m_pNextBuffer;	
		/// Critical section to synchronize access to m_pNextBuffer
		CCriticalSection	  m_CritSect;
		/// Size of max
		CSize               m_SensorSize;
		/// Size of AOI
		CSize               m_ImageSize;
		/// Origin of AOI
	public:
		CPoint              m_Origin;
		/// Current color mode
		HVColorCode			m_xdClCode;
		///
		HVResolution		m_Resolution;
		//////////////////////////////////////////////////////////////////////////


		/// does the bitmap match the current camera configuration?
		bool                m_fBitmapValid;
		/// flag to signal if we are currently grabbing in continuous mode
		bool                m_fContinuousGrabActive;  
		/// flag to signal if a single grab is pending
		bool                m_fSingleGrabActive;

		// if m_fSuspendGrab is true, the acquisition thread won't queue in any buffers
		bool                m_fSuspendGrab;
		/// is the display thread processing image buffer(s)?
		bool				m_fIsDisplayThreadActive;
		
		bool				m_bXdAOI; /// is AOI maximize 
		
		/// timer to measure the time needed to display one buffer
		CStopWatch          m_DisplayWatch;              
		/// Moving average of time needed to display one buffer
		CMovingAvg<double, 20>  m_DisplayAvg;
		/// MDI child window which will display our image data
		CChildFrame*        m_pChildFrame;
		/// Reference to main frame 
		CMainFrame&         m_MainFrame;
			
		/// shall we convert MONO8 images to RGB ( Bayer -> RGB conversion )
		int                 m_DisplayMode;
 		static long  s_cGrabsActive;
	
		//	friend class CImageView;
		friend class CMainFrame;
		friend class CChildFrame;
		friend class CDeviceManager;
		friend class CPluginManager;
	
	public:
		CString m_strModelName;
		HVTYPE  m_Type;

};

#endif // !defined(AFX_DEVICE_H__A6B47285_FBD5_4E5C_8939_56369F355777__INCLUDED_)





















