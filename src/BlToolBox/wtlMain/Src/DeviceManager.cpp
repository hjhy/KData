 

#include "stdafx.h"
#include "DeviceManager.h"
#include "mainfrm.h"
#include "ChildFrm.h" 
  
#include "Camera\SV1410FC.H" 
#include "Camera\PD1420EC.H" 
#include "Camera\SV1410GC.H"
 
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDeviceManager::CDeviceManager(CMainFrame &mf)
: m_mf4DM(mf),
m_pCurrentDevice(NULL),
m_SelectedDevice(0)
{
	
}

CDeviceManager::~CDeviceManager()
{	
	for ( DevNumber2Device_t::iterator it = m_DevNumber2Device.begin(); it != m_DevNumber2Device.end(); ++ it ){
		delete it->second;
	}		
}



//------------------------------------------------------------------------------
// CDevice* CDeviceManager::GetDevice(CString DeviceName)
//------------------------------------------------------------------------------
/**
* Returns the camera object for a given device name 
* \param     DeviceName
* \return the camera object. NULL is returned, if no object has been found  
*/
//------------------------------------------------------------------------------
CDevice* CDeviceManager::GetDevice(int nNumber)
{
	DevNumber2Device_t::iterator it = m_DevNumber2Device.find(nNumber);
	
	return it == m_DevNumber2Device.end() ? NULL : it->second;
}


CDevice* CDeviceManager::GetDevice(CChildFrame *pChild)
{
	CDevice* pDevice = NULL;
	Device2ChildFrame_t		&m = m_xd_Dev_2_ChildFrame;
	for ( Device2ChildFrame_t::iterator it = m.begin(); it != m.end(); ++it )
	{
		if (it->second == pChild){
			pDevice = it->first;			
			break;
		}
	} 
	assert (it != m.end());
	
	return pDevice;
}



DWORD CDeviceManager::CurrentDeviceChanged(int nNumber)
{
	CDevice* pOldDevice = m_pCurrentDevice;
	DWORD result = 0;
	
	m_SelectedDevice = nNumber;
	
	if (nNumber == 0) {
		// Actually in the bus view no BCAM object is selected. If there is no MDI associated
		// with m_pCurrentDevice, m_pCurrentDevice is set to NULL. Otherwise m_pCurrentDevice 
		// remains unchanged, i.e. m_pCurrentDevice corresponds to the MDI window.
		if ( GetCurrentChild() == NULL ){
			// no child window exists, set current device to NULL
			m_pCurrentDevice = NULL;
		}
		else{
			// current device remains unchanged
			return 0;
		}
	}
	else{
		CDevice *pDevice = GetDevice(nNumber);
		if (pDevice == m_pCurrentDevice) 
			return 0;   // nothing has changed
		m_pCurrentDevice = pDevice;
	}
	
	// open the new device
	try
	{
		if (m_pCurrentDevice != NULL)
			m_pCurrentDevice->OnXdActivate();	
		
		if ((pOldDevice != NULL) && pOldDevice->IsOpen() && (! pOldDevice->IsGrabActive()) ) 
		{
			pOldDevice->Deactivate();
		}
	}
	CATCH_REPORT();
	
	// inform the main frame
	m_mf4DM.CurrentDeviceChanged(m_pCurrentDevice);
	
	if (m_pCurrentDevice != NULL) {
		Device2ChildFrame_t		&m = m_xd_Dev_2_ChildFrame;
		Device2ChildFrame_t::iterator it = m.find(m_pCurrentDevice);
		if ( it != m.end() )
		{
			// a MDI child exists, activate it
			CChildFrame* pChild = it->second;
			pChild->SetFocus();
		}
	}
	
	return result;
}

void CDeviceManager::AddMDIChild()
{
	Device2ChildFrame_t		&m = m_xd_Dev_2_ChildFrame;
	assert ( m_pCurrentDevice != NULL );
	assert ( m.find(m_pCurrentDevice) == m.end() );  // only one window per device is allowed
	
	CChildFrame* pChild = new CChildFrame(m_pCurrentDevice, *this, m_mf4DM);
	
	pChild->m_WinName = m_pCurrentDevice->m_strModelName/* m_pInfo->DeviceName()*/ + 
		CString(" Viewer (") + 
		"111"/*m_pCurrentDevice->m_pInfo->NodeId()*/ + 
		CString(" )");

	pChild->CreateEx(m_mf4DM.m_hWndMDIClient,NULL, pChild->m_WinName);
	
	m_pCurrentDevice->SetMDIChild(pChild); 
	m_xd_Dev_2_ChildFrame[m_pCurrentDevice] = pChild;
	
	// try to restore the window layout
	pChild->v.RestoreLayout();
	
	BOOL bMaximized = FALSE;
	m_mf4DM.MDIGetActive(&bMaximized);
	if ( bMaximized )
		pChild->ShowWindow(SW_MAXIMIZE);
		
	// load all plugin from the default folder
	pChild->m_xdPlxMng.LoadPlugins();
}
 
CChildFrame* CDeviceManager::GetCurrentChild()
{
	if ( m_pCurrentDevice == NULL )
		return NULL;
	Device2ChildFrame_t &m = m_xd_Dev_2_ChildFrame;
	Device2ChildFrame_t::iterator it = m.find(m_pCurrentDevice);
	if ( it != m.end() ) 
		return it->second;
	else 
		return NULL;
}




//------------------------------------------------------------------------------
// bool CDeviceManager::ExistMDIChild()
//------------------------------------------------------------------------------
/**
* Is there already a MDI window associated with the current device
*
*/
//------------------------------------------------------------------------------
bool CDeviceManager::ExistMDIChild()
{
	assert ( m_pCurrentDevice != NULL );
	return ( m_xd_Dev_2_ChildFrame.find(m_pCurrentDevice) != m_xd_Dev_2_ChildFrame.end() );
}


//------------------------------------------------------------------------------
// bool CDeviceManager::IsDeviceValid()
//------------------------------------------------------------------------------
/**
* Is a given device valid, i.e. do we still know the device? 
*
*/
//------------------------------------------------------------------------------
bool CDeviceManager::IsDeviceValid(CDevice* pDevice)
{
	if ( pDevice == NULL ) 
		return false;
	DevNumber2Device_t::iterator it = m_DevNumber2Device.begin();
	while ( it != m_DevNumber2Device.end() )
	{
		if ( it->second == pDevice )
			return true;
		it++;
	}
	return false;
}




//------------------------------------------------------------------------------
// bool CDeviceManager::IsGrabActive()
// Author: 
//------------------------------------------------------------------------------
/**
* Is the current device grabbing?
*
*/
//------------------------------------------------------------------------------
bool CDeviceManager::IsGrabActive()
{
	assert ( m_pCurrentDevice != NULL );
	if ( m_pCurrentDevice != NULL )
		return ( m_pCurrentDevice->IsGrabActive() );
	else
		return false;
}


//------------------------------------------------------------------------------
// void CDeviceManager::MDIChildActivated(CChildFrame* pChild)
//------------------------------------------------------------------------------
/**
* A MDI child informs us that it has been activated by the user.
* \param     pChild Pointer to the child which has been activated
*/
//------------------------------------------------------------------------------
void CDeviceManager::MDIChildActivated(CChildFrame* pChild)
{
	Device2ChildFrame_t &m = m_xd_Dev_2_ChildFrame;
	for ( Device2ChildFrame_t::iterator it = m.begin(); it != m.end(); ++it )
	{
		if ( it->second == pChild )
		{
			CDevice* pDevice = it->first;
			CurrentDeviceChanged(pDevice->m_pInfo->DeviceNumber());
			break;
		}
	}
	
}

BOOL CDeviceManager::AddDevice(int nNumber, HWND hWndNotify)
{
	CDevice* pDevice = NULL; 
	HVTYPE type;
	int nIndex = 0;
	::HVGetTypeFromIndex(nNumber, &type, &nIndex); 
	switch(type){   
	case SV1410FCTYPE: //28
        pDevice = new CSV1410FC(nNumber, nIndex, hWndNotify, m_mf4DM);
		pDevice->m_strModelName.Format("SV1410FC%d",nNumber); 
        break;  
	case PD1420ECTYPE: //36
        pDevice = new CPD1420EC(nNumber, nIndex, hWndNotify, m_mf4DM);
		pDevice->m_strModelName.Format("Tools-%d",nNumber);
		
        break; 
	case SV1410GCTYPE: //38  xdTest_002: BtflKlines
        pDevice = new CSV1410GC(nNumber, nIndex, hWndNotify, m_mf4DM);
        //pDevice = new CDevice(nNumber, nIndex, hWndNotify, m_mf4DM);
		pDevice->m_strModelName.Format("BtflKlines-%d",nNumber); 
        break;  
	default:
		throw HVBaseException(STATUS_PARAMETER_INVALID, "CDeviceManager::AddDevice()");
	}
	if ( pDevice == NULL ) return FALSE;	
	m_DevNumber2Device[nNumber] = pDevice;
	pDevice->m_Type = type;

	return TRUE;
}

//------------------------------------------------------------------------------
// void CDeviceManager::RemoveDevice(CString DeviceName)
// Author: 
//------------------------------------------------------------------------------
/**
* Tell the camera manager to remove a device. This method is called for devices
* which were not opened when they have been removed
*
* \param     DeviceName
* \return    
*
* 
* \see       <delete line if not used>
* \todo      
*/
//------------------------------------------------------------------------------
BOOL CDeviceManager::RemoveDevice(int nNumber)
{
	CDevice* pDevice = GetDevice(nNumber);
	if ( pDevice != NULL )
	{
	}
	
	return TRUE;
}




//------------------------------------------------------------------------------
// void CDeviceManager::MDIChildClosed(CChildFrame* pChild)
//------------------------------------------------------------------------------
/**
* A MDI child window informs us that it has been closed.
*
* \param     pChild
* \return    
*
* <type Return description here>
* 
* \see       <delete line if not used>
* \todo      
*/
//------------------------------------------------------------------------------
void CDeviceManager::MDIChildClosed(CChildFrame* pChild)
{
	Device2ChildFrame_t &m = m_xd_Dev_2_ChildFrame;
	for ( Device2ChildFrame_t::iterator it = m.begin(); it != m.end(); ++it )
	{
		if (it->second == pChild){
			CDevice* pDevice = it->first;
			// cancel an active grab
			if ( pDevice->IsGrabActive() )
			{
				try
				{
					pDevice->GrabCancel();
					// If the camera is no longer selected, close it to allow other applications to access it
					if ( pDevice != m_pCurrentDevice && pDevice->IsOpen() )
					{
						pDevice->Deactivate();
					}
				}
				CATCH_REPORT();
			}
			m_xd_Dev_2_ChildFrame.erase(it);
			pDevice->SetMDIChild(NULL);

			pChild->m_xdPlxMng.UnloadPlugins();

			break;
		}
	} 
	assert ( it != m_xd_Dev_2_ChildFrame.end() );
	
	// If the user has selected a non BCAM node in the BusView window and no other 
    // child frames exist, there is no current device. In this case call the CurrentDeviceChanged()
    // method to update the DeviceManager's status
    if ( m_xd_Dev_2_ChildFrame.size() == 0 && m_SelectedDevice == 0)
		CurrentDeviceChanged(NULL);
}





//------------------------------------------------------------------------------
// void CDeviceManager::GrabSingle()
// Author: 
// Date: 20.09.2002
//------------------------------------------------------------------------------
/**
* Let the current device grab a single image
*
*/
//------------------------------------------------------------------------------
void CDeviceManager::GrabSingle()
{
	assert ( m_pCurrentDevice != NULL );
	if ( m_pCurrentDevice == NULL )
		return;
	assert ( ! m_pCurrentDevice->IsGrabActive() );
	if ( m_pCurrentDevice->IsGrabActive() )
		return;
	if ( ! ExistMDIChild() )
		AddMDIChild();
	m_pCurrentDevice->GrabSingle();
	
}



//------------------------------------------------------------------------------
// void CDeviceManager::GrabContinuous()
// Author: 
//------------------------------------------------------------------------------
/**
* Let the current device continously grabbing images
*
*/
//------------------------------------------------------------------------------
void CDeviceManager::GrabContinuous()
{
	assert ( m_pCurrentDevice != NULL );
	if ( m_pCurrentDevice == NULL )
		return;
	assert ( ! m_pCurrentDevice->IsGrabActive() );
	if ( m_pCurrentDevice->IsGrabActive() )
		return;
	if ( ! ExistMDIChild() )
		AddMDIChild();
	m_pCurrentDevice->GrabContinuous();
}


void CDeviceManager::OnSoftTrigger()
{ 
}



void CDeviceManager::XD_Show_All_FPS()
{
	Device2ChildFrame_t		&m = m_xd_Dev_2_ChildFrame;
	for ( Device2ChildFrame_t::iterator it = m.begin(); it != m.end(); ++ it)
	{
		CChildFrame* pFrm = it->second;
		CDevice* pDevice = it->first;
		if(pFrm&&pDevice){
			double fps;
			int n = it->first->XD_Get_FPS(fps);
			if(n>0){			
				CString str;
				str.Format("FPS:%3.1f [%04d] ", fps,n);
				pFrm->SetWindowText(str+pFrm->m_WinName);
			}
			else{
				pFrm->SetWindowText(pFrm->m_WinName);
			}
		}
	}
}


//------------------------------------------------------------------------------
// void CDeviceManager::GrabCancel()
// Author: 
//------------------------------------------------------------------------------
/**
* Let the current device cancel image acquisition
*
*/
//------------------------------------------------------------------------------
void CDeviceManager::GrabCancel()
{
	assert ( m_pCurrentDevice != NULL );
	if ( m_pCurrentDevice == NULL )
		return;
	assert ( m_pCurrentDevice->IsGrabActive() );
	m_pCurrentDevice->GrabCancel();
}

void CDeviceManager::GrabFromFile(CString FileName)
{
	assert ( m_pCurrentDevice != NULL );
	if ( m_pCurrentDevice == NULL )
		return;
	if ( ! ExistMDIChild() )
		AddMDIChild();
	m_pCurrentDevice->GrabFromFile( FileName);
}




//------------------------------------------------------------------------------
// void CDeviceManager::ShowPixelValue()
// Author: 
//------------------------------------------------------------------------------
/**
* Show the pixel value under the cursor in the main frame's status bar. 
* The z-ordering of the windows is considered.
*
*/
//------------------------------------------------------------------------------
void CDeviceManager::ShowPixelValue()
{
	
	// Iterate through the MDI child windows in descending z-order until the window is found which
	// is under the cursor
	
	// retrieve handle to first MDI child
	HWND hWndChild = GetWindow(m_mf4DM.m_hWndMDIClient, GW_CHILD);
	
	CPoint pt;
	GetCursorPos(&pt);
	
	bool eraseValuePane = true;
	bool erasePosPane = true;
	
	while ( hWndChild != NULL )
	{
		CRect rect;
		GetWindowRect(hWndChild, &rect);
		if ( rect.PtInRect(pt) )
		{
			Device2ChildFrame_t		&m = m_xd_Dev_2_ChildFrame;
			for ( Device2ChildFrame_t::iterator it = m.begin(); it != m.end(); ++ it)
			{
				if ( it->second->m_hWnd == hWndChild )
				{
					eraseValuePane = ! it->second->v.ShowPixelValue() ; 
					break;
				}
			}
			for ( it = m.begin(); it != m.end(); ++it )
			{
				if ( it->second->m_hWnd == hWndChild )
				{
					erasePosPane = ! it->second->v.ShowCursorPosXY();
					break;
				}
			}
			break;
		}
		hWndChild = GetWindow(hWndChild, GW_HWNDNEXT);
	}
	if ( eraseValuePane )
	{
		m_mf4DM.m_Sbar.SetPaneText(ID_VALUE_PANE, CString("") );
	}
	if ( erasePosPane )
	{
		m_mf4DM.m_Sbar.SetPaneText(ID_POS_PANE, CString("") );
	}
}

unsigned int CDeviceManager::OnXdGetFrameRate(double& d)
{
	unsigned int n = 0;
	CPoint pt;
	GetCursorPos(&pt);
	//	fps_acquired = -1;
	d = -1;
	
	HWND hWndChild = m_mf4DM.MDIGetActive();
	if ( hWndChild != NULL )
	{
		Device2ChildFrame_t &m = m_xd_Dev_2_ChildFrame;
		for ( Device2ChildFrame_t::iterator it = m.begin(); it != m.end(); ++ it)
		{
			if ( it->second->m_hWnd == hWndChild && it->first->IsContinuousGrabActive() )
			{
				n = it->first->OnXd_GetFrameRate(d);
				break;
			}
		}
	}
	return n;
}

 
void CDeviceManager::AppExit(bool b)
{	
	ATLTRACE("CDeviceManager::AppExit() \n");

	m_pCurrentDevice = NULL;
	for ( DevNumber2Device_t::iterator it = m_DevNumber2Device.begin(); it != m_DevNumber2Device.end(); ++ it )
	{
		
		CDevice* pDevice = it->second;
		if ( pDevice != NULL )
		{
			// try to save the current configuration and to the registry
			try
			{
				if ( ! pDevice->IsOpen() )	{
					pDevice->OnXdActivate();
				}
				// If we are still grabbing, cancel the grabbing
				if ( pDevice->IsGrabActive() ) {
					ATLTRACE("pDevice->GrabCancel() \n");
					pDevice->GrabCancel();
				}
				if(!b){
					CPropertyBagPtr cameraBag = CRegistryPropertyBag::Create(APPLICATION_KEY + pDevice->m_pInfo->DeviceName() + "\\Device");
					pDevice->SaveConfiguration(cameraBag);
				}
				else{
					CString keyName = APPLICATION_KEY;
					SHDeleteKey(HKEY_CURRENT_USER,keyName);
				}
				
			}
			catch ( HVBaseException &e) 
			{
				ATLTRACE("Exception occured when saving device's configuration: %d (%s)\n", e.Error(), e.Description());
			}

			// Save window Layout  
			Device2ChildFrame_t &m = m_xd_Dev_2_ChildFrame;
			Device2ChildFrame_t::iterator wnd = m.find(pDevice);
			if ( wnd != m.end() ){
				wnd->second->v.SaveLayout();
				
				// delete plugins and free resources
				wnd->second->m_xdPlxMng.UnloadPlugins(); 
			}

			// delete device and cancel all operations
			delete pDevice;
		}     
	}
	m_DevNumber2Device.clear();
}




//------------------------------------------------------------------------------
// void CDeviceManager::ReportError(BcamException& e)
// Author: 
// Date: 06.09.2002
//------------------------------------------------------------------------------
/**
* Convenience function to show an error message
*
*/
//------------------------------------------------------------------------------
void CDeviceManager::ReportError(HVBaseException& e)
{
	m_mf4DM.ReportError(e);
}