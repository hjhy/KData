// HV.cpp: implementation of the CHVBase class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "HVBase.h"
#include "Device.h"



/******************************************************************************/
using namespace xdHVDevice;
using namespace np_BTFL;

BTFLBaseException::BTFLBaseException( BTFLSTATUS e, CString Context, va_list *pArgs)
: Exception( e, Context, true )
{
	FormatMessage(e, Context);
}


void BTFLBaseException::FormatMessage( BTFLSTATUS e, CString Context )
{
	m_Description.Format("%s", BtfGetErrorString(e));
}



//------------------------------------------------------------------------------
// HVBaseException::HVBaseException( HVSTATUS e, CString context ) 
// Author: Hartmut Nebelung
//------------------------------------------------------------------------------
/**
* \overload
* \brief Constructs a new object
*
* \param     e The error code.
* \param     context The context string, describing the circumstances of the exception.
*/
//------------------------------------------------------------------------------
HVBaseException::HVBaseException( HVSTATUS e, CString Context, va_list *pArgs)
: Exception( e, Context, true )
{
	FormatMessage(e, Context);
}

void HVBaseException::FormatMessage( HVSTATUS e, CString Context )
{
	m_Description.Format("%s", HVGetErrorString(e));
}





//------------------------------------------------------------------------------
// HVBaseException::HVBaseException( DWORD e, CString context ) 
// Author: Hartmut Nebelung
//------------------------------------------------------------------------------
/**
* \overload
* \brief Constructs a new object
*
* \param     e The error code.
* \param     context The context string, describing the circumstances of the exception.
*/
//------------------------------------------------------------------------------
HVBaseException::HVBaseException( DWORD e, CString Context, va_list *pArgs) 
: Exception( e, Context, true )
{
	Exception::FormatMessage(pArgs);
}




//------------------------------------------------------------------------------
// unsigned short HVBaseUtility::BitsPerPixel(DCSColorCode ColorCode)
// Author: A.Happpe
//------------------------------------------------------------------------------
/**
* \brief Looks up the pixel size of a color code
*
* \param     ColorCode
* \return    
*
* Returns the number of bits used for a pixel.
*
* \throw HVBaseException BCAM_E_UNSUPPORTED_COLOR_CODE when passing an invalid color code to the function
*
* 
* \see       DCAM
*/
//------------------------------------------------------------------------------
unsigned short HVBaseUtility::BitsPerPixel(HVColorCode ColorCode)
{
	switch ( ColorCode )
	{
	case Color_Mono8       : return 8;
	case Color_YUV8_4_1_1  : return 12;
	case Color_YUV8_4_2_2  : return 16;
	case Color_YUV8_4_4_4  : return 24;
	case Color_Mono16      : return 16;
	case Color_RGB8        : return 24;
	case Color_RGB16       : return 48;
	default:                 throw HVBaseException(STATUS_PARAMETER_INVALID, "HVBaseUtility::BitsPerPixel");
	}
}



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHVBase::HVBaseMap_t		CHVBase::s_HVBaseMap;
HWND						CHVBase::s_hWndNotify		= NULL;

//------------------------------------------------------------------------------
// CHVBase::CHVBase()
// Author: 
//------------------------------------------------------------------------------
/**
* \brief Constructs a camera object without opening the driver.
*
*/
//------------------------------------------------------------------------------
CHVBase::CHVBase() :
m_hXdHhv( INVALID_HANDLE_VALUE )
{ 
	ATLTRACE(" CHVBase::CHVBase()\n");
}



//------------------------------------------------------------------------------
// CHVBase::~CHVBase()
// Author: 
//------------------------------------------------------------------------------
/**
* \brief Destructor - closes the driver
*
*/
//------------------------------------------------------------------------------
CHVBase::~CHVBase()
{
	ATLTRACE("  CHVBase::~CHVBase()\n");
	Close();
}




//------------------------------------------------------------------------------
// bool CHVBase::IsOpen()
// Author: 
// Date: 04.09.2002
//------------------------------------------------------------------------------
/**
* \brief Retrieve open status
*
* \return    
*
* True if the device is already open
* 
*/
//------------------------------------------------------------------------------
bool CHVBase::IsOpen()
{
	return IsValid();
}

 
void CHVBase::Open(int nNumber, HWND hWndNotify)
{
	ATLTRACE("CHVBase::Open device(%d)\n", nNumber);
	
	if (IsOpen())  
		Close();
	assert(m_hXdHhv == INVALID_HANDLE_VALUE);
	
	HVSTATUS status = BeginHVDevice(nNumber, &m_hXdHhv);
	if (HV_SUCCESS(status)){
		// add handle to DeviceMap
		s_HVBaseMap.insert(HVBaseMap_t::value_type(m_hXdHhv, this));
		m_nXdNumber = nNumber;
		// If the user wants to register an additional window => register it for handle related device notifications
		if(s_hWndNotify) s_hWndNotify = hWndNotify;
	}
	else{
		m_hXdHhv = INVALID_HANDLE_VALUE;
		throw HVBaseException(status, "CHVBase::Open");
	}	
}


//------------------------------------------------------------------------------
// void CHVBase::Close()
// Author: 
//------------------------------------------------------------------------------
/**
* \brief Close the device handle 
*
* 
* \throw HVBaseException The errorcode of WaitForCompletion is returned.
* \todo      
*/
//------------------------------------------------------------------------------
void CHVBase::Close()
{
	DWORD error = 0;
	if ( INVALID_HANDLE_VALUE != m_hXdHhv ) {
		EndHVDevice(m_hXdHhv);
		s_HVBaseMap.erase(m_hXdHhv) ;
		m_hXdHhv = INVALID_HANDLE_VALUE;
	}
}



//------------------------------------------------------------------------------
// bool CHVBase::IsValid()
// Author: 
//------------------------------------------------------------------------------
/**
* \brief check whether the device is still valid.
*
* \retval  true  if it is ok
* \retval  false if it is not ok
*/
//------------------------------------------------------------------------------
bool CHVBase::IsValid()
{
	return (m_hXdHhv != NULL) && (m_hXdHhv != INVALID_HANDLE_VALUE);
}



void CHVBase::SetResolution(HVResolution Mode)
{
	if ( ! IsValid())
		throw HVBaseException(STATUS_DEVICE_HANDLE_INVALID, "CHVBase::SetResolution()");
	 
}



void CHVBase::xdSetOutputWindow(int nLeft, int nTop, int nWidth, int nHeight)
{
	if ( ! IsValid())
		throw HVBaseException(STATUS_DEVICE_HANDLE_INVALID, "CHVBase::SetOutputWindow()");
	
	int l=nLeft,t=nTop;
	HVSTATUS status = HVSetOutputWindow(m_hXdHhv, l,t, nWidth, nHeight);
	if ( ! HV_SUCCESS(status))
		throw HVBaseException(status, "HVDAILT.dll : HVSetOutputWindow");
}



void CHVBase::AGCControl(BYTE byParam, long lValue)
{
	if ( ! IsValid())
		throw HVBaseException(STATUS_DEVICE_HANDLE_INVALID, "CHVBase::AGCControl()");
	 
}




void CHVBase::AECControl(BYTE byParam, long lValue)
{
	if ( ! IsValid())
		throw HVBaseException(STATUS_DEVICE_HANDLE_INVALID, "CHVBase::AECControl()");
	 
}





void CHVBase::ADCControl(BYTE byParam, long lValue)
{ 
}




void CHVBase::xdSnapShot(BYTE **ppBuffer, int nNumber)
{
	if ( ! IsValid())
		throw HVBaseException(STATUS_DEVICE_HANDLE_INVALID, "CHVBase::SnapShot()");
	
//	assert(! IsBadWritePtr(pBuffer, BufferSize ) );
	
	HVSTATUS status = HVSnapShot(m_hXdHhv, ppBuffer, nNumber);
	if ( ! HV_SUCCESS(status))
		throw HVBaseException(status, "HVDAILT.dll : HVSnapShot");    
}





void CHVBase::xdOpenSnap(HV_SNAP_PROC lpSnapFunc, void *pParam)
{
	if ( ! IsValid())
		throw HVBaseException(STATUS_DEVICE_HANDLE_INVALID, "CHVBase::OpenSnap()");
	
	HVSTATUS status = HVOpenSnap(m_hXdHhv, lpSnapFunc, pParam);
	if ( ! HV_SUCCESS(status))
		throw HVBaseException(status, "HVDAILT.dll : HVOpenSnap");    
}


void CHVBase::CloseSnap()
{
	if ( ! IsValid())
		throw HVBaseException(STATUS_DEVICE_HANDLE_INVALID, "CHVBase::CloseSnap()");
	
	HVSTATUS status = HVCloseSnap(m_hXdHhv);
	if ( ! HV_SUCCESS(status))
		throw HVBaseException(status, "HVDAILT.dll : HVCloseSnap");    
}



void CHVBase::xdStartSnap(BYTE **ppBuffer, size_t BufferNumber)
{
	if ( ! IsValid())
		throw HVBaseException(STATUS_DEVICE_HANDLE_INVALID, "CHVBase::StartSnap()");
	
//	assert(! IsBadWritePtr(pBuffer, BufferSize ) );
	
	HVSTATUS status = HVStartSnap(m_hXdHhv, ppBuffer, BufferNumber);
	if ( ! HV_SUCCESS(status))
		throw HVBaseException(status, "HVDAILT.dll : HVStartSnap");  
}



void CHVBase::StopSnap()
{
	if ( ! IsValid())
		throw HVBaseException(STATUS_DEVICE_HANDLE_INVALID, "CHVBase::StopSnap()");
	
	HVSTATUS status = HVStopSnap(m_hXdHhv);
	if ( ! HV_SUCCESS(status))
		throw HVBaseException(status, "HVDAILT.dll : HVStopSnap");  
}



BOOL CHVBase::GetDeviceInfo(HV_DEVICE_INFO Param, void *pContext, int *pSize)
{
	if ( ! IsValid())
		throw HVBaseException(STATUS_DEVICE_HANDLE_INVALID, "CHVBase::GetDeviceInfo()");
	
	HVSTATUS status = HVGetDeviceInfo(m_hXdHhv, Param, pContext, pSize);
	if(status == STATUS_PARAMETER_INVALID)
	{
		return FALSE;
	}

	else if ( ! HV_SUCCESS(status))
		throw HVBaseException(status, "HVDAILT.dll : HVGetDeviceInfo"); 

	return TRUE;
}



void CHVBase:: SetSnapMode(HV_SNAP_MODE mode)
{ 
} 

void CHVBase:: SetTriggerPolarity(HV_POLARITY polarity)
{ 
} 

void CHVBase:: SetStrobePolarity(HV_POLARITY polarity)
{ 
} 


void CHVBase::SetSnapSpeed(HV_SNAP_SPEED speed)
{ 
} 


void CHVBase::SetBlank(int hor, int vert)
{ 
}