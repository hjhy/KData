//-----------------------------------------------------------------------------
//  (c) 2002 by Basler Vision Technologies
//  Section:  Vision Components
//  Project:  BCAM
//  $Header: ImageView.h, 5, 10/31/02 3:02:26 PM, Albrecht M.$
//-----------------------------------------------------------------------------
/**
  \file     ImageView.h
 *
 * \brief Interface of the CImageView and CImageRectTracker classes
 *
 */
//-----------------------------------------------------------------------------


#if !defined(AFX_BCAMVIEWERVIEW_H__8C4B8AD4_7C21_11D5_920C_0090278E5E96__INCLUDED_)
#define AFX_BCAMVIEWERVIEW_H__8C4B8AD4_7C21_11D5_920C_0090278E5E96__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <math.h>
#include "controls\atlzoom.h"
#include "MainFrm.h"
#include "recttracker.h"

extern CAppModule _Module;

#define GUARD_HANDLER() \
    if ( m_RectTracker.OnMessageHandler(uMsg, wParam, lParam) ) \
      return TRUE;

class CImageView;

//------------------------------------------------------------------------------
// class CImageRectTracker
// Author: 
// Date: 
//------------------------------------------------------------------------------
/**
 * \brief   Rubberband to adjust camera's AOI
 *
 * 
 * \todo      
 */
//------------------------------------------------------------------------------


class CImageRectTracker : public CXdRectTracker
{
private:
  CImageView&		v;
  
public:
  CImageRectTracker(CImageView& view);
  virtual void AdjustRect(int nHandle, LPRECT lpRect);
  virtual void AdjustPoint(LPPOINT lpPoint);
  virtual void OnChangedRect(const CRect& rcOld );
  virtual void OnTrackingFinished( const BOOL bCancelled );

  virtual void LPtoDP(LPPOINT lpPoints, int nCount = 1) const;
  virtual void DPtoLP(LPPOINT lpPoints, int nCount = 1) const;
  
};
 
class CImageView : public CZoomWindowImpl<CImageView>
{
public:
  DECLARE_WND_CLASS_EX(NULL, 0, -1);  
  CImageView::CImageView(CDevice* pDevice, CMainFrame& MainFrame) :
    m_pDevice(pDevice),
    m_MainFrame(MainFrame),
    m_xdSensorSize(0,0), 
	m_SensorRect(0,0,0,0),
    m_VideoMode((HVResolution) -1),
    m_SizeInc(1,1),
    m_PosInc(1,1),
    m_RectTracker(*this),
    m_bXdScalable(true),
    m_bFit(false)
  { 
		m_bXdScalable = m_pDevice->IsScalable();
		m_RectTracker.m_rect = m_RectTracker.m_rect = CRect(0, 0, 1, 1);	
		m_hCursor = ::LoadCursor(_Module.GetModuleInstance(), MAKEINTRESOURCE(IDC_CROSSHAIR));
  }

  ~CImageView()
  {
    if (m_MainFrame.m_Sbar.IsWindow()) 
	{
		m_MainFrame.m_Sbar.SetPaneText(ID_AOI_PANE, CString(""));
		m_MainFrame.m_Sbar.SetPaneText(ID_POS_PANE, CString(""));
		m_MainFrame.m_Sbar.SetPaneText(ID_VALUE_PANE, CString("")); 
	}
  }


  BOOL PreTranslateMessage(MSG* pMsg)
  { 
	  m_pDevice->OnXdPreTranslateMsg(pMsg);
	switch(pMsg->message)
	{
	case WM_LBUTTONDOWN:
		//::MessageBox(m_hWnd,"lb down","test",IDOK);
		break;
	}
    return FALSE;
  }


  void xdConfigurationChanged(HVResolution, CSize SensorSize, CSize ImageSize, CPoint Origin);
  

  void DoPaint(CDCHandle dc);

  void OnXdZoomToFit();

  bool IsZoomedToFit() { return m_bFit; }

  bool SaveLayout();

  bool RestoreLayout();

  void CenterCursor();

// message handler 

  BEGIN_MSG_MAP(CImageView) 
    GUARD_HANDLER()
    MESSAGE_HANDLER(WM_ERASEBKGND, OnXdEraseBackground)
    MESSAGE_HANDLER(WM_CONTEXTMENU, OnContextMenu)
    MESSAGE_HANDLER(WM_LBUTTONDOWN, OnXdLButtonDown)
    MESSAGE_HANDLER(WM_CHAR, OnXdChar)
	MESSAGE_HANDLER(WM_KEYDOWN, OnXdKeyDown)
    MESSAGE_HANDLER(WM_MOUSEMOVE, OnXdMouseMove)
    //MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)  //added by hujun
    MESSAGE_HANDLER(WM_SETCURSOR, OnSetCursor)
    MESSAGE_HANDLER(WM_SIZE, OnSize)
    MESSAGE_HANDLER(WM_TIMER, OnTimer)

    CHAIN_MSG_MAP(CZoomWindowImpl<CImageView>)
  END_MSG_MAP()


  LRESULT OnContextMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
  {
    ::SetFocus(GetParent());

    CMenu menuContext;
    menuContext.LoadMenu(IDR_IMAGEVIEW_CONTEXT_MENU);
    CMenuHandle menuPopup(menuContext.GetSubMenu(0));
    m_MainFrame.m_CmdBar.TrackPopupMenu(menuPopup, 
		TPM_LEFTALIGN | TPM_RIGHTBUTTON, 
		LOWORD(lParam), HIWORD(lParam));

    return 0;
  }
  LRESULT OnXdChar(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled);
  
  LRESULT OnXdKeyDown(UINT /*uMsg*/, WPARAM wParam , LPARAM lParam, BOOL& bHandled)
  { 
	  BEAUTIFULLOVER_COMMAND_CODE	c = CMD_BTFL_B4_KEY_DOWN;
	  CMD_BEAUTIFULLOVER_CONTEXT		btfCtx;
	  btfCtx.code = c;
	  strcpy(btfCtx.szVerify,"BEAUTIFULLOVER"); 
	  MSG m;
	  m.wParam = wParam;
	  btfCtx.BTFL_PARAM.MSG.pMsg = &m; 
	  
	  BTFLSTATUS r = m_pDevice->OnXdCmd(m_hWnd,c,btfCtx); 
	  if(BTFL_STATUS_OK !=r)
	  {
		  //	::MessageBox(NULL,"OnXdChar XXXX","xd msg",IDOK);
	  }
	  return 0;
  }
  LRESULT OnXdMouseMove(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled);
  
  LRESULT OnXdLButtonDown(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled);
  LRESULT OnSetCursor(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled);
  LRESULT OnXdEraseBackground(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled);
  LRESULT OnTimer(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled);
  //LRESULT OnMouseMove(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled);

  void OnActivate(bool activated )
  {
    m_bActiveChild = activated;
    
    if ( activated  && m_pDevice != NULL )
    {
      xdZoomLevelChanged();  // causes update of UI 
      xdShowAOISize();
      ShowPixelValue(m_pDevice->GetBitmap());
    }
  }

  LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
  {
    bHandled = FALSE;

    m_bFit = false;
    return 0;
  }


  // overwritten, because we want to redraw the whole client area after doing the scrolling. Otherwise the rubber band
  // will not be drawn correctly
  void DoScroll(int nType, int nScrollCode, int& cxyOffset, int cxySizeAll, int cxySizePage, int cxySizeLine)
  {
    SCROLLINFO si;
    si.cbSize = sizeof(si);
    si.fMask = SIF_ALL;

    GetScrollInfo(nType, &si);
    int iPos = si.nPos;

    switch(nScrollCode)
    {
    case SB_TOP:    // top or all left
      si.nPos = 0;
      break;
    case SB_BOTTOM:   // bottom or all right
      si.nPos = si.nMax;
      break;
    case SB_LINEUP:   // line up or line left
      si.nPos -= cxySizeLine;
      break;
    case SB_LINEDOWN: // line down or line right
      si.nPos += cxySizeLine;
      break;
    case SB_PAGEUP:   // page up or page left
      si.nPos -= cxySizePage;
      break;
    case SB_PAGEDOWN: // page down or page right
      si.nPos += cxySizePage;
      break;
    case SB_THUMBTRACK:
      if(IsNoThumbTracking())
        break;
      // else fall through
    case SB_THUMBPOSITION:
      si.nPos = si.nTrackPos;
      break;
    case SB_ENDSCROLL:
    default:
      break;
    }
    si.fMask = SIF_POS;
    SetScrollInfo(nType, &si, TRUE);
    GetScrollInfo(nType, &si);

    if ( si.nPos != iPos )
    {
      if(nType == SB_VERT)
        ScrollWindowEx(0, iPos - si.nPos, m_uScrollFlags);
      else
        ScrollWindowEx(iPos - si.nPos, 0, m_uScrollFlags);
      cxyOffset = si.nPos;
      Invalidate();  // redraw the hole client are
      UpdateWindow();
    }
  }
    
  // show the value under the cursor
  bool ShowPixelValue(CHVBitmap* pBitmap = NULL);
  // show the position of the mosue cursor
  bool ShowCursorPosXY();

  BOOL ValidateRect(LPCRECT lpRect)
  {
    ATLASSERT(::IsWindow(m_hWnd));
    if ( lpRect != NULL )
    {
      CRect tmp = *lpRect;
      LPtoDP((LPPOINT) &tmp, 2);
      return ::ValidateRect(m_hWnd, lpRect);
    }
    else
      return ::ValidateRect(m_hWnd, lpRect);
  }



protected:

  void StartTimer();
  void StopTimer();


  BOOL HitTestTrackerBorder();
  void AdjustRect(int nHandle, LPRECT lpRect);
  void AdjustPoint(LPPOINT lpPoint);

  void GetLogicalCursorPos(CPoint& pt)
  {
    GetCursorPos(&pt);
    ScreenToClient(&pt);
    pt.x = floor(( pt.x - s_sizBorder.cx + m_ptOffset.x ) / m_dZoomScale);
    pt.y = floor(( pt.y - s_sizBorder.cy + m_ptOffset.y ) / m_dZoomScale);
  }

  void PrepareDC(HDC hDC)
  {
    SetMapMode(hDC, MM_ANISOTROPIC);
    SetWindowExtEx(hDC, m_SensorRect.Width(), m_SensorRect.Height(), NULL);  //Set up the logical window
    
    SetViewportExtEx(hDC, m_SensorRect.Width() * m_dZoomScale, m_SensorRect.Height() * m_dZoomScale, NULL); // in device coordinates
    SetViewportOrgEx(hDC, -m_ptOffset.x + s_sizBorder.cx, -m_ptOffset.y + s_sizBorder.cy, NULL); 
  }

  void UpdateViewPort(POINT* ptNewCenter)
  {
    if ( m_dZoomScale < 1.0 / 32.0 )
    {
      m_dZoomScale = 1.0 / 32.0; 
      MessageBeep(0);
    }
    else if ( m_dZoomScale > 32 )
    {
      MessageBeep(0);
      m_dZoomScale = 32;
    }
    CPoint ptCenter;
    if ( !ptNewCenter )
    {
      if ( ! m_bXdScalable )
      {
        ptCenter = GetLogicalCenter();  
      }
      else
      {
        ptCenter.x = 0.5 * ( m_RectTracker.m_rect.left + m_RectTracker.m_rect.right );
        ptCenter.y = 0.5 * ( m_RectTracker.m_rect.top + m_RectTracker.m_rect.bottom );
      }

    }
    else
    {
      ptCenter = *ptNewCenter;
    }
    
    m_sizeAll.cx = (long) (m_SensorRect.Width() * m_dZoomScale) + 2 * s_sizBorder.cx;
    m_sizeAll.cy = (long) (m_SensorRect.Height() * m_dZoomScale) + 2 * s_sizBorder.cy;
    RecalcBars(TRUE, FALSE);
    CenterOnLogicalPoint(ptCenter);
    xdZoomLevelChanged();
    Invalidate();
    
  }

  void SetNewAOI(); 

  void ReportError(HVBaseException& e)
  {
    m_MainFrame.ReportError(e);
  }

  void SetAOIRect(CRect rect)
  {
	  m_AOIRect = rect;
  }
  void RestorTrackRect()
  {
	  m_RectTracker.m_rect = m_AOIRect;
  }
  void SetTrackMinSize(CSize sz)
  {
	  m_TrackMinSize = sz;
  }
  void RestorTrackMinSize()
  {
	  m_RectTracker.m_sizeMin = m_TrackMinSize;
  }

private:
	void xd___________(){}; // xdPosition_1:
	
	void xdBitBlt(CDCHandle dc, CHVBitmap* pBitmap);
	void xdLBtnDown();
	void xdLBtnHit();
	void xdSetSensorSize(CSize SensorSize);
	
	void xdShowAOISize();
	BOOL xdMoveTrackerTo(CRect rcNew, CRect rcOld, BOOL fRepaint);
	
  
  virtual void xdZoomLevelChanged() 
  {
    // update main menu
    const double eps = 1e-4;
    bool enable25 = false;
    bool enable50 = false;
    bool enable100 = false;
    bool enable200 = false;
    bool enable400 = false;
    bool enableuser = true; 
    if ( fabs(m_dZoomScale - 0.25) <  eps )
    { enable25 = true; enableuser = false;}
    else if ( fabs(m_dZoomScale - 0.5 ) < eps)
    { enable50 = true; enableuser = false; }
    else if ( fabs(m_dZoomScale - 1.0 ) < eps )
    { enable100 = true; enableuser = false; }
    else if ( fabs(m_dZoomScale - 2.0 ) < eps )
    { enable200 = true; enableuser = false; }
    else if ( fabs(m_dZoomScale - 4.0 ) < eps )
    { enable400 = true; enableuser = false; }

    m_MainFrame.UISetCheck(ID_VIEW_ZOOM_25, enable25);
    m_MainFrame.UISetCheck(ID_VIEW_ZOOM_50, enable50);
    m_MainFrame.UISetCheck(ID_VIEW_ZOOM_100, enable100);
	m_MainFrame.UIEnable(ID_VIEW_ZOOM_1_1, !enable100); 
    m_MainFrame.UISetCheck(ID_VIEW_ZOOM_200, enable200);
    m_MainFrame.UISetCheck(ID_VIEW_ZOOM_400, enable400);
    m_MainFrame.UISetCheck(ID_VIEW_ZOOM_USER, enableuser);
    m_MainFrame.UpdateLayout();
  }

	CDevice*            m_pDevice;
	CSize               m_xdSensorSize;
	HVResolution		  m_VideoMode;
	CRect               m_SensorRect;
	CMainFrame&         m_MainFrame;
	
	//Added by hujun
	CRect				  m_AOIRect;
	CSize               m_TrackMinSize;
	
	CImageRectTracker   m_RectTracker;
	bool                m_bXdScalable;      // Does the camera allow setting of AOI ?
	bool                m_bFit;           // Has the user performed ZoomToFit ?
	UINT                m_nIdEvent;
	CSize               m_SizeInc;  
	CSize               m_PosInc;
	CCriticalSection    m_CriticalSection;  // protect device context
	
	HCURSOR             m_hCursor;
	bool                m_bActiveChild;
	
	
	static DWORD			s_dwDragTimerInterval;
	static const CSize		s_sizBorder;
	
	
	friend class CImageRectTracker;
	friend class CDevice;
	friend class CChildFrame;
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BCAMVIEWERVIEW_H__8C4B8AD4_7C21_11D5_920C_0090278E5E96__INCLUDED_)
