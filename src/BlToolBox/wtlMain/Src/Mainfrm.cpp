// MainFrm.cpp : implementation of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ChildFrm.h"
#include "MainFrm.h"
#include "utility.h"
#include "aboutdlg.h"
#include "zoomdlg.h" 
#include "Device.h"
#include "AutoTest.h"

#include <htmlhelp.h>

CMainFrame::CMainFrame() : 
			m_DeviceManager(*this),
			m_xdItemsView(m_DeviceManager, *this),
			m_PropertyView(m_DeviceManager, *this),
			m_fPropertyPageVisible(false),
			m_fBroadCastFps(false),
			m_nSaveTimes(0),
			m_nDeviceNum(0)
{
	m_bClearReg = FALSE;
	m_bToClose = FALSE;
	m_xdThread.OnXdRun();
}

CMainFrame::~CMainFrame()
{
}



BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
//	printf("CMainFrame::PreTranslateMessage: \n");

	if(CMDIFrameWindowImpl<CMainFrame>::PreTranslateMessage(pMsg))
		return TRUE;
	
	HWND hWnd = MDIGetActive();
	if(hWnd != NULL)
		return (BOOL)::SendMessage(hWnd, WM_FORWARDMSG, 0, (LPARAM)pMsg);
	
	return FALSE;
}

BOOL CMainFrame::OnIdle()
{
//	printf("CMainFrame::OnIdle: \n");
	m_xdDockWnd.OnIdle();
	
	UpdateUI();
	return FALSE;
}




LRESULT CMainFrame::OnFileSaveConfiguration(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CDevice* pDevice = m_DeviceManager.GetCurrentDevice();
	
	if ( pDevice == NULL )
		return 0;
	try
	{
		CFileDialog dlg(FALSE, _T("cfg"), pDevice->m_pInfo->ModelName()+".cfg", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("Bcam Configuration Files (*.cfg)\0*.cfg\0"), m_hWnd);
		if ( dlg.DoModal() == IDOK)
		{
			CString s = CString(dlg.m_szFileName);
			pDevice->SaveConfiguration(s);
		}
	} CATCH_REPORT();
	return 0;
}


LRESULT CMainFrame::OnFileRestoreConfiguration(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CDevice* pDevice = m_DeviceManager.GetCurrentDevice();
	if ( pDevice == NULL )
		return 0;
	try
	{
		CFileDialog dlg(TRUE, _T("cfg"), pDevice->m_pInfo->ModelName()+".cfg", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("Bcam Configuration Files (*.cfg)\0*.cfg\0"), m_hWnd);
		if ( dlg.DoModal() == IDOK)
		{ 
			BOOL bRestart = FALSE;
			if (pDevice->IsContinuousGrabActive()) {
				bRestart = TRUE;
			}
			
			try {
				if (bRestart) {
					pDevice->GrabCancel();
				}
				pDevice->RestoreConfiguration(CString(dlg.m_szFileName));
				m_PropertyView.Update_PRP(pDevice); 
				if (bRestart) {
					pDevice->GrabContinuous();
				}
			}
			CATCH_REPORT(); 
		}
	} CATCH_REPORT();
	return 0;
}



LRESULT CMainFrame::OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	try
	{
		m_bToClose = TRUE;
		m_DeviceManager.AppExit(m_bClearReg);
	} CATCH_REPORT();
	
	try
	{
		CPropertyBagPtr pDockViewBag = CRegistryPropertyBag::Create(CString(APPLICATION_KEY) + "DockView");
		
		RECT rc = { 0 };
		int iDockState = 0;
		m_xdDockWnd.GetWindowState(m_xdItemsView.m_hWnd, iDockState, rc);
		int cy=rc.bottom-rc.top;
		pDockViewBag->WriteLong("BusSize", cy);
		m_xdDockWnd.GetWindowState(m_PropertyView.m_hWnd, iDockState, rc);
		pDockViewBag->WriteLong("PropertySize", rc.bottom - rc.top);

		CPropertyBagPtr pMainFramBag = CRegistryPropertyBag::Create(CString(APPLICATION_KEY) + "MainFrame");
		WINDOWPLACEMENT p;
		p.length = sizeof(p);
		GetWindowPlacement(&p);
		pMainFramBag->WriteLong("left", p.rcNormalPosition.left);
		pMainFramBag->WriteLong("right", p.rcNormalPosition.right);
		pMainFramBag->WriteLong("bottom", p.rcNormalPosition.bottom);
		pMainFramBag->WriteLong("top", p.rcNormalPosition.top);
		pMainFramBag->WriteLong("showCmd", p.showCmd);
	
	}
	CATCH_REPORT();

	bHandled = false;
	return 0;
}

LRESULT CMainFrame::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	try
	{
		if (m_hMenu != NULL)
			::DestroyMenu(m_hMenu);
		m_hMenu = NULL;
		if (m_xdItemsView.IsWindow()) m_xdItemsView.DestroyWindow();
		if (m_PropertyView.IsWindow()) m_PropertyView.DestroyWindow();
		if (m_CmdBar.IsWindow()) m_CmdBar.DestroyWindow();
		
	}
	CATCH_REPORT();
	
	bHandled = false;
	return 0;
}



LRESULT CMainFrame::OnCommand(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	
	printf("OnCommand: wParam= x%x(%d)\n",wParam,wParam); 
	bHandled = false;
		
	if (wParam >= PLUGIN_MENU_MIN){
		CChildFrame* pChildFrame = m_DeviceManager.GetCurrentChild();
		if ( pChildFrame != NULL )
		{
			try
			{
				bHandled = pChildFrame->m_xdPlxMng.OnUserCommand(wParam);
			} CATCH_REPORT();
		}
	}
	
	return 0;
}


void CMainFrame::UpdateUI()
{
	// update UI elements
	UISetCheck(ID_VIEW_BUSVIEW, ::IsWindowVisible(m_xdItemsView) );
	UISetCheck(ID_VIEW_PROPERTYVIEW, ::IsWindowVisible(m_PropertyView) );
	UISetCheck(ID_FILE_CLEAR_REG,m_bClearReg);
	
	bool MDIOpen = false;
	bool GrabActive = false;
	bool SingleGrabActive = false;
	bool WhiteBalanceActive = false;
	bool bAOI = false;
	int snapmode=0;
	
	CDevice* pDevice = m_DeviceManager.GetCurrentDevice();
	if ( pDevice != NULL ){
		MDIOpen =  m_DeviceManager.ExistMDIChild();
		GrabActive = m_DeviceManager.IsGrabActive();
		SingleGrabActive = pDevice->IsSingleGrabActive();
		bAOI = pDevice->IsScalable(); 
	}
	
	bool mono8 = ( pDevice!= NULL  && pDevice->OnXdGetColorCode() == Color_Mono8);
	bool yuv422 = ( pDevice != NULL && pDevice->OnXdGetColorCode() == Color_YUV8_4_2_2);
	

	UIEnable(ID_FILE_NEW, (pDevice != NULL ) && ! MDIOpen);
	UIEnable(ID_FILE_OPEN, (pDevice != NULL ) && ! GrabActive );
	UIEnable(ID_FILE_SAVE, (pDevice != NULL ) && ! GrabActive && pDevice->GetBitmap() != NULL );
	UIEnable(ID_EDIT_COPY, (pDevice != NULL ) && ( pDevice->GetBitmap() != NULL )) ;
	UIEnable(ID_CAMERA_MAXIMIZE, (pDevice != NULL ) && MDIOpen && pDevice->IsScalable() && ! SingleGrabActive );
	UIEnable(ID_GRAB_CONTINUOUS, (pDevice != NULL ) && ( ! GrabActive ));
	UIEnable(ID_GRAB_SINGLE, (pDevice != NULL ) && ( ! GrabActive )&&(snapmode==0) );
	UIEnable(ID_GRAB_CANCEL, (pDevice != NULL ) && GrabActive );
    UIEnable(ID_SOFTTRIGGER, (pDevice != NULL ) && GrabActive&&(snapmode!=CONTINUATION) );
	UIEnable(ID_PLUGINMANAGER, ( m_DeviceManager.GetCurrentChild() != NULL) );
	UIEnable(ID_CENTERCURSOR, ( m_DeviceManager.GetCurrentChild() != NULL));
	UIEnable(ID_CAMERAINFO, (pDevice != NULL) && ! SingleGrabActive );
	UIEnable(ID_DEFT_VAL, (pDevice != NULL) && ! SingleGrabActive );
	UIEnable(ID_VIEW_ZOOM_BEST, (pDevice != NULL ) && MDIOpen && ! m_DeviceManager.GetCurrentChild()->v.IsZoomedToFit());
 	UIEnable(ID_VIEW_NO_CONVERSION, mono8); 
	UIEnable(ID_VIEW_CONVERSION, mono8); 
	
	UIEnable(ID_WINDOW_CASCADE, MDIOpen);
	UIEnable(ID_WINDOW_TILE_HORZ, MDIOpen);
	UIEnable(ID_WINDOW_TILE_VERT, MDIOpen);
	UIEnable(ID_WINDOW_ARRANGE, MDIOpen);
	
	//Added for Test by HYL 2006.12.22
    if((pDevice != NULL ) && GrabActive&&(snapmode!=CONTINUATION) )
	{
#define ID_CMD_TEST_ON			70004
		CChildFrame* pChildFrame = m_DeviceManager.GetCurrentChild();
		try
			{
				pChildFrame->m_xdPlxMng.OnUserCommand(ID_CMD_TEST_ON);
			} CATCH_REPORT();
#undef ID_CMD_TEST_ON
	}
	
	else{
#define ID_CMD_TEST_OFF		70005
		CChildFrame* pChildFrame = m_DeviceManager.GetCurrentChild();
		try
			{
				if(pChildFrame)	pChildFrame->m_xdPlxMng.OnUserCommand(ID_CMD_TEST_OFF);
			} CATCH_REPORT();
#undef ID_CMD_TEST_OFF

	}	
	//Test===================

	if ( ! GrabActive )
	{
		// delete fps panes
		m_Sbar.SetPaneText(ID_FPS_ACQUIRED_PANE, CString("") );
		m_Sbar.SetPaneText(ID_FPS_DISPLAYED_PANE, CString("") );
	}
	
	for (int i = 0; i < 2; i ++ )
	{
		UISetCheck(ID_VIEW_NO_CONVERSION + i, (pDevice != NULL) && pDevice->OnXdGetColorCode() == Color_Mono8 && pDevice->GetDisplayMode() == i );
	}

	UIUpdateToolBar();
}



LRESULT CMainFrame::OnTimer(UINT, WPARAM wParam, LPARAM, BOOL&)
{
	assert(wParam == TIMERID);
	 
	m_DeviceManager.XD_Show_All_FPS();
	
	CDevice *pCurrentDevice = m_DeviceManager.GetCurrentDevice();
	if ( pCurrentDevice != NULL )
	{
		double	dFpsAcquired = 0;
		double	dFpsDisplayed = 0;
		int		n	= 0;
		n = m_DeviceManager.OnXdGetFrameRate(dFpsDisplayed);
		CString out;
		if ( dFpsAcquired != -1 )
		{
			out.Format("FPS - acquired: %3.1f", dFpsAcquired);
			if ( m_fBroadCastFps )
			{
				::PostMessage(HWND_BROADCAST, m_uFpsBroadCastMessage, 0,  (long) ( dFpsAcquired * 10 + 0.5 )  );
			}
			
		}
		else
		{
			out = "";
			if ( m_fBroadCastFps )
			{
				::PostMessage(HWND_BROADCAST, m_uFpsBroadCastMessage, 0, 0 );
			}
		}
		m_Sbar.SetPaneText(ID_FPS_ACQUIRED_PANE, out);
		if ( dFpsDisplayed != -1 ){
			out.Format("FPS: %3.1f[%04d]", dFpsDisplayed,n);
			
		}
		else
			out = "";
		m_Sbar.SetPaneText(ID_FPS_DISPLAYED_PANE, out);
		CChildFrame* pChildFrame = m_DeviceManager.GetCurrentChild();
		if(pChildFrame){
			TCHAR szCaption[128] = { 0 };    // max text length is 127 for floating caption
			::GetWindowText(pChildFrame->m_hWnd, szCaption, (sizeof(szCaption) / sizeof(TCHAR)) - 1);
		}
		m_DeviceManager.ShowPixelValue();  
	}
	
	return 0;
}



LRESULT CMainFrame::OnSetCursor(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	m_DeviceManager.ShowPixelValue();
	
	return 0;
}



void CMainFrame::CurrentDeviceChanged(CDevice* pDevice)
{
	try
	{
		// refresh the feature views
		Refresh(pDevice);  
		m_xdItemsView.CurrentDeviceChanged(pDevice);
		m_PropertyView.Update_PRP(pDevice);

	}
	CATCH_REPORT()
}



void CMainFrame::Refresh(CDevice* pDevice)
{
	if ( m_DeviceManager.GetCurrentDevice() != pDevice )
		return; // nothing to be done
	// handle feature windows
	
}

  

LRESULT CMainFrame::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	// create command bar window
	HWND hWndCmdBar = m_CmdBar.Create(m_hWnd, rcDefault, NULL, ATL_SIMPLE_CMDBAR_PANE_STYLE);
	
	HMENU hMenu = GetMenu();
	m_CmdBar.AttachMenu(hMenu);

	// load command bar images
//	m_CmdBar.LoadImages(IDR_MAINFRAME);
	// remove old menu
	SetMenu(NULL);
	
	HWND hWndToolBar = CreateSimpleToolBarCtrl(m_hWnd, IDR_MAINFRAME, FALSE, ATL_SIMPLE_TOOLBAR_PANE_STYLE);
	
	CreateSimpleReBar(ATL_SIMPLE_REBAR_NOBORDER_STYLE);
	AddSimpleReBarBand(hWndCmdBar);
	AddSimpleReBarBand(hWndToolBar, NULL, TRUE);
	
	CreateSimpleStatusBar("");
	
	m_Sbar.SubclassWindow(m_hWndStatusBar);
	int arrParts[] =
	{
		    ID_DEFAULT_PANE,
			ID_AOI_PANE,
			ID_POS_PANE,
			ID_VALUE_PANE,
			//		ID_FPS_ACQUIRED_PANE,
			ID_FPS_DISPLAYED_PANE
	};
	m_Sbar.SetPanes(arrParts, sizeof(arrParts) / sizeof(int), false);
	
	CreateMDIClient();
	//
	m_MenuControl.Install(m_CmdBar, m_hWndMDIClient);
	m_CmdBar.SetMDIClient(m_hWndMDIClient);
	
	UIAddToolBar(hWndToolBar);
	UISetCheck(ID_VIEW_TOOLBAR, 1);
	UISetCheck(ID_VIEW_STATUS_BAR, 1);
	
	// register object for message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->AddMessageFilter(this);
	pLoop->AddIdleHandler(this);
	
	m_xdDockWnd.m_hwndClient = NULL;
	HWND hwndDock = m_xdDockWnd.Create(m_hWnd, rcDefault);
	m_xdDockWnd.SetExtendedDockStyle(DCK_EX_REMEMBERSIZE);
	
	::HVGetDeviceTotal(&m_nDeviceNum);

	int iBusCy = 0, iPropertyCy = 0;
	try
	{
		CPropertyBagPtr ptrRegistry = CRegistryPropertyBag::Open(CString(APPLICATION_KEY) + "DockView");
		iBusCy = ptrRegistry->ReadLong("BusSize");
		iPropertyCy = ptrRegistry->ReadLong("PropertySize");
	}
	catch ( HVBaseException& )
	{
	}

	iBusCy = 80 + 40 * GetDeviceNum();

	m_xdDockWnd.SetPaneSize(DOCK_LEFT, 250);
//	m_Dock.SetPaneSize(DOCK_RIGHT, 220);
//	m_Dock.SetPaneSize(DOCK_TOP, 150);

	
	// Create bus viewer window
	m_xdItemsView.Create(hwndDock, rcDefault);
	CString str;
	str.LoadString(IDS_BUS_VIEW);
	m_xdItemsView.SetWindowText(str);
	m_xdDockWnd.AddWindow(m_xdItemsView);
	m_xdDockWnd.DockWindow(m_xdItemsView, DOCK_RIGHT,//DOCK_LEFT,
		iBusCy); 
	UISetCheck(ID_VIEW_BUSVIEW, TRUE);
	
	// Create property viewer window
	m_PropertyView.Create(hwndDock, rcDefault); 
	str.LoadString(IDS_PROPERTY_VIEW);
	m_PropertyView.SetWindowText(str);	
	m_xdDockWnd.AddWindow(m_PropertyView);
	m_xdDockWnd.DockWindow(m_PropertyView,DOCK_RIGHT,// DOCK_LEFT, 
		iPropertyCy);
	UISetCheck(ID_VIEW_PROPERTYVIEW, TRUE);
 
	// Create docked views
	m_xdDockWnd.SetClient(m_hWndMDIClient);

	m_hWndClient = hwndDock;
/*
	// Restore the image size and position
	WINDOWPLACEMENT p;
	ZeroMemory(&p, sizeof(WINDOWPLACEMENT) );
	p.rcNormalPosition.left = 24;
	p.rcNormalPosition.right = 1000;
	p.rcNormalPosition.top = 30;
	p.rcNormalPosition.bottom = 734 ;
	p.showCmd = SW_SHOWNORMAL;
	int showCmd = SW_MAXIMIZE;
	p.length = sizeof(WINDOWPLACEMENT);
	try
	{
		CPropertyBagPtr ptrRegistry = CRegistryPropertyBag::Open(CString(APPLICATION_KEY) + "MainFrame");
		p.rcNormalPosition.left = ptrRegistry->ReadLong("left");
		p.rcNormalPosition.right = ptrRegistry->ReadLong("right");
		p.rcNormalPosition.bottom = ptrRegistry->ReadLong("bottom");
		p.rcNormalPosition.top = ptrRegistry->ReadLong("top");
		showCmd = ptrRegistry->ReadLong("showCmd");;
	}
	catch ( HVBaseException& )
	{
	}
	SetWindowPlacement(&p);
	
	if ( showCmd == SW_MAXIMIZE )
	{
		PostMessage(WM_SYSCOMMAND, SC_MAXIMIZE, 0);
	}
	*/
	// register fps broadcast message 
	m_uFpsBroadCastMessage = RegisterWindowMessage(BROADCAST_FPS_MSG);
	// check if we are asked to broadcast the frame rate
	CRegKey regKey;
	if ( regKey.Open(HKEY_CURRENT_USER, APPLICATION_KEY) == ERROR_SUCCESS )
	{
		DWORD val;
		if ( regKey.QueryValue(val, ENABLE_BROADCAST_KEY) != ERROR_SUCCESS )
		{
			regKey.SetValue((DWORD) 0, ENABLE_BROADCAST_KEY);  // if value entry doesn't exist, create it for convenience, but disable frame rate broadcast
		}
		else
		{
			m_fBroadCastFps = val != 0;
		}
	}
	
//	PostMessage(WM_TEST, TEST_GRAB_CONTINUOUS, 0);
	PostMessage(WM_TEST, XD_GRAB_CONTINUOUS, 0);
	

	return 0;
}





// An error occured in the thread procedure of an image acquisition thread. The thread sends
// a user defined message to the main frame before it terminates. Inform the according camera object that 
// its image acquisition thread got an error. We have chosen this complicated procedure to avoid to run a local message 
// pump to allow both, waiting for the terminating thread and dispatching incoming windows messages. 
LRESULT CMainFrame::OnError(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
	CDevice* pDevice = (CDevice*) lParam;
	
	if ( pDevice != NULL && m_DeviceManager.IsDeviceValid(pDevice) )
	{
		try
		{
			pDevice->OnContinuousGrabError(wParam);
		} CATCH_REPORT();
	}
	
	return 0;
}




LRESULT CMainFrame::OnTest(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{	
	CAutoTest testXD(*this);
	
	try{
		testXD.OnXdDispatch(wParam, (void *)lParam);
	}CATCH_REPORT();
	
	return 0;
}



LRESULT CMainFrame::OnFileClearReg(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
//	CString keyName = APPLICATION_KEY;
//	SHDeleteKey(HKEY_CURRENT_USER,keyName);// _T("SoftwareMicrosoftInternet ExplorerIntelliForms"));
	m_bClearReg = !m_bClearReg;
	return 0;
}



LRESULT CMainFrame::OnFileExit(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	PostMessage(WM_CLOSE);
	return 0;
}



LRESULT CMainFrame::OnFileNew(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	// TODO: add code to initialize document
	if (m_DeviceManager.GetCurrentDevice() != NULL && \
		!m_DeviceManager.ExistMDIChild())
	{
		m_DeviceManager.AddMDIChild();
	}
	
	return 0;
}



LRESULT CMainFrame::OnEditCopy(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if ( m_DeviceManager.GetCurrentDevice() != NULL && m_DeviceManager.GetCurrentDevice()->GetBitmap() != NULL )
	{
		try
		{
			bool res = m_DeviceManager.GetCurrentDevice()->GetBitmap()->CopyToClipboard(m_hWnd);
			if ( ! res )
				throw HVBaseException(::GetLastError(), "Copy to clipboard");
		}
		CATCH_REPORT();
	}
	
	return 0;
}



LRESULT CMainFrame::OnFileOpen(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if ( m_DeviceManager.GetCurrentDevice() != NULL && ! m_DeviceManager.IsGrabActive() )
	{
		
		CFileDialog dlg(TRUE, _T("bmp"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("Bitmap Files (*.bmp)\0*.bmp\0All Files (*.*)\0*.*\0"), m_hWnd);
		if(dlg.DoModal() == IDOK)
		{
			try
			{
				m_DeviceManager.GrabFromFile(dlg.m_szFileName);
			}
			CATCH_REPORT();
		}
	}
	
	return 0;
}



LRESULT CMainFrame::OnFileSave(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	
	if ( m_DeviceManager.GetCurrentDevice() != NULL && m_DeviceManager.GetCurrentDevice()->GetBitmap() != NULL )
	{
		
		try
		{
			CString strFilename;
			strFilename.Format("%s%03d","Image",m_nSaveTimes);

			//CFileDialog dlg(FALSE, _T("bmp"), _T("Temp"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("Bitmap Files (*.bmp)\0*.bmp\0JPEG Files (*.jpg)\0*.jpg\0All Files (*.*)\0*.*\0"), m_hWnd);			
			CFileDialog dlg(FALSE, _T("bmp"), strFilename, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("Bitmap Files (*.bmp)\0*.bmp\0All Files (*.*)\0*.*\0"), m_hWnd);			
			if(dlg.DoModal() == IDOK)
			{
				m_nSaveTimes++;
				CHVBitmap *pBitmap = m_DeviceManager.GetCurrentDevice()->GetBitmap();
			 	CString strExt;
				strExt = CString(dlg.m_szFileName).Right(3);
				strExt.MakeLower();
				if (strExt == "bmp")
				{
					bool res = pBitmap->Save(dlg.m_szFileName);
					if ( ! res )
						throw HVBaseException(::GetLastError(), "CImage::Save()");
				}
				else
					throw HVBaseException(STATUS_FILE_INVALID, "CImage::Save()"); 
			}
		} CATCH_REPORT();
	}
	return 0;
	
}



LRESULT CMainFrame::OnViewToolBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	
	printf("OnViewToolBar: ...\n"); 
	static BOOL bVisible = TRUE;	// initially visible
	bVisible = !bVisible;
	CReBarCtrl rebar = m_hWndToolBar;
	int nBandIndex = rebar.IdToIndex(ATL_IDW_BAND_FIRST + 1);	// toolbar is 2nd added band
	rebar.ShowBand(nBandIndex, bVisible);
	UISetCheck(ID_VIEW_TOOLBAR, bVisible);
	UpdateLayout();
	return 0;
}



LRESULT CMainFrame::OnViewStatusBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL bVisible = !::IsWindowVisible(m_hWndStatusBar);
	::ShowWindow(m_hWndStatusBar, bVisible ? SW_SHOWNOACTIVATE : SW_HIDE);
	UISetCheck(ID_VIEW_STATUS_BAR, bVisible);
	UpdateLayout();
	return 0;
}



LRESULT CMainFrame::OnViewBusView(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL visible;
	if( visible = ::IsWindowVisible(m_xdItemsView) ) 
		m_xdDockWnd.HideWindow(m_xdItemsView); 
	else 
		m_xdDockWnd.DockWindow(m_xdItemsView, DOCK_LASTKNOWN);
	UISetCheck(ID_VIEW_BUSVIEW, ! visible);
	UpdateLayout();
	return 0;
}



LRESULT CMainFrame::OnViewPropertyView(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL visible;
	if( visible = ::IsWindowVisible(m_PropertyView) ) 
		m_xdDockWnd.HideWindow(m_PropertyView); 
	else 
		m_xdDockWnd.DockWindow(m_PropertyView, DOCK_LASTKNOWN);
	UISetCheck(ID_VIEW_PROPERTYVIEW, ! visible);
	UpdateLayout();
	m_PropertyView.SetFocus();
	return 0;
}




LRESULT CMainFrame::OnCenterCursor(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CChildFrame* pChildFrame = m_DeviceManager.GetCurrentChild();
	if ( pChildFrame != NULL )
	{
		try
		{
			pChildFrame->v.CenterCursor();
		} CATCH_REPORT();
	}
	return 0;
}



LRESULT CMainFrame::OnViewZoom(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	try
	{
		CChildFrame* pChildFrame = m_DeviceManager.GetCurrentChild();
		if ( pChildFrame != NULL )
		{
			double zoom = 1.0;
			switch ( wID )
			{
			case ID_VIEW_ZOOM_25:
				zoom = 0.25;
				break;
			case ID_VIEW_ZOOM_50:
				zoom = 0.5;
				break;
			case ID_VIEW_ZOOM_100:
				zoom = 1.0;
				break;
			case ID_VIEW_ZOOM_200:
				zoom = 2.0;
				break;
			case ID_VIEW_ZOOM_400:
				zoom = 4.0;
				break;
			case ID_VIEW_ZOOM_USER:
				{
					CZoomDlg dlg;
					zoom = pChildFrame->v.GetZoomLevel();
					dlg.SetFactor(zoom);
					if ( dlg.DoModal() == IDOK )
					{
						zoom = dlg.GetFactor();
					}
					if ( zoom < 1.0 / 32.0 )
					{
						zoom = 1.0 / 32.0; 
						MessageBeep(0);
					}
					else if ( zoom > 32 )
					{
						MessageBeep(0);
						zoom = 32;
					}
				}
				break;
			default:
				assert(false);
			}
			
			pChildFrame->v.SetZoomLevel(zoom);
			pChildFrame->v.ZoomIn(NULL, 0);
		}
	} CATCH_REPORT();
	
	return 0;
}

LRESULT CMainFrame::OnViewZoomBest(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CChildFrame* pChildFrame = m_DeviceManager.GetCurrentChild();
	if ( pChildFrame != NULL )
	{
		try
		{
			pChildFrame->v.OnXdZoomToFit();
		} CATCH_REPORT();
	}
	
	return 0;
}

LRESULT CMainFrame::OnViewZoom_1_1(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	
	try
	{
		CChildFrame* pChildFrame = m_DeviceManager.GetCurrentChild();
		if ( pChildFrame != NULL )
		{
			double zoom = 1.0;
			switch ( ID_VIEW_ZOOM_100 )
			{
			case ID_VIEW_ZOOM_25:
				zoom = 0.25;
				break;
			case ID_VIEW_ZOOM_50:
				zoom = 0.5;
				break;
			case ID_VIEW_ZOOM_100:
				zoom = 1.0;
				break;
			case ID_VIEW_ZOOM_200:
				zoom = 2.0;
				break;
			case ID_VIEW_ZOOM_400:
				zoom = 4.0;
				break;
			case ID_VIEW_ZOOM_USER:
				{
					CZoomDlg dlg;
					zoom = pChildFrame->v.GetZoomLevel();
					dlg.SetFactor(zoom);
					if ( dlg.DoModal() == IDOK )
					{
						zoom = dlg.GetFactor();
					}
					if ( zoom < 1.0 / 32.0 )
					{
						zoom = 1.0 / 32.0; 
						MessageBeep(0);
					}
					else if ( zoom > 32 )
					{
						MessageBeep(0);
						zoom = 32;
					}
				}
				break;
			default:
				assert(false);
			}
			
			pChildFrame->v.SetZoomLevel(zoom);
			pChildFrame->v.ZoomIn(NULL, 0);
		}
	} CATCH_REPORT();
	
	return 0;
}

LRESULT CMainFrame::OnViewConversion(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CDevice *pDevice = m_DeviceManager.GetCurrentDevice();
	if ( pDevice  != NULL && pDevice->OnXdGetColorCode() == Color_Mono8 )
	{
		try
		{
			pDevice->SetDisplayMode(wID - ID_VIEW_NO_CONVERSION);
		} CATCH_REPORT();
	}
	
	return 0;
}




LRESULT CMainFrame::OnAppAbout(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CAboutDlg dlg;
	dlg.DoModal();
	return 0;
}


LRESULT CMainFrame::OnAppHelp(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
//	HtmlHelp(m_hWnd, "BeautifulloverWTL-help.chm::/index.htm", HH_DISPLAY_TOPIC, NULL);
	
	CAboutDlg dlg;
	dlg.DoModal();
	return 0;
}


LRESULT CMainFrame::OnWindowCascade(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	MDICascade();
	return 0;
}

LRESULT CMainFrame::OnWindowTileHorz(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	MDITile();
	return 0;
}



LRESULT CMainFrame::OnWindowTileVert(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	MDITile(MDITILE_VERTICAL);
	return 0;
}



LRESULT CMainFrame::OnWindowArrangeIcons(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	MDIIconArrange();
	return 0;
}



LRESULT CMainFrame::OnGrabSingle(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if ( m_DeviceManager.GetCurrentDevice() != NULL )
	{
		try
		{
			m_DeviceManager.GrabSingle();
		}
		CATCH_REPORT();
	}
	return 0;
}



LRESULT CMainFrame::OnGrabContinuous(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if ( m_DeviceManager.GetCurrentDevice() != NULL )
	{
		try
		{
			m_DeviceManager.GrabContinuous();
		}
		CATCH_REPORT();
	}
	
	return 0;
}
LRESULT CMainFrame::OnSetDefaultValue(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CDevice *pDevice = m_DeviceManager.GetCurrentDevice(); 
	return 0;
}




LRESULT CMainFrame::OnGrabCancel(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if ( m_DeviceManager.GetCurrentDevice() != NULL )
	{
		try
		{
			
				m_DeviceManager.GrabCancel();		
			
		}
		CATCH_REPORT();
	}
	
	return 0;
}


LRESULT CMainFrame::OnSoftTrigger(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    if ( m_DeviceManager.GetCurrentDevice() != NULL )
    {
        try
        {
            m_DeviceManager.OnSoftTrigger();
        }
        CATCH_REPORT();
    }
    
    return 0;
}



LRESULT CMainFrame::OnDeviceMaximize(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	
	if ( m_DeviceManager.GetCurrentDevice() != NULL )
	{
		try
		{
			m_DeviceManager.GetCurrentDevice()->MaximizeAOI();
		}
		CATCH_REPORT();
	}
	
	return 0;
}



LRESULT CMainFrame::OnCameraInfo(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if ( m_DeviceManager.GetCurrentDevice() != NULL )
	{
		try
		{
			CXdDlg dlg(m_DeviceManager.GetCurrentDevice());
			dlg.DoModal();
		}
		CATCH_REPORT();
	}

	return 0;
}



LRESULT CMainFrame::OnPluginManager(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CChildFrame* pChildFrame = m_DeviceManager.GetCurrentChild();
	if ( pChildFrame != NULL )
	{
		try
		{
			pChildFrame->OnPluginManager();
		} CATCH_REPORT();
	}

	return 0;
}


void CMainFrame::ReportError(HVBaseException& e)
{
	if ( ! ::IsWindow(m_ErrorBox) )
	{
		m_ErrorBox.Create(m_hWnd);
		m_ErrorBox.ShowWindow(SW_SHOW);
	}
	m_ErrorBox.ReportError(e);
}
 
void CMainFrame::Set_Def_AOI(CRect &rc)
{
	if ( m_DeviceManager.GetCurrentDevice() != NULL )
	{
		try
		{
		
			m_DeviceManager.GetCurrentDevice()->SetAOI(rc); 
		}
		CATCH_REPORT();
	}
	

} 
int CMainFrame::GetDeviceNum()
{
	return m_nDeviceNum;
} 