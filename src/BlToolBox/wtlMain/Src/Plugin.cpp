// Plugin.cpp: implementation of the CPlugin class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
#include "Plugin.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPlugin::CPlugin(CString &filename):
  m_hXdModule(NULL),
  m_hPlugin(NULL),
  m_filename(filename),
  m_fActive(false)
{
	m_hXdModule = LoadLibrary(filename);
	if (m_hXdModule) {
		xdCreateObject();
	}
}

CPlugin::~CPlugin()
{
	if (m_hXdModule) {
		xdDestoryObject();
		FreeLibrary(m_hXdModule);
	}
}


void CPlugin::xdCreateObject()
{
	assert(m_hXdModule != NULL);
	FARPROC pFunc = GetProcAddress(m_hXdModule, "PluginCreateObject");
	if (pFunc ) {
		m_hPlugin = (* (PLUGIN_CreateObject *)pFunc)(); 
	}
}


void CPlugin::xdDestoryObject()
{
	assert(m_hXdModule != NULL);
	
	if (m_hPlugin == NULL)
		return;

	FARPROC pFunc = GetProcAddress(m_hXdModule, "PluginDestoryObject");
	if (pFunc ) {
		(* (PLUGIN_DestoryObject *)pFunc)(m_hPlugin); 
		m_hPlugin = NULL;
	}
}



void CPlugin::xdInitialize(CCamera &camera, CManager &manager) const
{
	assert(m_hXdModule != NULL);
	assert(m_hPlugin != NULL);
	FARPROC pFunc = GetProcAddress(m_hXdModule, "PluginInitialize");
	if (pFunc ) {
		(* (PLUGIN_Initialize *)pFunc)(m_hPlugin, camera, manager); 
	}
}


void CPlugin::xdUninitialize() const
{
	assert(m_hXdModule != NULL);
	assert(m_hPlugin != NULL);
	FARPROC pFunc = GetProcAddress(m_hXdModule, "PluginUninitialize");
	if (pFunc ) {
		(* (PLUGIN_Uninitialize *)pFunc)(m_hPlugin); 
	}
}



CString CPlugin::GetName()
{
	CString name;

	if (m_hPlugin == NULL)
		return name;
	
	FARPROC pFunc = GetProcAddress(m_hXdModule, "PluginGetName");
	if (pFunc ) {
		char buffer[256] = {0};
		(* (PLUGIN_GetName *)pFunc)(m_hPlugin, buffer); 
		buffer[255] = 0;
		name.Format("%s", buffer);
	}

	return name;
}





CString CPlugin::GetInfo()
{
	CString info;

	if (m_hPlugin == NULL)
		return info;
	
	FARPROC pFunc = GetProcAddress(m_hXdModule, "PluginGetInfo");
	if (pFunc ) {
		char buffer[256] = {0};
		(* (PLUGIN_GetInfo *)pFunc)(m_hPlugin, buffer); 
		buffer[255] = 0;
		info.Format("%s", buffer);
	}

	return info;
}





DWORD CPlugin::GetSpecVersion()
{
	DWORD dwVer = 0;
	
	if (m_hPlugin == NULL)
		return dwVer;
	
	FARPROC pFunc = GetProcAddress(m_hXdModule, "PluginGetSpecVersion");
	if (pFunc ) {
		dwVer = (* (PLUGIN_GetSpecVersion *)pFunc)(m_hPlugin); 
	}

	return dwVer;
}




void CPlugin::lfActivate(CCamera &camera, CManager &manager)
{	
	if (m_hPlugin == NULL || m_fActive)
		return;
	
	xdInitialize(camera, manager);
	m_fActive = true;
}




void CPlugin::lfDeactivate()
{	
	if (m_hPlugin == NULL || (! m_fActive))
		return;

	xdUninitialize();
	m_fActive = false;
}



bool CPlugin::OnUserCommand(unsigned int id)
{
	bool flag = false;
	if (m_hPlugin == NULL)
		return false;
	
	FARPROC pFunc = GetProcAddress(m_hXdModule, "PluginOnUserCommand");
	if (pFunc ) {
		flag = (* (PLUGIN_OnUserCommand *)pFunc)(m_hPlugin, id); 
	}
	return flag;
}



void CPlugin::OnManagerNotify(CImageInfo &info)
{
	if (m_hPlugin == NULL)
		return ;
	
	FARPROC pFunc = GetProcAddress(m_hXdModule, "PluginOnManagerNotify");
	if (pFunc ) {
		(* (PLUGIN_OnManagerNotify *)pFunc)(m_hPlugin, info); 
	}
}



void CPlugin::OnStartGrabbing(CImageInfo &info)
{
	if (m_hPlugin == NULL)
		return ;
	
	FARPROC pFunc = GetProcAddress(m_hXdModule, "PluginOnStartGrabbing");
	if (pFunc ) {
		(* (PLUGIN_OnStartGrabbing *)pFunc)(m_hPlugin, info); 
	}
}



void CPlugin::OnStopGrabbing(CImageInfo &info, CImageData &data)
{
	if (m_hPlugin == NULL)
		return ;
	
	FARPROC pFunc = GetProcAddress(m_hXdModule, "PluginOnStopGrabbing");
	if (pFunc ) {
		(* (PLUGIN_OnStopGrabbing *)pFunc)(m_hPlugin, info, data); 
	}
}




void CPlugin::OnShowFrame(CImageInfo &info, CImageData &data)
{
	if (m_hPlugin == NULL)
		return ;
	
	FARPROC pFunc = GetProcAddress(m_hXdModule, "PluginOnShowFrame");
	if (pFunc ) {
		(* (PLUGIN_OnShowFrame *)pFunc)(m_hPlugin, info, data); 
	}
}




void CPlugin::OnPaletteSetting(void *PalEntry)
{
	if (m_hPlugin == NULL)
		return ;
	
	FARPROC pFunc = GetProcAddress(m_hXdModule, "PluginOnPaletteSetting");
	if (pFunc ) {
		(* (PLUGIN_OnPaletteSetting *)pFunc)(m_hPlugin, PalEntry); 
	}
}