// Plugin.h: interface for the CPlugin class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PLUGIN_H__47285370_C048_41A9_A32F_D34D16131024__INCLUDED_)
#define AFX_PLUGIN_H__47285370_C048_41A9_A32F_D34D16131024__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "PluginSpec.h"


class CPlugin  
{
public:
	CPlugin(CString &filename);
	virtual ~CPlugin();

	/// get address of plugin module
	CString GetFileName() { return m_filename; }
	/// get name of user plugin
	CString GetName();
	/// get description of user plugin
	CString GetInfo();
	/// get version of plugin specification
	DWORD GetSpecVersion();

	/// open the plugin and do some initialization
	void lfActivate(CCamera &camera, CManager &manager);
	/// close the plugin and do some cleanup
	void lfDeactivate();
	/// get state of plugin
	bool IsActive() { return m_fActive; }

	/// 
	bool OnUserCommand(unsigned int id);
	/// 
	void OnManagerNotify(CImageInfo &info);
	///  
	void OnStartGrabbing(CImageInfo &info);
	/// 
	void OnStopGrabbing(CImageInfo &info, CImageData &data);
	/// 
	void OnShowFrame(CImageInfo &info, CImageData &data);

	/// 
	void OnPaletteSetting(void *PalEntry);

private:
	/// hide copy constructor and assignment operator 

	friend class CPluginManager;
	
private:

	void xdInitialize(CCamera &camera, CManager &manager) const;
	void xdUninitialize() const;
	void xdCreateObject(); 
	void xdDestoryObject(); 

	/// 
	HMODULE				m_hXdModule; 
	HANDLE				m_hPlugin;
	CString				m_filename;
	//////////////////////////////////////////////////////////////////////////
	
	/// is AOI maximize 
	bool				m_fActive;
};

#endif // !defined(AFX_PLUGIN_H__47285370_C048_41A9_A32F_D34D16131024__INCLUDED_)
