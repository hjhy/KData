// PluginManager.h: interface for the CPluginManager class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PLUGINMANAGER_H__2635ED11_7AA2_412D_B4AA_A39B60F50C1B__INCLUDED_)
#define AFX_PLUGINMANAGER_H__2635ED11_7AA2_412D_B4AA_A39B60F50C1B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CChildFrame;
class CDevice;
class CMainFrame;

#include "Plugin.h"

class CPluginManager  
{
public:
	CPluginManager(CDevice *pDevice, CChildFrame &ChildFrame, CMainFrame &MainFrame);
	virtual ~CPluginManager();

	void LoadPlugins();
	void UnloadPlugins();

	/// 
	bool OnUserCommand(UINT id);
	/// 
	void OnManagerNotify(CImageInfo &info);
	/// 
	void OnStartGrabbing(CImageInfo &info);
	/// 
	void OnStopGrabbing(CImageInfo &info, CImageData &data);
	/// 
	void OnShowFrame(CImageInfo &info, CImageData &data);
	/// 
	void OnPaletteSetting(void *PalEntry);

	/// 
	void PluginActivate(CPlugin *pPlugin);
	void PluginDeactivate(CPlugin *pPlugin);

private:
	std::vector<CPlugin *> PluginSet() { return m_PluginsSet; }
	/// 
	void RebuildPluginSet(std::vector<CPlugin *> &PluginSet) ;

private:
	CDevice				*m_pDevice; 
	/// reference to the camera's child frame
	CChildFrame          &m_ChildFrame;
	/// 
	CMainFrame			 &m_MainFrame;
	/// 
	std::vector<CPlugin *> m_PluginsSet;

	friend class CPluginManagerDlg;
};

#endif // !defined(AFX_PLUGINMANAGER_H__2635ED11_7AA2_412D_B4AA_A39B60F50C1B__INCLUDED_)
