// PluginSpec.h: interface for the PluginSpec class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PLUGINSPEC_H__1B33C703_2E35_4997_829A_335FFA7D3A1F__INCLUDED_)
#define AFX_PLUGINSPEC_H__1B33C703_2E35_4997_829A_335FFA7D3A1F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


/**The different image storage formats supported.*/
enum IMAGEFORMAT
{
    FORMAT_Mono8 = 0,     ///< Y component has 8bit data
	FORMAT_YUV8_4_1_1,    ///< 4:1:1 YUV 8 format, each component has 8bit data
	FORMAT_YUV8_4_2_2,    ///< 4:2:2 YUV 8 format, each component has 8bit data
	FORMAT_YUV8_4_4_4,    ///< 4:4:4 YUV 8 format, each component has 8bit data
	FORMAT_RGB8,          ///< RGB 8 format, each component has 8bit data
	FORMAT_Mono16,        ///< Y component has 16bit data
	FORMAT_RGB16          ///< RGB 16 format, each component has 16bit data
};


struct CImageInfo {
	unsigned long ImageWidth;       // Image width in pixel
    unsigned long ImageHeight;      // Image height in pixel
    unsigned long ImageBitDepth;    // Image depth in bits (8,16,24,32)
    unsigned long ImageBitDepthReal;// Precise Image depth (x bits)
    unsigned long ImageSizeBytes;   // Size used to store one image.
    IMAGEFORMAT ImageFormat;       // Planar, Packed, color or mono

};



struct CImageData {
	void	*pRawBuffer;
	void	*pImageBuffer;
};
 
struct CCamera {
	HANDLE	hDevice;
	char	*pName;
};


struct CManager {
	HANDLE	hWndParent;
	HANDLE	hMenu;
	char	*pRegistryPath;
};
 
#ifdef __cplusplus
extern "C" {
#endif


/*------------------------- Standard 1.0 ----------------------------*/
typedef HANDLE (__stdcall PLUGIN_CreateObject )();
typedef void (__stdcall PLUGIN_DestoryObject)(HANDLE );

typedef void (__stdcall PLUGIN_GetName)(HANDLE, char*);
typedef void (__stdcall PLUGIN_GetInfo)(HANDLE, char*);
typedef DWORD (__stdcall PLUGIN_GetSpecVersion)(HANDLE);

typedef void (__stdcall PLUGIN_Initialize)(HANDLE, CCamera, CManager);
typedef void (__stdcall PLUGIN_Uninitialize)(HANDLE);
typedef bool (__stdcall PLUGIN_OnUserCommand)(HANDLE, unsigned int id);
typedef void (__stdcall PLUGIN_OnManagerNotify)(HANDLE, CImageInfo);
typedef void (__stdcall PLUGIN_OnStartGrabbing)(HANDLE, CImageInfo);
typedef void (__stdcall PLUGIN_OnStopGrabbing)(HANDLE, CImageInfo, CImageData);
typedef void (__stdcall PLUGIN_OnShowFrame)(HANDLE, CImageInfo, CImageData);

typedef void (__stdcall PLUGIN_OnPaletteSetting)(HANDLE, void *PalEntry);

/* extern "C" { */
#ifdef __cplusplus
}
#endif


#endif // !defined(AFX_PLUGINSPEC_H__1B33C703_2E35_4997_829A_335FFA7D3A1F__INCLUDED_)
