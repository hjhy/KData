// PropertyView.cpp: implementation of the CPropertyView class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DeviceManager.h"
#include "PropertyView.h"
#include "device.h"
#include "mainfrm.h"
#include "utility.h"
 
 
CPropertyView::CPropertyView(CDeviceManager& dm, CMainFrame& mf) : 
m_dm4PrpV(dm), 
m_refXdMf(mf)
{

}



LRESULT CPropertyView::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	m_prpListctrl.SubclassWindow(GetDlgItem(IDC_PROPERTYLIST));
	m_prpListctrl.SetExtendedListStyle(PLS_EX_CATEGORIZED | PLS_EX_XPLOOK);
	
	return 0;
}




LRESULT CPropertyView::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	
	bHandled = FALSE;
	return 0;
}




LRESULT CPropertyView::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	RECT rcClient;
	GetWindowRect(&rcClient);
	
	HWND hwnd;
	RECT rc;
	hwnd = GetDlgItem(IDC_PROPERTYLIST);
	::GetWindowRect(hwnd, &rc);
	::SetWindowPos(hwnd, HWND_TOP, 
		0, 0, 
		rcClient.right - rc.left, rcClient.bottom - rc.top,
		SWP_NOMOVE|SWP_NOACTIVATE|SWP_NOZORDER);
	
	return 0;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Protected / private member functions
//

//------------------------------------------------------------------------------
// void CPropertyView::ReportError(BcamException& e) 
// Author: 
//------------------------------------------------------------------------------
/**
* Shows an error message
*
* \param     e Reference to a BcamException
* \return    void
*
*/
//------------------------------------------------------------------------------
void CPropertyView::ReportError(HVBaseException& e) 
{
	m_refXdMf.ReportError(e);
}


 
void CPropertyView::Update_PRP(CDevice* pDevice)
{
	m_prpListctrl.ResetContent();
	
	if ( ! pDevice ){
		return;
	} 
		
	pDevice->_OnAddPRP(m_prpListctrl);   

}

LRESULT CPropertyView::OnXdClick(int idCtrl, LPNMHDR pnmh, BOOL& /*bHandled*/)
{//PIN_BROWSE
	LPNMPROPERTYITEM pnpi = (LPNMPROPERTYITEM) pnmh;
	CDevice *pDevice = m_dm4PrpV.GetCurrentDevice();
	if (pDevice == NULL){
		return 0;
	}

	
	CComVariant vValue;
	pnpi->prop->GetValue(&vValue);
	vValue.ChangeType(VT_BSTR);
	CString str = vValue.bstrVal;
	int id = m_prpListctrl.GetItemData(pnpi->prop);  
	 
    pDevice->OnXdPrpClickItem((BTFL_PRP_ID)id,(int)m_refXdMf.m_hWnd);
	return 1;
}

LRESULT CPropertyView::OnXdFileNameChanged(int idCtrl, LPNMHDR pnmh, BOOL& /*bHandled*/)
{//PIN_BROWSE
	LPNMPROPERTYITEM pnpi = (LPNMPROPERTYITEM) pnmh;
	
	CComVariant vValue;
	pnpi->prop->GetValue(&vValue);
	vValue.ChangeType(VT_BSTR);
	CString str = vValue.bstrVal;
	int id = m_prpListctrl.GetItemData(pnpi->prop); 
//	::MessageBox(NULL,str,"xdxdxd",IDOK);// xdTest_001
	return 1;
}

LRESULT CPropertyView::OnXdListItemChanged(int idCtrl, LPNMHDR pnmh, BOOL& /*bHandled*/)
{
	LPNMPROPERTYITEM pnpi = (LPNMPROPERTYITEM) pnmh;

	CDevice *pDevice = m_dm4PrpV.GetCurrentDevice();
	if (pDevice == NULL){
		return 0;
	}

	CComVariant vValue;
	pnpi->prop->GetValue(&vValue);
	vValue.ChangeType(VT_UINT);
	int index = vValue.uiVal;
	int id = m_prpListctrl.GetItemData(pnpi->prop); 
    
	
    pDevice->OnXdPrpListItemChanged((BTFL_PRP_ID)id,index);
	return 1; 
}



LRESULT CPropertyView::OnScalarChanged(int idCtrl, LPNMHDR pnmh, BOOL& /*bHandled*/)
{
	LPNMPROPERTYSCALAR pnpi = (LPNMPROPERTYSCALAR) pnmh;

	int value = pnpi->value;
	int id = m_prpListctrl.GetItemData(pnpi->prop);
	CDevice *pDevice = m_dm4PrpV.GetCurrentDevice();
	if (pDevice == NULL){
		return 0;
	}

    pDevice->OnPrpScalarChanged((BTFL_PRP_ID)id,value);
	return 1;
 
}
 
    
void CPropertyView::Spec_Fun_Interface_1(CDevice*pDevice,HV_INTERFACE1_ID Interface_ID,int value)
{
	HV_INTERFACE1_CONTEXT pInfcectx;
	pInfcectx.ID = Interface_ID;
	pInfcectx.dwVal=(DWORD)value;
	HHV hhv=pDevice->GetHandle();
	HVSTATUS status = HVCommand(hhv,CMD_SPEC_FUN_INTERFACE1,&pInfcectx,0);
	if ( ! HV_SUCCESS(status))
	   throw HVBaseException(status, "HVDAILT.dll : HVCommand");
} 
void CPropertyView::OnPrpChanged(CDevice *pDevice,int nID,int value)
{
	if (!pDevice)
	{
		return;
	}

	HHV hhv=pDevice->GetHandle();
	
	HVSTATUS status = STATUS_NOT_SUPPORT_INTERFACE;
		
	int nId=nID;// 0x01020000;
	BYTE*pB=(BYTE*)&nId;
	BYTE btName,btPram,btSubId,btOther;
	btName = pB[3];  //解析函数名
	btPram = pB[2];  //解析函数参数ID号
	btSubId= pB[1];  //解析函数参数子ID号
	btOther= pB[0]; 
	
	ATLTRACE("nID=0x%x:[btName=0x%x,btPram=0x%x,btSubID=0x%x,btOther=0x%x]value=%d",nId,btName,btPram,btSubId,btOther,value);
    
	switch(btName) {
	case 1://HVAGCControl
	 
		break;
	case 2://HVAECControl 
		break;
	case 3://HVADCControl 
		break;
	case 4://AOI
		{
			CRect rc;
			CSize Sz;
			CPoint pt;
			Sz=pDevice->GetImageSize();
			pt=pDevice->m_Origin;
			
			switch(btOther) {
			case 1: //left
				{
					rc.left=value;
					rc.top =pt.y;
					rc.right = value+Sz.cx;
					rc.bottom =pt.y+Sz.cy;
					pDevice->SetAOI(rc);
				}
				break;
			case 2: //top
				{
					rc.left=pt.x;
					rc.top =value;
					rc.right = pt.x+Sz.cx;
					rc.bottom =value+Sz.cy;
					pDevice->SetAOI(rc);
				}
				break;
			case 3: //width
				{
					rc.left=pt.x;
					rc.top =pt.y;
					rc.right = value+pt.x;
					rc.bottom =pt.y+Sz.cy;
					pDevice->SetAOI(rc);
				}
				break;
			case 4: //Height
				{
					rc.left=pt.x;
					rc.top =pt.y;
					rc.right = pt.x+Sz.cx;
					rc.bottom =pt.y+value;
					pDevice->SetAOI(rc);
				}
				break;
			default:
				break;
			} 
	
		}
		break;
	case 5://HVCommand
		{
			switch(btPram) {
			case CMD_SET_LUT_DWENTRIES: //0x15 
				break;
			case CMD_SPEC_FUN_INTERFACE1:  //0x80
				Spec_Fun_Interface_1(pDevice,(HV_INTERFACE1_ID)btSubId,value);
				break;
			default:
				break;
			}
		}
		break;
	case 6://HVSetSnapMode 
		break;
	case 7://HVSetTriggerPolarity 
		break;
	case 8://HVSetStrobePolarity 
		break;
	default:
		ATLTRACE("OnPrpChanged::Error!");
		break;
	}
}

