 
#if !defined(AFX_PropertyView_H__F4CED1A3_9CED_4DF5_9C91_692B10F7E930__INCLUDED_)
#define AFX_PropertyView_H__F4CED1A3_9CED_4DF5_9C91_692B10F7E930__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
#include "dialogview.h"
#include "controls\PropertyList.h"

class CMainFrame;
class CDeviceManager;

 

#define ITEM_VIDEOMODE			0 
#define ITEM_SHUTTERUNIT		2
#define SCALAR_SHUTTERSPEED		3
#define ITEM_SNAPMODE			4
#define ITEM_TRIGGERPOLARITY	5
#define ITEM_STROBEPOLARITY		6
#define SCALAR_GAINLEVEL		7 
#define ITEM_BLACKLEVELENABLE	9
#define SCALAR_BLACKLEVEL		10
#define SCALAR_BLANKHOR			11
#define SCALAR_BLANKVERT		12
#define SCALAR_PACKET           13
#define SCALAR_BRIGHTNESS       14
#define SCALAR_BLUE_GAIN		15
#define SCALAR_RED_GAIN			16
#define ITEM_LUT                17
#define ITEM_FRAMEFROZEN        18
#define ITEM_SOFTTRIGGER        19
#define ITEM_TEST_IMAGE         20  
#define ITEM_STROBE_ON_OFF      21
#define ITEM_TRIGGERDELAY_STATE 22   
#define SCALAR_TRIGGERDELAY_VALUE  23
#define ITEM_8or12BitMode       24  
#define ITEM_OUTPUTIO_0         25  
#define ITEM_OUTPUTIO_1         26
#define ITEM_OUTPUTIO_2         27
#define ITEM_OUTPUTIO_0_CONTROL 28   
#define ITEM_OUTPUTIO_1_CONTROL 29  
#define ITEM_OUTPUTIO_2_CONTROL 30  
#define ITEM_INPUTIOSET         31   
#define SCALAR_TRANSDELAY_VALUE 32

class CPropertyView : public CDialogView<CPropertyView>  
{
public:
	enum {IDD = IDD_PROPERTYVIEW};
	
	BEGIN_MSG_MAP(CPropertyView)
		CHAIN_MSG_MAP(CDialogView<CPropertyView>)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		NOTIFY_CODE_HANDLER(PIN_ITEMCHANGED, OnXdListItemChanged);
		NOTIFY_CODE_HANDLER(PIN_SCALARCHANGED, OnScalarChanged);
		NOTIFY_CODE_HANDLER(PIN_BROWSE, OnXdFileNameChanged);
		NOTIFY_CODE_HANDLER(PIN_CLICK, OnXdClick);
		REFLECT_NOTIFICATIONS()
		END_MSG_MAP()
		
		// Handler prototypes (uncomment arguments if needed):
		//	LRESULT MessageHandler(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
		//	LRESULT CommandHandler(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
		//	LRESULT NotifyHandler(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/)
		LRESULT OnScalarChanged(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/);
		LRESULT OnXdListItemChanged(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/);
		LRESULT OnXdFileNameChanged(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/);
		LRESULT OnXdClick(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/);

		CPropertyView(CDeviceManager& DeviceManager, CMainFrame& MainFrame);
	
public:
		/// A client informs us that the current device has changed ( e.g. the user activated an other 
		/// image view associated with an other device)
		void Update_PRP(CDevice* pdevice);

private: 
		void Spec_Fun_Interface_1(CDevice*pDevice,HV_INTERFACE1_ID Interface_ID,int value);
  
		void OnPrpChanged(CDevice *pDevice,int nID, int value); 
 
		
		
protected:
		// Message Handlers
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		
		/// Invoke the error message box
		void ReportError(HVBaseException& e);
		
		/// reference to the viewer's camera manager object
		CDeviceManager          &m_dm4PrpV;
		/// reference to the application's main window
		CMainFrame              &m_refXdMf;	
		/// list view control
		CPropertyListCtrl		m_prpListctrl;
	
		// camera video mode control 
		typedef map<int, HVResolution>         Idx2Mode_t;
		Idx2Mode_t      m_Idx2Mode;
		HVResolution	m_CurrentMode;
		int				m_CurrentModeIdx;
		
		  

};

#endif // !defined(AFX_PropertyView_H__F4CED1A3_9CED_4DF5_9C91_692B10F7E930__INCLUDED_)
