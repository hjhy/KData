// aboutdlg.cpp : implementation of the CAboutDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

#include "aboutdlg.h"



LRESULT CAboutDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	// change the OK button extended style
	DWORD dw = BMPBTN_AUTOSIZE | BMPBTN_AUTO3D_SINGLE | BMPBTN_SHAREIMAGELISTS;
	b.SetBitmapButtonExtendedStyle(dw);
	
	// create the OK button's imagelist
	b.m_ImageList.Create(IDB_BUTTONS, 64, 0, RGB(192,192,192)); 
	// set normal, pressed, hover, disabled images
	b.SetImages(0, 1, 2, 3);
	b.SetToolTipText(_T("OK button tip"));
	// subclass the OK button
	b.SubclassWindow(GetDlgItem(IDC_BTN_BMP));

	CenterWindow(GetParent());
	return TRUE;
}

LRESULT CAboutDlg::OnCloseCmd(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{ 
	EndDialog(wID);
	return 0;
}
