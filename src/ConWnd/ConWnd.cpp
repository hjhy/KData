#include "stdafx.h"

#include "windows.h"
#include "TCHAR.h"
#include "stdio.h"
#include "BlAPI.h" 

//声明自定义消息处理函数
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);  
 
#define    	MY_VERSION		"V0.0.1.6" 

class CMyDll
{
public:
	CMyDll(){}
	~CMyDll(){}
	BLSTATUS OnBlCallLibFun()
	{
		BLSTATUS r = prCallLibFun();
		return r;
	}
private:
	BLSTATUS pvCallLibFun(char *szLib,char *szFun)
	{
		BLSTATUS r = BL_STATUS_NOT_SUPPORT_INTERFACE;
		HMODULE h = LoadLibrary(szLib);
		
		if(NULL != h)
		{
			FARPROC pFunc = GetProcAddress(h, szFun);
			if (pFunc ) 
			{
				char sz[155];
				sprintf(sz,"ConWnd %s",MY_VERSION);
				BL_LPARAM_CONTEXT c = {0};
				c.m					= BL_WM_INIT; 
				sprintf(c.szVerify,"BEAUTIFULLOVER");
				c.pszExtra			= sz; 
				
				r = (* (OnBLAPI *)pFunc)(NULL,BL_WM_INIT,0,(LPARAM)&c);  
				FreeLibrary (h);
			}
		}
		else
		{
			r = BL_STATUS_NOT_FOUND_LIBARY;
		}
		return r;
	}
	BLSTATUS prCallLibFun()
	{
		BLSTATUS r = BL_STATUS_NOT_SUPPORT_INTERFACE;
		r = pvCallLibFun("Win32UI.dll","OnBlApi");
		return r;
	} 
};
int _tmain(int argc, _TCHAR* argv[])
{ 
	CMyDll d;
	BLSTATUS r = d.OnBlCallLibFun(); 
 
	return 0; 
} 
 