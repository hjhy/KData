
#include "stdafx.h" 
#include "MainFrm.h"

CMainFrame::CMainFrame()  
{
}

CMainFrame::~CMainFrame()
{
}
    
LRESULT CMainFrame::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	printf("OnCreate: \n"); 
	
	// register object for message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->AddMessageFilter(this);
	pLoop->AddIdleHandler(this);
	

	SetTimer(1, 111);
	return 0;
}
 
BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{ 
//	printf("CMainFrame::PreTranslateMessage: \n");

	if(CMDIFrameWindowImpl<CMainFrame>::PreTranslateMessage(pMsg))
		return TRUE;
	
 	return FALSE;
}

BOOL CMainFrame::OnIdle()
{ 
//	printf("OnIdle: \n");
	return FALSE;
}
