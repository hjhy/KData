// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__4E614EAB_75B9_4B7A_87FB_DDFFF6B86D50__INCLUDED_)
#define AFX_MAINFRM_H__4E614EAB_75B9_4B7A_87FB_DDFFF6B86D50__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

 

class CMainFrame :  public CMDIFrameWindowImpl<CMainFrame>, 
					public CUpdateUI<CMainFrame>,
					public CMessageFilter,
					public CIdleHandler
{ 	
public:
 	DECLARE_FRAME_WND_CLASS("WTL_MAIN_WND_CLASS", IDR_MAINFRAME)
		
	CMainFrame();
	~CMainFrame();
	
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnIdle();
	
	BEGIN_UPDATE_UI_MAP(CMainFrame)  
		 
	END_UPDATE_UI_MAP()
		
	BEGIN_MSG_MAP(CMainFrame)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)		
		CHAIN_MSG_MAP(CUpdateUI<CMainFrame>)
		CHAIN_MSG_MAP(CMDIFrameWindowImpl<CMainFrame>)		
	END_MSG_MAP()
		 
	LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	  
  
private:   
	
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__4E614EAB_75B9_4B7A_87FB_DDFFF6B86D50__INCLUDED_)
