// Win32UI.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"

#include "windows.h"
#include "TCHAR.h"
#include "stdio.h"
#include "BlAPI.h" 


#include "MainFrm.h"

CAppModule _Module;


BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{ 
    return TRUE;
}
class CBlApp
{
public:
	CBlApp(){}
	~CBlApp(){}
private:
	int pvWtlRun(LPTSTR /*lpstrCmdLine*/ = NULL, int nCmdShow = SW_SHOWDEFAULT)
	{ 
		HANDLE hExclusion = NULL;   // used to detect another running instance
		hExclusion = CreateMutex(NULL, FALSE, "{8FA6E763-1394-4db0-AFC3-FA53ED373513}");
		
		if ( GetLastError() == ERROR_ALREADY_EXISTS )
		{
			// The mutex already exists, bring the running instance to top
			
			HWND hWndPrev, hWndChild;
			
			// Determine if another window with your class name exists...
			if (hWndPrev = ::FindWindow("BeautifulloverWTL", NULL))
			{
				// If so, does it have any popups?
				hWndChild = ::GetLastActivePopup(hWndPrev);
				
				// If iconic, restore the main window
				if (::IsIconic(hWndPrev))
					::ShowWindow(hWndPrev, SW_RESTORE);
				
				// Bring the main window or its popup to
				// the foreground
				::SetForegroundWindow(hWndChild);
				
				// now the previous instance is activated 
				return 0;
			}
		}
		
		CMessageLoop theLoop;
		_Module.AddMessageLoop(&theLoop);
		
		CMainFrame xdWndMain;
		
		if(xdWndMain.CreateEx() == NULL)
		{
			printf(_T("Main window creation failed!\n"));
			return 0;
		}
		
		xdWndMain.ShowWindow(nCmdShow | SW_SHOWNORMAL);
		
		int nRet = theLoop.Run();
		
		_Module.RemoveMessageLoop();
		if ( hExclusion != NULL )
			CloseHandle(hExclusion);
		
		return nRet;
	}
	
	void w32Run(HINSTANCE hInstance)
	{ 
		WNDCLASS Render_WND;
		Render_WND.cbClsExtra = 0;
		Render_WND.cbWndExtra = 0;
		Render_WND.hCursor = LoadCursor(hInstance, IDC_ARROW); //鼠标风格
		Render_WND.hIcon = LoadIcon(hInstance, IDI_APPLICATION); //图标风格
		Render_WND.lpszMenuName = NULL; //菜单名
		Render_WND.style = CS_HREDRAW | CS_VREDRAW; //窗口的风格
		Render_WND.hbrBackground = (HBRUSH)COLOR_WINDOW; //背景色
		Render_WND.lpfnWndProc = WindowProc; //【关键】采用自定义消息处理函数，也可以用默认的DefWindowProc
		Render_WND.lpszClassName = _T("Win32Window"); //【关键】该窗口类的名称
		Render_WND.hInstance = hInstance; //【关键】表示创建该窗口的程序的运行实体代号
		
		RegisterClass(&Render_WND);
		
		HWND hwnd = CreateWindow(  
			_T("Win32Window"),  //【关键】上面注册的类名lpszClassName，要完全一致  
			"Win32UI",  //窗口标题文字  
			WS_OVERLAPPEDWINDOW, //窗口外观样式  
			0,             //窗口相对于父级的X坐标  
			0,             //窗口相对于父级的Y坐标  
			320,                //窗口的宽度   
			240,                //窗口的高度  
			NULL,               //没有父窗口，为NULL  
			NULL,               //没有菜单，为NULL  
			hInstance,          //当前应用程序的实例句柄  
			NULL);              //没有附加数据，为NULL  
		
		// 显示窗口  
		ShowWindow(hwnd, SW_SHOW);  
		
		// 更新窗口  
		UpdateWindow(hwnd);  
		
		// 消息循环  
		MSG msg;  
		while(GetMessage(&msg, NULL, 0, 0))  
		{  
			TranslateMessage(&msg);  
			DispatchMessage(&msg);  
		} 
		
	}
	
	static LRESULT CALLBACK WindowProc(HWND hW, UINT uM,WPARAM wP,LPARAM lP)
	{ 
		switch(uM)
		{
		case WM_LBUTTONDOWN:
			{
				printf("uM = 0x%x\n",uM); 
				return 0;
			}
		case WM_DESTROY:
			{
				printf("uM = 0x%x\n",uM);
				PostQuitMessage(0);
				return 0;
			}
		}
		return DefWindowProc(hW, uM, wP, lP);
	} 

public:		
	void _wtlMain()
	{ 	
		printf(_T("wtlMain: \n"));
		HRESULT hRes = ::CoInitialize(NULL); 
		ATLASSERT(SUCCEEDED(hRes)); 
		::DefWindowProc(NULL, 0, 0, 0L);
		
		AtlInitCommonControls(ICC_COOL_CLASSES | ICC_BAR_CLASSES); 
		HINSTANCE hInstance = GetModuleHandle(NULL); 
		hRes = _Module.Init(NULL, hInstance);
		ATLASSERT(SUCCEEDED(hRes));
		
		int nRet = pvWtlRun(NULL, NULL);
		
		_Module.Term();
		::CoUninitialize();
	}
		
		
};
extern "C" BLSTATUS __stdcall OnBlApi(HWND hWnd,UINT uM, WPARAM wP, LPARAM lP)
{	
	BLSTATUS	r = BL_STATUS_NOT_SUPPORT_INTERFACE;  
  	
	HINSTANCE h = GetModuleHandle(NULL); 
	CBlApp a;
//	a.w32Run(h); 
	a._wtlMain();

	return r;
}
