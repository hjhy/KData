// Get_Data.cpp: implementation of the CGet_Data class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
#include "Get_Data.h"

#include "SAStatusLog.h"

#include "wininet.h"
#pragma comment(lib,"wininet.lib")
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGet_Data::CGet_Data()
{ 
	m_lpszBufRawHtml = NULL;
}

CGet_Data::~CGet_Data()
{ 
	if(NULL != m_lpszBufRawHtml)
	{
			delete []m_lpszBufRawHtml;
			m_lpszBufRawHtml = NULL;
	}
}
//#include "Draw.h"

void CGet_Data::fn_From_File()
{
	FILE *fp;
	if((fp=fopen("ball.txt","r"))==NULL)  // 判断文件是否打开
	{
		AfxMessageBox("can't open this file");
		return  ;
	}

	
	for (int j=0;j<nLine;j++)
	{	
		fscanf(fp,"%d-%d: %d-%d-%d-%d-%d-%d %d",
			&p_Ball[j][0],&p_Ball[j][1],&p_Ball[j][2],
			&p_Ball[j][3],&p_Ball[j][4],&p_Ball[j][5],
			&p_Ball[j][6],&p_Ball[j][7],&p_Ball[j][8]);

		TRACE("p_ball[%d][0]=%d",j,p_Ball[j][0]);
		TRACE("p_ball[%d][1]=%d",j,p_Ball[j][1]);
		TRACE("p_ball[%d][2]=%d",j,p_Ball[j][2]);
		TRACE("p_ball[%d][3]=%d",j,p_Ball[j][3]);
		TRACE("p_ball[%d][4]=%d",j,p_Ball[j][4]);
		TRACE("p_ball[%d][5]=%d",j,p_Ball[j][5]);
		TRACE("p_ball[%d][6]=%d",j,p_Ball[j][6]);
		TRACE("p_ball[%d][7]=%d",j,p_Ball[j][7]);
		TRACE("p_ball[%d][8]=%d",j,p_Ball[j][8]);
	}
	rewind(fp);
	fclose(fp);
	p_Ball = &p_Ball[0];

/*	delete []p_Ball;
	
	return (head);
	*/

}

bool CGet_Data::fn_From_URL(CString str)
{
	m_lpszBufRawHtml=NULL;
	bool bRet = TRUE;
	CString strUrl = str;
	//判断地址是否有效,如为空或开头7个字符不是“http://"
	if(strUrl.IsEmpty()||strUrl.Left(7)!="http://")
	{
		AfxMessageBox("Sorry,It is a invalid address!");
		bRet = FALSE;
		goto EXIT;
	}
	//解析地址，得到server名字和文件路径
	fn_ParseURL(strUrl);
	//第一步:初始化internet DLL,这是第一个被调用的函数
	HINTERNET hSession;
	hSession = ::InternetOpen("Raw HTML Reader",PRE_CONFIG_INTERNET_ACCESS,
		                              "",INTERNET_INVALID_PORT_NUMBER,0);

	//判断会话句柄是否有效
	if(hSession==NULL)
	{
		AfxMessageBox("Internet session initalization failed!");
		bRet = FALSE;
		goto EXIT;
	}
	//第二步：初始化HTTP session
	HINTERNET	hConnect;
	hConnect = ::InternetConnect(hSession,//当前internet会话句柄
		                                   m_strServer,//server name
										   INTERNET_INVALID_PORT_NUMBER,
										   NULL,//"",//user name
										   "",//password
										   INTERNET_SERVICE_HTTP,//Type of service to access
										   0,
										   0);
	//判断连接句柄是否有效
	if(hConnect==NULL)
	{
		AfxMessageBox("Internet connect initalization failed!");
		//关闭会话句柄
		VERIFY(::InternetCloseHandle(hSession));
		bRet = FALSE;
		goto EXIT;
	}
    
	//第三步：打开一个HTTP请求句柄
	HINTERNET hHttpFile;
	hHttpFile = ::HttpOpenRequest(hConnect,
										  "GET",
										  m_strPath,
										  HTTP_VERSION,
										  NULL,
										  0,
										  INTERNET_FLAG_DONT_CACHE,
										  0);
	//判断连接句柄是否有效
	//判断会话句柄是否有效
	if(hHttpFile==NULL)
	{
		AfxMessageBox("Http request failed!");
		VERIFY(::InternetCloseHandle(hConnect));
		VERIFY(::InternetCloseHandle(hSession));
		bRet = FALSE;
		goto EXIT;
	}
 
 

	//第四步：发出请求
	BOOL bSendRequest;
	bSendRequest = ::HttpSendRequest(hHttpFile,
									   NULL,
									   0,
									   0,
									   0);
	if(bSendRequest)
	{
		//得到文件的大小
		char achQueryBuf[16];
		DWORD dwFileSize;
		DWORD dwQueryBufLen=sizeof(achQueryBuf);
		BOOL bQuery=::HttpQueryInfo(hHttpFile,
			                        HTTP_QUERY_CONTENT_LENGTH,
									achQueryBuf,
									&dwQueryBufLen,
									NULL);
		if(bQuery)
		{
			//查找成功，指出需要存放文件的内存大小???????
            dwFileSize=(DWORD)atol(achQueryBuf);
		}
		else
		{
			//失败，猜出一个最大文件数
			return false;
			dwFileSize=10*1024;
		}

		//分配一个缓冲区给文件数据
	//	 m_lpszBufRawHtml=new char[dwFileSize+1];
		fn_MyNewBuf(dwFileSize);
		//读文件
		DWORD dwBytesRead;
		BOOL bRead = ::InternetReadFile(hHttpFile,
			                          m_lpszBufRawHtml,
									  dwFileSize+1,
									  &dwBytesRead);

	 	// 关闭INTERNET句柄
		VERIFY(::InternetCloseHandle(hHttpFile));
		VERIFY(::InternetCloseHandle(hConnect));
		VERIFY(::InternetCloseHandle(hSession));
	}
	else{
		bRet = FALSE; //网络不通
	}

EXIT:
	return bRet;
}

void CGet_Data::fn_ParseURL(CString &strUrl)
{
		if(strUrl.IsEmpty())
	{
		return;
	}
	//去掉"http://"
	CString strTemp=strUrl.Mid(7);
	
	//检查主机的路径
	int nSlash=strTemp.Find("/");
	if (nSlash!=-1)  //如等于-1，就是没找到
	{
		m_strServer=strTemp.Left(nSlash);//取‘/’左边的服务器地址
        m_strPath=strTemp.Mid(nSlash);
	}
	else
		m_strServer=strTemp;


}

void CGet_Data::fn_MyNewBuf(DWORD dwSize)
{
	if(NULL != m_lpszBufRawHtml)
	{
			delete []m_lpszBufRawHtml;
			m_lpszBufRawHtml = NULL;
	}
	m_lpszBufRawHtml=new char[dwSize+1];
	memset(m_lpszBufRawHtml,0,dwSize+1);
}

bool CGet_Data::fn_Parse_RB_Data()
{
	bool bRet = false;
	int nLen = strlen(m_lpszBufRawHtml);
	if(nLen==0) {
		AfxMessageBox("m_lpszBufRawHtml,数据长度为 [0] !!!");
		return bRet;
	}
	int i = 0;
	char szAWord[1280];
	memset(szAWord,0,1280);
	int nIdxWord = 0;
	int nFind = 0;
	char *p = m_lpszBufRawHtml;
	if(i<nLen)
	{
		// Find : "<td "
		CString strShow;
		strShow.Format("%s",m_lpszBufRawHtml);
		AfxMessageBox(strShow);
	 
	}
	return bRet;	
}

void CGet_Data::fn_Update_RB_Date_2_File()
{
	CString strFileName = fn_GetRBLogFileName(); 
	char szFileName[1024];
	strcpy(szFileName,strFileName.GetBuffer(strFileName.GetLength()));
	FILE *pFile;
	if(!(pFile= fopen(szFileName,"r")))
	{
		AfxMessageBox("Cannot Open strFileName");
		return ;
	}

	//---Geg first line of data---
	int y,n,r[6],b;
	int nRet = fscanf(pFile,"%d-%d: %d-%d-%d-%d-%d-%d %d", &y,&n,
		&r[0],&r[1],&r[2],&r[3],&r[4],&r[5],&b);
	if(-1==nRet) {
		AfxMessageBox("数据文件中,数据有错!!!");
		fclose(pFile); 
		return;
	}
	fclose(pFile); 
	//===Geg first line of data===

	int nNew = n + 1;
	CString strUrl;
	strUrl.Format("http://www.bwlc.gov.cn/search/num_ss.asp?qitime=%d-%03d",y,nNew);
	bool bRet = fn_From_URL(strUrl); //Get new data
	if(bRet) 
	{		
		fn_From_File();//backup all old data to m_Idx2RBDate;
		CString str = fn_GetRBLogFileName();
		fn_Save_RBData_2_File(str,0);//save new data to file;
		fn_Save_OldData_2_File();//save all backup data to file;

		CString s = RBData2Str(); 
		AfxMessageBox(s);
		fn_From_File();
	}
	else
	{
		y++; //新的一年
		nNew = 1;
		strUrl.Format("http://www.bwlc.gov.cn/search/num_ss.asp?qitime=%d-%03d",y,nNew);
		bRet = fn_From_URL(strUrl); //Get new data
		if(bRet) 
		{		
			fn_From_File();//backup all old data to m_Idx2RBDate;
			CString str = fn_GetRBLogFileName();
			fn_Save_RBData_2_File(str,0);//save new data to file;
			fn_Save_OldData_2_File();//save all backup data to file;
			
			CString s = RBData2Str(); 
			AfxMessageBox(s);
			fn_From_File();
		}
		else{
			CString s;
			s.Format("%d-%03d , 无法更新数据!\n请确认网络是否通畅!",y,nNew);
			AfxMessageBox(s);
		}
	}

}

CString CGet_Data::fn_GetRBLogFileName()
{
	char szFileNames[260]; 
	
	char szCurDir[100]; 
	DWORD dwLen = GetCurrentDirectory(MAX_PATH,szCurDir);
	dwLen = GetModuleFileName(NULL, szFileNames, sizeof(szFileNames));
	for(DWORD offset=dwLen; offset>=0; offset--)
	{
		if(szFileNames[offset] == '\\')
		{
			szFileNames[offset] = 0;
			break;
		}
	}
	CString str;
	str.Format("%s\\RBLog.txt",szFileNames);
	return str;
}

bool CGet_Data::fn_Save_RBData_2_File(CString strFileName, int nMode)
{
		bool bRet = false;
	CString strLog;
	strLog.Format("%d-%03d:     %02d-%02d-%02d-%02d-%02d-%02d     %02d\n",m_RBData.nYear,m_RBData.nQitime,
		m_RBData.r[0],m_RBData.r[1],m_RBData.r[2],m_RBData.r[3],m_RBData.r[4],m_RBData.r[5],
		m_RBData.b);
	TRACE(strLog); 

	CString strRBLogFile = strFileName; 
	
	char szRBFileName[1024];
	strcpy(szRBFileName,strRBLogFile.GetBuffer(strRBLogFile.GetLength()));
	FILE *pFile;
	if(nMode==0){
		if(!(pFile= fopen(szRBFileName,"w")))
		{
			AfxMessageBox("Cannot Open strFileName");
			return bRet;
		}
	}
	else if(nMode==1){
		if(!(pFile= fopen(szRBFileName,"a")))
		{
			AfxMessageBox("Cannot Open strFileName");
			return bRet;
		}
	}
	else{
		AfxMessageBox("函数调用参数不对!");
		return bRet;
	}
 
    fprintf(pFile,"%s", strLog.GetBuffer(strLog.GetLength()));

	fclose(pFile); 
	
	bRet = true;
	return bRet;
}

bool CGet_Data::fn_Save_OldData_2_File()
{
	bool bRet = false;
	CString strRBLogFile = fn_GetRBLogFileName(); 
	
	char szFileName[1024];
	strcpy(szFileName,strRBLogFile.GetBuffer(strRBLogFile.GetLength()));
	FILE *pFile;
	if(!(pFile= fopen(szFileName,"a")))
	{
		AfxMessageBox("Cannot Open strFileName");
		return bRet;
	}

	//Save_OldData_2_File?

	fclose(pFile); 
	
	bRet = true;
	return bRet;
}

CString CGet_Data::RBData2Str()
{
		CString str;
	str.Format("%d-%03d:     %02d-%02d-%02d-%02d-%02d-%02d     %02d\n",m_RBData.nYear,m_RBData.nQitime,
		m_RBData.r[0],m_RBData.r[1],m_RBData.r[2],m_RBData.r[3],m_RBData.r[4],m_RBData.r[5],
		m_RBData.b);
	return str;
}

int CGet_Data::fn_Get_RB_Num()
{
	FILE *fp;
	if((fp=fopen("ball.txt","r"))==NULL)  // 判断文件是否打开
	{
		AfxMessageBox("can't open this file");
		return 0;
	}
	int istemp[9]={0};
	int n=0;
	
	TRACE("fp0=%d",ftell(fp));
	while (!feof(fp))
	{
		n++;
		fscanf(fp,"%d-%d: %d-%d-%d-%d-%d-%d %d",&istemp[0],
			&istemp[1],&istemp[2],&istemp[3],&istemp[4],
			&istemp[5],&istemp[6],&istemp[7],&istemp[8]);

	}
	
	fclose(fp);
	TRACE("n=%d",n);
	return n;
}

 

void CGet_Data::fn_Write_to_File(CString filename, CString str)
{
	FILE *fp;
	if((fp=fopen(filename,"w"))==NULL)
	{
		AfxMessageBox("fail to write");
	}
	fprintf(fp,str);
	fclose(fp);
}

void CGet_Data::fn_CopyFile1_to_File2(CString file1, CString open_method1, CString file2, CString open_method2)
{
		FILE *fp1,*fp2;
		if((fp1=fopen(file1,open_method1)) == NULL)
		{
			AfxMessageBox("can't open this file");
		}
		if((fp2= fopen(file2,open_method2)) == NULL)
		{
			AfxMessageBox("can't open that file");
		}
		while(!feof(fp1)) 
		{
			char c = fgetc(fp1);
			if(-1==c) continue;
			fputc(c,fp2);
		}
		fclose(fp1);
		fclose(fp2);
}


RB_DATA CGet_Data::fn_Get_First_Line()
{
	RB_DATA rbRet;
	memset(&rbRet,0,sizeof(RB_DATA));
	FILE *fp;
	bool b=TRUE;
	if((fp=fopen("ball.txt","r"))==NULL)  // 判断文件是否打开
	{
		AfxMessageBox("can't open this file");
		return rbRet;
	}

		fscanf(fp,"%d-%d: %d-%d-%d-%d-%d-%d %d",&rbRet.nYear,
			&rbRet.nQitime,&rbRet.r[0],&rbRet.r[1],&rbRet.r[2],
			&rbRet.r[3],&rbRet.r[4],&rbRet.r[5],&b);


	
    rewind(fp);
	fclose(fp);

	return rbRet;
}


CString CGet_Data::GetData()
{
		CString strRet;
		strRet.Format("%s",m_lpszBufRawHtml);
		return strRet;	
}
UINT CGet_Data::v_Thread_Fun_FineAll(LPVOID pParam)
{
	UINT uiRet = 0;
	CGet_Data *pMe = (CGet_Data*) pParam;
	pMe->vv_Fun_Fine_All();
	return uiRet;
}
void CGet_Data::FineAll()
{
	
	AfxBeginThread(v_Thread_Fun_FineAll,this,THREAD_PRIORITY_NORMAL); 

}

void CGet_Data::vv_Fun_Fine_All()
{
	int n=1;
	CString strName;
	CSAStatusLog xdLog;
	xdLog.Init("xdLog.txt");
	CSAStatusLog xdFile;
	xdFile.Init("File.txt");
	CString strAll="";
	while(n<1000)
	{

		CString strN;
		strN.Format("%d",n);
		CString str  = "http://api.baidao.com/api/hq/npdata.do?ids="+strN;
		
		BOOL b = fn_From_URL(str); 
		if(b)
		{
			char s1[256];
			CString strHtml = GetData();
			
			CString strRight;
			int iIdx = strHtml.Find("nickname");
			strRight=strHtml.Right(strHtml.GetLength()-iIdx);  
			if(iIdx>1)
			{
				sscanf(strRight.GetBuffer(strRight.GetLength()),"nickname\":\"%s",s1); 
				strRight =s1;
				strName = strRight.Left(strRight.Find("\""));
				CString ss;
				ss.Format("%03d	%s\n",n,strName.GetBuffer(strName.GetLength()));
				strAll+=ss;
				xdFile.StatusOut("%03d	%s\n",n,strName.GetBuffer(strName.GetLength()));
			}
			else{
				strName = "---";
			}
		}
		else
		{
			strName = "---";			
		}
		xdLog.StatusOut("%03d	%s\n",n,strName.GetBuffer(strName.GetLength()));

		n++;
	} 
	AfxMessageBox(strAll);

}
