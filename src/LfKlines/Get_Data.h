// Get_Data.h: interface for the CGet_Data class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GET_DATA_H__B830F3D8_89D4_4EF3_8321_EA0C0A2183EE__INCLUDED_)
#define AFX_GET_DATA_H__B830F3D8_89D4_4EF3_8321_EA0C0A2183EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
 struct RB_DATA {
		int		nYear;
		int		nQitime;
		int		r[6];
		int		b;
		struct RB_DATA *next;
};
 
typedef struct  {
		long	lDate;
		int		h;
		int		m;
		int		s; 
}UPDATATIME,*PUPDATATIME;
 
class CGet_Data  
{
public: 
	void FineAll();
	CString GetData();
	RB_DATA fn_Get_First_Line();
	void fn_CopyFile1_to_File2(CString file1,CString open_method1,CString file2,CString open_method2);
	void fn_Write_to_File(CString filename,CString str);
	int fn_Get_RB_Num();
	CString RBData2Str();
	bool fn_Save_OldData_2_File();
	bool fn_Save_RBData_2_File(CString strFileName,int nMode);
	CString fn_GetRBLogFileName();
	void fn_Update_RB_Date_2_File();
	bool fn_Parse_RB_Data();
	void fn_MyNewBuf(DWORD dwSize);
	void fn_ParseURL(CString &strUrl);
	bool fn_From_URL(CString str);
	void fn_From_File();
	CGet_Data();
	virtual		~CGet_Data();
	RB_DATA		m_RBData;
	char		*m_lpszBufRawHtml;
	
	CString		m_strServer;
	CString		m_strPath;
	int			nLine;
	int			(*p_Ball)[9];
private:
	void vv_Fun_Fine_All();
	static UINT  v_Thread_Fun_FineAll(LPVOID pParam);
};

#endif // !defined(AFX_GET_DATA_H__B830F3D8_89D4_4EF3_8321_EA0C0A2183EE__INCLUDED_)
