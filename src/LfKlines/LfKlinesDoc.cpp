// LfKlinesDoc.cpp : implementation of the CLfKlinesDoc class
//

#include "stdafx.h"
#include "LfKlines.h"

#include "LfKlinesDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLfKlinesDoc

IMPLEMENT_DYNCREATE(CLfKlinesDoc, CDocument)

BEGIN_MESSAGE_MAP(CLfKlinesDoc, CDocument)
	//{{AFX_MSG_MAP(CLfKlinesDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLfKlinesDoc construction/destruction

CLfKlinesDoc::CLfKlinesDoc()
{
	// TODO: add one-time construction code here

}

CLfKlinesDoc::~CLfKlinesDoc()
{
}

BOOL CLfKlinesDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CLfKlinesDoc serialization

void CLfKlinesDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CLfKlinesDoc diagnostics

#ifdef _DEBUG
void CLfKlinesDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLfKlinesDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLfKlinesDoc commands
