// LfKlinesDoc.h : interface of the CLfKlinesDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_LFKLINESDOC_H__362EC87C_C9BF_4211_99C4_8B121FF6DA8A__INCLUDED_)
#define AFX_LFKLINESDOC_H__362EC87C_C9BF_4211_99C4_8B121FF6DA8A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CLfKlinesDoc : public CDocument
{
protected: // create from serialization only
	CLfKlinesDoc();
	DECLARE_DYNCREATE(CLfKlinesDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLfKlinesDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLfKlinesDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLfKlinesDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LFKLINESDOC_H__362EC87C_C9BF_4211_99C4_8B121FF6DA8A__INCLUDED_)
