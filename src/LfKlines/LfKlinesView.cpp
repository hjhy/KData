// LfKlinesView.cpp : implementation of the CLfKlinesView class
//

#include "stdafx.h"
#include "LfKlines.h"

#include "LfKlinesDoc.h"
#include "LfKlinesView.h"
#include "XdKline.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLfKlinesView

IMPLEMENT_DYNCREATE(CLfKlinesView, CView)

BEGIN_MESSAGE_MAP(CLfKlinesView, CView)
	//{{AFX_MSG_MAP(CLfKlinesView)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_CREATE()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLfKlinesView construction/destruction

CLfKlinesView::CLfKlinesView()
{
	// TODO: add construction code here
	
	m_pMemDC=new CDC();
	m_pBitmap=new CBitmap();
	m_fY0 = -1;
	m_fY1 = -1;
}

CLfKlinesView::~CLfKlinesView()
{
	
	delete m_pMemDC;
	delete m_pBitmap;
}
UINT CLfKlinesView::_xdThreadFun_XaGUSD(LPVOID pParam)
{
	UINT uReturn = 0;
	
	CLfKlinesView* p = (CLfKlinesView*) pParam; 
	
	static	CXdKLine x0;

	int n = 0;
	while(1)
	{
		n++;
		CXdKLine x;
		BOOL bRet = x.GetDataFromHTML(); 
		CString s;
		BOOL b = x.GetUpdateTime(s);
		if(bRet&&b)
		{
			p->_xd_Add_2_1M(x);
			p->DrawMemDC(x);
			p->Invalidate();
		}
		Sleep(1);
	}
//	_xdThreadFun_XaGUSD(p);
	return uReturn;
}
BOOL CLfKlinesView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CLfKlinesView drawing

void CLfKlinesView::OnDraw(CDC* pDC)
{
	CLfKlinesDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	  
 
	CRect rect;
	GetClientRect(rect);
	pDC->BitBlt(0,0,rect.Width(),rect.Height(),m_pMemDC,0,0,SRCCOPY);
 

}

/////////////////////////////////////////////////////////////////////////////
// CLfKlinesView printing

BOOL CLfKlinesView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CLfKlinesView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CLfKlinesView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CLfKlinesView diagnostics

#ifdef _DEBUG
void CLfKlinesView::AssertValid() const 
{
	CView::AssertValid();
}

void CLfKlinesView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CLfKlinesDoc* CLfKlinesView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLfKlinesDoc)));
	return (CLfKlinesDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLfKlinesView message handlers

void CLfKlinesView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	static int n = 0;
	n++;
	XDDATA x;
	GetClientRect(&x.r);
	int x1 = rand()%x.r.Width();
	int y1 = rand()%x.r.Height();
	int x2 = rand()%x.r.Width();
	int y2 = rand()%x.r.Height();
	x.r.left = x1;
	x.r.top = y1;
	x.r.right = x2;
	x.r.bottom = y2;
	x.rgb = RGB(rand()%255,rand()%255,rand()%255);
	//	p->m_rV.push_back(x);
	
	CBrush brush; 
	brush.CreateSolidBrush(x.rgb);
	CBrush* pOldBrush = m_pMemDC->SelectObject(&brush);
	m_pMemDC->Ellipse(x.r);
	m_pMemDC->SelectObject(pOldBrush);
	CString s;
	s.Format("%d",n);
	m_pMemDC->TextOut(100,100,s,s.GetLength());
	
	this->Invalidate(TRUE);
	
	CView::OnTimer(nIDEvent);
}

void CLfKlinesView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
//	SetTimer(1,1,NULL);
	
	AfxBeginThread(_xdThreadFun_XaGUSD,this,THREAD_PRIORITY_NORMAL); 
}

void CLfKlinesView::OnDestroy() 
{
	CView::OnDestroy();
	
	// TODO: Add your message handler code here
	
	KillTimer(1);
}

int CLfKlinesView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	srand((unsigned)time(NULL));  
	//创建内存DC和位图
	int maxX=GetSystemMetrics(SM_CXSCREEN);
    int maxY=GetSystemMetrics(SM_CYSCREEN);
   
	CDC* pDC=GetDC();
	pDC->SetMapMode(MM_LOMETRIC);          //设置映射模式
	pDC->SetViewportOrg(250, 250);        //设置笛卡尔坐标系之坐标原点
 

	m_pMemDC->CreateCompatibleDC(pDC);
	m_pBitmap->CreateCompatibleBitmap(pDC,maxX,maxY);
	m_pMemDC->SelectObject(m_pBitmap);
	
	//初始化内存DC为全白
	CBrush brush;
	brush.CreateStockObject(WHITE_BRUSH);
	CBrush* poldbrush=m_pMemDC->SelectObject(&brush);
	m_pMemDC->PatBlt(0,0,maxX,maxY,PATCOPY);
	m_pMemDC->SelectObject(poldbrush);

	ReleaseDC(pDC);
	

	return 0;
}

BOOL CLfKlinesView::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	return 1;
//	return CView::OnEraseBkgnd(pDC);
}

void CLfKlinesView::DrawMemDC(CXdKLine x)
{
	static int n=0;n++;
	m_DQNew.push_back(x);
	if(26<m_DQNew.size())
	{
		m_DQNew.pop_front();
	}

	CString s;
	s.Format("%s",x.m_strHTML);
	m_pMemDC->TextOut(1,550,s,s.GetLength());

	XDKLineDEQUE::iterator pdeque;   
	 int i = 0;
	for( pdeque = m_DQNew.begin(); pdeque != m_DQNew.end(); i++,pdeque++)
	{
		s.Format(" %.0f %d ",(*pdeque).GetSell(),i);
		CString strT;
		(*pdeque).GetUpdateTime(strT);
		s = strT + s;
		m_pMemDC->TextOut(444,120+i*15,s,s.GetLength());
	}	
}

void CLfKlinesView::_xd_Add_2_1M(CXdKLine x)
{
	static int m;
	XDKLineDEQUE &d = m_DQ_1M;
	
	m_fY0 = x.GetHigh();
	m_fY1 = x.GetLow();
	if(m!=x.m_udt.m)
	{
		m = x.m_udt.m;
		x.SetOpen(x.GetSell());
		x.SetHigh(x.GetSell());
		x.SetLow(x.GetSell());
		x.SetClose(x.GetSell());
		d.push_back(x);
	}
	else
	{		
		XDKLineDEQUE::iterator pdeque; 
		pdeque = d.end();
		pdeque--;
		CXdKLine l = *pdeque; 
		d.pop_back();
		
		x.SetOpen(l.GetOpen());
		x.SetClose(x.GetSell());

		if(x.GetSell()>l.GetHigh()) 
			x.SetHigh(x.GetSell());
		else
			x.SetHigh(l.GetHigh());

		if(x.GetSell()<l.GetLow())
			x.SetLow(x.GetSell()); 
		else
			x.SetLow(l.GetLow()); 
		d.push_back(x);
	}	
	__xd_Draw_DQ_1M(25);
}

void CLfKlinesView::__xd_Draw_DQ_1M(int iLastNum)
{
	XDKLineDEQUE &d = m_DQ_1M;
	CRect r1m(10,300,400,500);
	m_pMemDC->Rectangle(r1m);
		
	float fY0 = m_fY0;
	float fY1 = m_fY1;
	 
	XDKLineDEQUE::iterator p;  
	int i = 0;
 
	p = d.begin();
	CXdKLine x = *p;
	float fOpen = x.GetOpen();
	float fHigh = x.GetHigh(); 
	
	float fPreClose = x.GetPreClose();

	int iDX = 15;
	int iPreclose = r1m.top + r1m.Height()*(fPreClose - fY0)/(fY1-fY0);
	m_pMemDC->MoveTo(r1m.left,iPreclose); 
	m_pMemDC->LineTo(r1m.right,iPreclose);  
	if(d.size()>iLastNum)
	{
		p = d.begin();
		p = p + (d.size()-iLastNum);
	}
	for( ; p != d.end(); i++,p++)
	{ 
		x = *p;
		float fOpen = x.GetOpen();
		int iOpen =  r1m.top + r1m.Height()*(fOpen - fY0)/(fY1-fY0);
		float fClose = x.GetClose();
		int iClose =  r1m.top + r1m.Height()*(fClose - fY0)/(fY1-fY0);
		
		HBRUSH hBrush = x.GetBrush() ;
		HBRUSH hOldBrush = (HBRUSH)m_pMemDC->SelectObject(hBrush);
		HPEN hPen = x.GetPen() ;
		HPEN hOldPen = (HPEN)m_pMemDC->SelectObject(hPen);
		
		if(iOpen==iClose) 
		{
			CPen p;
			p.CreatePen(PS_SOLID,1,RGB(255,255,0));
			hOldPen = (HPEN)m_pMemDC->SelectObject(hPen);
			m_pMemDC->MoveTo(r1m.left+iDX*(i+1),iOpen); 
			m_pMemDC->LineTo(r1m.left + iDX*(i+.5),iOpen); 
			m_pMemDC->SelectObject(hOldPen);
		}
		//K-Body
		m_pMemDC->Rectangle(r1m.left+iDX*(i+1),iOpen,r1m.left + iDX*(i+.5),iClose);		
		
		float fHight = x.GetHigh();
		float fLow = x.GetLow();
		int iHight = r1m.top + r1m.Height()*(fHight - fY0)/(fY1-fY0);
		int iLow = r1m.top + r1m.Height()*(fLow - fY0)/(fY1-fY0);
		m_pMemDC->MoveTo(r1m.left+iDX*(i+.75),iHight);
		m_pMemDC->LineTo(r1m.left+iDX*(i+.75),iLow); 

		m_pMemDC->SelectObject(hOldPen);
		DeleteObject (hPen) ; 
		m_pMemDC->SelectObject(hOldBrush);
		DeleteObject (hBrush) ;  
	}
}
