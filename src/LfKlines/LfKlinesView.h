// LfKlinesView.h : interface of the CLfKlinesView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_LFKLINESVIEW_H__C5A0DFD9_0D0E_495E_BD29_32F0EAFE4598__INCLUDED_)
#define AFX_LFKLINESVIEW_H__C5A0DFD9_0D0E_495E_BD29_32F0EAFE4598__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "XdKline.h"
#include <vector>
#include <deque>  

using namespace std;

typedef struct tagXDDATA {
    CRect			r;
	COLORREF		rgb;
} XDDATA, *PXDDATA;

typedef deque<int> INTDEQUE;  
typedef deque<CXdKLine> XDKLineDEQUE;  

class CLfKlinesView : public CView
{
protected: // create from serialization only
	XDKLineDEQUE	m_DQNew;  
	XDKLineDEQUE	m_DQ_1M; //1分钟周期数据
	float			m_fY0;
	float			m_fY1;
	CLfKlinesView();
	DECLARE_DYNCREATE(CLfKlinesView)

	static UINT  _xdThreadFun_XaGUSD(LPVOID pParam);

	CDC			* m_pMemDC;
	CBitmap		* m_pBitmap;
// Attributes
public:
	CLfKlinesDoc* GetDocument();

//    vector<XDDATA> m_rV; 

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLfKlinesView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLfKlinesView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLfKlinesView)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void __xd_Draw_DQ_1M(int iLastNum);//画后1分周期后 iLastNum 个数据
	void _xd_Add_2_1M(CXdKLine x);
	void DrawMemDC(CXdKLine x);
};

#ifndef _DEBUG  // debug version in LfKlinesView.cpp
inline CLfKlinesDoc* CLfKlinesView::GetDocument()
   { return (CLfKlinesDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LFKLINESVIEW_H__C5A0DFD9_0D0E_495E_BD29_32F0EAFE4598__INCLUDED_)
