LfKlines 

--2015.2.3 Tue 18:28--hyl--AnHui-1-16-10-602#--V1.0.0.3--
1. 

--2015.1.28 Wed 10:06--hyl--AnHui-1-16-10-602#--V1.0.0.2--
3. R:
这几天使用Highcharts制作系统报表，经过摸索总结下使用方法：
·1去官网下载http://www.highcharts.com/，和jquery  http://www.jquery.com/download
2.在页面引人js文件
<script type="text/javascript"src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/highcharts.js"></script>
注意引人的顺序，先jquery的，否则页面不显示。
<script type="text/javascript" src="js/grid.js"></script>
 这个是主题样式文件，可选的
3.<script type="text/javascript">
var chart1; // 全局变量
$(document).ready(function() {
chart1 = new Highcharts.Chart({
chart : {
renderTo : 'container',注意这里的名称和body里面的div的id要对应
type : 'bar'
},
title : {
text : 'Fruit Consumption'
},
xAxis : {
categories : [ 'Apples', 'Bananas', 'Oranges' ]
},
yAxis : {
title : {
text : 'Fruit eaten'
}
},
series : [ {
name : 'Jane',
data : [ 1, 2, 4 ]
}, {
name : 'John',
data : [ 5, 7, 3 ]
} ]
});
});
</script>
<body>
    <div id="container" style="width: 100%; height: 400px"></div>
</body>
第二个例子：
 
<script type="text/javascript">
var chart; 
$(function() { 
    chart = new Highcharts.Chart({ 
        chart: { 
            renderTo: 'chart_line', //图表放置的容器，DIV 
            defaultSeriesType: 'line', //图表类型line(折线图), 
            zoomType: 'x'   //x轴方向可以缩放 
        }, 
        credits: { 
            enabled: false   //右下角不显示LOGO 
        }, 
        title: { 
            text: '一楼湿度历史记录' //图表标题 
        }, 
        subtitle: { 
            text: '2012年'  //副标题 
        }, 
        xAxis: {  //x轴 
            categories: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', 
 '11月', '12月'], //x轴标签名称 
            gridLineWidth: 1, //设置网格宽度为1 
            lineWidth: 2,  //基线宽度 
            labels:{y:26}  //x轴标签位置：距X轴下方26像素 
        }, 
        yAxis: {  //y轴 
            title: {text: '平均气温(°C)'}, //标题 
            lineWidth: 2 //基线宽度 
        }, 
        plotOptions:{ //设置数据点 
            line:{ 
                dataLabels:{ 
                    enabled:true  //在数据点上显示对应的数据值 
                }, 
                enableMouseTracking: false //取消鼠标滑向触发提示框 
            } 
        }, 
        legend: {  //图例 
            layout: 'horizontal',  //图例显示的样式：水平（horizontal）/垂直（vertical） 
            backgroundColor: '#ffc', //图例背景色 
            align: 'left',  //图例水平对齐方式 
            verticalAlign: 'top',  //图例垂直对齐方式 
            x: 100,  //相对X位移 
            y: 70,   //相对Y位移 
            floating: true, //设置可浮动 
            shadow: true  //设置阴影 
        }, 
        exporting: { 
            enabled: false  //设置导出按钮不可用 
        }, 
        series: [
        {  //数据列 
            name: '一楼1号', 
            data: [ - 4.6, -2.2, 4.5, 13.1, 19.8, 24.0, 25.8, 24.4, 19.3, 12.4, 4.1, -2.7] 
        }, 
        { 
            name: '一楼2号', 
            data: [13.3, 14.4, 17.7, 21.9, 24.6, 27.2, 30.8, 32.1, 27.2, 23.7, 21.3, 15.6] 
        },{ 
            name: '一楼3号', 
            data: [10.3, 11.4, 13.7, 22.9, 24.6, 37.2, 35.8, 32.1, 29.2, 21.7, 11.3, 5.6] 
        },{ 
            name: '一楼4号', 
            data: [12.3, 15.4, 17.7, 22.9, 23.6, 27.2, 35.8, 32.1, 24.2, 21.7, 10.3, 9.6] 
        },{ 
            name: '一楼5号', 
            data: [14.3, 16.4, 13.7, 12.9, 26.6, 33.2, 36.8, 38.1, 22.2, 22.7, 21.3, 12.6] 
        }] 
    }); 
}); 
</script>
<div id="chart_line" style="width: 100%; height: 400px"></div>
第三个例子：
 
<script type = "text/javascript">
var chart; 
Highcharts.setOptions({ 
    global: { 
        useUTC: false 
    } 
}); 
$(function() { 
    chart = new Highcharts.Chart({ 
        chart: { 
            renderTo: 'chart_spline', //图表放置的容器，DIV 
            defaultSeriesType: 'spline', //图表类型为曲线图 
            events: { 
                load: function() {  
                    var series = this.series[0]; 
                    //每隔5秒钟，图表更新一次，y数据值在0-100之间的随机数 
                    setInterval(function() { 
                        var x = (new Date()).getTime(), // 当前时间 
                        y = Math.random()*20;  
                        series.addPoint([x, y], true, true); 
                    }, 
                    3000); 
                } 
            } 
        }, 
        title: { 
            text: '湿度走势图'  //图表标题 
        }, 
        xAxis: { //设置X轴 
            type: 'datetime',  //X轴为日期时间类型 
            tickPixelInterval: 150  //X轴标签间隔 
        }, 
        yAxis: { //设置Y轴 
            title: '', 
            max: 50, //Y轴最大值 
            min: 0  //Y轴最小值 
        }, 
        tooltip: {//当鼠标悬置数据点时的提示框 
            formatter: function() { //格式化提示信息 
                return 'CPU使用率'+ 
                Highcharts.dateFormat('%H:%M:%S', this.x) +''+  
                Highcharts.numberFormat(this.y, 2)+'%'; 
            } 
        }, 
        legend: { 
            enabled: false  //设置图例不可见 
        }, 
        exporting: { 
            enabled: false  //设置导出按钮不可用 
        }, 
        credits: { 
            text: 'lain.com.cn', //设置LOGO区文字 
            url: 'http://www.lain.com.cn' //设置LOGO链接地址 
        }, 
        series: [{ 
            data: (function() { //设置默认数据， 
                var data = [], 
                time = (new Date()).getTime(), 
                i; 
 
                for (i = -19; i <= 0; i++) { 
                    data.push({ 
                        x: time + i * 5000,  
                        y: Math.random()*100 
                    }); 
                } 
                return data; 
            })() 
        }] 
    }); 
}); 
</script>

<div id="chart_spline" style="width: 100%; height: 400px"></div>
分享：  分享到微米  分享到新浪Qing   
14
喜欢
0
赠金笔赠金笔
阅读(5521)┊ 评论 (7)┊	 收藏(2) ┊转载(5)	 ┊ 喜欢▼ ┊打印┊举报
前一篇：jsp中按钮使用、网页间跳转的几个小技巧
后一篇：【转】报错：The type javax.servlet.http.HttpServletRequest cannot be re
评论 重要提示：警惕虚假中奖信息 [发评论]
用户xgw5v5izw5
赞请问一下，如果单独设定当月条形图颜色的话，该怎么办？
2014-2-13  11:36回复(1)
zhangxiu2014
赞
2014-2-21  11:43回复(0)
洪军本人
赞怎么我生成的网页还是空白一片
2014-3-14  16:26回复(1)
i5998
请问如何在第一个demo的柱子上加链接呢？
2014-6-17  17:03回复(0)
viking
你好，问下为什么我的图表在浏览器展现的时候 貌似一开始只有数据，但是折线没有显示出来。感觉像是没有加载出来，但是一按F12调试就出来了。知道是什么原因么，有什么好的解决方法么
2014-10-25  11:05

2. R: http://www.zhengjin99.com/touzi/touzi_haixi.shtml
	http://www.zhengjin99.com/js/showChart.js
	http://www.zhengjin99.com/ForWeb/cmd3_1998005.txt
	http://www.zhengjin99.com/ForWeb/cmd2_1998005.txt
	http://www.zhengjin99.com/ForWeb/cmd1.txt

1. R: 
现货白银: http://api.baidao.com/api/hq/mtdata.do?quotationType=1&id=1&JSONP=?&ran=
现货钯金: http://api.baidao.com/api/hq/mtdata.do?quotationType=1&id=5&JSONP=?&ran=
现货铂金: http://api.baidao.com/api/hq/mtdata.do?quotationType=1&id=6&JSONP=?&ran=
现货镍: http://api.baidao.com/api/hq/mtdata.do?quotationType=1&id=7&JSONP=?&ran=
现货铜: http://api.baidao.com/api/hq/mtdata.do?quotationType=1&id=8&JSONP=?&ran=
现货铝: http://api.baidao.com/api/hq/mtdata.do?quotationType=1&id=9&JSONP=?&ran=

--2014.12.15 Mon 21:32--hyl--AnHui-1-16-10-602#--V1.0.0.1--
3. void __xd_Draw_DQ_1M(int iLastNum);//画后1分周期后 iLastNum 个数据
2. XDKLineDEQUE	m_DQ_1M; //1分钟周期数据
1. 建立本工程。
