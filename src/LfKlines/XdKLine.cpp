// XdKLine.cpp: implementation of the CXdKLine class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LfKlines.h"
#include "XdKLine.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CXdKLine::CXdKLine(float o,float h,float l,float c)
{
	m_fOpen = o;
	m_fClose = c;
	m_fHigh = h;
	m_fLow = l;

	m_iDy = 80; 
	m_fY0 = 3320.0;
	m_fY1 = 3160.0;
}

CXdKLine::CXdKLine()
{
	m_fR = 1.0;
	m_iDy = 80;
	m_fY0 = 3320.0;
	m_fY1 = 3160.0;
	m_strHTML = "";
}

CXdKLine::~CXdKLine()
{

}
void CXdKLine::Paint_01M (CDC* pDC)
{
	static int m,s;
	static int x = 10;
	int iBodyW = 10;
	if(m!=m_udt.m)
	{
		m = m_udt.m;
		x += iBodyW+1;
	}  

	HDC             hdc = pDC->m_hDC; 

	 static y=110;
	 y += 18;
	 int yOpen = Cal_K_2_Y(m_fOpen);
	 int yClose = Cal_K_2_Y(m_fClose);
	 int iYHight = Cal_K_2_Y(m_fHigh);
	 int iYLow = Cal_K_2_Y(m_fLow);
	 CRect r(x,iYHight,x+iBodyW,iYLow);
	 pDC->FillRect(&r, CBrush::FromHandle((HBRUSH)GetStockObject(LTGRAY_BRUSH)));
 

	 //---画K线--  
	 HBRUSH hBrush = GetBrush() ;
	 HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc,hBrush);
	 HPEN hPen = GetPen() ;
	 HPEN hOldPen = (HPEN)SelectObject(hdc,hPen);
 
	 if(yClose == yOpen) yClose++;
	 Rectangle(hdc,x,yOpen,x+iBodyW,yClose); 

	 MoveToEx(hdc,x+iBodyW/2,iYHight,NULL);
	 LineTo(hdc,x+iBodyW/2,iYLow); 

	 SelectObject(hdc,hOldPen);
	 DeleteObject (hPen) ; 
	 SelectObject(hdc,hOldBrush);
     DeleteObject (hBrush) ;  
}

void CXdKLine::PaintMe (CDC* pDC, int x, int iDx)
{ 
     HDC             hdc = pDC->m_hDC; 

	 static y=110;
	 y += 18;
	 //---画K线--  
	 int iBodyW = iDx;
	 HBRUSH hBrush = GetBrush() ;
	 HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc,hBrush);
	 HPEN hPen = GetPen() ;
	 HPEN hOldPen = (HPEN)SelectObject(hdc,hPen);
 
	 int yOpen = Cal_K_2_Y(m_fOpen);
	 int yClose = Cal_K_2_Y(m_fClose);
	 if(yClose == yOpen) yClose++;
	 Rectangle(hdc,x,yOpen,x+iBodyW,yClose); 

	 int iYHight = Cal_K_2_Y(m_fHigh);
	 int iYLow = Cal_K_2_Y(m_fLow);
	 MoveToEx(hdc,x+iBodyW/2,iYHight,NULL);
	 LineTo(hdc,x+iBodyW/2,iYLow);
 
	 SelectObject(hdc,hOldPen);
	 DeleteObject (hPen) ; 
	 SelectObject(hdc,hOldBrush);
     DeleteObject (hBrush) ;  
}

bool CXdKLine::IsUp()
{
	bool bRet = true;
	if(m_fClose > m_fOpen) 
	{
		bRet = true;     
	}
	else
	{
		bRet = false;
	}

	return bRet;
}

float CXdKLine::BodyHeighth()
{
	float fRet = 0.0;
	if(IsUp())
	{
		fRet = m_fClose - m_fOpen;
	}
	else
	{
		fRet = m_fOpen - m_fClose;
	}
	return fRet;
}

void CXdKLine::setR(float r)
{
	m_fR = r;
}

HPEN CXdKLine::GetPen()
{ 
	HPEN hRet = NULL;
    LOGBRUSH lb ;     
	
	lb.lbStyle = BS_SOLID ;
	lb.lbHatch = 0 ;  

	if(IsUp())
	{
		lb.lbColor = RGB (255, 0, 0) ;
	}
	else
	{
		lb.lbColor = RGB (0, 255, 0) ; 
	}
		hRet =  ExtCreatePen (PS_SOLID, 1, &lb, 0, NULL) ;
	return hRet;
	
}
HBRUSH CXdKLine::GetBrush()
{
	HBRUSH hRet = NULL;
	if(IsUp())
	{
		hRet = CreateSolidBrush(RGB(255,0,0));
	}
	else
	{
		hRet = CreateSolidBrush(RGB(0,255,0));
	}
	return hRet;
	
}

int CXdKLine::Cal_K_2_Y(float f)
{
	int iRet = 0;
	iRet =  (f - m_fY0)*m_iDy/(m_fY1-m_fY0);

	return iRet;
}
 

void CXdKLine::SetXY(int x, int y)
{
	m_iX = x;
	m_iY = y;
}

void CXdKLine::GetXY(int &x, int &y)
{
	x = m_iX;
	y = m_iY;
}

BOOL  CXdKLine::GetDataFromHTML()
{
	BOOL bRet = 0;
	CGet_Data	d;
	CString str  = "http://api.baidao.com/api/hq/npdata.do?ids=1";

	static int n=0;
	n++;
 
	
	bRet = d.fn_From_URL(str); 
	if(!bRet) return bRet;
    bRet = TRUE;

	m_strHTML = d.GetData();
	CString strHtml = m_strHTML;
	CString s,strRight;
	//	开盘价 open 值
	int  		iOpen;
	strRight=strHtml.Right(strHtml.GetLength()-strHtml.Find("open")); 
	sscanf(strRight.GetBuffer(strRight.GetLength()),"open\":%d",&iOpen); 
	s.Format("%d",iOpen); 
	m_fOpen = iOpen;

	//	开盘价 close 值
	int  		iClose;
	strRight=strHtml.Right(strHtml.GetLength()-strHtml.Find("close")); 
	sscanf(strRight.GetBuffer(strRight.GetLength()),"close\":%d",&iClose); 
	s.Format("%d",iClose); 
	m_fClose = iClose;

	
	//  卖出价 sell 值
	int  		iSell;
	strRight=strHtml.Right(strHtml.GetLength()-strHtml.Find("sell")); 
	sscanf(strRight.GetBuffer(strRight.GetLength()),"sell\":%d",&iSell); 
	s.Format("%d",iSell); 
	m_fSell = iSell;

	//  昨收价 preclose 值
	int  		iPreclose;
	strRight=strHtml.Right(strHtml.GetLength()-strHtml.Find("preclose")); 
	sscanf(strRight.GetBuffer(strRight.GetLength()),"preclose\":%d",&iPreclose); 
	s.Format("%d",iPreclose); 
	m_fPreClose = iPreclose;

	
	//  均价 avg 值
	int  		iAvg;
	strRight=strHtml.Right(strHtml.GetLength()-strHtml.Find("avg")); 
	sscanf(strRight.GetBuffer(strRight.GetLength()),"avg\":%d",&iAvg); 
	s.Format("%d",iAvg); 
	m_fAvg = iAvg;

	
	//  最高价 high 值
	int  		iHigh;
	strRight=strHtml.Right(strHtml.GetLength()-strHtml.Find("high")); 
	sscanf(strRight.GetBuffer(strRight.GetLength()),"high\":%d",&iHigh); 
	s.Format("%d",iHigh);
	m_fHigh = iHigh;

	//  最低价 low 值
	int  		iLow;
	strRight=strHtml.Right(strHtml.GetLength()-strHtml.Find("low")); 
	sscanf(strRight.GetBuffer(strRight.GetLength()),"low\":%d",&iLow); 
	s.Format("%d",iLow);
	m_fLow = iLow;

	//  更新时间 updatetime 值 
	long lD1=20141103;
	int i1 =11,i2=47,i3=12;
	strRight=strHtml.Right(strHtml.GetLength()-strHtml.Find("updatetime")); 
	sscanf(strRight.GetBuffer(strRight.GetLength()),"updatetime\":\"%d %d:%d:%d\",",&lD1,&i1,&i2,&i3); 
	s.Format("%d %d:%d:%d",lD1,i1,i2,i3);
	m_udt.h =i1;
	m_udt.m = i2;
	m_udt.s = i3;
	m_udt.lDate = lD1;
	return bRet;
}
void CXdKLine::SetOpen(float f)
{
	m_fOpen = f;
}
void CXdKLine::SetClose(float f)
{
	m_fClose = f;
}
void CXdKLine::SetHigh(float f)
{
	m_fHigh = f;
}
void CXdKLine::SetLow(float f)
{
	m_fLow = f;
}
float CXdKLine::GetPreClose()
{
	return m_fPreClose;
}
float CXdKLine::GetOpen()
{
	return m_fOpen;
}
float CXdKLine::GetSell()
{
	return m_fSell;
}

float CXdKLine::GetClose()
{
	return m_fClose;
}

float CXdKLine::GetHigh()
{
	return m_fHigh;
}
float CXdKLine::GetLow()
{
	return m_fLow;
}


float CXdKLine::GetAvg()
{
	return m_fAvg;
}
BOOL CXdKLine::GetUpdateTime(CString &str)
{
	BOOL bRet = FALSE;
	str.Format("%08d-%02d:%02d:%02d",m_udt.lDate,m_udt.h,m_udt.m,m_udt.s);
	static int m,s;
	static float fSell;

	if(m==m_udt.m && s == m_udt.s && fSell == m_fSell)
	{
		str += " Old";
	}
	else{
		str += " New";
		m = m_udt.m;
		s = m_udt.s;
		fSell = m_fSell;
		bRet = TRUE;
	}
	return bRet;
}