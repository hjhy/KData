// XdKLine.h: interface for the CXdKLine class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_XDKLINE_H__24337C4D_7530_4985_97AF_70F295FE6D5A__INCLUDED_)
#define AFX_XDKLINE_H__24337C4D_7530_4985_97AF_70F295FE6D5A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Get_Data.h"

class CXdKLine  
{
public: 
	CString m_strHTML;
	UPDATATIME	m_udt;
	void SetOpen(float f);
	void SetClose(float f);
	void SetHigh(float f);
	void SetLow(float f);
	float GetHigh();
	float GetLow();
	float GetOpen();
	float GetSell();
	float GetClose();
	float GetPreClose();
	float GetAvg();
	BOOL GetUpdateTime(CString &str);
	BOOL GetDataFromHTML();
	void GetXY(int &x, int &y);
	void SetXY(int x,int y);
	void setR(float r);
	float BodyHeighth();
	bool IsUp();
	CXdKLine(float o,float h,float l,float c);
	CXdKLine();
	virtual ~CXdKLine();

	void PaintMe (CDC* pDC, int x, int iDx);
	void Paint_01M (CDC* pDC );
	HBRUSH GetBrush();
	HPEN  GetPen();

private:
	int Cal_K_2_Y(float f);
	float	m_fOpen;
	float	m_fClose;
	float	m_fHigh;
	float	m_fLow;
	float	m_fSell;
	float	m_fPreClose;
	float	m_fAvg;
	float	m_fR; 
	float	m_fY0;
	float	m_fY1;
	int		m_iDy;

	int		m_iX;
	int		m_iY;
};

#endif // !defined(AFX_XDKLINE_H__24337C4D_7530_4985_97AF_70F295FE6D5A__INCLUDED_)
