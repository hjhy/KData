// Shell.h : main header file for the SHELL application
//

#if !defined(AFX_SHELL_H__9979FE25_7353_4D5F_B63E_F63E88C43351__INCLUDED_)
#define AFX_SHELL_H__9979FE25_7353_4D5F_B63E_F63E88C43351__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CShellApp:
// See Shell.cpp for the implementation of this class
//

class CShellApp : public CWinApp
{
public:
	CShellApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShellApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CShellApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHELL_H__9979FE25_7353_4D5F_B63E_F63E88C43351__INCLUDED_)
