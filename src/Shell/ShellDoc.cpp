// ShellDoc.cpp : implementation of the CShellDoc class
//

#include "stdafx.h"
#include "Shell.h"

#include "ShellDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CShellDoc

IMPLEMENT_DYNCREATE(CShellDoc, CDocument)

BEGIN_MESSAGE_MAP(CShellDoc, CDocument)
	//{{AFX_MSG_MAP(CShellDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CShellDoc construction/destruction

CShellDoc::CShellDoc()
{
	// TODO: add one-time construction code here

}

CShellDoc::~CShellDoc()
{
}

BOOL CShellDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	((CEditView*)m_viewList.GetHead())->SetWindowText(NULL);

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CShellDoc serialization

void CShellDoc::Serialize(CArchive& ar)
{
	// CEditView contains an edit control which handles all serialization
	((CEditView*)m_viewList.GetHead())->SerializeRaw(ar);
}

/////////////////////////////////////////////////////////////////////////////
// CShellDoc diagnostics

#ifdef _DEBUG
void CShellDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CShellDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CShellDoc commands
