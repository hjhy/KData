// ShellDoc.h : interface of the CShellDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SHELLDOC_H__67FA7567_F629_45F4_A3D1_ED345CA58A0D__INCLUDED_)
#define AFX_SHELLDOC_H__67FA7567_F629_45F4_A3D1_ED345CA58A0D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CShellDoc : public CDocument
{
protected: // create from serialization only
	CShellDoc();
	DECLARE_DYNCREATE(CShellDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShellDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CShellDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CShellDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHELLDOC_H__67FA7567_F629_45F4_A3D1_ED345CA58A0D__INCLUDED_)
