// MyTest.h: interface for the CMyTest class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MYTEST_H__2010AA6A_06F2_4D58_9448_C2402EF60199__INCLUDED_)
#define AFX_MYTEST_H__2010AA6A_06F2_4D58_9448_C2402EF60199__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//Defines the attributes of an arrow.

typedef struct tARROWSTRUCT
{ 
	int nWidth;     // width (in pixels) of the full base of the arrowhead
	
	float fTheta;   // angle (in radians) at the arrow tip between the two
	//  sides of the arrowhead
	
	bool bFill;     // flag indicating whether or not the arrowhead should be
	
	//  filled
	
} ARROWSTRUCT;
class CMyTest  
{
public:
	CMyTest();
	virtual ~CMyTest();
	
	//函数声明
	
	// Draws an arrow, using the current pen and brush, from the current position
	
	//  to the passed point using the attributes defined in the ARROWSTRUCT.
	
	
	void ArrowTo(HDC hDC, int x, int y, ARROWSTRUCT *pArrow); 
	void ArrowTo(HDC hDC, const POINT *lpTo, ARROWSTRUCT *pArrow);
	void ArrowTo(
		CDC *pDC,               //画刷
		CPoint point,           //终点坐标 
		int nPenStyle,          //线样式
		int nPenWidth,          //线宽度
		COLORREF color, //颜色
		int nWidth,             //三角形底边宽度
		
		float fTheta,           //三角形顶角角度
		bool bFill              //是否填充颜色
		);
};

#endif // !defined(AFX_MYTEST_H__2010AA6A_06F2_4D58_9448_C2402EF60199__INCLUDED_)
