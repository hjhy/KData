#include "StdAfx.h"
#include "IGTimeLine.h"

int CIGTimeLine::s_currentMoveObj = CIGTimeLine::DEFAULT;

IMPLEMENT_DYNCREATE( CIGTimeLine, CWnd)   
BEGIN_MESSAGE_MAP( CIGTimeLine, CWnd)   
	//{{AFX_MSG_MAP(CIGTimeLine)   
	ON_WM_LBUTTONDOWN()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP   
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP() 

CIGTimeLine::CIGTimeLine( UINT cols ) : m_lineTable( cols )
{
	this->m_lineTable.SetPosition( 0, 0 );
	
	this->m_currentSelRowIndex = -1;
	this->m_currentSelColIndex = 0;

	this->m_bindData = NULL;
}

CIGTimeLine::~CIGTimeLine(void)
{
	m_cellMenuA.DestroyMenu();
	m_cellMenuB.DestroyMenu();
}

BOOL CIGTimeLine::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, 
						 const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)    
{   
	lpszClassName = AfxRegisterWndClass( CS_HREDRAW | CS_VREDRAW , 
		AfxGetApp()->LoadStandardCursor(IDC_ARROW), (HBRUSH)GetStockObject(LTGRAY_BRUSH), NULL) ; 

	return CWnd::Create( lpszClassName, lpszWindowName, dwStyle | WS_VSCROLL | WS_HSCROLL | CS_DBLCLKS, rect, pParentWnd, nID, pContext);   
}   

void IGTimeLine::CIGTimeLine::OnPaint( void )
{
	PAINTSTRUCT ps;
	CDC* pDC = BeginPaint( &ps); 

	this->m_lineTable.m_colSelectIndex = this->m_currentSelColIndex;

	if( this->m_currentSelColIndex >= 0 && this->m_currentSelRowIndex >= 0 )
	{
		for( UINT i = 0; i < this->m_lineTable.GetRowCount(); i++ )
		{
			m_lineTable[i].SetUnSelStyle();
		}
		m_lineTable[this->m_currentSelRowIndex].SetSelStyle();

		this->SetCellSelectStyle( this->m_currentSelRowIndex, this->m_currentSelColIndex );
	}

	this->m_lineTable.Draw( pDC );
	EndPaint( &ps ); 
}

CIGTimeLine& IGTimeLine::CIGTimeLine::Append( LPCTSTR content, BOOL bShowSelectBox /*= TRUE */ )
{
	m_lineTable.Append( content, bShowSelectBox );
	UINT rIndex = this->m_lineTable.GetRowCount() - 1;
	m_lineTable[rIndex][0].ShowInsideRect();

	//m_vSi.nMax = this->GetRowCount() - 1;
	//SetScrollInfo( SB_VERT, &m_vSi );

	if( this->m_bindData != NULL )
		m_bindData->OnAppend( content, bShowSelectBox );

	return *this;
}

UINT IGTimeLine::CIGTimeLine::GetSelIndexOfRow( void )
{
	return this->m_currentSelRowIndex;
}

UINT IGTimeLine::CIGTimeLine::GetSelIndexOfCol( void )
{
	return this->m_currentSelColIndex;
}

void IGTimeLine::CIGTimeLine::Play( UINT offset /*= 1 */ )
{
	UINT currentSelColIndex = this->GetSelIndexOfCol();
	ASSERT( currentSelColIndex != UINT_MAX || this->GetColCount() );

	UINT maxIndex = this->GetColCount();
	offset %= maxIndex;
	currentSelColIndex += offset;
	if( currentSelColIndex == maxIndex )
		currentSelColIndex = 0;

	this->m_currentSelColIndex = currentSelColIndex;

	if( m_bindData != NULL )
		this->m_bindData->OnPlay( this->m_currentSelColIndex );
	Invalidate();
}

UINT IGTimeLine::CIGTimeLine::GetRowCount( void )
{
	return this->m_lineTable.GetRowCount();
}

UINT IGTimeLine::CIGTimeLine::GetColCount( void )
{
	return this->m_lineTable.GetColCount();
}

int IGTimeLine::CIGTimeLine::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码
	GetScrollInfo( SB_VERT, &m_vSi );
	m_vSi.cbSize = sizeof( SCROLLINFO );
	m_vSi.nMin = 0;
	m_vSi.nPage = 1;
	SetScrollInfo( SB_VERT, &m_vSi );

	GetScrollInfo( SB_HORZ, &m_hSi );
	m_hSi.cbSize = sizeof( SCROLLINFO );
	m_hSi.nMin = 0;
	m_hSi.nPage = 1;
	m_hSi.nMax = this->GetColCount() - 1;
	SetScrollInfo( SB_HORZ, &m_hSi );

	//初始化数据
	if( this->m_bindData != NULL )
		this->m_bindData->InitData( this->m_lineTable );

	//初始化菜单项
	m_cellMenuA.CreatePopupMenu();
	m_cellMenuA.AppendMenu( MF_STRING, ID_KEYFRAME_INSERT, _T("Insert KeyFrame") );
	//m_cellMenuA.AppendMenu( MF_STRING, ID_KEYFRAME_PASTE, _T("Paste KeyFrame") );

	m_cellMenuB.CreatePopupMenu();
	m_cellMenuB.AppendMenu( MF_STRING, ID_KEYFRAME_DELETE, _T("Delete") );
	//m_cellMenuB.AppendMenu( MF_STRING, ID_KEYFRAME_COPY, _T("Copy") );
	//m_cellMenuB.AppendMenu( MF_STRING, ID_KEYFRAME_CUT, _T("Cut") );

	m_rowHeaderMenu.CreatePopupMenu();

	m_rowHeaderMenu.AppendMenu( MF_STRING, ID_ROWHEADER_DELETE, _T("Delete") );
	//m_rowHeaderMenu.AppendMenu( MF_STRING, ID_ROWHEADER_CUT, _T("Cut") );
	//m_rowHeaderMenu.AppendMenu( MF_STRING, ID_ROWHEADER_COPY, _T("Copy") );
	//m_rowHeaderMenu.AppendMenu( MF_STRING, ID_ROWHEADER_PASTE, _T("Paste") );
	m_rowHeaderMenu.AppendMenu( 0 );
	m_rowHeaderMenu.AppendMenu( MF_STRING, ID_ROWHEADER_MOVEUP, _T("Move Up") );
	m_rowHeaderMenu.AppendMenu( MF_STRING, ID_ROWHEADER_MOVEDOWN, _T("Move Down") );
	return 0;
}

void IGTimeLine::CIGTimeLine::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	switch(nSBCode)
	{
	case SB_LINEDOWN:
		if( m_hSi.nPos < m_hSi.nMax )
			m_hSi.nPos++;
		break;
	case SB_LINEUP:
		if( m_hSi.nPos > m_hSi.nMin )
			m_hSi.nPos--;
		break;
	case SB_PAGEDOWN:
		m_hSi.nPos += m_hSi.nPage;
		break;
	case SB_PAGEUP:
		m_hSi.nPos -= m_hSi.nPage;
		break;
	case SB_THUMBTRACK:
		m_hSi.nPos = nPos;
		break;
	case SB_THUMBPOSITION:
		m_hSi.nPos = nPos;
		break;
	}

	this->m_lineTable.SetColStartIndex( m_hSi.nPos );
	SetScrollInfo( SB_HORZ, &m_hSi );
	Invalidate();

	CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
}

void IGTimeLine::CIGTimeLine::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	switch(nSBCode)
	{
	case SB_LINEDOWN:
		if( m_vSi.nPos < m_vSi.nMax )
			m_vSi.nPos++;
		break;
	case SB_LINEUP:
		if( m_vSi.nPos > m_vSi.nMin )
			m_vSi.nPos--;
		break;
	case SB_PAGEDOWN:
		m_vSi.nPos += m_vSi.nPage;
		break;
	case SB_PAGEUP:
		m_vSi.nPos -= m_vSi.nPage;
		break;
	case SB_THUMBTRACK:
		m_vSi.nPos = nPos;
		break;
	case SB_THUMBPOSITION:
		m_vSi.nPos = nPos;
		break;
	}

	this->m_lineTable.SetRowStartIndex( m_vSi.nPos );
	SetScrollInfo( SB_VERT, &m_vSi );
	Invalidate();

	CWnd::OnVScroll( nSBCode, nPos, pScrollBar );
}

CIGTimeLine& IGTimeLine::CIGTimeLine::Insert( UINT rowIndex, LPCTSTR content, BOOL bShowSelectBox /*= TRUE */ )
{
	this->m_lineTable.Insert( rowIndex, content, bShowSelectBox );
	return *this;
}

void IGTimeLine::CIGTimeLine::Remove( int rowIndex )
{
	this->m_lineTable.Remove( rowIndex );
	this->m_currentSelRowIndex--;

	if( this->m_bindData != NULL )
		this->m_bindData->OnRemove( rowIndex );
}

void IGTimeLine::CIGTimeLine::Exchange( UINT rowIndex1, UINT rowIndex2 )
{
	CIGTimeLineRow rowTmp = this->m_lineTable[rowIndex1];
	this->m_lineTable[rowIndex1] = this->m_lineTable[rowIndex2];
	this->m_lineTable[rowIndex2] = rowTmp;

	if( this->m_bindData != NULL )
		this->m_bindData->OnExchangeRow( rowIndex2, rowIndex1 );
}

void IGTimeLine::CIGTimeLine::RemoveAll( void )
{
	this->m_lineTable.RemoveAll();
}

void CIGTimeLine::OnLButtonDown(UINT nFlags, CPoint point)    
{   
	UINT cIndex = m_lineTable.GetColIndexByPosX( point.x );
	UINT rIndex = m_lineTable.GetRowIndexByPosY( point.y );

	if( UINT_MAX == rIndex )
		return;

	this->m_currentSelRowIndex = rIndex;
	
	if( m_bindData != NULL )
		m_bindData->OnRowClick( rIndex );

	//////////////////////////////////////////////////////////////////////////
	if( m_lineTable[rIndex].PtInRowHeader( point ) )
	{
		//点击到表头
		IGTimeLine::CIGTimeLine::s_currentMoveObj = CIGTimeLine::ROWHEADER;

		if( m_bindData != NULL )
			m_bindData->OnRowHeaderClick( rIndex );
	}

	//////////////////////////////////////////////////////////////////////////
	if( UINT_MAX == cIndex )
	{
		//处理行头选框
		BOOL bShowSelectBox = m_lineTable[rIndex].IsShowSelectBox();
		if( bShowSelectBox )
		{
			BOOL bInSelectBox = m_lineTable[rIndex].PtInSelectBox( point );
			if( bInSelectBox )
			{
				m_lineTable[rIndex].CheckSelectBox() ? m_lineTable[rIndex].SetSelectBoxUnSel()
					: m_lineTable[rIndex].SetSelectBoxSel();

				if( m_bindData != NULL )
					m_bindData->OnSelectBoxStateChanged( rIndex, m_lineTable[rIndex].CheckSelectBox() );
			}
		}
	}
	else
	{
		//点击到表格
		this->m_currentSelColIndex = cIndex;

		if( m_bindData != NULL )
			m_bindData->OnCellClick( this->m_currentSelRowIndex, this->m_currentSelColIndex );

		if( this->m_lineTable[this->m_currentSelRowIndex][this->m_currentSelColIndex].IsShowInsideRect() )
		{
			if( this->m_currentSelColIndex != 0 )
				IGTimeLine::CIGTimeLine::s_currentMoveObj = CIGTimeLine::INSIDERECT;
		}
	}

	Invalidate();
	CWnd::OnLButtonDown( nFlags, point );   
}  

void IGTimeLine::CIGTimeLine::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	if( nFlags & MK_LBUTTON )
	{
		UINT cIndex = m_lineTable.GetColIndexByPosX( point.x );
		UINT rIndex = m_lineTable.GetRowIndexByPosY( point.y );

		//复制单元格
		if( nFlags & MK_CONTROL )
		{
			if( rIndex == this->m_currentSelRowIndex && cIndex != UINT_MAX && this->m_currentSelColIndex != cIndex )
			{
				if( ( !this->m_lineTable[rIndex][cIndex].IsShowInsideRect() ) && 
					this->m_lineTable[this->m_currentSelRowIndex][this->m_currentSelColIndex].IsShowInsideRect() )
				{
					this->m_lineTable[rIndex][cIndex] = this->m_lineTable[this->m_currentSelRowIndex][this->m_currentSelColIndex];
					
					if( this->m_bindData != NULL )
						this->m_bindData->OnCopyCell( rIndex, this->m_currentSelColIndex, rIndex, cIndex );
					this->m_currentSelColIndex = cIndex;
					Invalidate();
				}
			}
		}
		//拖动单元格
		else if( rIndex == this->m_currentSelRowIndex && cIndex != UINT_MAX  && this->m_currentSelColIndex != cIndex )
		{
			if( IGTimeLine::CIGTimeLine::s_currentMoveObj == IGTimeLine::CIGTimeLine::INSIDERECT )
			{
				if( !this->m_lineTable[this->m_currentSelRowIndex][cIndex].IsShowInsideRect() && 
					this->m_lineTable[this->m_currentSelRowIndex][this->m_currentSelColIndex].IsShowInsideRect() )
				{
					this->m_lineTable[this->m_currentSelRowIndex].ExchangeCell( cIndex, this->m_currentSelColIndex );
					
					if( this->m_bindData != NULL )
						this->m_bindData->OnExchangeCell( this->m_currentSelRowIndex, this->m_currentSelColIndex, cIndex );
					
					this->m_currentSelColIndex = cIndex;

					Invalidate();
				}
			}
		}
		//拖动行
		else if( rIndex != UINT_MAX && this->m_currentSelRowIndex != rIndex )
		{
			if( IGTimeLine::CIGTimeLine::s_currentMoveObj == CIGTimeLine::ROWHEADER )
			{
				this->Exchange( rIndex, this->m_currentSelRowIndex );

				this->m_currentSelRowIndex = rIndex;

				Invalidate();
			}
		}
		
	}
	CWnd::OnMouseMove(nFlags, point);
}

void IGTimeLine::CIGTimeLine::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	IGTimeLine::CIGTimeLine::s_currentMoveObj = IGTimeLine::CIGTimeLine::DEFAULT;
	CWnd::OnLButtonUp(nFlags, point);
}

void IGTimeLine::CIGTimeLine::SetCellSelectStyle( UINT rIndex, UINT cIndex )
{
	for( UINT i = 0; i < m_lineTable.GetRowCount(); i++ )
	{
		for( UINT m = 0; m < m_lineTable.GetColCount(); m++ )
			m_lineTable[i][m].m_insideBgColor = RGB( 0, 0, 0 );
	}

	if( m_lineTable[rIndex][cIndex].IsShowInsideRect() )
	{
		m_lineTable[rIndex][cIndex].m_insideBgColor = RGB( 255, 0, 0 );
	}
}

void IGTimeLine::CIGTimeLine::BindData( IIGTimeLine* tableData )
{
	this->m_bindData = tableData;
}
void IGTimeLine::CIGTimeLine::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	UINT cIndex = m_lineTable.GetColIndexByPosX( point.x );
	UINT rIndex = m_lineTable.GetRowIndexByPosY( point.y );

	if( UINT_MAX == rIndex )
		return;

	CPoint ptTmp = point;
	ClientToScreen( &ptTmp );

	this->m_currentSelRowIndex = rIndex;
	if( cIndex != UINT_MAX )
	{
		this->m_currentSelColIndex = cIndex;
		Invalidate();

		if( m_lineTable[rIndex][cIndex].IsShowInsideRect() )
			m_cellMenuB.TrackPopupMenu( TPM_RIGHTBUTTON, ptTmp.x, ptTmp.y, this );
		else
			m_cellMenuA.TrackPopupMenu( TPM_RIGHTBUTTON, ptTmp.x, ptTmp.y, this );
	}
	else
	{
		Invalidate();
		m_rowHeaderMenu.TrackPopupMenu( TPM_RIGHTBUTTON, ptTmp.x, ptTmp.y, this );
	}
	CWnd::OnRButtonDown(nFlags, point);
}

BOOL IGTimeLine::CIGTimeLine::OnCommand( WPARAM wParam, LPARAM lParam )
{
	UINT_PTR uID = LOWORD( wParam );   

	switch( wParam )
	{
	case ID_KEYFRAME_INSERT:
		this->m_lineTable.GetCell( this->m_currentSelRowIndex, this->m_currentSelColIndex ).ShowInsideRect();
		this->m_bindData->OnInsertKeyFrame( this->m_currentSelRowIndex, this->m_currentSelColIndex );
		Invalidate();
		break;

	case ID_KEYFRAME_PASTE:
		break;

	case ID_KEYFRAME_DELETE:
		this->m_lineTable.GetCell( this->m_currentSelRowIndex, this->m_currentSelColIndex ).HideInsideRect();
		this->m_bindData->OnDeleteKeyFrame( this->m_currentSelRowIndex, this->m_currentSelColIndex );
		Invalidate();
		break;
	case ID_KEYFRAME_CUT:
		break;
	case ID_KEYFRAME_COPY:
		break;

	case ID_ROWHEADER_DELETE:
		this->Remove( this->m_currentSelRowIndex );
		Invalidate();
		break;
	case ID_ROWHEADER_MOVEUP:
		if( this->m_currentSelRowIndex > 0 )
		{
			this->Exchange( this->m_currentSelRowIndex, this->m_currentSelRowIndex - 1 );
			this->m_currentSelRowIndex--;
			Invalidate();
		}
		break;
	case ID_ROWHEADER_MOVEDOWN:
		if( this->m_currentSelRowIndex < (int)( this->GetRowCount() - 1 ) )
		{
			this->Exchange( this->m_currentSelRowIndex, this->m_currentSelRowIndex + 1 );
			this->m_currentSelRowIndex++;
			Invalidate();
		}
		break;
	}

	return CWnd::OnCommand( wParam, lParam );
}
