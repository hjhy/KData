#pragma once
#include "IGTimeLineTable.h"
#include "IIGTimeLine.h"
using namespace IGTimeLine;

#define	ID_KEYFRAME_INSERT		0x00000001
#define ID_KEYFRAME_PASTE		0x00000002
#define ID_KEYFRAME_DELETE		0x00000003
#define ID_KEYFRAME_COPY		0x00000004
#define ID_KEYFRAME_CUT			0x00000005
#define ID_ROWHEADER_DELETE		0x00000006
#define ID_ROWHEADER_CUT		0x00000007
#define ID_ROWHEADER_COPY		0x00000008
#define ID_ROWHEADER_PASTE		0x00000009
#define ID_ROWHEADER_MOVEUP		0x0000000A
#define ID_ROWHEADER_MOVEDOWN	0x0000000B

namespace IGTimeLine
{
	class CIGTimeLine : public CWnd
	{
	public:
		CIGTimeLine( UINT cols = 100  );
		virtual ~CIGTimeLine(void);

		DECLARE_DYNCREATE( CIGTimeLine )

		enum EM_MOVEOBJECT{DEFAULT, ROWHEADER, INSIDERECT };
		static int s_currentMoveObj;

		virtual BOOL Create( LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL );   

		void BindData( IIGTimeLine* tableData );							//绑定需要的处理数据
		CIGTimeLine& Append( LPCTSTR content, BOOL bShowSelectBox = TRUE );	//添加一行
		CIGTimeLine& Insert( UINT rowIndex, LPCTSTR content, BOOL bShowSelectBox = TRUE );
		void Exchange( UINT rowIndex1, UINT rowIndex2 );					//交换两行
		void Remove( int rowIndex );										//移除一行
		void RemoveAll( void );												//移除全部行
		UINT GetSelIndexOfRow( void );										//获取当前选中的行
		UINT GetSelIndexOfCol( void );										//获取当前选中的列
		UINT GetRowCount( void );											//获取表格行数
		UINT GetColCount( void );											//获取表格列数
		
		void Play( UINT offset = 1 );										//正向播放
	private:
		int m_currentSelRowIndex;
		int m_currentSelColIndex;
		IIGTimeLine* m_bindData;
		CMenu m_cellMenuA, m_cellMenuB, m_rowHeaderMenu;
	protected:
		CIGTimeLineTable m_lineTable;
		SCROLLINFO m_vSi;
		SCROLLINFO m_hSi;
	protected:   
		DECLARE_MESSAGE_MAP()
		void SetCellSelectStyle( UINT rIndex, UINT cIndex );
		afx_msg void OnPaint( void );
		afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
		afx_msg int	 OnCreate(LPCREATESTRUCT lpCreateStruct);
		afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
		afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
		afx_msg void OnMouseMove(UINT nFlags, CPoint point);
		afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	public:
		afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
		afx_msg BOOL OnCommand(WPARAM  wParam, LPARAM lParam);

	};
}