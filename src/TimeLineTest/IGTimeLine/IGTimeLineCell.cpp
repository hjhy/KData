#include "StdAfx.h"
#include "IGTimeLineCell.h"
using namespace IGTimeLine;

int CIGTimeLineCell::w = 8;
int CIGTimeLineCell::h = 16;
int CIGTimeLineCell::padding = 2;

CIGTimeLineCell::CIGTimeLineCell(void)
{
	this->SetPosition( 0, 0 );
	this->HideInsideRect();
	this->m_bgColor = RGB( 224, 255, 255 );
	this->m_insideBgColor = RGB( 0, 0, 0 );
}

CIGTimeLineCell::~CIGTimeLineCell(void)
{
}

void CIGTimeLineCell::Draw( CDC* pDC )
{
	RECT rt;
	rt.left = this->x;
	rt.top = this->y;
	rt.right = rt.left + CIGTimeLineCell::w;
	rt.bottom = rt.top + CIGTimeLineCell::h;

	RECT rtInside;
	rtInside.left = rt.left + CIGTimeLineCell::padding;
	rtInside.top = rt.top + CIGTimeLineCell::padding;
	rtInside.right = rt.right - CIGTimeLineCell::padding;
	rtInside.bottom = rt.bottom - CIGTimeLineCell::padding;

	CBrush brush( this->m_bgColor );
	CBrush insideBrush( this->m_insideBgColor );
	pDC->SelectObject( &brush );
	pDC->FillRect( &rt, &brush );

	if( TRUE == this->m_bShowInsideRect )
	{
		pDC->FillRect( &rtInside, &insideBrush );
	}
}

void CIGTimeLineCell::SetPosition( UINT x, UINT y )
{
	this->x = x;
	this->y = y;
}

void IGTimeLine::CIGTimeLineCell::ShowInsideRect( void )
{
	this->m_bShowInsideRect = TRUE;
}

void IGTimeLine::CIGTimeLineCell::HideInsideRect( void )
{
	this->m_bShowInsideRect = FALSE;
}

BOOL IGTimeLine::CIGTimeLineCell::IsShowInsideRect( void )
{
	return this->m_bShowInsideRect;
}

IGTimeLine::CIGTimeLineCell& IGTimeLine::CIGTimeLineCell::operator=( const IGTimeLine::CIGTimeLineCell& other )
{
	this->x = other.x;
	this->y = other.y;
	this->m_bShowInsideRect = other.m_bShowInsideRect;
	this->m_insideBgColor = other.m_insideBgColor;
	return *this;
}
