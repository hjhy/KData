#pragma once
namespace IGTimeLine
{
	class CIGTimeLineCell
	{
	public:
		CIGTimeLineCell(void);
		~CIGTimeLineCell(void);
		
		void Draw( CDC* pDC );

		void SetPosition( UINT x, UINT y );
		void ShowInsideRect( void );
		void HideInsideRect( void );
		BOOL IsShowInsideRect( void );
		CIGTimeLineCell& operator=( const CIGTimeLineCell& other );
	public:
		static int w, h;
		static int padding;				//内矩形与外矩形间距
		COLORREF m_bgColor;	
		COLORREF m_insideBgColor;		//内矩形背景色
		int x, y;
		BOOL m_bShowInsideRect;			//是否显示内矩形
	};
}