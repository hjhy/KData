#include "StdAfx.h"
#include "IGTimeLineColHeader.h"
using namespace IGTimeLine;

CIGTimeLineColHeader::CIGTimeLineColHeader(void)
{
	x = 0;
	y = 0;
	w = 8;
	h = 16;

	m_start = 0;
	m_end = 1000;
	m_divisor = 5;
	m_currentSel = 0;
	m_lineThick = 1;
	m_lineLength_selected = 500;
	m_lineColor_default = RGB( 0, 0, 0 );
	m_bgColor_default = RGB( 212, 208, 200 );
	m_lineColor_selected = RGB( 255, 0, 0 );
	m_bgColor_selected = RGB( 255, 153, 153 );
	m_textColor = RGB( 0, 0, 0 );
}

CIGTimeLineColHeader::~CIGTimeLineColHeader(void)
{

}

void CIGTimeLineColHeader::Draw( CDC* pDC )
{
	int hTmp = this->h / 4;
	int current = this->m_start;
	int size = this->m_end - this->m_start;
	short bState = 0;
	//获取总长度
	int wTmp = this->GetWidth();	

	CRect rt( x, y, x + wTmp, y + h );
	CBrush bgBrush( this->m_bgColor_default );
	pDC->FillRect( rt, &bgBrush );

	int xTmp = x;
	for( int i = 0; i < size; i++ )
	{
		if( m_currentSel == current )
		{
			//////////////////////////////////////////////////////////////////////////
			//被选中
			CPen pen( PS_SOLID, this->m_lineThick, this->m_lineColor_selected );
			CBrush brush( this->m_bgColor_selected );
			pDC->SelectObject( pen );
			pDC->SelectObject( brush );
			pDC->Rectangle( xTmp, y, xTmp + w + this->m_lineThick * 2, y + h + this->m_lineThick );

			pDC->MoveTo( xTmp + w / 2, y + h );
			pDC->LineTo( xTmp + w / 2, y + h + m_lineLength_selected );
			//////////////////////////////////////////////////////////////////////////
			bState = 2;
		}

		if( bState == 0 )
		{
			//////////////////////////////////////////////////////////////////////////
			//画刻度
			CPen pen( PS_SOLID, this->m_lineThick, this->m_lineColor_default );
			CBrush brush( this->m_bgColor_default );
			pDC->SelectObject( &pen );
			pDC->SelectObject( &brush );

			pDC->MoveTo( xTmp, y );
			pDC->LineTo( xTmp, y + hTmp );
			pDC->MoveTo( xTmp, y + this->h );
			pDC->LineTo( xTmp, y + this->h - hTmp );
			//////////////////////////////////////////////////////////////////////////
		}

		//////////////////////////////////////////////////////////////////////////
		//画文字
		if( current % this->m_divisor == 0 )
		{
			CFont font;
			VERIFY( font.CreatePointFont( 60, _T("Arial"), pDC ) );
			CFont* def_font = pDC->SelectObject( &font );
			CString str = _T("");
			str.Format( _T("%d"), current );
			CSize m_size = pDC->GetTextExtent( str );
			pDC->SetTextColor( this->m_textColor );
			pDC->SetBkMode( TRANSPARENT );
			CRect rt( xTmp - m_size.cx / 2, y, xTmp + w + m_size.cx / 2, y + h );
			pDC->DrawText( str, &rt, DT_CENTER | DT_VCENTER | DT_SINGLELINE );

			pDC->SelectObject( def_font );
			font.DeleteObject();
		}
		//////////////////////////////////////////////////////////////////////////

		if( bState > 0 )
			bState--;

		xTmp += this->m_lineThick + w;
		current++;
	}
}

UINT CIGTimeLineColHeader::GetWidth( void )
{
	return ( w + this->m_lineThick ) * ( this->m_end - this->m_start );
}

void IGTimeLine::CIGTimeLineColHeader::SetPosition( UINT x, UINT y )
{
	this->x = x;
	this->y = y;
}

void IGTimeLine::CIGTimeLineColHeader::SetScaleSize( UINT w, UINT h )
{
	this->w = w;
	this->h = h;
}

void IGTimeLine::CIGTimeLineColHeader::SetArea( UINT start, UINT end )
{
	this->SetStart( start );
	this->SetEnd( end );
}

void IGTimeLine::CIGTimeLineColHeader::SetStart( UINT start )
{
	this->m_start = start;
}

void IGTimeLine::CIGTimeLineColHeader::SetEnd( UINT end )
{
	this->m_end = end;
}

void IGTimeLine::CIGTimeLineColHeader::SetCurrentSel( UINT currentSel )
{
	this->m_currentSel = currentSel;
}

UINT IGTimeLine::CIGTimeLineColHeader::GetLeft( void )
{
	return this->x;
}

UINT IGTimeLine::CIGTimeLineColHeader::GetTop( void )
{
	return this->y;
}

UINT IGTimeLine::CIGTimeLineColHeader::GetHeight( void )
{
	return this->h;
}

UINT IGTimeLine::CIGTimeLineColHeader::GetScaleWidth( void )
{
	return this->w;
}

void IGTimeLine::CIGTimeLineColHeader::SetDivisor( UINT divisor )
{
	this->m_divisor = divisor;
}
