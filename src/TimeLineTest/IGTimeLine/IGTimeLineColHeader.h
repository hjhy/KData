#pragma once

namespace IGTimeLine
{
	class CIGTimeLineColHeader
	{
	public:
		CIGTimeLineColHeader(void);
		~CIGTimeLineColHeader(void);

		void Draw( CDC* pDC );
		
		void SetStart( UINT start );
		void SetEnd( UINT end );
		void SetArea( UINT start, UINT end );	//设置范围
		void SetPosition( UINT x, UINT y );
		void SetScaleSize( UINT w, UINT h );	//设置一刻度的宽高
		void SetDivisor( UINT divisor );		//设置显示刻度间隔
		void SetCurrentSel( UINT currentSel );	//设置当前选中的列

		UINT GetLeft( void );
		UINT GetTop( void );
		UINT GetHeight( void );
		UINT GetWidth( void );			//总宽度
		UINT GetScaleWidth( void );		//获得一刻度的宽度
	public:
		int m_lineThick;				//线条宽度
		int m_lineLength_selected;		//选中线条长度
		COLORREF m_lineColor_default;	//默认线条颜色
		COLORREF m_bgColor_default;		//默认背景颜色
		COLORREF m_lineColor_selected;	//选中线条颜色
		COLORREF m_bgColor_selected;	//选中背景颜色
		COLORREF m_textColor;			//刻度颜色
		int m_divisor;					//是它的倍数则显示刻度
		int x, y;						//起始坐标
		int w, h;						//刻度宽高
		int m_start, m_end;				//从开始到结束
		int m_currentSel;				//当前选中的列
	};
}