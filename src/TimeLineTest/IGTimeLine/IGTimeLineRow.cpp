#include "StdAfx.h"
#include "IGTimeLineRow.h"

CIGTimeLineRow::CIGTimeLineRow(void)
{
	this->m_lineColor = RGB( 128, 128, 128 );
	this->m_lineThick = 1;

	this->SetCellStartIndex( 0 );
	this->SetDivisor( 5 );
}

CIGTimeLineRow::CIGTimeLineRow( LPCTSTR content, UINT cols/*单元格数*/, BOOL bShowSelectBox /*= TRUE */ )
{
	this->CIGTimeLineRow::CIGTimeLineRow();

	this->m_rowHeader.m_content = content;
	bShowSelectBox ? this->ShowSelectBox() : this->HideSelectBox();

	CIGTimeLineCell lineCell;
	for( UINT i = 0; i < cols; i++ )
	{
		this->m_rowCells.push_back( lineCell );
	}
}

CIGTimeLineRow::~CIGTimeLineRow(void)
{
}

void CIGTimeLineRow::Draw( CDC* pDC )
{
	//画行头
	this->m_rowHeader.Draw( pDC );

	CRect rtHeader = this->m_rowHeader.GetRect();
	CIGTimeLineCell::h = rtHeader.Height();
	UINT xTmp = rtHeader.right;
	
	for( UINT i = this->m_startIndex; i < this->GetColCount(); i++ )
	{
		//画竖线
		CPen pen( PS_SOLID, this->m_lineThick, this->m_lineColor );
		pDC->SelectObject( &pen );
		pDC->MoveTo( xTmp, rtHeader.top );
		pDC->LineTo( xTmp, rtHeader.bottom );
		xTmp += this->m_lineThick;

		//画单元格
		this->m_rowCells[i].SetPosition( xTmp, rtHeader.top );

		if( ( i % this->m_divisor ) == 0 )
			this->m_rowCells[i].m_bgColor = RGB( 176, 255, 255 );

		this->m_rowCells[i].Draw( pDC );
		xTmp += CIGTimeLineCell::w;
	}
}

void CIGTimeLineRow::SetHeight( UINT height )
{
	this->m_rowHeader.SetHeight( height );
}

void CIGTimeLineRow::SetPosition( UINT x, UINT y )
{
	this->m_rowHeader.SetPosition( x, y );
}

UINT CIGTimeLineRow::GetWidth( void )
{
	int width = this->m_rowHeader.GetWidth();
	
	for( UINT i = this->m_startIndex; i < this->GetColCount(); i++ )
		width += ( this->m_lineThick + CIGTimeLineCell::w );
	
	return width;
}

UINT CIGTimeLineRow::GetHeight( void )
{
	return this->m_rowHeader.GetHeight();
}

CIGTimeLineCell& CIGTimeLineRow::GetCell( UINT colIndex )
{
	ASSERT( colIndex < this->GetColCount() );

	return this->m_rowCells[colIndex];
}

CIGTimeLineCell& CIGTimeLineRow::operator[]( UINT colIndex )
{
	return this->GetCell( colIndex );
}

UINT IGTimeLine::CIGTimeLineRow::GetColCount( void )
{
	return (UINT)this->m_rowCells.size();
}

void IGTimeLine::CIGTimeLineRow::SetHeaderWidth( UINT width )
{
	this->m_rowHeader.SetWidth( width );
}

UINT IGTimeLine::CIGTimeLineRow::GetLeft( void )
{
	return this->m_rowHeader.GetLeft();
}

UINT IGTimeLine::CIGTimeLineRow::GetTop( void )
{
	return this->m_rowHeader.GetTop();
}

void IGTimeLine::CIGTimeLineRow::SetCellStartIndex( UINT startIndex )
{
	this->m_startIndex = startIndex;
}

void IGTimeLine::CIGTimeLineRow::SetDivisor( UINT divisor )
{
	this->m_divisor = divisor;
}

void IGTimeLine::CIGTimeLineRow::ShowSelectBox( void )
{
	this->m_rowHeader.ShowSelectBox();
}

void IGTimeLine::CIGTimeLineRow::HideSelectBox( void )
{
	this->m_rowHeader.HideSelectBox();
}

BOOL IGTimeLine::CIGTimeLineRow::IsShowSelectBox( void )
{
	return this->m_rowHeader.IsShowSelectBox();
}

BOOL IGTimeLine::CIGTimeLineRow::CheckSelectBox( void )
{
	return this->m_rowHeader.CheckSelectBox();
}

void IGTimeLine::CIGTimeLineRow::SetSelectBoxSel( void )
{
	this->m_rowHeader.SetSelectBoxSel();
}

void IGTimeLine::CIGTimeLineRow::SetSelectBoxUnSel( void )
{
	this->m_rowHeader.SetSelectBoxUnSel();
}

BOOL IGTimeLine::CIGTimeLineRow::PtInSelectBox( POINT pt )
{
	return this->m_rowHeader.PtInSelectBox( pt );
}

BOOL IGTimeLine::CIGTimeLineRow::PtInSelectBox( int x, int y )
{
	return this->m_rowHeader.PtInSelectBox( x, y );
}

void IGTimeLine::CIGTimeLineRow::SetHeaderBgColor( COLORREF clr )
{
	this->m_rowHeader.m_bgColor = clr;
}

void IGTimeLine::CIGTimeLineRow::SetHeaderFontColor( COLORREF clr )
{
	this->m_rowHeader.m_fontColor = clr;
}

void IGTimeLine::CIGTimeLineRow::SetSelStyle( void )
{
	this->SetHeaderBgColor( RGB( 10, 36, 106 ) );
	this->SetHeaderFontColor( RGB( 255, 255, 255 ) );
}

void IGTimeLine::CIGTimeLineRow::SetUnSelStyle( void )
{
	this->SetHeaderBgColor( RGB( 212, 208, 200 ) );
	this->SetHeaderFontColor( RGB( 0, 0, 0 ) );
}

BOOL IGTimeLine::CIGTimeLineRow::CheckSel( void )
{
	return this->m_rowHeader.m_fontColor == RGB( 255, 255, 255 );
}

BOOL IGTimeLine::CIGTimeLineRow::PtInRowHeader( POINT pt )
{
	return this->m_rowHeader.PtInRowHeader( pt );
}

BOOL IGTimeLine::CIGTimeLineRow::PtInRowHeader( int x, int y )
{
	return this->m_rowHeader.PtInRowHeader( x, y );
}

void IGTimeLine::CIGTimeLineRow::ExchangeCell( UINT cIndex1, UINT cIndex2 )
{
	CIGTimeLineCell& lineCellA = this->GetCell( cIndex1 );
	CIGTimeLineCell& lineCellB = this->GetCell( cIndex2 );
	CIGTimeLineCell lineCellC;
	lineCellC = lineCellA;
	lineCellA = lineCellB;
	lineCellB = lineCellC;
}

void IGTimeLine::CIGTimeLineRow::ShowCellInsideRect( UINT cIndex )
{
	this->m_rowCells[cIndex].ShowInsideRect();
}

void IGTimeLine::CIGTimeLineRow::HideCellInsideRect( UINT cIndex )
{
	this->m_rowCells[cIndex].HideInsideRect();
}

IGTimeLine::CIGTimeLineRow& IGTimeLine::CIGTimeLineRow::operator=( const CIGTimeLineRow& other )
{
	this->m_lineColor = other.m_lineColor;
	this->m_lineThick = other.m_lineThick;
	this->m_rowHeader = other.m_rowHeader;
	this->m_startIndex = other.m_startIndex;
	this->m_divisor = other.m_divisor;
	
	for( UINT i = 0; i < this->m_rowCells.size(); i++ )
	{
		m_rowCells[i] = other.m_rowCells[i];
	}

	return *this;
}

UINT IGTimeLine::CIGTimeLineRow::GetDivisor( void )
{
	return this->m_divisor;
}

UINT IGTimeLine::CIGTimeLineRow::GetCellStartIndex( void )
{
	return this->m_startIndex;
}
