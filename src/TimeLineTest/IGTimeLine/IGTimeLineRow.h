#pragma once
#include "IGTimeLineRowHeader.h"
#include "IGTimeLineCell.h"
#include <vector>
using namespace std;
using namespace IGTimeLine;

namespace IGTimeLine
{
	class CIGTimeLineRow
	{
	public:
		CIGTimeLineRow(void);
		CIGTimeLineRow( LPCTSTR content, UINT cols/*单元格数*/, BOOL bShowSelectBox = TRUE );
		~CIGTimeLineRow(void);

		void Draw( CDC* pDC );
		UINT GetColCount( void );
		UINT GetWidth( void );
		UINT GetHeight( void );
		UINT GetLeft( void );
		UINT GetTop( void );
		UINT GetDivisor( void );
		UINT GetCellStartIndex( void );

		void SetHeight( UINT height );
		void SetHeaderWidth( UINT width );
		void SetPosition( UINT x, UINT y );
		void SetCellStartIndex( UINT startIndex );
		void SetDivisor( UINT divisor );

		void ShowSelectBox( void );
		void HideSelectBox( void );
		BOOL IsShowSelectBox( void );

		void SetSelectBoxSel( void );			//设置选框选中
		void SetSelectBoxUnSel( void );			//设置选框未选中
		BOOL CheckSelectBox( void );			//获取选框状态

		void SetSelStyle( void );			//设置行选中样式
		void SetUnSelStyle( void );			//取消行选中样式
		BOOL CheckSel( void );				//判断行是否被选中

		BOOL PtInSelectBox( POINT pt );			//判断坐标是否在选框内
		BOOL PtInSelectBox( int x, int y );
		BOOL PtInRowHeader( POINT pt );
		BOOL PtInRowHeader( int x, int y );

		void ExchangeCell( UINT cIndex1, UINT cIndex2 );	//交换两个单元格
		void ShowCellInsideRect( UINT cIndex );
		void HideCellInsideRect( UINT cIndex );

		CIGTimeLineCell& GetCell( UINT colIndex );
		CIGTimeLineCell& operator[]( UINT colIndex );

		CIGTimeLineRow& operator=( const CIGTimeLineRow& other );
	private:
		void SetHeaderBgColor( COLORREF clr );		//设置行头背景色
		void SetHeaderFontColor( COLORREF clr );	//设置行头文字颜色
	public:
		int m_lineThick;
		COLORREF m_lineColor;
		CIGTimeLineRowHeader m_rowHeader;
		vector<CIGTimeLineCell> m_rowCells;
		int m_startIndex;
		int m_divisor;
	};
}