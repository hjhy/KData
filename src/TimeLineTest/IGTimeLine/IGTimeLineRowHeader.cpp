#include "StdAfx.h"
#include "IGTimeLineRowHeader.h"

CIGTimeLineRowHeader::CIGTimeLineRowHeader(void)
{
	this->m_fontSize = 70;
	this->m_fontFamily = _T("Arial");
	this->m_fontColor = RGB( 0, 0, 0 );
	this->m_bgColor = RGB( 212, 208, 200 );

	this->SetPosition( 0, 0 );
	this->SetSize( 100, 25 );
	this->ShowSelectBox();
}

CIGTimeLineRowHeader::~CIGTimeLineRowHeader(void)
{

}

void CIGTimeLineRowHeader::Draw( CDC* pDC )
{
	CRect rt = this->GetRect();
	int hTmp = rt.Height() / 2;

	CBrush bgBrush( this->m_bgColor );
	pDC->FillRect( &rt, &bgBrush );

	CFont font;
	VERIFY( font.CreatePointFont( this->m_fontSize, this->m_fontFamily, pDC ) );
	CFont* def_font = pDC->SelectObject( &font );

	CSize m_size = pDC->GetTextExtent( this->m_content ); //计算buf中的字体总宽为多少个像素
	pDC->SetBkMode( TRANSPARENT );						  //设置字体背景为透明
	CRect fontRect( rt.TopLeft(), CPoint( rt.right, rt.top + hTmp ) );

	pDC->SetTextColor( this->m_fontColor );
	pDC->DrawText( this->m_content, &fontRect, DT_LEFT | DT_VCENTER | DT_SINGLELINE );

	pDC->SelectObject( def_font );
	font.DeleteObject();

	if( TRUE == this->m_bShowSelectBox )
	{
		this->m_selectBox.SetPosition( rt.left, rt.top + hTmp );
		this->m_selectBox.Draw( pDC );
	}
}

void CIGTimeLineRowHeader::ShowSelectBox( void )
{
	this->m_bShowSelectBox = TRUE;
}

void CIGTimeLineRowHeader::HideSelectBox( void )
{
	this->m_bShowSelectBox = FALSE;
}

BOOL CIGTimeLineRowHeader::PtInSelectBox( int x, int y )
{
	POINT pt;
	pt.x = x;
	pt.y = y;

	return this->PtInSelectBox( pt );
}

BOOL CIGTimeLineRowHeader::PtInSelectBox( POINT pt )
{
	RECT rt = this->m_selectBox.GetRect();
	return ::PtInRect( &rt, pt );
}

void IGTimeLine::CIGTimeLineRowHeader::SetSize( UINT w, UINT h )
{
	this->SetWidth( w );
	this->SetHeight( h );
}

void IGTimeLine::CIGTimeLineRowHeader::SetPosition( UINT x, UINT y )
{
	this->x = x;
	this->y = y;
}

void IGTimeLine::CIGTimeLineRowHeader::SetHeight( UINT h )
{
	this->h = h;
}

void IGTimeLine::CIGTimeLineRowHeader::SetWidth( UINT w )
{
	this->w = w;
}

UINT IGTimeLine::CIGTimeLineRowHeader::GetLeft( void )
{
	return this->x;
}

UINT IGTimeLine::CIGTimeLineRowHeader::GetTop( void )
{
	return this->y;
}

UINT IGTimeLine::CIGTimeLineRowHeader::GetWidth( void )
{
	return this->w;
}

UINT IGTimeLine::CIGTimeLineRowHeader::GetHeight( void )
{
	return this->h;
}

RECT IGTimeLine::CIGTimeLineRowHeader::GetRect( void )
{
	RECT rt;
	rt.left = this->GetLeft();
	rt.top = this->GetTop();
	rt.right = rt.left + this->GetWidth();
	rt.bottom = rt.top + this->GetHeight();

	return rt;
}

BOOL IGTimeLine::CIGTimeLineRowHeader::IsShowSelectBox( void )
{
	return this->m_bShowSelectBox;
}

void IGTimeLine::CIGTimeLineRowHeader::SetSelectBoxSel( void )
{
	this->m_selectBox.ShowInsideEllipse();
}

void IGTimeLine::CIGTimeLineRowHeader::SetSelectBoxUnSel( void )
{
	this->m_selectBox.HideInsideEllipse();
}

BOOL IGTimeLine::CIGTimeLineRowHeader::PtInRowHeader( POINT pt )
{
	RECT rt = this->GetRect();
	return ::PtInRect( &rt, pt );
}

BOOL IGTimeLine::CIGTimeLineRowHeader::PtInRowHeader( int x, int y )
{
	POINT pt;
	pt.x = x;
	pt.y = y;
	return this->PtInRowHeader( pt );
}

CIGTimeLineRowHeader& IGTimeLine::CIGTimeLineRowHeader::operator=( const CIGTimeLineRowHeader& other )
{
	this->m_content = other.m_content;
	this->m_fontFamily = other.m_fontFamily;
	this->m_fontSize = other.m_fontSize;
	this->m_fontColor = other.m_fontColor;
	this->m_bgColor = other.m_bgColor;
	this->x = other.x;
	this->y = other.y;
	this->w = other.w;
	this->h = other.h;
	this->m_bShowSelectBox = other.m_bShowSelectBox;
	this->m_selectBox = other.m_selectBox;

	return *this;
}

BOOL CIGTimeLineRowHeader::CheckSelectBox( void )
{
	return this->m_selectBox.IsShowInsideEllipse();
}