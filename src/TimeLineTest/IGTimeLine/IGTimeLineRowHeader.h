#pragma once
#include "IGTimeLineSelectBox.h"
using namespace IGTimeLine;

namespace IGTimeLine
{
	class CIGTimeLineRowHeader
	{
	public:
		CIGTimeLineRowHeader(void);
		~CIGTimeLineRowHeader(void);
		
		void Draw( CDC* pDC );

		void SetHeight( UINT h );
		void SetWidth( UINT w );
		void SetSize( UINT w, UINT h );
		void SetPosition( UINT x, UINT y );

		UINT GetLeft( void );
		UINT GetTop( void );
		UINT GetWidth( void );
		UINT GetHeight( void );
		RECT GetRect( void );

		void ShowSelectBox( void );
		void HideSelectBox( void ); 
		BOOL IsShowSelectBox( void );

		void SetSelectBoxSel( void );			//设置选框选中
		void SetSelectBoxUnSel( void );			//设置选框未选中
		BOOL CheckSelectBox( void );			//获取选框状态

		BOOL PtInSelectBox( POINT pt );
		BOOL PtInSelectBox( int x, int y );
		BOOL PtInRowHeader( POINT pt );
		BOOL PtInRowHeader( int x, int y );

		CIGTimeLineRowHeader& operator=( const CIGTimeLineRowHeader& other );
	public:
		LPCTSTR m_content;		//显示文字
		LPCTSTR m_fontFamily;	//字体名称
		int m_fontSize;			//字体大小
		COLORREF m_fontColor;	//字体颜色
		COLORREF m_bgColor;		//背景颜色
		int x, y, w, h;			//位置大小
		BOOL m_bShowSelectBox;	//是否显示选框
		CIGTimeLineSelectBox m_selectBox;
	};
}