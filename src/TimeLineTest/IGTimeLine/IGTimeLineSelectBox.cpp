#include "StdAfx.h"
#include "IGTimeLineSelectBox.h"
using namespace IGTimeLine;

CIGTimeLineSelectBox::CIGTimeLineSelectBox(void)
{
	this->SetPosition( 0, 0 );
	this->SetSize( 13, 9 );
	this->ShowInsideEllipse();

	this->m_lineColor = RGB( 62, 87, 128 );
	this->m_bgColor = RGB( 255, 255, 255 );
	this->m_insideEllipseBgColor = RGB( 31, 59, 105 );
	this->m_insideEllipseRadii = 2;
	this->m_lineThick = 1;
}

CIGTimeLineSelectBox::~CIGTimeLineSelectBox(void)
{
}

void CIGTimeLineSelectBox::Draw( CDC* pDC )
{
	CRect rt = this->GetRect();
	CPen pen( PS_SOLID, this->m_lineThick, this->m_lineColor );
	pDC->SelectObject( &pen );
	CBrush brush( this->m_bgColor );
	pDC->SelectObject( &brush );
	pDC->Ellipse( &rt );

	if( TRUE == this->IsShowInsideEllipse() )
	{
		CBrush sBrush( this->m_insideEllipseBgColor );
		pDC->SelectObject( &sBrush );

		CRect rtCenterEllipse;
		CPoint ptCenter = rt.CenterPoint();

		rtCenterEllipse.left = ptCenter.x - this->m_insideEllipseRadii;
		rtCenterEllipse.top = ptCenter.y - this->m_insideEllipseRadii;
		rtCenterEllipse.right = ptCenter.x + this->m_insideEllipseRadii;
		rtCenterEllipse.bottom = ptCenter.y + this->m_insideEllipseRadii;
		pDC->Ellipse( &rtCenterEllipse );
	}
}

void IGTimeLine::CIGTimeLineSelectBox::SetPosition( UINT x, UINT y )
{
	this->x = x;
	this->y = y;
}

void IGTimeLine::CIGTimeLineSelectBox::SetSize( UINT w, UINT h )
{
	this->w = w;
	this->h = h;
}

UINT IGTimeLine::CIGTimeLineSelectBox::GetLeft( void )
{
	return this->x;
}

UINT IGTimeLine::CIGTimeLineSelectBox::GetTop( void )
{
	return this->y;
}

UINT IGTimeLine::CIGTimeLineSelectBox::GetHeight( void )
{
	return this->h;
}

UINT IGTimeLine::CIGTimeLineSelectBox::GetWidth( void )
{
	return this->w;
}

void IGTimeLine::CIGTimeLineSelectBox::ShowInsideEllipse( void )
{
	this->m_bShowInsideEllipse = TRUE;
}

void IGTimeLine::CIGTimeLineSelectBox::HideInsideEllipse( void )
{
	this->m_bShowInsideEllipse = FALSE;
}

BOOL IGTimeLine::CIGTimeLineSelectBox::IsShowInsideEllipse( void )
{
	return this->m_bShowInsideEllipse;
}

RECT IGTimeLine::CIGTimeLineSelectBox::GetRect( void )
{
	RECT rt;
	rt.left = this->GetLeft();
	rt.top = this->GetTop();
	rt.right = rt.left + this->GetWidth();
	rt.bottom = rt.top + this->GetHeight();

	return rt;
}

CIGTimeLineSelectBox& IGTimeLine::CIGTimeLineSelectBox::operator=( const CIGTimeLineSelectBox& other )
{
	this->m_bgColor = other.m_bgColor;
	this->m_bShowInsideEllipse = other.m_bShowInsideEllipse;
	this->m_insideEllipseBgColor = other.m_insideEllipseBgColor;
	this->m_insideEllipseRadii = other.m_insideEllipseRadii;
	this->m_lineColor = other.m_lineColor;
	this->m_lineThick = other.m_lineThick;
	this->x = other.x;
	this->y = other.y;
	this->w = other.w;
	this->h = other.h;

	return *this;
}
