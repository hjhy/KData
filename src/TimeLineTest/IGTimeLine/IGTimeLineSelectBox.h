#pragma once
namespace IGTimeLine
{
	class CIGTimeLineSelectBox
	{
	public:
		CIGTimeLineSelectBox(void);
		~CIGTimeLineSelectBox(void);

		void Draw( CDC* pDC );
		void SetPosition( UINT x, UINT y );
		void SetSize( UINT w, UINT h );
		void ShowInsideEllipse( void );
		void HideInsideEllipse( void );
		BOOL IsShowInsideEllipse( void );
		RECT GetRect( void );
		UINT GetLeft( void );
		UINT GetTop( void );
		UINT GetHeight( void );
		UINT GetWidth( void );

		CIGTimeLineSelectBox& operator=( const CIGTimeLineSelectBox& other );
	public:
		int m_lineThick;					//边框宽度
		COLORREF m_lineColor, m_bgColor;	//边框色 背景色
		COLORREF m_insideEllipseBgColor;	//中心圆颜色
		int m_insideEllipseRadii;			//中心圆半径
		BOOL m_bShowInsideEllipse;			//是否显示中心圆
		int x, y, w, h;						//选框位置大小
	};
}