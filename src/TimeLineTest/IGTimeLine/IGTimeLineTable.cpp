#include "StdAfx.h"
#include "IGTimeLineTable.h"

IGTimeLine::CIGTimeLineTable::CIGTimeLineTable(void)
{
	this->m_selectLineLength = 500;
	this->m_rowSelectIndex = ~0;
	this->m_colSelectIndex = ~0;
	this->m_gridLineColor = RGB( 128, 128, 128 );
	this->m_gridLineThick = 1;
	this->m_divisor = 5;

	this->m_colsCount = 500;
	this->SetRowStartIndex( 0 );
	this->SetColStartIndex( 0 );
	this->SetPosition( 0, 0 );
	this->SetCellSize( 8, 23 );
	this->SetColHeaderHeight( 20 );
	this->SetRowHeaderWidth( 100 );
}

IGTimeLine::CIGTimeLineTable::CIGTimeLineTable( UINT ColCount, UINT CellWidth /*= 8*/, UINT CellHeight /*= 23*/, UINT ColHeaderHeight /*= 20*/, UINT RowHeaderWidth /*= 100*/, UINT divisor /*= 5 */ )
{
	this->m_selectLineLength = 500;
	this->m_rowSelectIndex = ~0;
	this->m_colSelectIndex = ~0;
	this->m_gridLineColor = RGB( 128, 128, 128 );
	this->m_gridLineThick = 1;
	this->m_divisor = divisor;

	this->m_colsCount = ColCount;
	this->SetRowStartIndex( 0 );
	this->SetColStartIndex( 0 );
	this->SetPosition( 0, 0 );
	this->SetCellSize( CellWidth, CellHeight );
	this->SetColHeaderHeight( ColHeaderHeight );
	this->SetRowHeaderWidth( RowHeaderWidth );
}

CIGTimeLineTable::~CIGTimeLineTable(void)
{
	
}

void CIGTimeLineTable::Draw( CDC* pDC )
{
	//////////////////////////////////////////////////////////////////////////
	//����
	CIGTimeLineCell::w = this->m_cellWidth;

	int xTmp = this->x;
	int yTmp = this->y + this->m_colHeaderHeight;
	for( UINT i = m_rowStartIndex; i < this->GetRowCount(); i++ )
	{
		this->m_rows[i].m_lineColor = this->m_gridLineColor;
		this->m_rows[i].m_lineThick = this->m_gridLineThick;
		this->m_rows[i].SetCellStartIndex( this->m_colStartIndex );
		this->m_rows[i].SetHeaderWidth( this->m_rowHeaderWidth );
		this->m_rows[i].SetHeight( this->m_cellHeight );
		this->m_rows[i].SetPosition( xTmp, yTmp );
		this->m_rows[i].Draw( pDC );

		CPen pen( PS_SOLID, this->m_gridLineThick, this->m_gridLineColor );
		pDC->SelectObject( &pen );
		pDC->MoveTo( xTmp, yTmp );
		pDC->LineTo( xTmp + this->m_rows[i].GetWidth(), yTmp );

		pDC->MoveTo( xTmp, yTmp + this->m_rows[i].GetHeight() );
		pDC->LineTo( xTmp + this->m_rows[i].GetWidth(), yTmp + this->m_rows[i].GetHeight() );

		yTmp += this->m_cellHeight;
	}

	//////////////////////////////////////////////////////////////////////////
	//������ͷ
	xTmp = this->x;
	yTmp = this->y;
	
	RECT rt;
	rt.left = xTmp;
	rt.top = yTmp;
	rt.right = rt.left + this->m_rowHeaderWidth;// + this->m_gridLineThick;
	rt.bottom = rt.top + this->m_colHeaderHeight;
	CBrush brush( this->m_colHeader.m_bgColor_default );
	pDC->FillRect( &rt, &brush );

	xTmp = rt.right;
	this->m_colHeader.SetPosition( xTmp, yTmp );
	this->m_colHeader.SetScaleSize( this->m_cellWidth, this->m_colHeaderHeight );
	this->m_colHeader.SetDivisor( this->m_divisor );
	this->m_colHeader.SetArea( this->m_colStartIndex, this->m_colsCount );
	this->m_colHeader.SetCurrentSel( this->m_colSelectIndex );
	this->m_colHeader.m_lineLength_selected = this->m_selectLineLength;
	this->m_colHeader.Draw( pDC );
	//////////////////////////////////////////////////////////////////////////
}

void CIGTimeLineTable::SetPosition( UINT x, UINT y )
{
	this->x = x;
	this->y = y;
}

CIGTimeLineTable& CIGTimeLineTable::Append( CIGTimeLineRow& lineRow )
{
	this->m_rows.push_back( lineRow );

	return *this;
}

UINT CIGTimeLineTable::GetWidth( void )
{
	return this->m_rowHeaderWidth + this->m_colHeader.GetWidth();
}

UINT CIGTimeLineTable::GetHeight( void )
{
	UINT height = this->m_colHeaderHeight;
	for( UINT i = m_rowStartIndex; i < this->GetRowCount(); i++ )
	{
		height += this->m_rows[i].GetHeight() + this->m_gridLineThick;
	}
	return height;
}

CIGTimeLineRow& CIGTimeLineTable::GetRow( UINT rowIndex )
{
	ASSERT( rowIndex < this->GetRowCount() && rowIndex >= 0 );
	return this->m_rows[rowIndex];
}

CIGTimeLineCell& CIGTimeLineTable::GetCell( UINT rowIndex, UINT colIndex )
{
	return this->GetRow( rowIndex ).GetCell( colIndex );
}

UINT CIGTimeLineTable::GetColIndexByPosX( UINT x )
{
	UINT xTmp = this->m_colHeader.GetLeft() + this->m_colHeader.m_lineThick;

	for( UINT i = this->m_colStartIndex; i < this->GetColCount(); i++ )
	{
		if( x >= xTmp && x <= ( xTmp + this->m_colHeader.GetScaleWidth() ) )
			return i;

		xTmp += this->m_colHeader.GetScaleWidth() + this->m_colHeader.m_lineThick;
	}

	return UINT_MAX;
}

UINT CIGTimeLineTable::GetRowIndexByPosY( UINT y )
{
	UINT yTmp = 0;
	for( UINT i = this->m_rowStartIndex; i < this->GetRowCount(); i++ )
	{
		yTmp = this->m_rows[i].GetTop();
		if( y >= yTmp && y <= ( yTmp + this->m_rows[i].GetHeight() ) )
			return i;
	}
	return UINT_MAX;
}

CIGTimeLineRow& CIGTimeLineTable::operator[]( UINT rowIndex )
{
	return this->GetRow( rowIndex );
}

CIGTimeLineTable& IGTimeLine::CIGTimeLineTable::Append( LPCTSTR content, BOOL bShowSelectBox /*= TRUE */ )
{
	CIGTimeLineRow timeLineRow( content, this->m_colsCount, bShowSelectBox );
	return this->Append( timeLineRow );
}

UINT IGTimeLine::CIGTimeLineTable::GetRowCount( void )
{
	return this->m_rows.size();
}

void IGTimeLine::CIGTimeLineTable::SetRowStartIndex( UINT rowIndex )
{
	this->m_rowStartIndex = rowIndex;
}

void IGTimeLine::CIGTimeLineTable::SetColStartIndex( UINT colIndex )
{
	this->m_colStartIndex = colIndex;
}

void IGTimeLine::CIGTimeLineTable::SetCellSize( UINT CellWidth, UINT CellHeight )
{
	this->m_cellWidth = CellWidth;
	this->m_cellHeight = CellHeight;
}

void IGTimeLine::CIGTimeLineTable::SetRowHeaderWidth( UINT RowHeaderWidth )
{
	this->m_rowHeaderWidth = RowHeaderWidth;
}

void IGTimeLine::CIGTimeLineTable::SetColHeaderHeight( UINT ColHeaderHeight )
{
	this->m_colHeaderHeight = ColHeaderHeight;
}

UINT IGTimeLine::CIGTimeLineTable::GetColCount( void )
{
	return this->m_colsCount;
}

void IGTimeLine::CIGTimeLineTable::Remove( UINT rowIndex )
{
	vector<CIGTimeLineRow>::iterator iter = this->m_rows.begin();

	for( UINT i = 0; i < this->GetRowCount(); i++ )
	{
		if( i == rowIndex )
		{
			this->m_rows.erase( iter );
			return;
		}

		iter++;
	}
}

void IGTimeLine::CIGTimeLineTable::Insert( UINT rowIndex, CIGTimeLineRow& lineRow )
{
	vector<CIGTimeLineRow>::iterator iter = this->m_rows.begin();

	for( UINT i = 0; i < this->GetRowCount(); i++ )
	{
		if( i == rowIndex )
		{
			this->m_rows.insert( iter, lineRow );
			return;
		}

		iter++;
	}
}

void IGTimeLine::CIGTimeLineTable::Insert( UINT rowIndex, LPCTSTR content, BOOL bShowSelectBox /*= TRUE */ )
{
	CIGTimeLineRow timeLineRow( content, this->m_colsCount, bShowSelectBox );
	this->Insert( rowIndex, timeLineRow );
}

void IGTimeLine::CIGTimeLineTable::RemoveAll( void )
{
	this->m_rows.clear();
}

