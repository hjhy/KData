#pragma once
#include "IGTimeLineCell.h"
#include "IGTimeLineRow.h"
#include "IGTimeLineColHeader.h"
#include <vector>
using namespace std;
using namespace IGTimeLine;

namespace IGTimeLine
{
	class CIGTimeLineTable
	{
	public:
		CIGTimeLineTable( void );
		CIGTimeLineTable( UINT ColCount, UINT CellWidth = 8, UINT CellHeight = 23, UINT ColHeaderHeight = 20, UINT RowHeaderWidth = 100, UINT divisor = 5 );
		~CIGTimeLineTable(void);

		void Draw( CDC* pDC );
		UINT GetWidth( void );
		UINT GetHeight( void );
		void SetCellSize( UINT CellWidth, UINT CellHeight );
		void SetRowHeaderWidth( UINT RowHeaderWidth );
		void SetColHeaderHeight( UINT ColHeaderHeight );
		CIGTimeLineTable& Append( LPCTSTR content, BOOL bShowSelectBox = TRUE );
		CIGTimeLineTable& Append( CIGTimeLineRow& lineRow );
		void Insert( UINT rowIndex, CIGTimeLineRow& lineRow );
		void Insert( UINT rowIndex, LPCTSTR content, BOOL bShowSelectBox = TRUE );
		void Remove( UINT rowIndex );
		void RemoveAll( void );
		void SetPosition( UINT x, UINT y );
		UINT GetRowCount( void );
		UINT GetColCount( void );
		CIGTimeLineRow& GetRow( UINT rowIndex );
		CIGTimeLineCell& GetCell( UINT rowIndex, UINT colIndex );

		UINT GetColIndexByPosX( UINT x );
		UINT GetRowIndexByPosY( UINT y );

		void SetRowStartIndex( UINT rowIndex );
		void SetColStartIndex( UINT colIndex );

		CIGTimeLineRow& operator[]( UINT rowIndex );
	public:
		int m_gridLineColor;
		int m_gridLineThick;
		int m_rowSelectIndex;
		int m_colSelectIndex;
	private:
		int x, y;
		int m_cellWidth;
		int m_cellHeight;
		int m_colHeaderHeight;
		int m_rowHeaderWidth;
		int m_colsCount;
		int m_selectLineLength;
		int m_divisor;
		int m_rowStartIndex;
		int m_colStartIndex;

		CIGTimeLineColHeader m_colHeader;
		vector<CIGTimeLineRow> m_rows;
	};
}