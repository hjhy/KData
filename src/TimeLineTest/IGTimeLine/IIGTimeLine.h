#pragma once
#include "IGTimeLineTable.h"

namespace IGTimeLine
{
	class IIGTimeLine
	{
	public:
		//初始化数据
		virtual void InitData( CIGTimeLineTable& lineTable ) = 0;
		//交换单元格时触发
		virtual void OnExchangeCell( UINT rIndex, UINT cIndex1, UINT cIndex2 ){}
		//复制单元格
		virtual void OnCopyCell( UINT rIndex1, UINT cIndex1/*源*/, UINT rIndex2, UINT cIndex2/*目的*/ ){}
		//交换行时触发
		virtual void OnExchangeRow( UINT rIndex1, UINT rIndex2 ){}
		//删除一行时触发
		virtual void OnRemove( UINT rIndex ){}
		//添加一行时触发
		virtual void OnAppend( LPCTSTR content, BOOL bShowSelectBox ){}
		//修改行头时触发
		virtual void OnModify( UINT rIndex, LPCTSTR content, BOOL bShowSelectBox ){}
		//选框状态发生改变
		virtual void OnSelectBoxStateChanged( UINT rIndex, BOOL bSelectBoxSelected ){}
		//选中行时触发
		virtual void OnRowClick( UINT rIndex ){}
		//选中行头时触发
		virtual void OnRowHeaderClick( UINT rIndex ){}
		//选中列时触发
		virtual void OnCellClick( UINT rIndex, UINT cIndex ){}
		//播放时触发
		virtual void OnPlay( UINT cIndex ){}
		//插入KeyFrame
		virtual void OnInsertKeyFrame( UINT rIndex, UINT cIndex ){}
		//删除keyFrame
		virtual void OnDeleteKeyFrame( UINT rIndex, UINT cIndex ){}

	};
}