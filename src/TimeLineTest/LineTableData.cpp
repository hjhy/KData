#include "StdAfx.h"
#include "LineTableData.h"

CLineTableData::CLineTableData(void)
{
}

CLineTableData::~CLineTableData(void)
{
}

void CLineTableData::InitData( CIGTimeLineTable& lineTable )
{
	this->m_pCurrentLineTable = &lineTable;

	lineTable.Append( _T("A"), TRUE );
	lineTable.Append( _T("B"), FALSE );
	lineTable.Append( _T("C"), TRUE );
	lineTable.Append( _T("D"), FALSE );
	lineTable.Append( _T("E"), TRUE );

	lineTable[0][0].ShowInsideRect();
	lineTable[1][0].ShowInsideRect();
	lineTable[2][0].ShowInsideRect();
	lineTable[3][0].ShowInsideRect();
	lineTable[4][0].ShowInsideRect();

	lineTable[4][20].ShowInsideRect();
	lineTable[4][30].ShowInsideRect();
}

void CLineTableData::OnExchangeCell( UINT rIndex, UINT cIndex1, UINT cIndex2 )
{
	//CString str;
	//str.Format( _T("%d行%d列与%d列交换数据"), rIndex, cIndex1, cIndex2 );
	//AfxMessageBox( str );
}

void CLineTableData::OnCellClick( UINT rIndex, UINT cIndex )
{
	CString str;
	str.Format( _T("%d行%d列被单击"), rIndex, cIndex );
	//AfxMessageBox( str );
}

void CLineTableData::OnSelectBoxStateChanged( UINT rIndex, BOOL bSelectBoxSelected )
{
	CString str;
	bSelectBoxSelected ? str.Format( _T("%d行选框被选中"), rIndex ) : str.Format( _T("%d行选框取消选中"), rIndex );
	//AfxMessageBox( str );
}

void CLineTableData::OnRowClick( UINT rIndex )
{
	CString str;
	str.Format( _T("%d行被点击"), rIndex );
	//AfxMessageBox( str );
}

void CLineTableData::OnRowHeaderClick( UINT rIndex )
{
	CString str;
	str.Format( _T("%d行行头被点击"), rIndex );
	//AfxMessageBox( str );
}

void CLineTableData::OnExchangeRow( UINT rIndex1, UINT rIndex2 )
{
	CString str;
	str.Format( _T("%d行与%d行交换数据"), rIndex1, rIndex2 );
	//AfxMessageBox( str );
}

void CLineTableData::OnInsertKeyFrame( UINT rIndex, UINT cIndex )
{
	CString str;
	str.Format( _T("%d行%d列插入帧数据"), rIndex, cIndex );
	AfxMessageBox( str );
}

void CLineTableData::OnDeleteKeyFrame( UINT rIndex, UINT cIndex )
{
	CString str;
	str.Format( _T("删除%d行%d列的帧数据"), rIndex, cIndex );
	AfxMessageBox( str );
}

void CLineTableData::OnRemove( UINT rIndex )
{
	CString str;
	str.Format( _T("删除第%d行"), rIndex );
	AfxMessageBox( str );
}
