#pragma once
#include "IGTimeLine\iigtimeline.h"
using namespace IGTimeLine;

class CLineTableData :
	public IGTimeLine::IIGTimeLine
{
public:
	CLineTableData(void);
	~CLineTableData(void);

	virtual void InitData( CIGTimeLineTable& lineTable );
	virtual void OnExchangeCell( UINT rIndex, UINT cIndex1, UINT cIndex2 );
	virtual void OnCellClick( UINT rIndex, UINT cIndex );
	virtual void OnSelectBoxStateChanged( UINT rIndex, BOOL bSelectBoxSelected );
	virtual void OnRowClick( UINT rIndex );
	virtual void OnRowHeaderClick( UINT rIndex );
	virtual void OnExchangeRow( UINT rIndex1, UINT rIndex2 );
	virtual void OnRemove( UINT rIndex );
	virtual void OnInsertKeyFrame( UINT rIndex, UINT cIndex );
	virtual void OnDeleteKeyFrame( UINT rIndex, UINT cIndex );

	//复制单元格
	virtual void OnCopyCell( UINT rIndex1, UINT cIndex1/*源*/, UINT rIndex2, UINT cIndex2/*目的*/ ){}
	//添加一行时触发
	virtual void OnAppend( LPCTSTR content, BOOL bShowSelectBox ){}
	//修改行头时触发
	virtual void OnModify( UINT rIndex, LPCTSTR content, BOOL bShowSelectBox ){}
	//播放时触发
	virtual void OnPlay( UINT cIndex ){}

public:
	CIGTimeLineTable* m_pCurrentLineTable;
};
