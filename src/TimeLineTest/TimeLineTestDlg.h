// TimeLineTestDlg.h : 头文件
//

#pragma once
#include "IGTimeLine/IGTimeLine.h"
#include "LineTableData.h"

// CTimeLineTestDlg 对话框
class CTimeLineTestDlg : public CDialog
{
// 构造
public:
	CTimeLineTestDlg(CWnd* pParent = NULL);	// 标准构造函数
	
	CIGTimeLine m_timeLine;
	CLineTableData m_timeLineData;
// 对话框数据
	enum { IDD = IDD_TIMELINETEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
};
