#ifndef _Command_h_
#define _Command_h_

#include "base.h"

#ifdef __cplusplus

enum ECommandType	// 命令类
{
	ctUnknown = 0,
//  创建对象命令类
	ctCreateLine = 1,		// 创建直线
	ctCreateRectangle = 2,	// 创建矩形
	ctCreateCircle = 3,		// 创建圆
	ctCreateArc = 4,		// 创建圆弧
	ctCreateOther = 5,		// 创建Other
	// ...其他创建类型
//  修改命令类
	ctMove = 11,			// 移动
	ctRotate = 12,			// 旋转
	ctMirror = 13,			// 镜像
	// ...其他创建类型
	ctPan = 14
};

class CCommand
{
protected:
	int		m_nStep ; // 命令操作步
public:
	CCommand() {}
	~CCommand() {}

	virtual int GetType() = 0; // 返回命令类型 ECommandType
	virtual int OnLButtonDown(UINT nFlags, const Position& pos) = 0 ;
	virtual int OnMouseMove(UINT nFlags, const Position& pos) = 0 ;
	virtual int OnRButtonDown(UINT nFlags, const Position& pos) = 0 ;
	virtual int Cancel() = 0 ;	
	virtual int xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) = 0 ;
} ;

#endif

#endif
