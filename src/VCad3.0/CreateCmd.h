#ifndef _CreateCmd_h_
#define _CreateCmd_h_

#include "base.h"

#ifdef __cplusplus

///////////////////////////////////////////////////////////////
/*	
 *	CCreateOther
 */
class CCreateOther : public CCommand
{
private:
	Position m_PtCreate1;	// 直线的起点
	Position m_PtCreate2;		// 直线的终点 
	CString		m_strCreate;
public:
	CCreateOther() ;
	~CCreateOther() ;

	int		GetType();
	int		OnLButtonDown(UINT nFlags, const Position& pos) ;
	int		OnMouseMove(UINT nFlags, const Position& pos) ;
	int		OnRButtonDown(UINT nFlags, const Position& pos) ;

	int		Cancel() ;
	int		xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
} ;

///////////////////////////////////////////////////////////////
/*	
 *	CCreateLine
 */
class CCreateLine : public CCommand
{
private:
	Position m_begin;	// 直线的起点
	Position m_end;		// 直线的终点 
public:
	CCreateLine() ;
	~CCreateLine() ;

	int		GetType();
	int		OnLButtonDown(UINT nFlags, const Position& pos) ;
	int		OnMouseMove(UINT nFlags, const Position& pos) ;
	int		OnRButtonDown(UINT nFlags, const Position& pos) ;

	int		Cancel() ;
	int		xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
} ;

///////////////////////////////////////////////////////////////
/*	
 *	CCreateRect
 */
class CCreateRect : public CCommand
{
private:
	Position m_LeftTop;
	Position m_RightBottom;
public:
	CCreateRect() ;
	~CCreateRect() ;

	int		GetType();
	int		OnLButtonDown(UINT nFlags, const Position& pos) ;
	int		OnMouseMove(UINT nFlags, const Position& pos) ;
	int		OnRButtonDown(UINT nFlags, const Position& pos) ;

	int		Cancel() ;
	int		xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
} ;

///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
/*	
 *	CCreateArc
 */
class CCreateCircle : public CCommand
{
private:
	Position	m_center ;
	Position	m_pos ;
public:
	CCreateCircle() ;
	~CCreateCircle() ;

	int		GetType();
	int		OnLButtonDown(UINT nFlags, const Position& pos) ;
	int		OnMouseMove(UINT nFlags, const Position& pos) ;
	int		OnRButtonDown(UINT nFlags, const Position& pos) ;

	int		Cancel() ;
	int		xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
} ;

///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
/*	
 *	CCreateArc
 */
class CCreateArc : public CCommand
{
private:
	Position m_center;
	Position m_begin;
	Position m_end ;
public:
	CCreateArc() ;
	~CCreateArc() ;

	int		GetType();
	int		OnLButtonDown(UINT nFlags, const Position& pos) ;
	int		OnMouseMove(UINT nFlags, const Position& pos) ;
	int		OnRButtonDown(UINT nFlags, const Position& pos) ;

	int		Cancel() ;
	int		xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
} ;
#endif // #ifdef __cplusplus

#endif