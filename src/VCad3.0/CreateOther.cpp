#include "stdafx.h"
#include "math.h"
#include "VCad.h"
#include "VCadDoc.h"
#include "VCadView.h"
#include "MainFrm.h"
#include "Entity.h"
#include "Command.h"
#include "CreateCmd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CCreateOther::CCreateOther()
	: m_PtCreate1(0,0), m_PtCreate2(0,0)
{
	m_nStep = 0; // 初始化操作步为 0
	m_strCreate = "Create";
}

CCreateOther::~CCreateOther()
{
}

int CCreateOther::GetType()
{
	return ctCreateOther; 
}

int	CCreateOther::OnLButtonDown(UINT nFlags, const Position& pos) 
{
	m_nStep ++; // 每次单击鼠标左键时操作步加 1 
	switch(m_nStep) // 根据操作步执行相应的操作
	{
		case 1: 
		{
			m_PtCreate1 = m_PtCreate2 = pos;
			::Prompt("请输入直线的末端点：");
			m_strCreate = "";
			break;
		}
		case 2:
		{
			CDC*	pDC = g_pView->GetDC(); // 得到设备环境指针 
			

			// 擦除在拖动状态时显示的最后一条线
			COther*	pTempOther = new COther(m_PtCreate1,m_PtCreate2,m_strCreate); 
			pTempOther->Draw(pDC, dmDrag);
			delete pTempOther;

			// 如果在按鼠标左键的过程中同时按下了Shift键，
			//	那么根据鼠标单击位置绘制水平线或竖直线
			if( nFlags & MK_SHIFT ){ 
				double dx = pos.x - m_PtCreate1.x;
				double dy = pos.y - m_PtCreate1.y;
				if(fabs(dx) <= fabs(dy)) // 如果鼠标单击位置在X方向靠近起点
					m_PtCreate2.Set(m_PtCreate1.x, pos.y); // 那么终点的x坐标与起点的相同
				else
					m_PtCreate2.Set(pos.x,m_PtCreate1.y);
			}
			else
				m_PtCreate2 = pos; // 如果未按下Shift键, 则终点为鼠标单击位置			
						
			COther*	pNewLine = new COther(m_PtCreate1,m_PtCreate2,m_strCreate);// 根据起点和终点创建直线
			pNewLine->Draw(pDC,dmNormal); // 绘制直线
			g_pDoc->m_EntityList.AddTail(pNewLine); // 将直线指针添加到图元链表
			g_pDoc->SetModifiedFlag(TRUE);// set modified flag ;
			
			g_pView->ReleaseDC(pDC); // 释放设备环境指针
			
			m_nStep = 0;  // 将操作步重置为 0
			::Prompt("请输入直线的起点：");
			// 如果将当前的终点设置为起点，并将操作步设置为1，即可绘制Ployline
			//	m_pBegin = m_pEnd;
			//	m_nStep = 0; 
			break;
		}
		
	}
	return 0;
}

int	CCreateOther::OnMouseMove(UINT nFlags, const Position& pos)
{
	::SetCursor(AfxGetApp()->LoadCursor(IDC_DRAW_LINE));

	// 用一静态变量nPreRefresh记录进入OnMouseMove状态时的刷新次数
	static	int nPreRefresh = g_nRefresh; 
	// 布尔变量bRefresh说明在OnMouseMove过程中视窗是否被刷新
	BOOL	bRefresh = FALSE; 
	// nCurRefresh用于记录当前的刷新次数
	int		nCurRefresh = g_nRefresh; 
	// 如果nCurRefresh和nPreRefresh不相等，说明视窗曾被刷新过
	if(nCurRefresh != nPreRefresh){ 
		bRefresh = TRUE;
		nPreRefresh = nCurRefresh; 
	}

	switch(m_nStep)
	{
		case 0:
			::Prompt("请输入直线的起点：");
			break;
		case 1:
		{
			Position	prePos, curPos;
			prePos = m_PtCreate2; // 获得鼠标所在的前一个位置
			
			// 如果在按鼠标左键的过程中同时按下了Shift键，
			//	那么根据鼠标单击位置绘制水平线或竖直线
			if( nFlags & MK_SHIFT )
			{
				double dx = pos.x - m_PtCreate1.x;
				double dy = pos.y - m_PtCreate1.y;
				if(fabs(dx)>=fabs(dy))
				{
					curPos.Set(pos.x,m_PtCreate1.y);
				}
				else 
				{
					curPos.Set(m_PtCreate1.x, pos.y);
				}
			}
			else
			{
				curPos = pos;
			}

			CDC*	pDC = g_pView->GetDC(); // 得到设备环境指针
			
			// 创建临时对象擦除上一条橡皮线
			COther*	pTempOther1 = new COther(m_PtCreate1, prePos,m_strCreate);
			if(!bRefresh) // 当视窗没有被刷新时，重画原来的橡皮线使其被擦除
				pTempOther1->Draw(pDC, dmDrag);

			delete pTempOther1;

			// 创建临时对象，根据当前位置绘制一条橡皮线
			COther*	pTempOther2 = new COther(m_PtCreate1, curPos,m_strCreate);	
			pTempOther2->Draw(pDC, dmDrag);
			delete pTempOther2;
 
			g_pView -> ReleaseDC(pDC); // 释放设备环境指针			

			m_PtCreate2 = curPos; // 将当前位置设置为直线终点，以备下一次鼠标移动时用
			break;
		}
	}
	return 0;
}
// 单击鼠标右键取消当前的操作
int	CCreateOther::OnRButtonDown(UINT nFlags, const Position& pos) 
{
	// 如果当前的操作步为 1 ，那么要在结束本次操作前擦除上次鼠标移动时绘制的橡皮线
	if(m_nStep == 1){ 
		CDC*	pDC = g_pView->GetDC(); // 得到设备环境指针
		Position	prePos = m_PtCreate2; // 获得鼠标所在的前一个位置
		COther*	pTempLine = new COther(m_PtCreate1, prePos,m_strCreate); 
		pTempLine->Draw(pDC, dmDrag); // 擦除上一次绘制的橡皮线
		delete pTempLine;
		g_pView->ReleaseDC(pDC); // 释放设备环境指针
	}
	m_nStep = 0; // 将操作步重置为 0 
	::Prompt("请输入直线的起点：");
	return 0;
}
// 调用Cancel 函数取消本次操作
int CCreateOther::Cancel()
{
	// 如果当前的操作步为 1 ，那么要在结束本次操作前擦除上次鼠标移动时绘制的橡皮线
	if(m_nStep == 1){ 
		CDC*	pDC = g_pView->GetDC(); // 得到设备环境指针
		Position	prePos = m_PtCreate2; // 获得鼠标所在的前一个位置
		COther*	pTempLine = new COther(m_PtCreate1, prePos,m_strCreate); 
		pTempLine->Draw(pDC, dmDrag); // 擦除上一次绘制的橡皮线
		delete pTempLine;
		g_pView->ReleaseDC(pDC); // 释放设备环境指针
	}
	m_nStep = 0; // 将操作步重置为 0 
	::Prompt("就绪"); // 等待提示新类型的命令操作
	return 0 ;
}


// 调用 xd_OnChar 函数 
int CCreateOther::xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{

	// 用一静态变量nPreRefresh记录进入OnMouseMove状态时的刷新次数
	static	int nPreRefresh = g_nRefresh; 
	// 布尔变量bRefresh说明在OnMouseMove过程中视窗是否被刷新
	BOOL	bRefresh = FALSE; 
	// nCurRefresh用于记录当前的刷新次数
	int		nCurRefresh = g_nRefresh; 
	// 如果nCurRefresh和nPreRefresh不相等，说明视窗曾被刷新过
	if(nCurRefresh != nPreRefresh){ 
		bRefresh = TRUE;
		nPreRefresh = nCurRefresh; 
	}

	switch(m_nStep)
	{
		case 0:
			::Prompt("请输入直线的起点：");
			break;
		case 1:
		{		 
			CString s;
			s.Format("%c",nChar);
			m_strCreate += s;

			CDC*	pDC = g_pView->GetDC(); // 得到设备环境指针
			
			// 创建临时对象擦除上一条橡皮线
			COther*	pTempOther1 = new COther(m_PtCreate1, m_PtCreate2,m_strCreate);
			if(!bRefresh) // 当视窗没有被刷新时，重画原来的橡皮线使其被擦除
				pTempOther1->Draw(pDC, dmDrag);
			delete pTempOther1;
			// 创建临时对象，根据当前位置绘制一条橡皮线
			COther*	pTempOther2 = new COther(m_PtCreate1, m_PtCreate2,m_strCreate);	
			pTempOther2->Draw(pDC, dmDrag);
			delete pTempOther2;
 
			g_pView->ReleaseDC(pDC); // 释放设备环境指针			

			break;
		}
	}
	return 0;
}
