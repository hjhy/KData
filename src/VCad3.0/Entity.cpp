#include "stdafx.h"
#include "math.h"
#include "VCad.h"
#include "VCadDoc.h"
#include "VCadView.h"
#include "Entity.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//////////////////////////////////
// API function
//

// 根据绘图模式设置绘图环境：设置设备环境的模式并创建画笔
void	SetDrawEnvir(CDC*	pDC, int drawMode, CPen* pPen)
{
	int			lineStyle, lineWidth;
	COLORREF	color;
	switch(drawMode) 
	{
		case dmSelect: // 选中状态
		{
			pDC->SetROP2(R2_COPYPEN);
			lineStyle = PS_SOLID;
			lineWidth = 1;
			color = RGB(255,0,0);
			break;
		}
		case dmPrompt: // 提示状态
		{
			pDC->SetROP2(R2_COPYPEN);
			lineStyle = PS_DASH;
			lineWidth = 1;
			color = RGB(0,255,255);
			break;
		}
		case dmDrag: // 拖动状态bvg
		{
			pDC->SetROP2(R2_XORPEN);
			lineStyle = PS_SOLID;
			lineWidth = 1;
			color = RGB(0,192,0);
			break;
		}
		case dmInvalid: // 擦除状态
		{
			pDC->SetROP2(R2_COPYPEN);
			lineStyle = PS_SOLID;
			lineWidth = 1;
			color = ::GetBkColor(*pDC); // 用背景色画
			break;
		}
		case dmNormal:   
		default:
		{
			pDC->SetROP2(R2_COPYPEN);
			lineStyle = PS_SOLID;
			lineWidth = 1;
			color = RGB(0,0,0);
			break;
		}		
	}
	pPen->CreatePen(lineStyle,lineWidth,color) ;
}

IMPLEMENT_SERIAL(CEntity, CObject, 0)	

CEntity::CEntity()
{
	m_type = etUnknow;		// EEntityType
	Init();
}

CEntity::CEntity(const CEntity& entity)
{
	m_color = entity.m_color;
	m_lineStyle = entity.m_lineStyle;
	m_lineWidth = entity.m_lineWidth;
	m_type		= entity.m_type;
}

void CEntity::Init()
{
	m_color = g_CurColor;
	m_lineStyle = g_CurLineStyle;
	m_lineWidth = g_CurLineWidth ;
}

CEntity CEntity::operator = (const CEntity& entity)
{
	m_color = entity.m_color;
	m_lineStyle = entity.m_lineStyle;
	m_lineWidth = entity.m_lineWidth;
	m_type		= entity.m_type;
	return *this;
}

void CEntity::Serialize(CArchive& ar)
{
	if(ar.IsStoring())
		ar << m_color << m_lineStyle << m_lineWidth ;
	else
		ar >> m_color >> m_lineStyle >> m_lineWidth ;
}

