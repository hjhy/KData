#ifndef _ENTITY_H_
#define _ENTITY_H_

#include "base.h"

#define PICK_RADIUS 0.01 // 选取半径

#define ENTITY_NUM	9 // 图元的种类数
enum EEntityType // 图元的类型
{
	etUnknow	= 0,
	etPoint		= 1,
	etLine		= 2,
	etRectangle = 3,
	etCircle	= 4,
	etArc		= 5,
	etBspline	= 6,
	etEllipse	= 7,
	etText		= 8,
	etOther		= 9	
};

enum EDrawMode // 绘图模式
{
	dmNormal = 0, // 正常状态
	dmSelect = 1, // 选中状态	
	dmPrompt = 2, // 提示状态
	dmDrag   = 3, // 拖动状态
	dmInvalid = 4 // 擦除状态
};

//////////////////////////
// define API function
//
void	SetDrawEnvir(CDC*	pDC, int drawMode, CPen* pPen);


class CEntity : public CObject
{
	DECLARE_SERIAL(CEntity) 
protected:
	// 成员变量
	int			m_type;		// 图元类型（EEntityType）
	COLORREF	m_color ;   // 图元颜色
	UINT		m_lineStyle ; // 图元的线型
	int			m_lineWidth ; // 图元的线宽	
public:
	// 构造函数和析构函数
	CEntity() ;
	CEntity(const CEntity& entity);
	~CEntity() {}
	
	CEntity operator = (const CEntity& entity); // 重载等号
	virtual	CEntity*	Copy() { return NULL; } // 指针拷贝

	virtual	void	Init(); // 初始化成员变量值
	virtual	int		GetType() { return m_type; }  // 返回图元的类型（EEntityType）
	COLORREF		GetColor() { return m_color; } // 返回图元颜色
	void			SetColor(COLORREF color) { m_color = color; } // 设置图元颜色
		
	virtual void Draw(CDC * pDC, int drawMode = dmNormal ) {}; // 绘制图元对象

	// 给定一点及拾取半径，判断图元是否被选中
	virtual BOOL Pick(const Position& pos, const double pick_radius) { return FALSE; } 
	// 得到对象的最小包围盒，该包围盒将被用于图元的选取和全屏显示
	virtual void GetBox(BOX2D* pBox){}
	// 给定一个基点和一个目标点平移图元
	virtual void Move(const Position& basePos,const Position& desPos) {}
	// 给定一个基点和一个角度值旋转图元
	virtual void Rotate(const Position& basePos, const double angle) {}
	// 给定两点镜像图元，布尔变量bCopy用于确定是否删除原来位置的图元
	virtual void Mirror(const Position& pos1, const Position& pos2){}
	// 改变光标
	virtual	void LoadPmtCursor() {}
	// Get a feature position of a entity near the giving position.
	// Input : giving a pos
	// output : TRUE or FALSE
	// Note : if true , the giving pos is reset to the feature position
	virtual BOOL GetSnapPos(Position& pos) { return FALSE; }

	virtual void Serialize(CArchive& ar) ; // 图元对象串行化
};

////////////////////////////
// CLASS CLine
//
class CLine : public CEntity
{
	DECLARE_SERIAL(CLine)
protected:
	Position	m_begin , m_end ; // 起点和终点
public:
	CLine() ;
	CLine(const CLine& line);
	CLine(const Position& begin,const Position& end);
	~CLine() ;

	CLine&	operator = (const CLine& line);
	CEntity*	Copy();

	int			GetType();
	void		Init(); 
	Position	GetBeginPos(); // 返回起点值
	Position	GetEndPos();   // 返回终点值
	
	void Draw(CDC * pDC, int drawMode = dmNormal ) ;

	//对直线的编辑操作：拾取，平移，旋转，镜向和获得最小包围盒
	BOOL Pick(const Position& pos, const double pick_radius) ;
	void GetBox(BOX2D* pBox);
	
	void Move(const Position& basePos,const Position& desPos);
	void Rotate(const Position& basePos, const double angle);
	void Mirror(const Position& pos1, const Position& pos2);
	
	BOOL GetSnapPos(Position& pos);
	void LoadPmtCursor();

	void Serialize(CArchive& ar) ;
};

////////////////////////////
// CLASS COther
//
class COther : public CEntity
{
	DECLARE_SERIAL(COther)
protected:
	Position	m_pt1 , m_pt2 ; // 起点和终点
	CString		m_str;
public:
	COther() ;
	COther(const COther& oher);
	COther(const Position& begin,const Position& end,const CString &str);
	~COther() ;

	COther&	operator = (const COther& oher);
	CEntity*	Copy();

	int			GetType();
	void		Init(); 
	Position	GetBeginPos(); // 返回起点值
	Position	GetEndPos();   // 返回终点值
	
	void Draw(CDC * pDC, int drawMode = dmNormal ) ;

	//对直线的编辑操作：拾取，平移，旋转，镜向和获得最小包围盒
	BOOL Pick(const Position& pos, const double pick_radius) ;
	void GetBox(BOX2D* pBox);
	
	void Move(const Position& basePos,const Position& desPos);
	void Rotate(const Position& basePos, const double angle);
	void Mirror(const Position& pos1, const Position& pos2);
	
	BOOL GetSnapPos(Position& pos);
	void LoadPmtCursor();

	void Serialize(CArchive& ar) ;
};
////////////////////////////
// CLASS CRectangle
//
class CRectangle : public CEntity
{
	DECLARE_SERIAL(CRectangle)
protected:
	Position	m_LeftTop , m_RightBottom ; // 起点和终点
public:
	CRectangle() ;
	CRectangle(const CRectangle& rect);
	CRectangle(const Position& LeftTop,const Position& RightBottom);
	~CRectangle() ;

	CRectangle&	operator = (const CRectangle& rect);
	CEntity*	Copy();

	int			GetType();
	void		Init(); 
	Position	GetLeftTopPos();		// 返回左上角的值
	Position	GetRightBottomPos();	// 返回右下角的值
	
	void Draw(CDC * pDC, int drawMode = dmNormal ) ;

	//对直线的编辑操作：拾取，平移，旋转，镜向和获得最小包围盒
	BOOL Pick(const Position& pos, const double pick_radius) ;
	void GetBox(BOX2D* pBox);
	
	void Move(const Position& basePos,const Position& desPos);
	void Rotate(const Position& basePos, const double angle);
	void Mirror(const Position& pos1, const Position& pos2);
	
	BOOL GetSnapPos(Position& pos);
	void LoadPmtCursor() ;

	void Serialize(CArchive& ar) ;
};

/////////////////
// Class CCircle
//
class CCircle : public CEntity
{
	DECLARE_SERIAL(CCircle)
protected:
	//member variables
	Position	m_center ;
	double		m_dRadius ;
public:
	// constructor and destructor
	CCircle() ;
	CCircle(const CCircle& circle);
	CCircle(const Position& center,const double& radius);
	CCircle(const Position& center,const Position& aroud);
	~CCircle() ;

	CEntity*	Copy();
	//-----------------------------------------------
	// member function
	// Attributes
	int			GetType();
	void		Init(); // initialize member variables
	Position	GetCenterPos();
	double		GetRadius();
	
	void Draw(CDC * pDC, int drawMode = dmNormal ) ;

	//对直线的编辑操作：拾取，平移，旋转，镜向和获得最小包围盒
	BOOL Pick(const Position& pos, const double pick_radius) ;
	void GetBox(BOX2D* pBox);
	
	void Move(const Position& basePos,const Position& desPos);
	void Rotate(const Position& basePos, const double angle);
	void Mirror(const Position& pos1, const Position& pos2);
	
	BOOL GetSnapPos(Position& pos);
	void LoadPmtCursor() ;

	void Serialize(CArchive& ar) ;
};

////////////////////////////
// CLASS CArc
//
class CArc : public CEntity
{
	DECLARE_SERIAL(CArc)
protected:
	//member variables
	Position	m_center ;
	Position	m_begin  ;
	Position    m_end    ;

public:
	// constructor and destructor
	CArc() ;
	CArc(const CArc& arc);
	CArc(const Position& center,const Position& startPos, const Position& endPos);
	~CArc() ;

	CEntity*	Copy();
	//-----------------------------------------------
	// member function
	// Attributes
	int			GetType();
	void		Init(); // initialize member variables
	Position	GetStartPos();
	Position	GetEndPos();
	Position	GetCenterPos() ;
	BOOL		GetSnapPos(Position& pos) ;

	void Draw(CDC * pDC, int drawMode = dmNormal ) ;

	//对直线的编辑操作：拾取，平移，旋转，镜向和获得最小包围盒
	BOOL Pick(const Position& pos, const double pick_radius) ;
	void Move(const Position& basePos,const Position& desPos) ;
	void Rotate(const Position& basePos, const double angle) ;
	void Mirror(const Position& FstPos, const Position& SecPos);
	void GetBox(BOX2D* pBox);

	void LoadPmtCursor();
	void Serialize(CArchive& ar) ;
};
#endif