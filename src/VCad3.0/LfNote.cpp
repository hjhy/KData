// LfNote.cpp: implementation of the CLfNote class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "LfBox.h"
#include "LfNote.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLfNote::CLfNote()
{
	m_strNote = "";
	m_nX = 0;
	m_nY = 0;
}

CLfNote::CLfNote(CString str,int x, int y)
{
	Set(str,x,y);
}

CLfNote::~CLfNote()
{

}

void CLfNote::Draw(CDC *pDC)
{
//	pDC->TextOut(m_nX,m_nY,m_strNote);
	CString str = m_strNote;   
	 CString* pStr;     
	 int iSubStrs;     
	 pStr = zz_SplitString(str, ';', iSubStrs);     
	 //如果子字符串的数量为1     
	 if (iSubStrs == 1)    
	 {  
		 Draw1Note(pDC,str,m_nX,m_nY,30);
		 
	 }     
	 else   
	 {             //输出所有子字符串     
		 for (int i = 0; i < iSubStrs; i++) 
		 {           		  
			 Draw1Note(pDC,pStr[i],m_nX,m_nY,30);
			 m_nX += 12;//pStr[i].GetLength()*15; //音符长度
		 }      
		 delete []pStr;     
	 } 

}

void CLfNote::Set(CString str,int x, int y)
{
	m_strNote = str;
	m_nX = x;
	m_nY = y;
}

void CLfNote::Test()
{
	 CString str = m_strNote;   
	 CString* pStr;     
	 int iSubStrs;     
	 pStr = zz_SplitString(str, ' ', iSubStrs);     
	 //如果子字符串的数量为1     
	 if (iSubStrs == 1)    
	 {         //Convert CString to char         
		 char* pCh = (LPSTR)(LPCTSTR)str;         
		 printf("%s\n", pCh);  
		 
	 }     
	 else   
	 {             //输出所有子字符串     
		 for (int i = 0; i < iSubStrs; i++) 
		 {             //Convert CString to char   
			 char* pCh = (LPSTR)(LPCTSTR)pStr[i];       
			 printf("%s\n", pCh);   
		 }      
		 delete []pStr;     
	 } 


}

// 
CString * CLfNote::zz_SplitString(CString str, char split, int& iSubStrs)
{     
	int iPos = 0; //分割符位置
	int iNums = 0; //分割符的总数
	CString strTemp = str;
	CString strRight;     //先计算子字符串的数量     
	while (iPos != -1)     
	{         
		iPos = strTemp.Find(split);         
		if (iPos == -1)         
		{             
			break;         
		}         
		strRight = strTemp.Mid(iPos + 1, str.GetLength());         
		strTemp = strRight;         
		iNums++;     
	}     
	if (iNums == 0) //没有找到分割符     
	{         //子字符串数就是字符串本身         
		iSubStrs = 1;          
		return NULL;     
	}     
	//子字符串数组     
	iSubStrs = iNums + 1; //子串的数量 = 分割符数量 + 1     
	CString* pStrSplit;    
	pStrSplit = new CString[iSubStrs];     
	strTemp = str;     
	CString strLeft;     
	for (int i = 0; i < iNums; i++)     
	{         
		iPos = strTemp.Find(split);         //左子串         
		strLeft = strTemp.Left(iPos);         //右子串         
		strRight = strTemp.Mid(iPos + 1, strTemp.GetLength());        
		strTemp = strRight;         
		pStrSplit[i] = strLeft;     
	}     
	pStrSplit[iNums] = strTemp;     
	return pStrSplit; 
} 

void CLfNote::z_DrawMusicNote(CDC *pDC,CString strIn,int nX,int nY)
{ 
	//时值线
	int nT = zz_Draw_Note_Time(pDC,strIn,nX,nY);

	//音调
	int nTone = zz_Draw_Note_Tone(pDC,strIn,nX,nY,nT);

	//前连音线
	int nB = zz_Draw_B_Note_Line(pDC,strIn,nX,nY);
	//后连音线
	int nA = zz_Draw_A_Note_Line(pDC,strIn,nX,nY);

}

void CLfNote::Draw1Note(CDC *pDC,CString strIn,int nX,int nY,int nDy)
{
	CString str = strIn;   
	CString* pStr;     
	int iSubStrs;     
	pStr = zz_SplitString(str, '^', iSubStrs);  
	
	//如果子字符串的数量为1     
	if (iSubStrs == 1)    
	{ 
	//	 str="("+str+")"; //音符
	//	 pDC->TextOut(nX,nY,str);
		 z_DrawMusicNote(pDC,str,nX,nY);
		 
	}     
	else   
	{             
		
	//	 pStr[0]="("+pStr[0]+")"; //音符
	//	 pDC->TextOut(nX,nY,pStr[0]); 
		 z_DrawMusicNote(pDC,str,nX,nY);
		 //输出所有子字符串     
		 for (int i = 1; i < iSubStrs; i++) 
		 {    
			 
		//	 pStr[i]="<"+pStr[i]+">";  //歌词
			 pDC->TextOut(nX,nY-i*nDy+10,pStr[i]); 
		 }      
		 delete []pStr;     
	} 

}

void CLfNote::z_DrawPoint(CDC *pDC, int nX, int nY)
{
	
	CBrush   brush(RGB(0,0,0));  	//红色 
	CBrush     *old   =   pDC->SelectObject(&brush);   
	CRect   rect;    rect.SetRect(nX,nY,nX+1,nY+2);  //圆心是(100,100) 
	rect.InflateRect(1,1);        //半径是50 
	pDC->Ellipse(rect);   
	pDC->SelectObject(old);    
 

}

int CLfNote::zz_Draw_Note_Tone(CDC *pDC,CString strIn,int nX,int nY,int nT)
{
	
	char* pCh = (LPSTR)(LPCTSTR)strIn;  
	int nLen = strIn.GetLength();
	if(nLen<1)  return 1;
	char c0 = strIn.GetAt(0);
	pDC->TextOut(nX,nY,c0);
	

	if(nLen<2)  return 2;
	char c1 = strIn.GetAt(1); 

	//*
	if(','==c1)
	{
		z_DrawPoint(pDC,nX+3,nY+15+3+nT*3);
	} 
	else if('\''==c1)
	{
		z_DrawPoint(pDC,nX+3,nY);
	}
	//*/
	if(nLen<3)  return 3;
	char c2 = strIn.GetAt(2); 

	//*
	if(','==c2)
	{
		z_DrawPoint(pDC,nX+3,nY+15+6+nT*3);
	} 
	else if('\''==c2)
	{
		z_DrawPoint(pDC,nX+3,nY-3);
	}
	//*/
	return 0;
}
int CLfNote::zz_Draw_Note_Time(CDC *pDC,CString strIn,int nX,int nY)
{
	
	int nT;
	zz_SplitString(strIn,'/',nT);
	nT--;
	for(int n=0; n < nT; n++)
	{
		int nDy = 3*n + 22; 
		int nDx = 12;
		CPen new2Pen(PS_SOLID, 2, RGB(0,0,0));                //创建绿色画笔
		CPen* old2Pen = pDC->SelectObject(&new2Pen);         //将红色画笔选入DC
		
		pDC->MoveTo(nX,nY-nDy); pDC->LineTo(nX+nDx,nY-nDy);
		
		pDC->SelectObject(old2Pen); 
	}
	return nT;
}

int CLfNote::zz_Draw_B_Note_Line(CDC *pDC,CString strIn,int nX,int nY)
{
	
	int nRet=0; 
	zz_SplitString(strIn,'~',nRet);
	nRet--; 
	//*
	int ndx=10;
	int ndy=5;
	int x1=nX;
	int y1=nY;
	int x2=nX-ndx*2;
	int y2=nY-ndy*2;
	int x3=nX;
	int y3=nY-ndy;
	int x4=nX-ndx;
	int y4=nY-ndy*2;
	int x5=x2;
	int y5=y3;
	
	CPen new1Pen(PS_SOLID, 2, RGB(0,0,0));             //创建绿色画笔
	CPen* old1Pen = pDC->SelectObject(&new1Pen);         //将红色画笔选入DC
	for(int n=0; n < nRet; n++)
	{ 
		
		if(0==n)
		{
			//用Arc()函数画弧线 
			pDC->Arc(x1,y1,x2,y2,x3,y3,x4,y4);
			pDC->SelectObject(old1Pen); 
			
		}
		else if(1<=n && n<(nRet-1))
		{
			CPen new2Pen(PS_SOLID, 2, RGB(0,0,0));         //创建绿色画笔
			CPen* old2Pen = pDC->SelectObject(&new2Pen);         //将红色画笔选入DC
			pDC->MoveTo(x4-(n-1)*ndx,y4); 
			pDC->LineTo(x4-n*ndx,y4); 
			pDC->SelectObject(old2Pen); 
		}
		else if(n==(nRet-1))
		{ 
			pDC->Arc(x1-(n-1)*ndx,y1,x2-(n-1)*ndx,y2,x4-(n-1)*ndx,y4,x3-(n+2)*ndx,y3);
			 
		}
	}
	//*/
	return nRet;
}

int CLfNote::zz_Draw_A_Note_Line(CDC *pDC,CString strIn,int nX,int nY)
{
	
	int nRet=0; 
	return nRet;
}
