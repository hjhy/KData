// LfNote.h: interface for the CLfNote class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LFNOTE_H__86A2B4E3_6DF2_4715_AFE9_2DD6E7037A0B__INCLUDED_)
#define AFX_LFNOTE_H__86A2B4E3_6DF2_4715_AFE9_2DD6E7037A0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//������ CLfNote

class CLfNote  
{
public:
	void Draw1Note(CDC *pDC,CString strIn,int nX,int nY,int nDy);
	void Test();
	CLfNote();
	CLfNote(CString str,int x, int y);
	virtual ~CLfNote();
	void Set(CString str,int x = 0, int y = 0);
	void Draw(CDC* pDC);

private:
	int zz_Draw_B_Note_Line(CDC *pDC,CString strIn,int nX,int nY);
	int zz_Draw_A_Note_Line(CDC *pDC,CString strIn,int nX,int nY);
	int zz_Draw_Note_Time(CDC *pDC,CString strIn,int nX,int nY);
	int zz_Draw_Note_Tone(CDC *pDC,CString strIn,int nX,int nY,int nT);
	void z_DrawMusicNote(CDC *pDC,CString strIn,int nX,int nY);
	void z_DrawPoint(CDC *pDC,int nX,int nY);
	CString		m_strNote;
	int			m_nX,m_nY;
	CStringList m_sl_Note;

	CString *	zz_SplitString(CString str, char split, int& iSubStrs);
};

#endif // !defined(AFX_LFNOTE_H__86A2B4E3_6DF2_4715_AFE9_2DD6E7037A0B__INCLUDED_)
