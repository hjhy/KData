// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "VCad.h"

#include "MainFrm.h"
#include "Splash.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
		ID_SEPARATOR,
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};
static UINT PropertyButtons[] = 
{
//	ID_SETTING_GRID,
//	ID_SETTING_ORTH,
	ID_PROPERTY,	//0
	ID_SEPARATOR,	//1
	ID_SEPARATOR,	//2:LINESTYLE
	ID_SEPARATOR,	//3
	ID_SEPARATOR,	//4:LINECOLOR
	ID_SEPARATOR,	//5
	ID_SEPARATOR,	//6:LINEWIDTH
};

//--------------------------------------------------------//
//--------------------------------------------------------//
// 全局函数：Prompt
// 功能：在状态栏上给出操作过程的提示信息
void Prompt(char* pmt)
{
	CStatusBar* pStatus=(CStatusBar*)
		AfxGetApp()->m_pMainWnd->GetDescendantWindow(ID_VIEW_STATUS_BAR);
	
	ASSERT(pStatus) ;
	if(pStatus)
		pStatus->SetPaneText(0, pmt, TRUE);
}
//--------------------------------------------------------//
//--------------------------------------------------------//

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
	if(!m_wndModifyBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndModifyBar.LoadToolBar(IDR_MODIFY))
	{
		TRACE0("Failed to create modify toolbar\n");
		return -1;      // fail to create
	}
	//设置修改工具栏的标题
	m_wndModifyBar.SetWindowText(_T("修改工具栏"));

	if(!m_wndCreateBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_LEFT
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndCreateBar.LoadToolBar(IDR_CREATE))
	{
		TRACE0("Failed to create modify toolbar\n");
		return -1;      // fail to create
	}
	//设置绘制工具栏的标题
	m_wndCreateBar.SetWindowText(_T("绘制工具栏"));

	if(!m_wndViewBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_LEFT
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndViewBar.LoadToolBar(IDR_VIEW))
	{
		TRACE0("Failed to create modify toolbar\n");
		return -1;      // fail to create
	}
	m_wndViewBar.SetWindowText(_T("视图工具栏"));

	if(!CreatePropertyBar())
		return -1;      // fail to create
	m_wndPropBar.SetWindowText(_T("属性工具栏")) ;

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndModifyBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndCreateBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndViewBar.EnableDocking(CBRS_ALIGN_ANY) ;
	m_wndPropBar.EnableDocking(CBRS_ALIGN_ANY) ;
	EnableDocking(CBRS_ALIGN_ANY);

	DockControlBar(&m_wndToolBar);
	DockControlBar(&m_wndModifyBar) ;

	//////////important !!!!!!!
	DockControlBarLeftOf(&m_wndCreateBar, &m_wndToolBar);
	DockControlBarLeftOf(&m_wndPropBar, &m_wndModifyBar);
	DockControlBarLeftOf(&m_wndViewBar, &m_wndPropBar) ;

	// CG: The following line was added by the Splash Screen component.
	CSplashWnd::ShowSplashScreen(this);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
BOOL CMainFrame::CreatePropertyBar()
{
	const int nDropHeight = 100 ;
	if(!m_wndPropBar.Create(this, WS_CHILD|WS_VISIBLE|CBRS_LEFT|
		CBRS_SIZE_DYNAMIC|CBRS_TOOLTIPS|CBRS_FLYBY, IDR_PROPERTYBAR)||
		!m_wndPropBar.LoadBitmap(IDR_PROPERTYBAR)||
		!m_wndPropBar.SetButtons(PropertyButtons,sizeof(PropertyButtons)/sizeof(UINT)))	
	{
		TRACE0("Failed to create propertybar\n") ;
		return FALSE ;
	}
	//设置 IDW_LINESTYLE
	m_wndPropBar.SetButtonInfo(1,ID_SEPARATOR, TBBS_SEPARATOR, 12) ;
	//设置线型组合框的宽度
	m_wndPropBar.SetButtonInfo(2,IDW_LINESTYLE , TBBS_SEPARATOR, 150) ;
	//
	m_wndPropBar.SetButtonInfo(3,ID_SEPARATOR, TBBS_SEPARATOR, 12) ;
	////设置颜色组合框的宽度
	m_wndPropBar.SetButtonInfo(4,IDW_COLOR, TBBS_SEPARATOR,100) ;
	//
	m_wndPropBar.SetButtonInfo(5,ID_SEPARATOR, TBBS_SEPARATOR, 12) ;
	//设置线宽组合框的宽度
	m_wndPropBar.SetButtonInfo(6,IDW_LINEWIDTH , TBBS_SEPARATOR, 150) ;

	CRect rc ;
	/*绘制线型组合框*/
	//得到线型组合框的矩形框
	m_wndPropBar.GetItemRect(2,&rc) ;
	rc.bottom = rc.top + nDropHeight ;
	//创建线型组合框
	if(!m_wndPropBar.m_wndLineStyleCmb.Create(
		CBS_DROPDOWNLIST|WS_VISIBLE|WS_TABSTOP|
		WS_VSCROLL|CBS_OWNERDRAWVARIABLE,
		rc, &m_wndPropBar, IDW_LINESTYLE))
	{
		TRACE0("Failed to create linestyle combo--box!") ;
		return FALSE ;
	}
	//在线型组合框添加选项
	m_wndPropBar.m_wndLineStyleCmb.AddItem(PS_SOLID, "Solid") ;
	m_wndPropBar.m_wndLineStyleCmb.AddItem(PS_DASH, "Dash") ;
	m_wndPropBar.m_wndLineStyleCmb.AddItem(PS_DOT, "Dot") ;
	m_wndPropBar.m_wndLineStyleCmb.AddItem(PS_DASHDOT, "DashDot") ;
	m_wndPropBar.m_wndLineStyleCmb.AddItem(PS_DASHDOTDOT, "DashDotDot") ;
	m_wndPropBar.m_wndLineStyleCmb.AddItem(PS_NULL, "Null") ;
	m_wndPropBar.m_wndLineStyleCmb.AddItem(PS_INSIDEFRAME, "InsideFrame") ;
	//设置当前缺省的选项
	m_wndPropBar.m_wndLineStyleCmb.SetCurSel(0) ;
	
	/*绘制颜色组合框*/
	m_wndPropBar.GetItemRect(4, &rc) ;
	rc.bottom = rc.top + nDropHeight ;
	//创建颜色组合框
	if(!m_wndPropBar.m_wndColorCmb.Create(
		CBS_DROPDOWNLIST|WS_VISIBLE|WS_TABSTOP|
		WS_VSCROLL|CBS_OWNERDRAWVARIABLE,
		rc, &m_wndPropBar,IDW_COLOR))
	{
		TRACE0("Failed to create combo--box!\n") ;
		return FALSE ;
	}
	//在颜色组合框添加选项
	m_wndPropBar.m_wndColorCmb.AddItem(RGB(255,0,0),"red") ;
	m_wndPropBar.m_wndColorCmb.AddItem(RGB(0,255,0),"Green") ;
	m_wndPropBar.m_wndColorCmb.AddItem(RGB(0,0,255),"Blue") ;
	m_wndPropBar.m_wndColorCmb.AddItem(RGB(255,255,0),"Yellow") ;
	m_wndPropBar.m_wndColorCmb.AddItem(RGB(0,255,255),"LightBlue") ;
	m_wndPropBar.m_wndColorCmb.AddItem(RGB(255,0,255),"Cyan") ;
	m_wndPropBar.m_wndColorCmb.AddItem(RGB(0,0,0),"Black") ;
	//设置当前缺省的选项
	m_wndPropBar.m_wndColorCmb.SetCurSel(6) ;

	/*绘制线宽组合框*/
	//得到线型组合框的矩形框
	m_wndPropBar.GetItemRect(6,&rc) ;
	rc.bottom = rc.top + nDropHeight ;
	//创建线型组合框
	if(!m_wndPropBar.m_wndLineWidthCmb.Create(
		CBS_DROPDOWNLIST|WS_VISIBLE|WS_TABSTOP|
		WS_VSCROLL|CBS_OWNERDRAWVARIABLE,
		rc, &m_wndPropBar, IDW_LINEWIDTH))
	{
		TRACE0("Failed to create lineWidth combo--box!") ;
		return FALSE ;
	}
	//在线型组合框添加选项
	m_wndPropBar.m_wndLineWidthCmb.AddItem(1, "线宽1") ;
	m_wndPropBar.m_wndLineWidthCmb.AddItem(2, "线宽2") ;
	m_wndPropBar.m_wndLineWidthCmb.AddItem(3, "线宽3") ;
	m_wndPropBar.m_wndLineWidthCmb.AddItem(4, "线宽4") ;
//	m_wndPropBar.m_wndLineStyleCmb.AddItem(PS_DASHDOTDOT, "DashDotDot") ;
//	m_wndPropBar.m_wndLineStyleCmb.AddItem(PS_NULL, "Null") ;
//	m_wndPropBar.m_wndLineStyleCmb.AddItem(PS_INSIDEFRAME, "InsideFrame") ;
	//设置当前缺省的选项
	m_wndPropBar.m_wndLineWidthCmb.SetCurSel(0) ;
	
	return TRUE ;
}
void CMainFrame::DockControlBarLeftOf(CControlBar* Bar,CControlBar* LeftOf)
{
	CRect rect;
	DWORD dw;
	UINT n;

	// get MFC to adjust the dimensions of all docked ToolBars
	// so that GetWindowRect will be accurate
	RecalcLayout();
	LeftOf->GetWindowRect(&rect);
	rect.OffsetRect(1,0);
	dw=LeftOf->GetBarStyle();
	n = 0;
	n = (dw&CBRS_ALIGN_TOP) ? AFX_IDW_DOCKBAR_TOP : n;
	n = (dw&CBRS_ALIGN_BOTTOM && n==0) ? AFX_IDW_DOCKBAR_BOTTOM : n;
	n = (dw&CBRS_ALIGN_LEFT && n==0) ? AFX_IDW_DOCKBAR_LEFT : n;
	n = (dw&CBRS_ALIGN_RIGHT && n==0) ? AFX_IDW_DOCKBAR_RIGHT : n;

	// When we take the default parameters on rect, DockControlBar will dock
	// each Toolbar on a seperate line.  By calculating a rectangle, we in effect
	// are simulating a Toolbar being dragged to that location and docked.
	DockControlBar(Bar,n,&rect);
}