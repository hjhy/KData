/*	
 *	Header File of Class CModifyCmd
 */
#ifndef _ModifyCmd_h_
#define _ModifyCmd_h_

#include "base.h"

#ifdef __cplusplus

class CVPlotDoc ;
class CVPlotView ;
///////////////////////////////////////////////////////////////
/*	
 *	CMove
 */
class CMove : public CCommand
{
private:
	Position m_basePos;
	Position m_desPos;
public:
	CMove() ;
	~CMove() ;
	
	int		GetType();
	int		OnLButtonDown(UINT nFlags, const Position& pos) ;
	int		OnMouseMove(UINT nFlags, const Position& pos) ;
	int		OnRButtonDown(UINT nFlags, const Position& pos) ;

	int		Cancel() ;
	int		xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
} ;
///////////////////////////////////////////////////////////////
/*	
 *	CRotate
 */
class CRotate : public CCommand
{
private:
	Position m_basePos;
	Position m_desPos;
public:
	CRotate() ;
	~CRotate() ;
	
	int		GetType();
	int		OnLButtonDown(UINT nFlags, const Position& pos) ;
	int		OnMouseMove(UINT nFlags, const Position& pos) ;
	int		OnRButtonDown(UINT nFlags, const Position& pos) ;

	int		Cancel() ;
	int		xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
} ;
///////////////////////////////////////////////////////////////
/*	
 *	CMirror
 */

class CMirror : public CCommand
{
private:
	Position m_basePos;
	Position m_desPos;
public:
	CMirror() ;
	~CMirror() ;
	
	int		GetType();
	int		OnLButtonDown(UINT nFlags, const Position& pos) ;
	int		OnMouseMove(UINT nFlags, const Position& pos) ;
	int		OnRButtonDown(UINT nFlags, const Position& pos) ;

	int		Cancel() ;
	int		xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
} ;
///////////////////////////////////////////////////////////////
/*	
 *	CViewPanCmd
 */
class CViewPanCmd : public CCommand
{
private:
	int		 m_nStep ;
	Position m_basePos ;
	Position m_desPos ;
public:
	CViewPanCmd() ;
	~CViewPanCmd() ;

	int		GetType();
	int		OnLButtonDown(UINT nFlags, const Position& pos) ;
	int		OnMouseMove(UINT nFlags, const Position& pos) ;
	int		OnRButtonDown(UINT nFlags, const Position& pos) ;

	int		Cancel() ;
	int		xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
};
///////////////////////////////////////////////////////////////
/*	
 *	CZoomRgnCmd
 */
class CZoomRgnCmd : public CCommand
{
private:
	int		 m_nStep ;
	Position m_RgnCorner ;
	Position m_RgnCorner1 ;
public:
	CZoomRgnCmd() ;
	~CZoomRgnCmd() ;

	int		GetType();
	int		OnLButtonDown(UINT nFlags, const Position& pos) ;
	int		OnMouseMove(UINT nFlags, const Position& pos) ;
	int		OnRButtonDown(UINT nFlags, const Position& pos) ;

	int		Cancel() ;
	int		xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
};
///////////////////////////////////////////////////////////////
#endif // __cplusplus

#endif // _ModifyCmd_h_
