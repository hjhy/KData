#include "stdafx.h"
#include "math.h"
#include "VCad.h"
#include "VCadDoc.h"
#include "VCadView.h"
#include "Entity.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_SERIAL(COther, CEntity, 0)

COther::COther()
{
	Init();
}

COther::COther(const COther& other)
		: CEntity(other)
{
	m_pt1 = other.m_pt1;
	m_pt2	 = other.m_pt2;
	m_str	= other.m_str;
}

COther::COther(const Position& begin,const Position& end,const CString &str)
{
	Init();
	m_pt1 = begin ;
	m_pt2 = end ;
	m_str	= str;
}

COther::~COther()
{
}

COther& COther::operator = (const COther& other)
{
	// 处理特殊情况：other1 = other1
	if(this == &other)
		return *this;
	// 一般情形：other2 = other1
	CEntity::operator = (other); // 调用基类的重载“=”操作
	m_pt1 = other.m_pt1;
	m_pt2 = other.m_pt2;
	m_str	= other.m_str;
	return *this;
}

CEntity* COther::Copy()
{
	COther*	pEntity = new COther(m_pt1, m_pt2,m_str);
	return pEntity;
}

void COther::Init()
{
	CEntity::Init();
	m_type = etLine;
	m_pt1.Init();
	m_pt2.Init();
	m_str="txt";
}

int	COther::GetType()
{
	return etOther;
}

Position COther::GetBeginPos()
{
	return m_pt1;
}

Position COther::GetEndPos()
{
	return m_pt2;
}

#include "LfNote.h"

void COther::Draw(CDC * pDC, int drawMode /* = dmNormal */)
{
	CPoint pt_begin, pt_end; // 屏幕坐标的起点和终点
	g_pView->WorldtoScreen(m_pt1,pt_begin); // 将世界坐标转化为屏幕坐标
	g_pView->WorldtoScreen(m_pt2,pt_end) ;

	int		n = GetROP2(pDC->GetSafeHdc()); // 得到原来的绘图模式
	CPen	pen; 
	if( drawMode == dmNormal )  // 如果为正常状态的绘制，根据成员变量创建画笔
		pen.CreatePen(m_lineStyle,m_lineWidth,m_color) ;
	else // 非正常状态调用SetDrawEnvir函数设置绘图环境
		::SetDrawEnvir(pDC, drawMode, &pen);
	
	CPen* pOldPen = pDC->SelectObject(&pen); // 得到原来的画笔
	pDC->SetMapMode(MM_LOENGLISH); 
	
	pDC->Rectangle(pt_begin.x,pt_begin.y,pt_end.x,pt_end.y);
	pDC->MoveTo(pt_begin); // 根据屏幕坐标绘制图元
	pDC->LineTo(pt_end);
	pDC->TextOut(pt_begin.x-10,pt_begin.y+40,m_str);

	CLfNote lfn1(m_str,pt_begin.x,pt_begin.y-20); 
	lfn1.Draw(pDC);

	pDC->SelectObject(pOldPen); // 恢复原来的画笔 
	pDC->SetROP2(n); // 恢复原来的绘图模式
}

BOOL COther::Pick(const Position& pos, const double pick_radius)
{
	Position objPos = pos;
	BOX2D sourceBox,desBox;
	GetBox(&sourceBox); // 得到直线段的最小包围盒
	// 将最小包围盒向四周放大，得到测试包围盒
	desBox.min[0] = sourceBox.min[0] - pick_radius;
	desBox.min[1] = sourceBox.min[1] - pick_radius;
	desBox.max[0] = sourceBox.max[0] + pick_radius;
	desBox.max[1] = sourceBox.max[1] + pick_radius;
	// 判断拾取点是否在测试包围盒中，如果不是，则图元未被选中
	if( !objPos.IsInBox(desBox) )
		return FALSE;
	double angle = ::GetAngleToXAxis(m_pt1,m_pt2);
	// DIST = fabs(X * cos(a) + Y * sin(a) - P)
	Position dp = objPos - m_pt1;
	double dist = fabs(dp.x*cos(angle) + dp.y*sin(angle) - objPos.Distance(m_pt1));
	if(dist <= pick_radius)
		return TRUE;
	return FALSE;
}

void COther::GetBox(BOX2D* pBox)
{
	pBox->min[0] = min( m_pt1.x, m_pt2.x );
	pBox->min[1] = min( m_pt1.y, m_pt2.y );
	pBox->max[0] = max( m_pt1.x, m_pt2.x );
	pBox->max[1] = max( m_pt1.y, m_pt2.y );
}

void COther::Move(const Position& basePos,const Position& desPos)
{
	m_pt1 = m_pt1.Offset(desPos - basePos);
	m_pt2 = m_pt2.Offset(desPos - basePos);
}

void COther::Rotate(const Position& basePos, const double angle)
{
	m_pt1 = m_pt1.Rotate(basePos, angle) ;
	m_pt2 =m_pt2.Rotate(basePos, angle) ;
}

void COther::Mirror(const Position& pos1, const Position& pos2)
{
	m_pt1 = m_pt1.Mirror(pos1, pos2) ;
	m_pt2 =m_pt2.Mirror(pos1, pos2) ;
}

void COther::LoadPmtCursor() 
{
	::SetCursor(AfxGetApp()->LoadCursor(IDC_PROMPT_OTHER));
}


BOOL COther::GetSnapPos(Position& pos)
{
	BOOL ret = FALSE;
	
	Position p[3]; // feature position: start pt, end pt, mid pt
	p[0] = m_pt1;
	p[1] = m_pt2;
	p[2] = (p[0] + p[1]) * 0.5;

	for( int i=0; i<3; i++ ){
		if( pos.Distance(p[i]) < 0.5 ){
			pos = p[i];
			ret = TRUE;
			break;
		}
	}
	return ret;
}

void COther::Serialize(CArchive& ar) 
{
	CEntity::Serialize(ar);
	m_pt1.Serialize(ar);
	m_pt2.Serialize(ar);

	if(ar.IsStoring())
		ar << m_str;
	else
		ar >> m_str;
}

