; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CVCadView
LastTemplate=CComboBox
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "vcad.h"
LastPage=0

ClassCount=10
Class1=CMainFrame
Class2=CVCadApp
Class3=CAboutDlg
Class4=CVCadDoc
Class5=CVCadView

ResourceCount=6
Resource1=IDR_VIEW
Class6=CSplashWnd
Class7=CPropertyBar
Class8=CLineStyleCmb
Class9=CColorCmb
Class10=CLineWidthCmb
Resource2=IDR_MODIFY
Resource3=IDR_CREATE
Resource4=IDR_MAINFRAME
Resource5=IDR_PROPERTYBAR
Resource6=IDD_ABOUTBOX

[CLS:CMainFrame]
Type=0
BaseClass=CFrameWnd
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp

[CLS:CVCadApp]
Type=0
BaseClass=CWinApp
HeaderFile=VCad.h
ImplementationFile=VCad.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=VCad.cpp
ImplementationFile=VCad.cpp
LastObject=CAboutDlg

[CLS:CVCadDoc]
Type=0
BaseClass=CDocument
HeaderFile=VCadDoc.h
ImplementationFile=VCadDoc.cpp
Filter=N
VirtualFilter=DC
LastObject=CVCadDoc

[CLS:CVCadView]
Type=0
BaseClass=CView
HeaderFile=VCadView.h
ImplementationFile=VCadView.cpp
Filter=C
VirtualFilter=VWC
LastObject=ID_CREATE_LINE

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
CommandCount=8

[MNU:IDR_MAINFRAME]
Type=1
Class=CVCadDoc
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=IDM_SAVE_DXF
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_PRINT
Command7=ID_FILE_PRINT_PREVIEW
Command8=ID_FILE_PRINT_SETUP
Command9=ID_FILE_MRU_FILE1
Command10=ID_APP_EXIT
Command11=ID_CREATE_LINE
Command12=ID_CREATE_RECTANGLE
Command13=ID_CREATE_CIRCLE
Command14=ID_CREATE_ARC
Command15=ID_CREATE_OTHER
Command16=ID_PICK
Command17=ID_REDRAW
Command18=ID_MODIFY_MOVE
Command19=ID_MODIFY_ROTATE
Command20=ID_MODIFY_MIRROR
Command21=ID_MODIFY_ERASE
Command22=ID_VIEW_PAN
Command23=ID_VIEW_ZOOMIN
Command24=ID_VIEW_ZOOMOUT
Command25=ID_VIEW_ZOOMALL
Command26=ID_VIEW_ZOOMRGN
Command27=ID_VIEW_TOOLBAR
Command28=ID_VIEW_STATUS_BAR
Command29=ID_APP_ABOUT
CommandCount=29

[ACL:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[CLS:CSplashWnd]
Type=0
HeaderFile=Splash.h
ImplementationFile=Splash.cpp
BaseClass=CWnd
LastObject=IDM_SAVE_DXF

[CLS:CPropertyBar]
Type=0
HeaderFile=PropertyBar.h
ImplementationFile=PropertyBar.cpp
BaseClass=CToolBarCtrl
Filter=W

[CLS:CLineStyleCmb]
Type=0
HeaderFile=propertybar.h
ImplementationFile=propertybar.cpp
BaseClass=CComboBox
Filter=W
VirtualFilter=cWC
LastObject=CLineStyleCmb

[CLS:CColorCmb]
Type=0
HeaderFile=propertybar.h
ImplementationFile=propertybar.cpp
BaseClass=CComboBox
Filter=W
VirtualFilter=cWC
LastObject=CColorCmb

[CLS:CLineWidthCmb]
Type=0
HeaderFile=propertybar.h
ImplementationFile=propertybar.cpp
BaseClass=CComboBox
Filter=W
VirtualFilter=cWC
LastObject=CLineWidthCmb

[TB:IDR_CREATE]
Type=1
Class=?
Command1=ID_CREATE_LINE
Command2=ID_CREATE_RECTANGLE
Command3=ID_CREATE_CIRCLE
Command4=ID_CREATE_ARC
Command5=ID_CREATE_OTHER
CommandCount=5

[TB:IDR_MODIFY]
Type=1
Class=?
Command1=ID_PICK
Command2=ID_MODIFY_MOVE
Command3=ID_MODIFY_MIRROR
Command4=ID_MODIFY_ROTATE
Command5=ID_REDRAW
Command6=ID_MODIFY_ERASE
CommandCount=6

[TB:IDR_VIEW]
Type=1
Class=?
Command1=ID_VIEW_PAN
Command2=ID_VIEW_ZOOMIN
Command3=ID_VIEW_ZOOMOUT
Command4=ID_VIEW_ZOOMALL
Command5=ID_VIEW_ZOOMRGN
CommandCount=5

[TB:IDR_PROPERTYBAR]
Type=1
Class=?
Command1=ID_PROPERTY
CommandCount=1

