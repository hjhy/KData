# Microsoft Developer Studio Generated NMAKE File, Based on VCad.dsp
!IF "$(CFG)" == ""
CFG=VCad - Win32 Debug
!MESSAGE No configuration specified. Defaulting to VCad - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "VCad - Win32 Release" && "$(CFG)" != "VCad - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "VCad.mak" CFG="VCad - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "VCad - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "VCad - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "VCad - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\VCad.exe"


CLEAN :
	-@erase "$(INTDIR)\Base.obj"
	-@erase "$(INTDIR)\CreateLine.obj"
	-@erase "$(INTDIR)\Entity.obj"
	-@erase "$(INTDIR)\Line.obj"
	-@erase "$(INTDIR)\MainFrm.obj"
	-@erase "$(INTDIR)\MirrorCmd.obj"
	-@erase "$(INTDIR)\MoveCmd.obj"
	-@erase "$(INTDIR)\RotateCmd.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\VCad.obj"
	-@erase "$(INTDIR)\VCad.pch"
	-@erase "$(INTDIR)\VCad.res"
	-@erase "$(INTDIR)\VCadDoc.obj"
	-@erase "$(INTDIR)\VCadView.obj"
	-@erase "$(OUTDIR)\VCad.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Fp"$(INTDIR)\VCad.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x804 /fo"$(INTDIR)\VCad.res" /d "NDEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\VCad.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\VCad.pdb" /machine:I386 /out:"$(OUTDIR)\VCad.exe" 
LINK32_OBJS= \
	"$(INTDIR)\Base.obj" \
	"$(INTDIR)\CreateLine.obj" \
	"$(INTDIR)\Entity.obj" \
	"$(INTDIR)\Line.obj" \
	"$(INTDIR)\MainFrm.obj" \
	"$(INTDIR)\MoveCmd.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\VCad.obj" \
	"$(INTDIR)\VCadDoc.obj" \
	"$(INTDIR)\VCadView.obj" \
	"$(INTDIR)\VCad.res" \
	"$(INTDIR)\RotateCmd.obj" \
	"$(INTDIR)\MirrorCmd.obj"

"$(OUTDIR)\VCad.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "VCad - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\VCad.exe" "$(OUTDIR)\VCad.bsc"


CLEAN :
	-@erase "$(INTDIR)\Base.obj"
	-@erase "$(INTDIR)\Base.sbr"
	-@erase "$(INTDIR)\CreateLine.obj"
	-@erase "$(INTDIR)\CreateLine.sbr"
	-@erase "$(INTDIR)\Entity.obj"
	-@erase "$(INTDIR)\Entity.sbr"
	-@erase "$(INTDIR)\Line.obj"
	-@erase "$(INTDIR)\Line.sbr"
	-@erase "$(INTDIR)\MainFrm.obj"
	-@erase "$(INTDIR)\MainFrm.sbr"
	-@erase "$(INTDIR)\MirrorCmd.obj"
	-@erase "$(INTDIR)\MirrorCmd.sbr"
	-@erase "$(INTDIR)\MoveCmd.obj"
	-@erase "$(INTDIR)\MoveCmd.sbr"
	-@erase "$(INTDIR)\RotateCmd.obj"
	-@erase "$(INTDIR)\RotateCmd.sbr"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\StdAfx.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\VCad.obj"
	-@erase "$(INTDIR)\VCad.pch"
	-@erase "$(INTDIR)\VCad.res"
	-@erase "$(INTDIR)\VCad.sbr"
	-@erase "$(INTDIR)\VCadDoc.obj"
	-@erase "$(INTDIR)\VCadDoc.sbr"
	-@erase "$(INTDIR)\VCadView.obj"
	-@erase "$(INTDIR)\VCadView.sbr"
	-@erase "$(OUTDIR)\VCad.bsc"
	-@erase "$(OUTDIR)\VCad.exe"
	-@erase "$(OUTDIR)\VCad.ilk"
	-@erase "$(OUTDIR)\VCad.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\VCad.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x804 /fo"$(INTDIR)\VCad.res" /d "_DEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\VCad.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\Base.sbr" \
	"$(INTDIR)\CreateLine.sbr" \
	"$(INTDIR)\Entity.sbr" \
	"$(INTDIR)\Line.sbr" \
	"$(INTDIR)\MainFrm.sbr" \
	"$(INTDIR)\MoveCmd.sbr" \
	"$(INTDIR)\StdAfx.sbr" \
	"$(INTDIR)\VCad.sbr" \
	"$(INTDIR)\VCadDoc.sbr" \
	"$(INTDIR)\VCadView.sbr" \
	"$(INTDIR)\RotateCmd.sbr" \
	"$(INTDIR)\MirrorCmd.sbr"

"$(OUTDIR)\VCad.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=/nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\VCad.pdb" /debug /machine:I386 /out:"$(OUTDIR)\VCad.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\Base.obj" \
	"$(INTDIR)\CreateLine.obj" \
	"$(INTDIR)\Entity.obj" \
	"$(INTDIR)\Line.obj" \
	"$(INTDIR)\MainFrm.obj" \
	"$(INTDIR)\MoveCmd.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\VCad.obj" \
	"$(INTDIR)\VCadDoc.obj" \
	"$(INTDIR)\VCadView.obj" \
	"$(INTDIR)\VCad.res" \
	"$(INTDIR)\RotateCmd.obj" \
	"$(INTDIR)\MirrorCmd.obj"

"$(OUTDIR)\VCad.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("VCad.dep")
!INCLUDE "VCad.dep"
!ELSE 
!MESSAGE Warning: cannot find "VCad.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "VCad - Win32 Release" || "$(CFG)" == "VCad - Win32 Debug"
SOURCE=.\Base.cpp

!IF  "$(CFG)" == "VCad - Win32 Release"


"$(INTDIR)\Base.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ELSEIF  "$(CFG)" == "VCad - Win32 Debug"


"$(INTDIR)\Base.obj"	"$(INTDIR)\Base.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ENDIF 

SOURCE=.\CreateLine.cpp

!IF  "$(CFG)" == "VCad - Win32 Release"


"$(INTDIR)\CreateLine.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ELSEIF  "$(CFG)" == "VCad - Win32 Debug"


"$(INTDIR)\CreateLine.obj"	"$(INTDIR)\CreateLine.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ENDIF 

SOURCE=.\Entity.cpp

!IF  "$(CFG)" == "VCad - Win32 Release"


"$(INTDIR)\Entity.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ELSEIF  "$(CFG)" == "VCad - Win32 Debug"


"$(INTDIR)\Entity.obj"	"$(INTDIR)\Entity.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ENDIF 

SOURCE=.\Line.cpp

!IF  "$(CFG)" == "VCad - Win32 Release"


"$(INTDIR)\Line.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ELSEIF  "$(CFG)" == "VCad - Win32 Debug"


"$(INTDIR)\Line.obj"	"$(INTDIR)\Line.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ENDIF 

SOURCE=.\MainFrm.cpp

!IF  "$(CFG)" == "VCad - Win32 Release"


"$(INTDIR)\MainFrm.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ELSEIF  "$(CFG)" == "VCad - Win32 Debug"


"$(INTDIR)\MainFrm.obj"	"$(INTDIR)\MainFrm.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ENDIF 

SOURCE=.\MirrorCmd.cpp

!IF  "$(CFG)" == "VCad - Win32 Release"


"$(INTDIR)\MirrorCmd.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ELSEIF  "$(CFG)" == "VCad - Win32 Debug"


"$(INTDIR)\MirrorCmd.obj"	"$(INTDIR)\MirrorCmd.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ENDIF 

SOURCE=.\MoveCmd.cpp

!IF  "$(CFG)" == "VCad - Win32 Release"


"$(INTDIR)\MoveCmd.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ELSEIF  "$(CFG)" == "VCad - Win32 Debug"


"$(INTDIR)\MoveCmd.obj"	"$(INTDIR)\MoveCmd.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ENDIF 

SOURCE=.\RotateCmd.cpp

!IF  "$(CFG)" == "VCad - Win32 Release"


"$(INTDIR)\RotateCmd.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ELSEIF  "$(CFG)" == "VCad - Win32 Debug"


"$(INTDIR)\RotateCmd.obj"	"$(INTDIR)\RotateCmd.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ENDIF 

SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "VCad - Win32 Release"

CPP_SWITCHES=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Fp"$(INTDIR)\VCad.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\VCad.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "VCad - Win32 Debug"

CPP_SWITCHES=/nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\VCad.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\StdAfx.sbr"	"$(INTDIR)\VCad.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ENDIF 

SOURCE=.\VCad.cpp

!IF  "$(CFG)" == "VCad - Win32 Release"


"$(INTDIR)\VCad.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ELSEIF  "$(CFG)" == "VCad - Win32 Debug"


"$(INTDIR)\VCad.obj"	"$(INTDIR)\VCad.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ENDIF 

SOURCE=.\VCad.rc

"$(INTDIR)\VCad.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\VCadDoc.cpp

!IF  "$(CFG)" == "VCad - Win32 Release"


"$(INTDIR)\VCadDoc.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ELSEIF  "$(CFG)" == "VCad - Win32 Debug"


"$(INTDIR)\VCadDoc.obj"	"$(INTDIR)\VCadDoc.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ENDIF 

SOURCE=.\VCadView.cpp

!IF  "$(CFG)" == "VCad - Win32 Release"


"$(INTDIR)\VCadView.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ELSEIF  "$(CFG)" == "VCad - Win32 Debug"


"$(INTDIR)\VCadView.obj"	"$(INTDIR)\VCadView.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\VCad.pch"


!ENDIF 


!ENDIF 

