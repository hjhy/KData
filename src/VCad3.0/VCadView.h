// VCadView.h : interface of the CVCadView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_VCADVIEW_H__C161FBAC_7612_11D4_BF0F_0000E8A36431__INCLUDED_)
#define AFX_VCADVIEW_H__C161FBAC_7612_11D4_BF0F_0000E8A36431__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class Position;

class CVCadView : public CView
{
protected: // create from serialization only
	CVCadView();
	DECLARE_DYNCREATE(CVCadView)

// Attributes
public:
	CVCadDoc* GetDocument();
protected:	
	// 世界坐标与屏幕坐标的关系
	double	m_dOrgX, m_dOrgY; // 世界坐标的原点坐标
	double	scale; // 屏幕坐标到世界坐标的比例因子
	class CCommand* m_pCmd; 

	int m_nPageWidth,m_nPageHeight;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVCadView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CVCadView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CVCadView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnPick();
	afx_msg void OnUpdatePick(CCmdUI* pCmdUI);
	afx_msg void OnRedraw();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	afx_msg void OnCreateEntity(int m_nID);
	afx_msg void OnUpdateCreateCommand(CCmdUI* pCmdUI);
	afx_msg void OnModifyEntity(int m_nID);
	afx_msg void OnUpdateModifyCommand(CCmdUI* pCmdUI);
	afx_msg void OnDispStyle(int m_nID);
	afx_msg void OnUpdateDispStyleCommand(CCmdUI* pCmdUI);

	DECLARE_MESSAGE_MAP()
private:
	void PrintPageFooter(CDC* pDC, CPrintInfo* pInfo);
	void PrintPageHeader(CDC* pDC, CPrintInfo* pInfo);
public:	
	// 世界坐标转换为屏幕坐标，输入：世界坐标pos， 输出：屏幕坐标screenPt
	void WorldtoScreen(const Position& pos, CPoint& screenPt); 
	// 屏幕坐标转换为世界坐标，输入：屏幕坐标screenPt， 输出：世界坐标pos
	void ScreentoWorld(const CPoint& screenPt, Position& pos);
	// 得到屏幕坐标到世界坐标的比例因子
	double GetScale();
	double GetOrgX() ;
	double GetOrgY() ;
	void	SetOrgX(double) ;
	void	SetOrgY(double) ;
	void	ZoomAll(void) ;
	void	SetScale(double) ;
	void	DeleteSel() ;
};

#ifndef _DEBUG  // debug version in VCadView.cpp
inline CVCadDoc* CVCadView::GetDocument()
   { return (CVCadDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VCADVIEW_H__C161FBAC_7612_11D4_BF0F_0000E8A36431__INCLUDED_)
