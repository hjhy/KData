#include "stdafx.h"
#include "math.h"
#include "Entity.h"
#include "MainFrm.h"
#include "VCad.h"
#include "VCadDoc.h"
#include "VCadView.h"
#include "Command.h"
#include "ModifyCmd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

class CVCadDoc ;
class CVCadView ;

CViewPanCmd::CViewPanCmd()
	: m_basePos(0,0), m_desPos(0,0)
{
	m_nStep = 0;// 初始化操作步为 0
}
CViewPanCmd::~CViewPanCmd()
{
}
int	CViewPanCmd::GetType()
{
	return ctPan;
}
int	CViewPanCmd::OnLButtonDown(UINT nFlags, const Position& pos) 
{
	m_nStep ++ ;
	switch(m_nStep)
	{
		case 1:
			// 第一次单击鼠标左键时得到基点位置，并初步设定目标位置
			m_basePos = m_desPos = pos;
			::Prompt("请输入移动的目标点") ;
			break;
		case 2:
		{
			m_desPos = pos;
			double dx = m_desPos.x - m_basePos.x ;
			double dy = m_desPos.y - m_basePos.y ;
			double x =  g_pView->GetOrgX() -dx;
			double y =  g_pView->GetOrgY() -dy;
			g_pView->SetOrgX(x) ;
			g_pView->SetOrgY(y) ;
			g_pDoc->UpdateAllViews(NULL) ;
	
			m_nStep = 0; 
			break;
		}
		default:
			break;
	}
	
	return 0;
}
int	CViewPanCmd::OnMouseMove(UINT nFlags, const Position& pos)
{

	switch(m_nStep)
	{
		case 0:
			::Prompt("请输入移动画面的起始点：") ;
			break;
		case 1:
		{
			Position	prePos, curPos;
			prePos = m_desPos; // 获得上一个目标位置
			curPos = pos; // 得到当前位置
			
			double dx = curPos.x - prePos.x ;
			double dy = curPos.y - prePos.y ;
			double x =  g_pView->GetOrgX() - dx;
			double y =  g_pView->GetOrgY() - dy;
			g_pView->SetOrgX(x) ;
			g_pView->SetOrgY(y) ;
			g_pDoc->UpdateAllViews(NULL) ;

			m_desPos.x = pos.x - dx; // 将目标设置为当前位置
			m_desPos.y = pos.y - dy;

		}
		break;
		default:
			break;
	}
	return 0;
}
// 单击鼠标右键取消正在进行的操作
int	CViewPanCmd::OnRButtonDown(UINT nFlags, const Position& pos) 
{
	return 0;
}
// 调用Cancel 函数取消本次操作
int CViewPanCmd::Cancel()
{

	return 0;
}
// 调用 xd_OnChar 函数 
int CViewPanCmd::xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{

	return 0;
}
