#include "stdafx.h"
#include "math.h"
#include "Entity.h"
#include "MainFrm.h"
#include "VCad.h"
#include "VCadDoc.h"
#include "VCadView.h"
#include "Command.h"
#include "ModifyCmd.h"
#include "Entity.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

class CVCadDoc ;
class CVCadView ;

CZoomRgnCmd::CZoomRgnCmd()
	: m_RgnCorner(0,0), m_RgnCorner1(0,0)
{
	m_nStep = 0;// 初始化操作步为 0
}
CZoomRgnCmd::~CZoomRgnCmd()
{
}
int	CZoomRgnCmd::GetType()
{
	return ctPan;
}
int	CZoomRgnCmd::OnLButtonDown(UINT nFlags, const Position& pos) 
{
	m_nStep ++ ;
	switch(m_nStep)
	{
		case 1:
			// 第一次单击鼠标左键时得到基点位置，并初步设定目标位置
			m_RgnCorner = m_RgnCorner1 = pos;
			::Prompt("请输入移动的目标点") ;
			break;
		case 2:
		{
			m_RgnCorner1 = pos;
			CRect rect ;
			g_pView->GetClientRect(&rect) ;

//			double RgnMaxX, RgnMaxY, RgnMinX, RgnMinY ;
			
//			RgnMaxX = max(m_RgnCorner1.x,m_RgnCorner.x);
//			RgnMinX = min(m_RgnCorner1.x,m_RgnCorner.x);
//			RgnMaxY = max(m_RgnCorner1.y , m_RgnCorner.y);
//			RgnMinY = min(m_RgnCorner1.y , m_RgnCorner.y);

			double dx = m_RgnCorner1.x - m_RgnCorner.x ;
			double dy = m_RgnCorner1.y - m_RgnCorner.y ;
//			double dx = RgnMaxX - RgnMinX ;
//			double dy = RgnMaxY - RgnMinY ;
			double temSal = fabs(dx/(double)rect.right) ;
			double temSal1 = fabs(dy/(double)rect.bottom) ;
			double tem = max(temSal, temSal1) ;
			
			double orgx = m_RgnCorner1.x + m_RgnCorner.x ;
			double orgy = m_RgnCorner1.y + m_RgnCorner.y ;

			g_pView->SetOrgX(orgx/2.) ;
			g_pView->SetOrgY(orgy/2.) ;
			g_pView->SetScale(tem) ;
			
			g_pDoc->UpdateAllViews(NULL) ;
	
			m_nStep = 0; 
			break;
		}
		default:
			break;
	}
	
	return 0;
}
int	CZoomRgnCmd::OnMouseMove(UINT nFlags, const Position& pos)
{
	// 用一静态变量nPreRefresh记录进入OnMouseMove状态时的刷新次数
	static	int nPreRefresh = g_nRefresh; 
	// 布尔变量bRefresh说明在OnMouseMove过程中视窗是否被刷新
	BOOL	bRefresh = FALSE; 
	// nCurRefresh用于记录当前的刷新次数
	int		nCurRefresh = g_nRefresh; 
	// 如果nCurRefresh和nPreRefresh不相等，说明视窗曾被刷新过
	if(nCurRefresh != nPreRefresh){ 
		bRefresh = TRUE;
		nPreRefresh = nCurRefresh; 
	}
	switch(m_nStep)
	{
		case 0:
			::Prompt("请输入区域的起始点：") ;
			break;
		case 1:
		{
			Position	prePos, curPos;
			prePos = m_RgnCorner; // 获得上一个目标位置
			curPos = pos; // 得到当前位置

			CDC* pDC = g_pView->GetDC() ;
			pDC->SelectStockObject(NULL_BRUSH) ;
			pDC->SetROP2(R2_NOT) ;
			
			CRectangle * pTan1 = new CRectangle(prePos, m_RgnCorner1) ;
			pTan1->Draw(pDC ,dmNormal) ;
			CRectangle* pTan2 = new CRectangle(prePos, curPos) ;
			pTan2->Draw(pDC ,dmNormal) ;
			
			delete pTan1 ;
			delete pTan2 ;
			m_RgnCorner1 = pos; // 将目标设置为当前位置
			g_pView->ReleaseDC(pDC) ;
		}
		default:
			break;
	}
	return 0;
}
// 单击鼠标右键取消正在进行的操作
int	CZoomRgnCmd::OnRButtonDown(UINT nFlags, const Position& pos) 
{
	return 0;
}
// 调用Cancel 函数取消本次操作
int CZoomRgnCmd::Cancel()
{

	return 0;
}
 
int CZoomRgnCmd::xd_OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{

	return 0;
}
