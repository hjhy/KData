//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by VCad.rc
//
#define IDD_ABOUTBOX                    100
#define IDB_SPLASH                      102
#define IDR_MAINFRAME                   128
#define IDR_VCADTYPE                    129
#define IDR_CREATE                      130
#define IDR_MODIFY                      132
#define IDR_VIEW                        134
#define IDR_PROPERTYBAR                 136
#define IDC_DRAW_LINE                   138
#define IDC_DRAW_ARC                    139
#define IDC_DRAW_RECT                   140
#define IDC_DRAW_CIRCLE                 141
#define IDC_PROMPT_ARC                  142
#define IDC_PROMPT_CIRCLE               143
#define IDC_PROMPT_LINE                 144
#define IDC_PROMPT_RECT                 145
#define IDC_PROMPT_OTHER                146
#define IDC_CURSOR1                     147
#define ID_CREATE_LINE                  32771
#define ID_CREATE_RECTANGLE             32772
#define ID_CREATE_CIRCLE                32773
#define ID_CREATE_ARC                   32774
#define ID_CREATE_OTHER                 32775
#define ID_PICK                         32776
#define ID_MODIFY_MOVE                  32777
#define ID_MODIFY_ROTATE                32778
#define ID_MODIFY_MIRROR                32779
#define ID_MODIFY_ERASE                 32780
#define IDM_SAVE_DXF                    32781
#define ID_REDRAW                       32785
#define ID_PROPERTY                     32786
#define ID_VIEW_PAN                     32787
#define ID_VIEW_ZOOMIN                  32789
#define ID_VIEW_ZOOMOUT                 32790
#define ID_VIEW_ZOOMALL                 32791
#define ID_VIEW_ZOOMRGN                 32792
#define IDW_COLOR                       32794
#define IDW_LINEWIDTH                   32795
#define IDW_LINESTYLE                   59002

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        148
#define _APS_NEXT_COMMAND_VALUE         32798
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           103
#endif
#endif
