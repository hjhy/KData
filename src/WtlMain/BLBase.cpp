
#include "stdafx.h"
typedef enum tagMAX_VALUE{ 
		
	MAX_STRING      	= 256,
		
} MAX_VALUE;

/*
	Global variable 
 */
char g_szBtflErrorString[][MAX_STRING] = {
	"BTFL_STATUS_OK",
	"BTFL_STATUS_NO_DEVICE_FOUND",				//Hardware error
	"BTFL_STATUS_DEVICE_HANDLE_INVALID",
	"BTFL_STATUS_HW_DEVICE_TYPE_ERROR",
	"BTFL_STATUS_HW_INIT_ERROR",
	"BTFL_STATUS_HW_RESET_ERROR",
	"BTFL_STATUS_NOT_ENOUGH_SYSTEM_MEMORY",
	"BTFL_STATUS_HW_IO_ERROR",
	"BTFL_STATUS_HW_IO_TIMEOUT",
	"BTFL_STATUS_HW_ACCESS_ERROR",
	
	"BTFL_STATUS_OPEN_DRIVER_FAILED",			//Software error
	"BTFL_STATUS_NOT_SUPPORT_INTERFACE",
	"BTFL_STATUS_PARAMETER_INVALID",
	"BTFL_STATUS_PARAMETER_OUT_OF_BOUND",
	"BTFL_STATUS_IN_WORK",  
	"BTFL_STATUS_NOT_OPEN_SNAP",
	"BTFL_STATUS_NOT_START_SNAP",
	"BTFL_STATUS_FILE_CREATE_ERROR",
	"BTFL_STATUS_FILE_INVALID",
	"BTFL_STATUS_NOT_START_SNAP_INT",
	"BTFL_STATUS_INTERNAL_ERROR"					//Internal error
};

char *  BtfGetErrorString(BTFLSTATUS status)
{
	return g_szBtflErrorString[abs(status)];
}

BTFLBaseException::BTFLBaseException( BTFLSTATUS e, CString Context, va_list *pArgs)
: Exception( e, Context, true )
{
	FormatMessage(e, Context);
}
BTFLBaseException::BTFLBaseException( DWORD e, CString Context, va_list *pArgs) 
: Exception( e, Context, true )
{
	Exception::FormatMessage(pArgs);
}



void BTFLBaseException::FormatMessage( BTFLSTATUS e, CString Context )
{
	m_Description.Format("%s", BtfGetErrorString(e));
}
