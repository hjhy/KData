// BLBase.h: interface for the BeautifulLover API. 
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BL_H__F2EAD769_A3AB_48E2_99A7_CF32024FCA7A__INCLUDED_)
#define AFX_BL_H__F2EAD769_A3AB_48E2_99A7_CF32024FCA7A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
   
#include "Exception.h"
#include "Shlwapi.h"
#include "BeautifulLover.h"

// #XdAdd: 001:  V0.1
namespace np_BTFL
{
	typedef enum
	{
		Color_Mono8 = 0,     ///< Y component has 8bit data
		Color_YUV8_4_1_1,    ///< 4:1:1 YUV 8 format, each component has 8bit data
		Color_YUV8_4_2_2,    ///< 4:2:2 YUV 8 format, each component has 8bit data
		Color_YUV8_4_4_4,    ///< 4:4:4 YUV 8 format, each component has 8bit data
		Color_RGB8,          ///< RGB 8 format, each component has 8bit data
		Color_Mono16,        ///< Y component has 16bit data
		Color_RGB16          ///< RGB 16 format, each component has 16bit data
	}	HVColorCode;

	typedef enum
	{
		Res_IgnoreVideoMode = -1,
		Res_Mode0 = 0,
		Res_Mode1 = 1,
		Res_Mode2 = 2,
		Res_Mode3 = 3,
		Res_Mode4 = 4,
		Res_Mode5 = 5,
		Res_Mode6 = 6,
		Res_Mode7 = 7
	}	HVResolution;

	class BTFLBaseException : public XdBvc::Exception
	{
	public:
		BTFLBaseException( BTFLSTATUS	e, 
						   CString		Context = "", 
						   va_list *pArgs = NULL);
		BTFLBaseException( DWORD e, 
						   CString Context = "", 
						   va_list *pArgs = NULL );
		virtual ~BTFLBaseException(){}
		
		/// retrieve the error code
		DWORD Error() const { return m_Error; }
		/// retrieve the error message
		CString Description() const { return m_Description; }
		/// retrieve additional context information
		CString Context() const { return m_Context; }
		
	protected:
		void FormatMessage( BTFLSTATUS e, CString Context );
	};

 
	/**
	* \define Used by nested classes to get access to the outer class
	*/
#define IMPLEMENT_THIS(OUTER, MEMBER_VARIABLE) \
	OUTER* This() \
	{ \
	return (OUTER*)((BYTE*)this - offsetof(OUTER, MEMBER_VARIABLE)); \
		};
	class BTFLBaseUtility
	{
	public:
		/// Mapping DCSColorCode to number of bits per pixel
		static unsigned short BitsPerPixel(HVColorCode ColorCode);

	};
 
	
	//! Wrapper class for the Win32 thread api
	class CThread
	{
	public:
		
		//! Default constructor 
		CThread()
		{
			m_hThread = NULL;
			m_dwID = 0;
			m_bSuspended = false;
		}
		
		//! destructor
		virtual ~CThread() 
		{ 
			Release(1000); 
		}
		//! Get the thread's ID
		operator DWORD()
		{
			return m_dwID;
		}
//	protected:
		//! Creates a thread
		BOOL xdCreate(
			LPTHREAD_START_ROUTINE pProcess,        //!< Thread function
			LPVOID pParam=NULL,                     //!< Parameter handed over to the thread function
			int Priority=THREAD_PRIORITY_NORMAL,    //!< The thread's priority
			DWORD CreationFlags = 0                 //!< Flags controlling the thread's creation
			)
		{
			assert(pProcess);
			m_hThread = ::CreateThread(NULL, 0, pProcess, pParam, CreationFlags, &m_dwID);
			ATLTRACE("CThread.Create(). Handle = %x, ID = %x\n", m_hThread, m_dwID);
			m_bSuspended = CreationFlags && CREATE_SUSPENDED;
			if( m_hThread==NULL ) 
				return FALSE;
			if( !::SetThreadPriority(m_hThread, Priority) ) {
				::CloseHandle(m_hThread);
				return FALSE;
			}
			return TRUE;
		}
		
		//! Close the thread handle and wait for the thread to die
		BOOL Release(DWORD timeout = INFINITE)
		{
			if( m_hThread==NULL ) 
				return TRUE;
			if ( WaitForSingleObject(m_hThread, timeout) != WAIT_OBJECT_0 )
			{
				// The thread didn't die. Terminate it
				ATLTRACE("Must terminate thread. Handle = %x\n", m_hThread);
				Terminate(0);
				WaitForSingleObject(m_hThread, 10000);
			}
			
			return Detach();
		}
		
		//! Close the thread handle, but don't wait for the thread to die
		BOOL Detach()
		{
			if ( m_hThread == NULL )
				return TRUE;
			if( ::CloseHandle(m_hThread)==FALSE ) 
				return FALSE;
			m_hThread = NULL;
			m_dwID = 0;
			return TRUE;
		}
		
		//! Set the thread's priority
		BOOL SetPriority(int Priority)
		{
			assert(m_hThread);
			return ::SetThreadPriority(m_hThread, Priority);
		}
		
		//! Suspend the thread
		BOOL Suspend()
		{
			assert(m_hThread);
			if( m_bSuspended ) 
				return TRUE;
			if( ::SuspendThread(m_hThread)==-1 ) 
				return FALSE;
			m_bSuspended = true;
			return TRUE;
		}
		
		//! Resume the thread
		BOOL Resume()
		{
			assert(m_hThread);
			if( !m_bSuspended ) return TRUE;
			if( ::ResumeThread(m_hThread)==-1 ) return FALSE;
			m_bSuspended = false;
			return TRUE;
		}
		
		//! Terminate the thread (don't...)
		BOOL Terminate(DWORD dwExitCode)
		{
			assert(m_hThread);
			return ::TerminateThread(m_hThread, dwExitCode);
		}
		
		//! Get the thread's exit code
		BOOL GetExitCode(DWORD *pExitCode)
		{
			assert(m_hThread);
			assert(pExitCode);
			return ::GetExitCodeThread(m_hThread, pExitCode);
		}
		
		//! Get the thread's handle
		operator HANDLE() 
		{ 
			return m_hThread; 
		}
		
		
		//! Check if the thread is currently suspended
		bool IsSuspended() 
		{ 
			return m_bSuspended; 
		}
		
	private:
		//! The thread's handle
		HANDLE			m_hThread;
		
		//! The thread's ID
		DWORD			m_dwID;
		
		//! Indicates if the thread is currently suspended
		bool m_bSuspended;
		
	};   ///CThread
	
	//! Wrapper class for the Win32 events
	class CEvent
	{
		HANDLE m_hEvent;
		
	public:
		CEvent()
		{
			m_hEvent = NULL;
		}
		
		CEvent( const CEvent & src )
		{
			Duplicate(&m_hEvent, src.m_hEvent);
		}
		
		CEvent & operator =(const CEvent& src)
		{
			if ( m_hEvent != NULL )
				::CloseHandle(m_hEvent);
			Duplicate(&m_hEvent, src.m_hEvent);
			return *this;
		}
		
		BOOL Create(BOOL bManualReset = FALSE, BOOL bInitialState = FALSE, LPSECURITY_ATTRIBUTES lpEventAttributes = NULL, LPCTSTR lpName = NULL)
		{
			m_hEvent = ::CreateEvent(lpEventAttributes, bManualReset, bInitialState, lpName);
			return m_hEvent != NULL;
		}
		
		BOOL Set()
		{
			return ::SetEvent(m_hEvent);
		}
		
		BOOL Reset()
		{
			return ::ResetEvent(m_hEvent);
		}
		
		operator HANDLE() const
		{ 
			
			
			return m_hEvent;
		}
		
		HANDLE* operator &()
		{ 
			return &m_hEvent;
		}
		
		
		~CEvent()
		{
			if ( m_hEvent != NULL )
			{
				CloseHandle(m_hEvent);
				m_hEvent = NULL;
			}
		}
		
		
	private:
		BOOL Duplicate(HANDLE * phDest, HANDLE hSource)
		{
			if (hSource == NULL ) {
				
				*phDest = hSource; 
				
				return TRUE;
				
			} else {
				
				return ::DuplicateHandle(
					GetCurrentProcess(), hSource,
					GetCurrentProcess(), phDest,
					0,                        
					FALSE,                    
					DUPLICATE_SAME_ACCESS);
			}
		}
	}; // CEvent
	
	
	 
	
	 
}

#endif // !defined(AFX_BL_H__F2EAD769_A3AB_48E2_99A7_CF32024FCA7A__INCLUDED_)
