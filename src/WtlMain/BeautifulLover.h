/*
 *-----------------------------------------------------------------------------
 *       Name				BeautifulLover.h
 *       Purpose			Constant definition
 *       Development Kit	Microsoft Win32 SDK, Visual C++ 6.00 
 *       Author				LittleFlute
 *       Date          		2016/01/14
 *		 Version			1.0.0.1
 *
 *       Description
 *		 This header file defines constant and external struct type
 *       
 协议说明:
		1)	code:	CMD_BEAUTIFULLOVER_0001 (_0002,_0003,...) _0000 为起始标志。
		2)  Param:	CMD_BEAUTIFULLOVER_CONTEXT
						char szVerify[16]; // 值为必为: "BEAUTIFULLOVER"
						
				
 *-----------------------------------------------------------------------------
 */

#if !defined(BEAUTIFULLOVER)         
#define BEAUTIFULLOVER

/* By C++ language compiler */
#ifdef __cplusplus
extern "C" {
#endif	
//-------------------------------------------------------------------------

	// #def0001: 标识句柄
	typedef HANDLE		HBTFL;			
	//////////////////////////////////////////////////////////////////////////
	// #def0002: / the return status for the interface function
	typedef enum tagBTFLSTATUS{
		BTFL_STATUS_OK								= 0,
		BTFL_STATUS_NOT_SUPPORT_INTERFACE			= -1,
		BTFL_STATUS_NOT_FOUND_LIBARY				= -2,

		
		BTFL_STATUS_PARAMETER_INVALID				= -12,
		BTFL_STATUS_INTERNAL_ERROR					= -13,
		BTFL_STATUS_NOT_ENOUGH_SYSTEM_MEMORY		= -14,
		BTFL_STATUS_FILE_INVALID					= -15,
	}BTFLSTATUS;
	


	// #def0003: command code
	typedef enum tagBEAUTIFULLOVER_COMMAND_CODE {  
		CMD_BTFL_BEA01000____________			= 0xBEA01000,
		CMD_BTFL_G_PRETRASLATEMSG				= 0xBEA07001,
		CMD_BTFL_AOI							= 0xBEA0F00A,

		CMD_BTFL_B0_UI							= 0xBEA0B001,

		CMD_BTFL_B4_LBUTTONDOWN					= 0xBEA0B40A,
		CMD_BTFL_B4_MOUSEMOVE					= 0xBEA0B40B,
		CMD_BTFL_B4_CHAR						= 0xBEA0B40C,
		CMD_BTFL_B4_KEY_DOWN					= 0xBEA0B40D,
		CMD_BTFL_B4_REGISTER_CALLBACK			= 0xBEA0B4BC,
		
		CMD_BTFL_BEA0F000____________			= 0xBEA0F000,
		CMD_BEAUTIFULLOVER_0001					= 0xBEA0F001, // Hit
		CMD_BTFL_ADD_PRP						= 0xBEA0F002, // Add PRP
		CMD_BTFL_PRP_CHANGED					= 0xBEA0F003, // Prp Changed
		CMD_BTFL_PRP_HIT						= 0xBEA0F004, // Prp Hit
		
		CMD_BTFL_0004							= 0xBEA0F104, //  
		CMD_BTFL_0005							= 0xBEA0F105, // set	对齐方式


			
	}BEAUTIFULLOVER_COMMAND_CODE; 

	typedef struct tagBTFLPARAM 
	{
		//----确认值-------------------------------------------------------------------
		char							szVerify[16];	// 值为必为: "BEAUTIFULLOVER"
		BEAUTIFULLOVER_COMMAND_CODE		code;			//调用时再次确认
		HINSTANCE						hInstance;
		HWND							hWnd;
		
	}BTFLPARAM;
	 

	//#def0004:
	//------------------------------
	typedef enum tagBTFL_PRP_TYPE{
		NULL_TYPE								= -1,
		TYPE_CATEGORY							= 0,
		TYPE_READONLY	                        = 1,
		TYPE_SCALABLE						    = 2, 
		TYPE_LIST							    = 3, 
		TYPE_FILENAME						    = 4, 
	}BTFL_PRP_TYPE;
 
	typedef enum tagBTFL_PRP_ID{
		NULL_ID										= 0x00000000,
		
		BTFL_B1_READONLY_FC00						= 0xB100FC00,
		BTFL_B1_READONLY_FC01						= 0xB100FC01,
		BTFL_B1_READONLY_FC02						= 0xB100FC02,
		BTFL_B1_READONLY_FC03						= 0xB100FC03,

		BTFL_B4_CATEGORY_FC01						= 0xB4F4FC01, //

		BTFL_B4_READONLY_FC01						= 0xB4F4FD01, // 
		BTFL_B4_READONLY_FC02						= 0xB4F4FD02, //

		SCALABLE_0000								= 0xF000F000,
		SCALABLE_0001								= 0xF000F001,

		SCALABLE_MFCBTFL_F100						= 0xF000F100,
		SCALABLE_MFCBTFL_F101						= 0xF000F101,
		
		LIST_0000									= 0xF100F000, 
		LIST_0001									= 0xF100F001, 
		LIST_F100									= 0xF100F100, //数据源
		LIST_DATA_SOURCE_2							= 0xF100F101, //数据源
		lIST_F1_DATA_FLIPE							= 0xf100F102, //颠倒数据

		LIST_F3_JPG_ON_OFF							= 0xF300F101, //JPG开关
		LIST_F3_CLIPBOARD_ON_OFF					= 0xF300F102,//剪帖板开关
		LIST_F3_BMPSHOW_ON_OFF						= 0xF300F103,//位图演示开关

		FILENAME_0000								= 0xf000f201,
		FILENAME_SAVE_CLIPBOARD_AS_BMP				= 0xF000F202,
		FILENAME_SAVE_CLIPBOARD_AS_JPG				= 0xF000F203,

	}BTFL_PRP_ID;
   


	typedef struct tagBTFL_ADD_PRP_INFO{ 
		BTFL_PRP_TYPE		type;
		char				szName[16];
		int					id;
		int					v;
		int					min;
		int					max;
		char				szV[256];  
		void				*pxd1Param; //PRP回调参数使用
	}BTFL_ADD_PRP_INFO;
	//===================================
	//--Call_BACK_FUN---
	typedef int (CALLBACK* BTFL_PRP_INFO_PROC)(BTFL_ADD_PRP_INFO *pInfo);
	//===================

	//*
	//#def0005: command param
	typedef struct tagHV_CMD_BEAUTIFULLOVER_CONTEXT {
	
		//----确认值-------------------------------------------------------------------
		char							szVerify[16];	// 值为必为: "BEAUTIFULLOVER"
		BEAUTIFULLOVER_COMMAND_CODE		code;			//调用时再次确认
		HINSTANCE						hInstance;
		HWND							hWnd;
		
		//----[确认值]被确认后，才能使用以下值----------------------------------------- 
		//--CMD_BEAUTIFULLOVER_0001,2 参数--
		union tag_CMD_PARAM{
			struct{
				DWORD				b		:1; //是否可变 AOI
				DWORD				id		:2; //1-get[b],2-get[x,y,w,h],3-set[x,y,w,h]
				DWORD				type	:2;
				DWORD				x		:13;
				DWORD				y		:13;
				DWORD				w		:13;
				DWORD				h		:13;
				DWORD				n		:2;//not used
			}UI; 
			struct{
				DWORD				b		:1; //是否可变 AOI
				DWORD				id		:2; //1-get[b],2-get[x,y,w,h],3-set[x,y,w,h]
				DWORD				x		:14;
				DWORD				y		:14;
				DWORD				w		:14;
				DWORD				h		:14;
			}AOI; 
			struct{
				BTFL_PRP_TYPE		type;
				char				szName[16];
				BTFL_PRP_ID			id;
				DWORD				v;
			}PrpChanged;
			struct{
				BTFL_PRP_INFO_PROC	pFun;
				void				*pParam; 
			}CallBack;
			struct{
				void*				p;
			}PV;
			struct{
					MSG *			pMsg;
			}MSG;
		}BTFL_PARAM;

		//--CMD_BEAUTIFULLOVER_0001 参数--
		
	} CMD_BEAUTIFULLOVER_CONTEXT;
	//	*/ 
 
	//=================================================================================

	//--macros----------------------------------------------------------------------
	// #MAC0001:  
	#define BTFL_SUCCESS(status) ( ((status) == BTFL_STATUS_OK))
	// #MAC0002: 
	#define BTFL_VERIFY_POINTER(p, status)	\
				if ((p) == NULL){	return (status); }

	
	typedef BTFLSTATUS ( __stdcall BTFL_FUNC_CMD )(HBTFL hBtfl, BEAUTIFULLOVER_COMMAND_CODE code, CMD_BEAUTIFULLOVER_CONTEXT *pContext);

	//==============================================================================
	//=================================================================================
	/* extern "C" { */
#ifdef __cplusplus
}
#endif

#endif

