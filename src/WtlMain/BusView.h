 
#if !defined(AFX_BUSVIEW_H__F4CED1A3_9CED_4DF5_9C91_692B10F7E930__INCLUDED_)
#define AFX_BUSVIEW_H__F4CED1A3_9CED_4DF5_9C91_692B10F7E930__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
#include "dialogview.h"
#include "DeviceManager.h"

class CMainFrame;
class CDeviceManager;

class CXdSubItemsView : public CDialogView<CXdSubItemsView>  
{
public:
	enum {IDD = IDD_BUSVIEW};
	
	BEGIN_MSG_MAP(CXdSubItemsView)
		CHAIN_MSG_MAP(CDialogView<CXdSubItemsView>)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		MESSAGE_HANDLER(WM_CONTEXTMENU, OnContextMenu) 
		NOTIFY_HANDLER(IDC_BUSTREE, TVN_SELCHANGED, OnSelChanged)
		NOTIFY_HANDLER(IDC_BUSTREE, NM_DBLCLK, OnLButtonDblClk)
		REFLECT_NOTIFICATIONS()
		END_MSG_MAP()
		
		// Handler prototypes (uncomment arguments if needed):
		//	LRESULT MessageHandler(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
		//	LRESULT CommandHandler(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
		//	LRESULT NotifyHandler(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/)
		
		CXdSubItemsView(CDeviceManager& DeviceManager, CMainFrame& MainFrame); 
	
public:
	const char* UNABLE_TO_OPEN;
	void CurrentDeviceChanged(CDevice* pdevice);
protected:
	 class CBusNode 
	{
	public: 
		CBusNode(CString devName, int nNumber, int type) :
		  m_DeviceName(devName),
			  m_nNumber(nNumber),
			  m_Type(type)
		  {};
		  virtual ~CBusNode() { ATLTRACE("~CBusNode()\n");};
		  
		  /// get Win32 device name
		  int GetDeviceNumber() const {return m_nNumber;}
		  /// get Win32 device name
		  CString GetDeviceName() const {return m_DeviceName;}
		  /// get type of node 
		  
	protected:
		CString		m_DeviceName;
		int			m_Type;
		int			m_nNumber;
	}; 
	
	/// map tree item handels to associated CBusNode objects
	typedef map<HTREEITEM, CBusNode*> NodeMap_t; 
	public:

	protected:
		// Message Handlers
		LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		LRESULT OnContextMenu(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		/// Another tree item is selected
		LRESULT OnSelChanged(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);
		/// User has double clicked on a tree item
		LRESULT OnLButtonDblClk(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);
		 
		 
		/// Remove a node from the tree
		void RemoveNode(int nNumber);
		/// bool check if the device list alread contains a node for a given device
		bool ContainsNode(int nNumber);
 
		CMainFrame              &m_xdMainFrm; 
		CDeviceManager          &m_refXdDM;  
		CTreeViewCtrl           m_tvcXdItems; 
		CImageList              m_ImageList; 
		CTreeItem               m_RootItem;
		 
		NodeMap_t               m_NodeMap;
		
	private:
		friend class CAutoTest;
		void xdAddNode(int nNumber);
		void plReportError(BTFLBaseException& e); 

};

#endif // !defined(AFX_BUSVIEW_H__F4CED1A3_9CED_4DF5_9C91_692B10F7E930__INCLUDED_)
