// ChildFrm.cpp : implementation of the CChildFrame class
//
/////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "ChildFrm.h"
 

LRESULT CChildFrame::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
    m_hWndClient = v.Create(m_hWnd, rcDefault, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, WS_EX_CLIENTEDGE);
	
    bHandled = FALSE;
    return 1;
}



LRESULT CChildFrame::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	if (m_hWndClient != NULL && ::IsWindow(m_hWndClient))
		  v.DestroyWindow();	  
	
    bHandled = FALSE;
    return 1;
}



LRESULT CChildFrame::OnShowWindow(UINT uMsg, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
    BOOL fShow = (BOOL)wParam;

    bHandled = FALSE;
    return 1;
}



  
LRESULT CChildFrame::OnForwardMsg(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
    LPMSG pMsg = (LPMSG)lParam;

    if(CMDIChildWindowImpl<CChildFrame>::PreTranslateMessage(pMsg))
      return TRUE;

    return v.PreTranslateMessage(pMsg);
}


 
LRESULT CChildFrame::OnChildActivate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
    bHandled = FALSE;
    if ( GetFocus() != v )
      v.SetFocus();

    return 0;
}

  

LRESULT CChildFrame::OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
    bHandled = FALSE;
    v.SaveLayout();
    m_dm4CFrm.MDIChildClosed(this);


    return 0;
}


  
LRESULT CChildFrame::OnActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    if ( (HWND) lParam == m_hWnd )
    {
      ATLTRACE("OnActivate: %ld \n", wParam);
      m_dm4CFrm.MDIChildActivated(this); 
      v.OnActivate( true );
    }
    if ( ( HWND ) wParam == m_hWnd  ){
      v.OnActivate( false );
    }

    bHandled = false;
    return 0;
}
  

 
void CChildFrame::plReportError(BTFLBaseException& e)
{
	if ( ! ::IsWindow(m_ErrorBox) )
	{
		m_ErrorBox.Create(m_hWnd);
		m_ErrorBox.ShowWindow(SW_SHOW);
	}
	m_ErrorBox.plReportError(e);
}


void CChildFrame::plXdTest()
{

}
