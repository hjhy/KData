
#include "stdafx.h" 
#include "Device.h"
#include "ChildFrm.h"

void CDevice::plXdTest()
{
}

void CDevice::SetMDIChild(CChildFrame* pChild)
{
	m_pChildFrame = pChild; 
	if ( pChild != NULL )
	{
		m_pChildFrame->plXdTest();
	}
}
void CDevice::GrabFromFile(CString FileName)
	 { 
		 assert ( m_pChildFrame != NULL );
		 
		 // release image buffers and current bitmap
		 xdReleaseBuffers();
		 if ( m_pBitmap != NULL )
		 {
			 delete m_pBitmap;
			 m_pBitmap = NULL; 
		 }
		 
		 // Load bitmap from file
		 m_pBitmap = new CBlBitmap(FileName);
		 if ( m_pBitmap != NULL )
		 {  
			 //*
			 
			 m_pChildFrame->v.xdConfigurationChanged((HVResolution)-1,
				 m_pBitmap->GetSensorSize(),
				 m_pBitmap->GetSize(), 
				 m_pBitmap->GetOrigin());
			 //*/ 
		 }
	 }
