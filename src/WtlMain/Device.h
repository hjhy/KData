// Device.h: interface for the CDevice class.

#if !defined(AFX_DEVICE_H__A6B47285_FBD5_4E5C_8939_56369F355777__INCLUDED_)
#define AFX_DEVICE_H__A6B47285_FBD5_4E5C_8939_56369F355777__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000 
#include "image.h"  
class CMainFrame;
class CChildFrame;

class CDevice  
{ 
public:
	CDevice(int nNumber, int nIndex, HWND hWndNotify, CMainFrame& MainFrame) 
		:m_ppXdBuffers(NULL),
		m_pBitmap(NULL) 
	{
		m_Type = 0;
	}
	virtual ~CDevice()
	{
	}
	void plXdTest();
	CBlBitmap* GetBitmap() 
	{ 
		return m_fBitmapValid ? m_pBitmap : NULL;
	}
protected:
	bool                m_fBitmapValid;
	/// array of buffers to be filled by the DMA
	CBlBitmap**			m_ppXdBuffers;   
	/// number of buffers we use for image acquisition
	int                 m_cBuffers;   
	/// bitmap to display. 
	CBlBitmap*			m_pBitmap;     

public:
	 int		            m_nNumber;
	 CString				m_strModelName;
	 int					m_Type; 
	 void SetMDIChild(CChildFrame* pChild);
	 
	 void CDevice::xdReleaseBuffers()
	 {
		 if ( m_ppXdBuffers != NULL )
		 {
			 for ( int i = 0; i < m_cBuffers; i ++ )
			 {
				 if ( m_ppXdBuffers[i] != NULL && m_ppXdBuffers[i] != m_pBitmap )
				 {
					 delete m_ppXdBuffers[i];
				 }
			 }
			 delete[] m_ppXdBuffers;
			 m_ppXdBuffers = NULL;
			 m_cBuffers = 0;
		 }
	 } 
	 void GrabFromFile(CString FileName);
	 


	 CChildFrame*        m_pChildFrame;

};

#endif // !defined(AFX_DEVICE_H__A6B47285_FBD5_4E5C_8939_56369F355777__INCLUDED_)










