 //deviceManager.h
#if !defined(AFX_DEVICEMANAGER_H__AFBE39EB_B98A_400C_89B2_EC36A2AF5EDA__INCLUDED_)
#define AFX_DEVICEMANAGER_H__AFBE39EB_B98A_400C_89B2_EC36A2AF5EDA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CDevice;
class CMainFrame;
class CChildFrame;  
#include "Device.h"

#define	PLUGIN_MENU_POSITION		4


class CDeviceManager  
{
public:
	CDeviceManager(CMainFrame &MainFrame);
	virtual ~CDeviceManager();

	CDevice* GetDevice(int nNumber);
	/// Returns the device object for a given child frame
	CDevice* GetDevice(CChildFrame *pChild);
	/// The device manager gets informed that the current device has been changed
	DWORD CurrentDeviceChanged(int nNumber);
	/// Returns the current device
	CDevice *GetCurrentDevice() { return m_pCurrentDevice; }
	/// The device manager gets informed that an image window has been closed


	void MDIChildClosed(CChildFrame* pChild);
	/// The device manager gets informed that image window has been activated
	void  MDIChildActivated(CChildFrame* pChild);
	/// Add a new device to the device manager's list of devices
	BOOL AddDevice(int nNumber, HWND hWndNotify);
	/// Delete a device which has been removed from the system
	BOOL RemoveDevice(int nNumber);
	/// Create a new image window associated with the current device
	void AddMDIChild();
	/// Returns the image window assiciated with the current device 
	CChildFrame *GetCurrentChild();
	/// Is there already a image window for the current device
	bool ExistMDIChild();
 	 
	
	/// Let the current device grag an image
	void GrabSingle();
	/// Let the current device grab continuosly images
	void GrabContinuous();
	/// Cancel the current device's grab
	void GrabCancel();

	/// Load an image from a file show it in the image window associated with the current device
	void GrabFromFile(CString FileName);
	/// Shows the pixel value under the cursor in the main frame's status bar
	void ShowPixelValue();
	/// Retrieves the current frame rate
	unsigned int OnXdGetFrameRate(double& d);
	
	/// The device manager get's informed that the application is to be closed. The settings of the current device will be saved to the registry
	void AppExit(bool b);
	
    void OnSoftTrigger();

	void XD_Show_All_FPS();
	
private:
	/// Display an error message
	void plReportError(BTFLBaseException& e);
	
	/// maps device names to device objects
	typedef map<int, CDevice*> DevNumber2Device_t;
	DevNumber2Device_t      m_DevNumber2Device;
	/// maps device objects to child Device
	typedef map<CDevice*, CChildFrame*> Device2ChildFrame_t;
	Device2ChildFrame_t		m_xd_Dev_2_ChildFrame;
 
	
	/// reference to the application's main frame
	CMainFrame          &m_mf4DM; 
	CDevice				*m_pCurrentDevice;
	int					m_SelectedDevice;
};

#endif // !defined(AFX_DEVICEMANAGER_H__AFBE39EB_B98A_400C_89B2_EC36A2AF5EDA__INCLUDED_)
