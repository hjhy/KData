 

#include "stdafx.h"
#include "DeviceManager.h"
#include "mainfrm.h" 
#include "ChildFrm.h" 
 
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDeviceManager::CDeviceManager(CMainFrame &mf)
: m_mf4DM(mf), 
m_pCurrentDevice(NULL),
m_SelectedDevice(0)
{
	
}

CDeviceManager::~CDeviceManager()
{	 	
}
 DWORD CDeviceManager::CurrentDeviceChanged(int nNumber)
{
	CDevice* pOldDevice = m_pCurrentDevice;
	DWORD result = 0;
	
	m_SelectedDevice = nNumber;
	
	if (nNumber == 0) {
		// Actually in the bus view no BCAM object is selected. If there is no MDI associated
		// with m_pCurrentDevice, m_pCurrentDevice is set to NULL. Otherwise m_pCurrentDevice 
		// remains unchanged, i.e. m_pCurrentDevice corresponds to the MDI window.
		if ( GetCurrentChild() == NULL ){
			// no child window exists, set current device to NULL
			m_pCurrentDevice = NULL;
		}
		else{
			// current device remains unchanged
			return 0;
		}
	}
	else{
		CDevice *pDevice = GetDevice(nNumber);
		if (pDevice == m_pCurrentDevice) 
			return 0;   // nothing has changed
		m_pCurrentDevice = pDevice;
	}
	
	// open the new device
	try
	{ 
	}
	CATCH_REPORT();
	
	// inform the main frame
	m_mf4DM.CurrentDeviceChanged(m_pCurrentDevice);
	
	if (m_pCurrentDevice != NULL) {
		Device2ChildFrame_t		&m = m_xd_Dev_2_ChildFrame;
		Device2ChildFrame_t::iterator it = m.find(m_pCurrentDevice);
		if ( it != m.end() )
		{
			// a MDI child exists, activate it
			CChildFrame* pChild = it->second;
			pChild->SetFocus();
		}
	}
	
	return result;
}


void CDeviceManager::AddMDIChild()
{ 
	Device2ChildFrame_t		&m = m_xd_Dev_2_ChildFrame;
	assert ( m_pCurrentDevice != NULL );
	assert ( m.find(m_pCurrentDevice) == m.end() );  // only one window per device is allowed
	CChildFrame* pChild = new CChildFrame(m_pCurrentDevice, *this, m_mf4DM);
	
	pChild->m_WinName =  
		CString(" Viewer (") + 
		"111"/*m_pCurrentDevice->m_pInfo->NodeId()*/ + 
		CString(" )");

	pChild->CreateEx(m_mf4DM.m_hWndMDIClient,NULL, pChild->m_WinName);
	
	m_pCurrentDevice->SetMDIChild(pChild); 
	m_xd_Dev_2_ChildFrame[m_pCurrentDevice] = pChild;
	
	// try to restore the window layout
	pChild->v.RestoreLayout();
	
	BOOL bMaximized = FALSE;
	m_mf4DM.MDIGetActive(&bMaximized);
	if ( bMaximized )
		pChild->ShowWindow(SW_MAXIMIZE);
		
}

CChildFrame* CDeviceManager::GetCurrentChild()
{
	if ( m_pCurrentDevice == NULL )
		return NULL;
	Device2ChildFrame_t &m = m_xd_Dev_2_ChildFrame;
	Device2ChildFrame_t::iterator it = m.find(m_pCurrentDevice);
	if ( it != m.end() ) 
		return it->second;
	else 
		return NULL;
}



bool CDeviceManager::ExistMDIChild()
{
	assert ( m_pCurrentDevice != NULL );
	return ( m_xd_Dev_2_ChildFrame.find(m_pCurrentDevice) != m_xd_Dev_2_ChildFrame.end() );
}
 
 
void CDeviceManager::MDIChildActivated(CChildFrame* pChild)
{
	 
	
}

BOOL CDeviceManager::AddDevice(int nNumber, HWND hWndNotify)
{
	CDevice* pDevice = NULL;  
	int nIndex = 0; 
	int type = 0;
	switch(type){   
	case 1: //28
        pDevice = new CDevice(nNumber, nIndex, hWndNotify, m_mf4DM);
		pDevice->m_strModelName.Format("SV1410FC%d",nNumber); 
        break;  
	case 2: //36
        pDevice = new CDevice(nNumber, nIndex, hWndNotify, m_mf4DM);
		pDevice->m_strModelName.Format("Tools-%d",nNumber);
		
        break; 
	case 3: //38  xdTest_002: BtflKlines
        pDevice = new CDevice(nNumber, nIndex, hWndNotify, m_mf4DM); 
		pDevice->m_strModelName.Format("BtflKlines-%d",nNumber); 
        break;  
	default:
        pDevice = new CDevice(nNumber, nIndex, hWndNotify, m_mf4DM); 
		pDevice->m_strModelName.Format("BtflKlines-%d",nNumber); 
	}
	if ( pDevice == NULL ) return FALSE;	
	m_DevNumber2Device[nNumber] = pDevice;
	pDevice->m_Type = type; 

	return TRUE;
}
CDevice* CDeviceManager::GetDevice(int nNumber)
{
	DevNumber2Device_t::iterator it = m_DevNumber2Device.find(nNumber);
	
	return it == m_DevNumber2Device.end() ? NULL : it->second;
}

BOOL CDeviceManager::RemoveDevice(int nNumber)
{
	 
	
	return TRUE;
} 
 
void CDeviceManager::MDIChildClosed(CChildFrame* pChild)
{
	Device2ChildFrame_t &m = m_xd_Dev_2_ChildFrame;
	for ( Device2ChildFrame_t::iterator it = m.begin(); it != m.end(); ++it )
	{
		if (it->second == pChild){
			CDevice* pDevice = it->first;
			 
			m_xd_Dev_2_ChildFrame.erase(it);
			pDevice->SetMDIChild(NULL); 

			break;
		}
	} 
	assert ( it != m_xd_Dev_2_ChildFrame.end() );
	 
    if ( m_xd_Dev_2_ChildFrame.size() == 0 && m_SelectedDevice == 0)
		CurrentDeviceChanged(NULL);
}

void CDeviceManager::GrabSingle()
{ 
	
}
 
//------------------------------------------------------------------------------
void CDeviceManager::GrabContinuous()
{
 
}


void CDeviceManager::OnSoftTrigger()
{ 
}



void CDeviceManager::XD_Show_All_FPS()
{
}
 //------------------------------------------------------------------------------
void CDeviceManager::GrabCancel()
{

}

void CDeviceManager::GrabFromFile(CString FileName)
{
	assert ( m_pCurrentDevice != NULL );
	if ( m_pCurrentDevice == NULL )
		return;
	if ( ! ExistMDIChild() )
		AddMDIChild();
	m_pCurrentDevice->GrabFromFile( FileName);

}

 void CDeviceManager::ShowPixelValue()
{ 
	HWND hWndChild = GetWindow(m_mf4DM.m_hWndMDIClient, GW_CHILD);
	
	CPoint pt;
	GetCursorPos(&pt);
	
	bool eraseValuePane = true;
	bool erasePosPane = true;
	
	while ( hWndChild != NULL )
	{
		CRect rect;
		GetWindowRect(hWndChild, &rect);
		if ( rect.PtInRect(pt) )
		{
			 
		}
		hWndChild = GetWindow(hWndChild, GW_HWNDNEXT);
	}
	if ( eraseValuePane )
	{
		m_mf4DM.m_Sbar.SetPaneText(ID_VALUE_PANE, CString("") );
	}
	if ( erasePosPane )
	{
		m_mf4DM.m_Sbar.SetPaneText(ID_POS_PANE, CString("") );
	}
}

unsigned int CDeviceManager::OnXdGetFrameRate(double& d)
{
	unsigned int n = 0;
	CPoint pt;
	GetCursorPos(&pt);
	//	fps_acquired = -1;
	d = -1;
	
	HWND hWndChild = m_mf4DM.MDIGetActive();
	if ( hWndChild != NULL )
	{
		 
	}
	return n;
}

 
void CDeviceManager::AppExit(bool b)
{	
	ATLTRACE("CDeviceManager::AppExit() \n");


}

 void CDeviceManager::plReportError(BTFLBaseException& e)
{
	m_mf4DM.plReportError(e);
}