
//ErrorBox.h

#if !defined(AFX_ERRORBOX_H__0D8F7C56_C6C9_11D5_9264_0090278E5E96__INCLUDED_)
#define AFX_ERRORBOX_H__0D8F7C56_C6C9_11D5_9264_0090278E5E96__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CErrorBox  : public CDialogImpl<CErrorBox>
{
public:
	enum {IDD=IDD_ERRORBOX};
	
	BEGIN_MSG_MAP(CErrorBox)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_ID_HANDLER(IDOK, OnCloseCmd)
		END_MSG_MAP()
		
		CErrorBox()  {};
	
	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	
	LRESULT OnCloseCmd(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	
	void plReportError(BTFLBaseException& e);
	
	
private:
};

#endif // !defined(AFX_ERRORBOX_H__0D8F7C56_C6C9_11D5_9264_0090278E5E96__INCLUDED_)
