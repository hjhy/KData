#include "stdafx.h"
#include "resource.h"
#include "MainFrm.h"
#include "ChildFrm.h"

CMainFrame::CMainFrame() :
	m_DeviceManager(*this),
	m_xdItemsView(m_DeviceManager, *this),
	m_nDeviceNum(2)
{ 
}

CMainFrame::~CMainFrame()
{
}
void CMainFrame::pvViewZoomBest(WORD wID)
{
	if(ID_VIEW_ZOOM_BEST == wID)
	{
		CChildFrame* pChildFrame = m_DeviceManager.GetCurrentChild();
		if ( pChildFrame != NULL )
		{
			try
			{
				pChildFrame->v.OnXdZoomToFit();
			} CATCH_REPORT();
		}
		
	}
}
void CMainFrame::pvViewZoomSwitch(WORD wID)
{
	try
	{
		CChildFrame* pChildFrame = m_DeviceManager.GetCurrentChild();
		if ( pChildFrame != NULL )
		{
			double zoom = 1.0;
			switch ( wID )
			{
			case ID_VIEW_ZOOM_25:
				zoom = 0.25;
				break;
			case ID_VIEW_ZOOM_50:
				zoom = 0.5;
				break;
			case ID_VIEW_ZOOM_100:
				zoom = 1.0;
				break;
			case ID_VIEW_ZOOM_200:
				zoom = 2.0;
				break;
			case ID_VIEW_ZOOM_400:
				zoom = 4.0;
				break;
			case ID_VIEW_ZOOM_USER:
				{/*
					CZoomDlg dlg;
					zoom = pChildFrame->v.GetZoomLevel();
					dlg.SetFactor(zoom);
					if ( dlg.DoModal() == IDOK )
					{
						zoom = dlg.GetFactor();
					}
					if ( zoom < 1.0 / 32.0 )
					{
						zoom = 1.0 / 32.0; 
						MessageBeep(0);
					}
					else if ( zoom > 32 )
					{
						MessageBeep(0);
						zoom = 32;
					}
					*/
				}
				break;
			default:
				return;//assert(false);
			}
			
			pChildFrame->v.SetZoomLevel(zoom);
			pChildFrame->v.ZoomIn(NULL, 0);
		}
	}
	CATCH_REPORT(); 
}

void CMainFrame::plUpdateUI()
{
	// update UI elements 
	bool MDIOpen = false;
	bool GrabActive = false;
	bool SingleGrabActive = false;
	bool WhiteBalanceActive = false;
	bool bAOI = false;
	int snapmode=0;
	
	CDevice* pDevice = m_DeviceManager.GetCurrentDevice();
	if ( pDevice != NULL ){
		MDIOpen =  m_DeviceManager.ExistMDIChild(); 
	}
	
 	

	UIEnable(ID_FILE_NEW, (pDevice != NULL ) && ! MDIOpen);
	UIEnable(ID_FILE_OPEN, (pDevice != NULL ) && ! GrabActive );
	UIEnable(ID_FILE_SAVE, (pDevice != NULL ) && ! GrabActive && pDevice->GetBitmap() != NULL );
	UIEnable(ID_EDIT_COPY, (pDevice != NULL ) && ( pDevice->GetBitmap() != NULL )) ;

 	UIEnable(ID_VIEW_ZOOM_BEST, (pDevice != NULL ) && MDIOpen && ! m_DeviceManager.GetCurrentChild()->v.IsZoomedToFit());
 	
	UIEnable(ID_WINDOW_CASCADE, MDIOpen);
	UIEnable(ID_WINDOW_TILE_HORZ, MDIOpen);
	UIEnable(ID_WINDOW_TILE_VERT, MDIOpen);
	UIEnable(ID_WINDOW_ARRANGE, MDIOpen);
 
 
	UIUpdateToolBar();
}
BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
	
//	printf("PreTranslateMessage: ->\n");
	if(CMDIFrameWindowImpl<CMainFrame>::PreTranslateMessage(pMsg))
		return TRUE;
	  
	HWND hWnd = MDIGetActive();
	if(hWnd != NULL)
		return (BOOL)::SendMessage(hWnd, WM_FORWARDMSG, 0, (LPARAM)pMsg);
	return FALSE; 
}

BOOL CMainFrame::OnIdle()
{  
//	printf("OnIdle: ...\n");
	
	plUpdateUI();
	return FALSE;
}
 
LRESULT CMainFrame::OnCommand(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{ 
	printf("OnCommand: wParam= x%x(%d)\n",wParam,wParam); 
	WORD wID = LOWORD(wParam);
	pvFileNew(wID);
	PvFileOpen(wID);
	pvFileExit(wID);
	pvViewToolBar(wID);
	pvHelpAbout(wID);
	pvViewStatusBar(wID);
	pvViewBus(wID);
	pvWindowTileHorz(wID);	
	pvWindowTileVert(wID);
	pvWindowCascade(wID);
	pvViewZoomBest(wID);
	pvViewZoomSwitch(wID);
	
	return 0;
}


LRESULT CMainFrame::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	printf("OnCreate: ->\n");
	HWND hWndCmdBar = m_CmdBar.Create(m_hWnd, rcDefault, NULL, ATL_SIMPLE_CMDBAR_PANE_STYLE);
	
	HMENU hMenu = GetMenu();
	m_CmdBar.AttachMenu(hMenu);

	SetMenu(NULL);

 	HWND hWndToolBar = CreateSimpleToolBarCtrl(m_hWnd, IDR_MAINFRAME, FALSE, ATL_SIMPLE_TOOLBAR_PANE_STYLE);
	
 	CreateSimpleReBar(ATL_SIMPLE_REBAR_NOBORDER_STYLE);
	AddSimpleReBarBand(hWndCmdBar);
 	AddSimpleReBarBand(hWndToolBar, NULL, TRUE);
	CreateSimpleStatusBar("");
	
	m_Sbar.SubclassWindow(m_hWndStatusBar);
	int arrParts[] =
	{
		    ID_DEFAULT_PANE,
			ID_AOI_PANE,
			ID_POS_PANE,
			ID_VALUE_PANE, 
			ID_FPS_DISPLAYED_PANE
	};
	m_Sbar.SetPanes(arrParts, sizeof(arrParts) / sizeof(int), false);

	
	CreateMDIClient(); 
	m_MenuControl.Install(m_CmdBar, m_hWndMDIClient);
	m_CmdBar.SetMDIClient(m_hWndMDIClient);
	UIAddToolBar(hWndToolBar);
	
	UISetCheck(ID_VIEW_TOOL_BAR, 1);
	UISetCheck(ID_VIEW_STATUSBAR, 1);


	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->AddMessageFilter(this);
	pLoop->AddIdleHandler(this);
	
	m_xdDockWnd.m_hwndClient = NULL;
	HWND hwndDock = m_xdDockWnd.Create(m_hWnd, rcDefault);
	m_xdDockWnd.SetExtendedDockStyle(DCK_EX_REMEMBERSIZE);

	m_xdDockWnd.SetPaneSize(DOCK_LEFT, 250);

	
	int iBusCy = 150, iPropertyCy = 150;

	// Create bus viewer window
	m_xdItemsView.Create(hwndDock, rcDefault);
	CString str;
	str.LoadString(IDS_BUS_VIEW);
	m_xdItemsView.SetWindowText(str);
	m_xdDockWnd.AddWindow(m_xdItemsView);
	m_xdDockWnd.DockWindow(m_xdItemsView, DOCK_RIGHT,//DOCK_LEFT,
		iBusCy); 
	
	UISetCheck(ID_VIEW_BUSVIEW, TRUE);
 
	m_xdDockWnd.SetClient(m_hWndMDIClient);
	m_hWndClient = hwndDock;

	pvMyTitle();

	return 0; 
}
void CMainFrame::CurrentDeviceChanged(CDevice* pDevice)
{
	try
	{
		// refresh the feature views
 		m_xdItemsView.CurrentDeviceChanged(pDevice); 

	}
	CATCH_REPORT()
}

