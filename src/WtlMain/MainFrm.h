// MainFrm.h : interface of the CMainFrame class

#if !defined(AFX_MAINFRM_H__4E614EAB_75B9_4B7A_87FB_DDFFF6B86D50__INCLUDED_)
#define AFX_MAINFRM_H__4E614EAB_75B9_4B7A_87FB_DDFFF6B86D50__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include "resource.h"
#include "Devicemanager.h"
#include "Device.h"
#include "busview.h"

#include "aboutdlg.h"
class CDevicemanager;

class CMainFrame :  public CMDIFrameWindowImpl<CMainFrame>, 
					public CUpdateUI<CMainFrame>,
					public CMessageFilter,
					public CIdleHandler
{  	
	public: 
		DECLARE_FRAME_WND_CLASS(NULL, IDR_MAINFRAME)
			
		CMainFrame();
		~CMainFrame(); 
		void plUpdateUI();
		
		virtual BOOL PreTranslateMessage(MSG* pMsg);
		virtual BOOL OnIdle(); 
		
		BEGIN_UPDATE_UI_MAP(CMainFrame) 
			UPDATE_ELEMENT(ID_FILE_NEW, UPDUI_MENUPOPUP | UPDUI_TOOLBAR)
			UPDATE_ELEMENT(ID_VIEW_TOOL_BAR, UPDUI_MENUPOPUP)
			UPDATE_ELEMENT(ID_VIEW_STATUSBAR, UPDUI_MENUPOPUP)	
			UPDATE_ELEMENT(ID_VIEW_BUSVIEW,UPDUI_MENUPOPUP)	
			
			UPDATE_ELEMENT(ID_VIEW_ZOOM_BEST, UPDUI_MENUPOPUP | UPDUI_TOOLBAR )
		END_UPDATE_UI_MAP()

		
		BEGIN_MSG_MAP(CMainFrame)
			MESSAGE_HANDLER(WM_CREATE, OnCreate) 	
			MESSAGE_HANDLER(WM_COMMAND, OnCommand)
 
			CHAIN_MSG_MAP(CUpdateUI<CMainFrame>)
			CHAIN_MSG_MAP(CMDIFrameWindowImpl<CMainFrame>) 
		END_MSG_MAP() 

		LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
		LRESULT OnCommand(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled);
 		
	public:
		
		void CurrentDeviceChanged(CDevice* pDevice);
		int CMainFrame::GetDeviceNum()
		{
			return m_nDeviceNum;
		} 
		void CMainFrame::plReportError(BTFLBaseException& e)
		{
			if ( ! ::IsWindow(m_ErrorBox) )
			{
				m_ErrorBox.Create(m_hWnd);
				m_ErrorBox.ShowWindow(SW_SHOW);
			}
			m_ErrorBox.plReportError(e);
		}
 
			
	private:
		void pvViewToolBar(WORD wID)
		{
			if(ID_VIEW_TOOL_BAR == wID)
			{
				printf("OnCommand: wParam= ID_VIEW_TOOL_BAR\n"); 
				static BOOL bVisible = TRUE;	// initially visible
				bVisible = !bVisible;
				CReBarCtrl rebar = m_hWndToolBar;
				int nBandIndex = rebar.IdToIndex(ATL_IDW_BAND_FIRST + 1);	// toolbar is 2nd added band
				rebar.ShowBand(nBandIndex, bVisible);
				UISetCheck(ID_VIEW_TOOL_BAR, bVisible);
				UpdateLayout();
			}
		}
		void pvHelpAbout(WORD wID)
		{
			if(ID_APP_ABOUT == wID)
			{
				printf("OnCommand: wParam= ID_APP_ABOUT\n");
				
				CAboutDlg dlg;
				dlg.DoModal();
			}
		}
		void pvViewZoomSwitch(WORD wID);

		void pvFileNew(WORD wID)
		{
			if(ID_FILE_NEW == wID)
			{
				printf("OnCommand: wParam= ID_FILE_NEW\n");
				if (m_DeviceManager.GetCurrentDevice() != NULL && \
					!m_DeviceManager.ExistMDIChild())
				{
					m_DeviceManager.AddMDIChild();
				}
				
			}
		}
		void PvFileOpen(WORD wID)
		{
			if(ID_FILE_OPEN == wID) 
			{
				printf("OnCommand: wParam= ID_FILE_OPEN\n");
				if ( m_DeviceManager.GetCurrentDevice() != NULL )
				{
					
					CFileDialog dlg(TRUE, _T("bmp"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("Bitmap Files (*.bmp)\0*.bmp\0All Files (*.*)\0*.*\0"), m_hWnd);
					if(dlg.DoModal() == IDOK)
					{
						try
						{
							m_DeviceManager.GrabFromFile(dlg.m_szFileName);
						}
						CATCH_REPORT();
					}
				}
				
			}
		}
		void pvFileExit(WORD wID)
		{
			if(ID_APP_EXIT == wID)
			{
				printf("ID_APP_EXIT: ...\n"); 
				PostMessage(WM_CLOSE);
			}
		}
		void pvWindowTileHorz(WORD wID)
		{
			if(ID_WINDOW_TILE_HORZ == wID)
			{
				printf("pvWindowTileHorz: ...\n");
				MDITile();
			}
		}
		void pvWindowTileVert(WORD wID)
		{
			if(ID_WINDOW_TILE_VERT == wID)
			{
				printf("ID_WINDOW_TILE_VERT: ...\n");
				MDITile(MDITILE_VERTICAL);
			}
		}
		void pvWindowCascade(WORD wID)
		{
			if(ID_WINDOW_CASCADE == wID)
			{
				printf("ID_WINDOW_CASCADE: ...\n");
				MDICascade();
			}
		}
		void pvViewStatusBar(WORD wID)
		{
			if(ID_VIEW_STATUSBAR == wID)
			{
				printf("OnCommand: wParam= ID_VIEW_STATUSBAR\n");  
				BOOL bVisible = !::IsWindowVisible(m_hWndStatusBar);
				::ShowWindow(m_hWndStatusBar, bVisible ? SW_SHOWNOACTIVATE : SW_HIDE);
				UISetCheck(ID_VIEW_STATUSBAR, bVisible);
				UpdateLayout();
			}
		}
		void pvViewBus(WORD wID)
		{
			if(ID_VIEW_BUSVIEW == wID)
			{
				BOOL visible;
				if( visible = ::IsWindowVisible(m_xdItemsView) ) 
					m_xdDockWnd.HideWindow(m_xdItemsView); 
				else 
					m_xdDockWnd.DockWindow(m_xdItemsView, DOCK_LASTKNOWN);
				UISetCheck(ID_VIEW_BUSVIEW, ! visible);
				UpdateLayout();
			}
		}
		void pvViewZoomBest(WORD wID);
		void pvMyTitle()
		{ 
			char szFilePath[256]; 
			memset(szFilePath,0,256); 
			::GetModuleFileName(NULL,szFilePath,256);
		//	PathRemoveFileSpec(szFilePath); 
			 
			
			char szV[256];
			xdGetModuleVersion(szFilePath,szV);
			
			CString strTitle;
			strTitle.Format("WtlMain.exe[%s]", szV);
			SetWindowText(strTitle);
		}
		void xdGetModuleVersion(IN char *szFullPath,OUT char *szV)
	{ 
		DWORD dwVerInfoSize = 0;
		DWORD dwVerHnd;
		VS_FIXEDFILEINFO * pFileInfo;
		
		//	GetModuleFileName(NULL, szFullPath, sizeof(szFullPath));
		dwVerInfoSize = GetFileVersionInfoSize(szFullPath, &dwVerHnd);
		if (dwVerInfoSize)
		{
			// If we were able to get the information, process it:
			HANDLE  hMem;
			LPVOID  lpvMem;
			unsigned int uInfoSize = 0;
			
			hMem = GlobalAlloc(GMEM_MOVEABLE, dwVerInfoSize);
			lpvMem = GlobalLock(hMem);
			GetFileVersionInfo(szFullPath, dwVerHnd, dwVerInfoSize, lpvMem);
			
			::VerQueryValue(lpvMem, (LPTSTR)_T("\\"), (void**)&pFileInfo, &uInfoSize);
			
			WORD m_nProdVersion[4];
			
			// Product version from the FILEVERSION of the version info resource 
			m_nProdVersion[0] = HIWORD(pFileInfo->dwProductVersionMS); 
			m_nProdVersion[1] = LOWORD(pFileInfo->dwProductVersionMS);
			m_nProdVersion[2] = HIWORD(pFileInfo->dwProductVersionLS);
			m_nProdVersion[3] = LOWORD(pFileInfo->dwProductVersionLS); 
			
			//*
			sprintf(szV,_T("%d.%d.%d.%d"),m_nProdVersion[0],
				m_nProdVersion[1],m_nProdVersion[2],m_nProdVersion[3]);
			//*/
			
			GlobalUnlock(hMem);
			GlobalFree(hMem);
			
			
		}
	}
	public:
		CMultiPaneStatusBarCtrl m_Sbar;
	private:
		
		CDeviceManager          m_DeviceManager;
		CMDICommandBarCtrl		m_CmdBar;
		CMDIMenuControl			m_MenuControl;
		CDotNetDockingWindow	m_xdDockWnd;
		CXdSubItemsView         m_xdItemsView;

		int						m_nDeviceNum;
		CErrorBox               m_ErrorBox;

		friend class CDeviceManager; 
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__4E614EAB_75B9_4B7A_87FB_DDFFF6B86D50__INCLUDED_)
