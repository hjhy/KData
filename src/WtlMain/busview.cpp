// BusView.cpp: implementation of the CXdSubItemsView class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
 #include "BusView.h"
 #include "mainfrm.h"
 


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


 
CXdSubItemsView::CXdSubItemsView(CDeviceManager& dm,CMainFrame& mf) 
: m_refXdDM(dm), m_xdMainFrm(mf), 
 UNABLE_TO_OPEN("<Cannot open device>") 
{
}



LRESULT CXdSubItemsView::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	CTreeViewCtrl &t = m_tvcXdItems;
	t.Attach(GetDlgItem(IDC_BUSTREE));
	
	// Create Image List
	m_ImageList.Create(16, 16, ILC_MASK, 4, 4);
	CBitmap img;
	CBitmap mask;
	// insert image for "This PC"
	img.LoadBitmap(IDB_BITMAP_KLINES);
	mask.LoadBitmap(IDB_BITMAP_KLINES1);
	m_ImageList.Add(img, mask);
	// insert image for "BCAM device"
	img.Detach();
	mask.Detach();
	img.LoadBitmap(IDB_BITMAP_SONG);
	mask.LoadBitmap(IDB_BITMAP_SONG1);
	m_ImageList.Add(img, mask);
	//xdTest_004:
	img.Detach();
	mask.Detach();
	img.LoadBitmap(IDB_BITMAP5);
	mask.LoadBitmap(IDB_BITMAP6);
	m_ImageList.Add(img, mask);
	//xdTest_004:  3,3
	img.Detach();
	mask.Detach();
	img.LoadBitmap(IDB_BITMAP_KLINES);
	mask.LoadBitmap(IDB_BITMAP_KLINES1);
	m_ImageList.Add(img, mask);
	
	//xdTest_004:  4,4
	img.Detach();
	mask.Detach();
	img.LoadBitmap(IDB_BITMAP_SONG);
	mask.LoadBitmap(IDB_BITMAP_SONG1);
	m_ImageList.Add(img, mask);

	t.SetImageList(m_ImageList, TVSIL_NORMAL);
	
	// Insert root item
	m_RootItem = t.InsertItem("Ư������԰", 0, 0, TVI_ROOT, TVI_LAST); 
    
	int nTotal = m_xdMainFrm.GetDeviceNum(); 	
	
	for(int i = 1; i <= nTotal; i++){
		xdAddNode(i);
	}
	
	t.Expand(m_RootItem);
	
	return 0;
}




LRESULT CXdSubItemsView::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{ 
	return 0;
}




LRESULT CXdSubItemsView::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	RECT rcClient;
	GetWindowRect(&rcClient);
	
	HWND hwnd;
	RECT rc;
	hwnd = GetDlgItem(IDC_BUSTREE);
	::GetWindowRect(hwnd, &rc);
	int w=rcClient.right - rc.left;
	int h=rcClient.bottom - rc.top;

	::SetWindowPos(hwnd, HWND_TOP, 
		0, 0, 
		w, h,
		SWP_NOMOVE|SWP_NOACTIVATE|SWP_NOZORDER);
	
	return 0;
}




LRESULT CXdSubItemsView::OnContextMenu(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
	UINT Flags;
	CPoint point = CPoint(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
	CTreeViewCtrl &t = m_tvcXdItems;
	t.ScreenToClient(&point);
	HTREEITEM hItem = t.HitTest(point, &Flags);
	if (hItem && (Flags & TVHT_ONITEM))
	{
		t.SelectItem(hItem); 
		 
	}
	
	return 0;
}
  void CXdSubItemsView::CurrentDeviceChanged(CDevice* pDevice)
{
	if ( ! pDevice )
		return;
	int DeviceNumber = pDevice->m_nNumber;// pDevice->m_pInfo->DeviceNumber();
	
	NodeMap_t::iterator it;
	for ( it = m_NodeMap.begin(); it != m_NodeMap.end(); ++it )
	{
		if ( it->second->GetDeviceNumber() == DeviceNumber )
		{
			m_tvcXdItems.SelectItem(it->first);
			break;
		}
	}
} 
LRESULT CXdSubItemsView::OnLButtonDblClk(int idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	try
	{
		CTreeViewCtrl &t = m_tvcXdItems;
		HTREEITEM hItem = t.GetSelectedItem();
		if ( m_NodeMap.find(hItem) != m_NodeMap.end() )
		{
			CBusNode* pNode = m_NodeMap[hItem];
			printf(" pNode = 0x%x\n",pNode);
			// open new MDI child
			if ( m_refXdDM.GetCurrentDevice() != NULL  && ! m_refXdDM.ExistMDIChild() )
				m_refXdDM.AddMDIChild();
			
		}
	} CATCH_REPORT();
	return 0;
}

LRESULT CXdSubItemsView::OnSelChanged(int idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{	 
	try{
		LPNMTREEVIEW pnmtv = (LPNMTREEVIEW) pnmh;
		HTREEITEM hItem = pnmtv->itemNew.hItem;
		if ( m_NodeMap.find(hItem) != m_NodeMap.end() ){
			CBusNode* pNode = m_NodeMap[hItem];
			int n = pNode->GetDeviceNumber(); 
			printf("n = %d\n",n); 
			DWORD error = m_refXdDM.CurrentDeviceChanged(n);
		}
		else{ 
			m_refXdDM.CurrentDeviceChanged(0);
			printf("n = 0\n");
		}
	}
	CATCH_REPORT();
	
	return 0;
}
void CXdSubItemsView::plReportError(BTFLBaseException& e) 
		{
			m_xdMainFrm.plReportError(e);
		}
void CXdSubItemsView::xdAddNode(int nNumber)
{
	CBusNode *pNode = NULL;
	CString ModelName = UNABLE_TO_OPEN;
 	
	if ( ContainsNode(nNumber) ){
		RemoveNode(nNumber);
	}
	
	try
	{
		// add a camera device
		CDevice *pDevice = NULL;
		m_refXdDM.AddDevice(nNumber, m_hWnd);
		pDevice = m_refXdDM.GetDevice(nNumber);
		assert ( pDevice != NULL );
		
		try
		{
			ModelName = pDevice->m_strModelName; 
			try
			{
			}
			catch ( BTFLBaseException& e)
			{
				if ( e.Error() != BTFL_STATUS_INTERNAL_ERROR )
					plReportError(e);
			} 
		}
		CATCH_REPORT(); 
	
		
		pNode = new CBusNode(ModelName, nNumber, 0);
		if ( pNode == NULL )
		{
			throw BTFLBaseException(ERROR_OUTOFMEMORY, "CXdSubItemsView::AddNode()");
		}
	}
	CATCH_REPORT();

	CTreeViewCtrl &t = m_tvcXdItems;
	HTREEITEM hItem;
	//xdTest_004:
	if(-1 != ModelName.Find("Klines",0))
	{
		 hItem= t.InsertItem(ModelName, 3, 3, m_RootItem, TVI_LAST);
	}
	else if(-1 != ModelName.Find("Tools",0))
	{
		 hItem= t.InsertItem(ModelName, 4, 4, m_RootItem, TVI_LAST);
	}
	else
	{
		 hItem= t.InsertItem(ModelName, 1, 1, m_RootItem, TVI_LAST);
	}
	t.Expand(m_RootItem);
	m_NodeMap[hItem] = pNode;
}

bool CXdSubItemsView::ContainsNode(int nNumber)
{ 
	return false;
}
 
void CXdSubItemsView::RemoveNode(int nNumber)
{ 
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Protected / private member functions
//
 
 