//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Script1.rc
//
#define IDR_MAINFRAME                   1
#define IDR_MDICHILD                    2
#define IDC_CURSOR1                     101
#define IDD_BUSVIEW                     101
#define IDC_CURSOR2                     103
#define IDB_BITMAP1                     108
#define IDB_BITMAP2                     109
#define IDD_ERRORBOX                    109
#define IDB_BITMAP3                     110
#define IDD_ABOUTBOX                    110
#define IDB_BITMAP4                     111
#define IDB_BITMAP_SONG1                112
#define IDB_BITMAP_SONG                 113
#define IDB_BITMAP_KLINES1              114
#define IDB_BITMAP_KLINES               115
#define IDB_BITMAP5                     116
#define IDB_BITMAP6                     117
#define IDB_BUTTONS                     118
#define IDC_CROSSHAIR                   119
#define IDC_TRACKNWSE                   120
#define IDC_TRACKNESW                   121
#define IDC_TRACKNS                     122
#define IDC_TRACKWE                     123
#define IDC_TRACK4WAY                   124
#define IDC_MOVE4WAY                    125
#define IDC_BUSTREE                     1000
#define IDC_STATICMESSAGE               1001
#define IDC_BTN_BMP                     1002
#define IDC_EDIT_MESSAGE                1003
#define ID_FILE_1                       40001
#define ID_VIEW_TOOL_BAR                40002
#define ID_VIEW_STATUSBAR               40003
#define ID_VIEW_BUSVIEW                 40004
#define ID_VIEW_ZOOM_25                 40005
#define ID_VIEW_ZOOM_50                 40006
#define ID_VIEW_ZOOM_100                40007
#define ID_VIEW_ZOOM_200                40008
#define ID_VIEW_ZOOM_400                40009
#define ID_VIEW_ZOOM_USER               40010
#define ID_VIEW_ZOOM_BEST               40011
#define ID_AOI_PANE                     59393
#define ID_POS_PANE                     59394
#define ID_VALUE_PANE                   59395
#define ID_FPS_DISPLAYED_PANE           59396
#define IDS_BUS_VIEW                    59397

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        120
#define _APS_NEXT_COMMAND_VALUE         40012
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
