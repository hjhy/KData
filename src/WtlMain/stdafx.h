// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__FA203538_A46F_4159_A6E6_4C552951EB8A__INCLUDED_)
#define AFX_STDAFX_H__FA203538_A46F_4159_A6E6_4C552951EB8A__INCLUDED_

// Change these values to use different versions
//#define WINVER		0x0400
#define _WIN32_WINNT	0x0400
//#define _WIN32_IE	0x0400
//#define _RICHEDIT_VER	0x0100

#define _WTL_USE_CSTRING

#include <atlbase.h>
#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>

#define _ATL_USE_CSTRING_FLOAT

#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>
#include <atlctrlw.h>
#include <atlmisc.h>
#include <atlctrlx.h> 
#include <atlddx.h>

/*
#pragma warning(disable:4786)
#pragma warning(disable:4251)
#pragma warning(disable:4273)
*/ 

#include <vector>
#include <map>
#include <list>
using namespace std; 

#include <assert.h>
#include <windef.h>
 

#include "CMDIMenuControl.h"
#include "atldock.h"
#include "atldock2.h"

#include "resource.h"
#include "BLBase.h"
using namespace np_BTFL;
#include "ErrorBox.h"
#include "utility.h"

#define MINWIDTH				16
#define MINPOINT				16
 
#pragma comment(lib,"version.lib")
#pragma comment(lib,"Shlwapi.lib ")  

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__FA203538_A46F_4159_A6E6_4C552951EB8A__INCLUDED_)
