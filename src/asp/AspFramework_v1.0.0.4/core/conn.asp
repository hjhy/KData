<!--#include file="config.asp"-->
<!--#include file="tools.asp"-->
<%
'#########################################################
'##	Version		:	v1.0.0.4
'##	Author		:	Meilixueshan(44476511@qq.com)
'##	Update Date	:	2014-07-20
'##	License		:	MIT
'#########################################################
Class AspFrameworkClass
	Dim af_rs
	
	Public Conn
	Public ViewData
	Public Helper

	Private Sub Class_Initialize
		Set ViewData = New AspFrameworkViewDataClass
		Set Helper = New AspFrameworkToolsClass

		Set conn = Server.CreateObject("ADODB.Connection") 
		conn.Open const_ConnectionString
	End Sub

	Private Sub Class_Terminate
		Set ViewData = Nothing
		Set Helper = Nothing

		conn.close
		set conn = nothing
	End Sub

	Public Sub ExecuteNonQuery(sql)
		conn.execute(sql)
	End Sub

	Public Function GetValue(sql)
		Dim returnValue
		Set af_rs = conn.execute(sql)
		If Not( af_rs.Eof Or af_rs.Bof ) Then
			returnValue = af_rs(0)
		Else
			returnValue = ""
		End if
		af_rs.close
		Set af_rs = Nothing
		GetValue = returnValue
	End Function

	Public Function GetNextID(tableName, idFieldName)
		Dim sqlNextIdQuery
		sqlNextIdQuery = "select max("&idFieldName&") from ["&tableName&"]"
		nextId = GetValue(sqlNextIdQuery)
		If nextId = "" Then
			GetNextID = 1
		Else
			GetNextID = CInt(nextId) + 1
		End If
	End Function

	Public Sub DataRender(sql, execMethodStr)
		Set af_rs = conn.execute(sql)
		if not af_rs.bof and not af_rs.eof Then
			Execute execMethodStr&" af_rs"
		end if
		af_rs.close
		Set af_rs = nothing
	End Sub

	Public Sub DataRender_Simple(sql, strMethod)
		Set af_rs = conn.execute(sql)
		if not af_rs.bof and not af_rs.eof Then
			itemIndex = 0
			do while not af_rs.eof
				Execute strMethod&" af_rs, "&itemIndex&""
				af_rs.movenext
				itemIndex = itemIndex + 1
			loop
		end if
		af_rs.close
		Set af_rs = nothing
	End Sub
End Class

Class AspFrameworkViewDataClass
	Dim vdata
	Private Sub Class_Initialize
		Set vdata = Server.CreateObject("Scripting.Dictionary")
	End Sub

	Private Sub Class_Terminate
		set vdata = nothing
	End Sub

	Public Function SetValue(ByVal strKey, ByVal value)
		if vdata.Exists(strKey)=true then
			vdata.Remove(strKey)
		end If
		vdata.add strKey, value
	End Function

	Public Function GetValue(ByVal strKey)
		if vdata.Exists(strKey)=true then
			GetValue = vdata.Item(strKey)
		Else
			GetValue = ""
		end If
	End Function
End Class

Set	AF = New AspFrameworkClass
%>