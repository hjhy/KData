<%
'////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'分页显示类
'使用时，先定义一个css： .pagecssB{font-size:12px} .pagecssR{font-size:12px; color:#FF0000}
Class AspFrameworkPagerClass
	Private Show_PageSize,Show_CurrentPage,Show_MaxPage,Show_RecordCount
	Private Show_Rs,Show_Conn,Show_Sql
	
	'类初始化
	Private Sub Class_Initialize()
		Show_PageSize=10	'每页10条
		Show_CurrentPage=Trim(request("Page"))
		if trim(request.form("page"))<>"" then Show_CurrentPage=trim(request.form("page"))
		if (Show_CurrentPage="") or (not IsNumeric(Show_CurrentPage)) then
			Show_CurrentPage=1
		else
			Show_CurrentPage=clng(Show_CurrentPage)
		end if
		if Show_CurrentPage<1 then Show_CurrentPage=1
		
	End Sub
	
	'数据库引擎
	Public Property Let Conn(ByVal objConn)
		Show_Conn=objConn
	End Property
	
	'数据库查询语句
	Public Property Let Sql(ByVal str_sql)  
		Show_Sql=str_sql
	End Property
	
	'================================================================
	' PageSize 设置每一页记录条数,默认10记录
	'================================================================
	Public Property Let PageSize(ByVal intPageSize)
		Show_PageSize=clng(intPageSize)
	End Property
	
	Public Property Get PageSize
		PageSize=Show_PageSize
	End Property
	
	'最后一页（或者说是共多少页）
	Public Property Get LastPage()
		LastPage=Show_MaxPage
	End Property
	
	'记录数
	Public Property Get RecCount()
		RecCount=Show_RecordCount
	End Property
	
	'返回记录集
	Public Property Get ResultSet()
		Set Show_Rs=server.CreateObject("adodb.recordset")
		Show_Rs.PageSize=Show_PageSize
		Show_Rs.Open Show_Sql,Show_Conn,1,1
		If Not Show_Rs.Bof And Not Show_Rs.Eof Then
			Show_MaxPage=Show_Rs.PageCount	'总页数
			if Show_CurrentPage>Show_MaxPage then
				Show_CurrentPage=Show_MaxPage
			end if
			Show_Rs.AbsolutePage=Show_CurrentPage	'当前页面
		Else
			Show_MaxPage=1	
			Show_CurrentPage=1
		End if
		Show_RecordCount=Show_Rs.RecordCount	'总记录数	
		Set ResultSet=Show_Rs
	End Property
	
	'分页显示
	Public Sub ShowPage(ByVal strPageLinkUrl)
		Response.Write("<div class='pagecssB' align=center>")
		Response.Write("页次:<span style='color:#FF0000'>"&Show_CurrentPage&"</span>/"&Show_MaxPage)
		Response.Write("　")
		Response.Write("每页:<span style='color:#FF0000'>"&Show_PageSize&"</span>条")
		Response.Write("　")
		Response.Write("共计:<span style='color:#FF0000'>"&Show_RecordCount&"</span>条")
		Response.Write("　　")
		If Show_CurrentPage<=1 Then
			Response.Write("<span class='pagelink'>&lt;</span> ")	'首页
			'Response.Write("<span class='pagelink'>&lt;</span>")	'上一页
		Else
			Response.Write("<a href='?page=1"&strPageLinkUrl&"' class='pagelink' title='首页'>&lt;</a> ")	'首页
			'Response.Write("<a href='?page="&Show_CurrentPage-1&strPageLinkUrl&"' class='pagelink' title='上一页'>&lt;</a>")	'上一页
		End if
		'////////数字页码区////////////////////////////////////
		OncePageCount=5		'一次显示多少页
		HalfNum=OncePageCount\2
		If Show_MaxPage<=OncePageCount Then
			'如果总页数小于指定页数，则全部输出
			For iCount=1 to Show_MaxPage
				If iCount=Show_CurrentPage Then
					Response.Write(" <span class='pagelink currentPage'>"&iCount&"</span> ")
				Else
					Response.Write(" <a href='?page="&iCount&strPageLinkUrl&"' class='pagelink'>"&iCount&"</a> ")
				End If
			Next
		Else
			For iCount=(Show_CurrentPage-HalfNum) to (Show_CurrentPage-HalfNum+OncePageCount-1)
				If iCount>=1 and iCount<=Show_MaxPage Then
					If iCount=Show_CurrentPage Then
						Response.Write(" <span class='pagelink currentPage'>"&iCount&"</span>")
					Else
						Response.Write(" <a href='?page="&iCount&strPageLinkUrl&"' class='pagelink'>"&iCount&"</a> ")
					End If
				End If
			Next
		End If
		'//////////////////////////////////////////////////////
		If Show_CurrentPage>=Show_MaxPage Then
			'Response.Write(" <span class='pagelink'>&gt;</span> ")	'下一页
			Response.Write(" <span class='pagelink'>&gt;</span>")	'尾页
		Else
			'Response.Write("<a href='?page="&Show_CurrentPage+1&strPageLinkUrl&"' class='pagelink' title='下一页'>&gt;</a> ")	'下一页
			Response.Write("<a href='?page="&Show_MaxPage&strPageLinkUrl&"' class='pagelink' title='尾页'>&gt;</a>")	'尾页
		End If
		Response.Write("</div>")
	End Sub
	
	'================================================================
	' Class_Terminate 类注销
	'================================================================
	Private Sub Class_Terminate()
		Show_Rs.Close
		Set Show_Rs=Nothing
	End Sub
End Class
%>