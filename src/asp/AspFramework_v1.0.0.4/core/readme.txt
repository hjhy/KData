开源协议：MIT

使用方法：
1、根据您的开发或运行环境修改config.asp中的相关信息
2、在页面的第一行，加入<!--#include file="core/conn.asp"-->
3、在页面的最后一行，加入<!--#include file="core/conn_close.asp"-->

这样，就可以进行了使用和开发了。

小贴士：
用浏览器访问/core/sql_builder.asp页面，可以自动帮你生成insert和update语句，减少一些工作量。

示例：
1、simple_article
是一个关于文章的管理和展示的演示系统。它采用了一个类似asp.net中母版、用户组件的技术，展示了asp也可以采用一种另类的开发模式，可以降低开发和维护难度。