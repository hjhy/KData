<!--#include file="conn.asp"-->
<%
	Dim selTableName
	selTableName = Request("t")

	'获取表名
	Sub getTables(tableName)
		Set tmpRs = AF.Conn.openSchema(20)
		tmpRs.Filter = "table_type='table'"
		While not tmpRs.EOF
			If tableName = tmpRs(2) Then
				Response.Write("<option value='" & tmpRs(2) & "' selected='selected'>" & tmpRs(2) & "</option>")
			Else 
				Response.Write("<option value='" & tmpRs(2) & "'>" & tmpRs(2) & "</option>")
			End if
		tmpRs.MoveNext
		Wend
		tmpRs.close
		Set tmpRs = nothing
	End Sub
	
	Function NeedQuotationMark_Access(num)
		If num = 3 Or num = 6 Then
			NeedQuotationMark_Access = false
		Else 
			NeedQuotationMark_Access = true
		End if
	End Function

	Function NeedQuotationMark_SqlServer(num)
		Select Case num
			Case 2
			Case 3
			Case 4
			Case 5
			Case 6
			Case 11
			Case 17
			Case 20
				NeedQuotationMark_SqlServer = false
			Case 128
			Case 129
			Case 130
			Case 131
			Case 135
			Case 200
			Case 201
			Case 202
			Case 203
			Case 204
			Case 205
				NeedQuotationMark_SqlServer = true
			End Select
	End Function

	'生成数据库脚本
	Sub createSqlScript(tableName)
		Dim varValuePart
		Dim insertSql, insertFieldPart, insertValuePart
		Dim updateSql, updateFieldPart
		

		sql="select top 1 * from ["&tableName&"]"
		Set tmpRs = server.CreateObject("adodb.recordset")
		Set tmpRs = AF.conn.execute(sql)
		For i=0 To tmpRs.fields.count-1
			fieldname = tmpRs.fields(i).name
			fieldtype = tmpRs.fields(i).type
			needQuotationMark = False	'是否需要引号
			If const_DatabaseType = "Access" Then
				needQuotationMark = NeedQuotationMark_Access(fieldtype)
			Else
				needQuotationMark = NeedQuotationMark_SqlServer(fieldtype)
			End If

			varValuePart = varValuePart & "m_" & fieldname & " = AF.Helper.FormToAccess("""") <br />"

			If i>0 Then
				insertFieldPart = insertFieldPart & ","
				insertValuePart = insertValuePart & ","
			End If
			insertFieldPart = insertFieldPart & fieldname

			If needQuotationMark = true Then
				insertValuePart = insertValuePart & "'""&m_" & fieldname & "&""'"
			else
				insertValuePart = insertValuePart & """&m_" & fieldname & "&"""
			End if
			
			updateFieldPart = updateFieldPart & ", "
			If needQuotationMark = true Then
				updateFieldPart = updateFieldPart & fieldname & "='""&m_" & fieldname & "&""'"
			Else 
				updateFieldPart = updateFieldPart & fieldname & "=""&m_" & fieldname & "&"""
			End if
		Next
		tmpRs.MoveNext 

		tmpRs.close
		Set tmpRs = Nothing

		insertSql = varValuePart
		insertSql = insertSql & "sqlInsert = ""insert into ["&tableName&"](" & insertFieldPart & ") values(" & insertValuePart & ")"""

		updateSql = varValuePart
		updateSql = updateSql & "sqlUpdate = ""update ["&tableName&"] set " & Mid(updateFieldPart, 2) & " where 【此处加入条件语句】"""
		
		Response.write("<div class='divInsert'><div>插入数据：</div>" & insertSql & "</div>")
		Response.write("<hr />")
		Response.write("<div class='divUpdate'><div>更新数据：</div>" & updateSql & "</div>")
	End sub
%>
<!Doctype html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=gb2312" />
	<title>SQL语句生成器</title>
	<style type="text/css">
	body {font-family:'\5FAE\8F6F\96C5\9ED1','\9ED1\4F53',Arial,sans-serif; font-size:14px; line-height:20px;}
	.divInsert, .divUpdate {background-color:#e5e5e5; padding:10px;}
	</style>
	<script type="text/javascript">
		function drpTablesChange() {
			var tableName = document.getElementById("drpTables").value;
			document.location = "sql_builder.asp?t=" + tableName;
		}
	</script>
</head>
<body>
	<div>选择表：
		<select id="drpTables" name="drpTables" onchange="drpTablesChange()">
		<option value="">---请选择数据表---</option>
		<%Call getTables(selTableName)%>
		</select>
	</div>
	<hr />
	<div>
		<% If selTableName <> "" Then Call createSqlScript(selTableName) End if%>
	</div>
</body>
</html>
<!--#include file="conn_close.asp"-->