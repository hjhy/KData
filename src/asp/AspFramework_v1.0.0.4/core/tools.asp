<!--#include file="easp.asp"-->
<%
Class AspFrameworkToolsClass
	Public Function Url(relativePath)
		Url = const_SiteRoot & relativePath
	End Function

	'检测是否是站内提交
	Public Function IsInsidePost()
		Dim URL1,URL2
		ChkPost = False
		URL1 = Cstr(Request.ServerVariables("HTTP_REFERER"))
		URL2 = Cstr(Request.ServerVariables("SERVER_NAME"))
		If Mid(URL1,8,Len(URL2))<>URL2 Then
			IsInsidePost = False
		Else
			IsInsidePost = True
		End If
	End Function

	'转换成数字
	Public Function GetNumerValue(str)
		If isnull(str) Or isEmpty(str) or not isNumeric(str) then 
			GetNumerValue = 0
		else 
			GetNumerValue = str 
		end if
	End Function

	'克隆值
	Public Function CloneValue(str)
		If isnull(str) Or isEmpty(str) Then
			CloneValue = ""
		Else 
			CloneValue = Mid(str, 1)
		End if
	End Function

	'去掉Html语法字符
	Public Function RemoveHTML(fString)  
		dim re  
		set re= New RegExp
		re.Global = True
		re.IgnoreCase = True
		if not isnull(fString) then 
			re.Pattern="<(.[^>]*)>"
			fString = re.Replace(fString,"")
			RemoveHTML = fString
		end if
	End Function  
	  
	'表格字符过滤函数
	Public Function FormToAccess(formstr)
		FormToAccess=replace(formstr,"&","&amp;")
		FormToAccess=replace(FormToAccess,"'","&apos;")
		FormToAccess=replace(FormToAccess,"<","&lt;")
		FormToAccess=replace(FormToAccess,">","&gt;")
		FormToAccess=replace(FormToAccess,"""","&quot;")
		FormToAccess=replace(FormToAccess,VBCRLF,"<br/>")
		FormToAccess=replace(FormToAccess," ","&nbsp;")
	End Function

	'数据字符还原成表格字符
	Public Function AccessToForm(accessStr)
		AccessToForm=replace(accessStr,"&apos;","'")
		AccessToForm=replace(AccessToForm,"&lt;","<")
		AccessToForm=replace(AccessToForm,"&gt;",">")
		AccessToForm=replace(AccessToForm,"&quot;","""")
		AccessToForm=replace(AccessToForm,"<br/>",VBCRLF)
		AccessToForm=replace(AccessToForm,"&nbsp;"," ")
		AccessToForm=replace(AccessToForm,"&amp;","&")
	End Function

	'过滤非法的SQL字符
	Public Function ReplaceBadChar(strChar)
		If strChar = "" Or IsNull(strChar) Then
			ReplaceBadChar = ""
			Exit Function
		End If
		Dim strBadChar, arrBadChar, tempChar, i
		strBadChar = "',%,^,&,?,(,),<,>,[,],{,},\,;,:," & Chr(34) & "," & Chr(0) & ""
		arrBadChar = Split(strBadChar, ",")
		tempChar = strChar
		For i = 0 To UBound(arrBadChar)
			tempChar = Replace(tempChar, arrBadChar(i), "")
		Next
		ReplaceBadChar = tempChar
	End Function

	'获取当前目录名称
	Public function GetCurrentDirName
		set fso=server.CreateObject("Scripting.FileSystemObject")
		set f=fso.GetFolder(server.MapPath("./"))
		GetCurrentDirName=f.name
		set fso=nothing
	end function

	'创建文件夹
	Public sub CreateDir(fldr)
		Set fso = CreateObject("Scripting.FileSystemObject")
		If not fso.FolderExists(fldr) Then
			fso.CreateFolder(fldr)
		End if
		Set fso=nothing
	end sub

	'拷贝目录
	Public sub CopyDir(sourceDir,destination)
		Set fso = CreateObject("Scripting.FileSystemObject")
		if fso.FolderExists(sourceDir) then
			fso.CopyFolder sourceDir,destination
		end if
		set fso=nothing
	end sub

	'拷贝文件
	Public sub CopyFile(sourcefile,destination)
		Set fso = CreateObject("Scripting.FileSystemObject")
		If (fso.FileExists(sourcefile)) Then
			fso.CopyFile sourcefile,destination
		End if
		set fso=nothing
	end sub

	'删除文件
	Public Sub DelFile(filespec)
	  Set fso = CreateObject("Scripting.FileSystemObject")
	  If (fso.FileExists(filespec)) Then
		fso.DeleteFile(filespec)
	  End if
	  Set fso=nothing
	End Sub

	'删除文件夹
	Public sub DelFoler(fldr)
		Set fso = CreateObject("Scripting.FileSystemObject")
		if fso.FolderExists(fldr) then
			fso.DeleteFolder(fldr)
		end if
		set fso=nothing
	end sub

	'检测文件是否存在
	Public function FileIsExists(strfile)
		Set fso = CreateObject("Scripting.FileSystemObject")
		If (fso.FileExists(strfile)) Then
			FileIsExists=true
		else
			FileIsExists=false	
		End if
		set fso=nothing
	end function

	'检测文件夹是否存在
	Public Function FolderIsExists(fldr) 
		Set fso = CreateObject("Scripting.FileSystemObject")
		If (fso.FolderExists(fldr)) Then
			FolderIsExists=true
		Else
			FolderIsExists=false
		End If
		set fso=nothing
	End Function

	'发送电子邮件Email
	Public sub SendEmailToUser(toEmail, EmailTitle, EmailContent, EmailName, EmailUserPwd, EmailSMTP)
		'ASP使用Windows 2000内置CDO对象发送邮件，需要已注册cdosys.dll
		on error resume next
		EmailUserName=left(EmailName,Instr(1,EmailName,"@",1)-1)
		'建立CDO对象
		Set objEmail = CreateObject("CDO.Message")
		'邮件发送者
		objEmail.From = "来自<"&EmailName&">"
		'邮件接收者
		objEmail.To = toEmail
		'邮件标题
		objEmail.Subject = EmailTitle
		'发送文本信息
		'objEmail.TextBody = " "
		'发送HTML信息
		objEmail.HTMLbody = EmailContent
		'发送HTML页面
		'objEmail.CreateMHTMLBody ""
		'发送附件
		'objEmail.AddAttachment ""
		
		'1 使用本地SMTP服务
		'2 使用网络连接
		objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		'0 不需要验证 Anonymous
		'1 Basic 验证方式
		'2 NTLM 验证方式
		objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
		'SMTP服务器
		objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = EmailSMTP
		'SMTP服务端口
		objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
		'连接超时
		objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 50
		'验证用户名
		objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = EmailUserName
		'验证密码
		objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = EmailUserPwd
		
		'更新设置
		objEmail.Configuration.Fields.Update
		'发送邮件
		objEmail.Send
		'销毁对象
		set objEmail=nothing
	end Sub
	
	'弹出信息提示，并跳到指定页面
	Public sub Alter(str, url)
		str = Replace(str, "'", "''")
		response.write("<script type='text/javascript'>alert('"&str&"');</script>")
		If isNull(url) Or isEmpty(url) then
			response.write("<script type='text/javascript'>window.close();</script>")
		Else
			response.write("<script type='text/javascript'>window.location='"&url&"';</script>")
		End If
	end sub

	Public Function IncludeFile(ByVal filePath)
		Set Easp = New EasyASP
		IncludeFile = Easp.Include(filePath)
		Set Easp = nothing
	End Function

End Class
%>