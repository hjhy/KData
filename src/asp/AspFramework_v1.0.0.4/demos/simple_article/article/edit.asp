<%Dim m_id, m_title, m_content, m_picfilename%>
<!--#include file="../master.asp"-->
<!--#include file="../core/pager.asp"-->
<%
Sub Page_Load
	m_id = AF.Helper.GetNumerValue(request("id"))

	sql = "select top 1 * from News where ID="&m_id
	Call AF.DataRender(sql, "showInfo")
End Sub 

Sub showInfo(rs)
	With AF.Helper
		'对全局变量进行赋值（如：标题、内容、类型等等）
		m_title = .AccessToForm(.CloneValue(rs("Title")))
		m_content = .CloneValue(rs("Content"))
		m_picfilename = .CloneValue(rs("PicFileName"))
		'........................
	End With
End sub
%>
<%Sub container_body %>
<form method="post" action="edit_save.asp?action=save">
<input type="hidden" name="txtId" value="<%=m_id%>" />
<input type="hidden" id="hideRender" value="" />
<table>
	<tr>
		<td>标题：</td>
		<td><input type="text" name="txtTitle" id="txtTitle" value="<%=m_title%>" /></td>
	</tr>
	<tr>
		<td>图片：</td>
		<td><input type="text" name="txtPicFileName" id="txtPicFileName" value="<%=m_picfilename%>" onclick="uploadFile(this)" /></td>
	</tr>
	<tr>
		<td>内容：</td>
		<td><textarea name="txtContent" id="txtContent"><%=m_content%></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" name="submit" value="提交" /></td>
	</tr>
</table>
</form>
<script type="text/javascript" charset="gbk" src="<%=AF.Helper.Url("/ueditor/ueditor.config.js")%>"></script>
<script type="text/javascript" charset="gbk" src="<%=AF.Helper.Url("/ueditor/ueditor.all.min.js")%>"></script>
<script type="text/javascript" charset="gbk" src="<%=AF.Helper.Url("/ueditor/lang/zh-cn/zh-cn.js")%>"></script>
<script type="text/javascript" src="<%=AF.Helper.Url("/js/jquery.min.js")%>"></script>
<script type="text/javascript">
	var b_ueditor = null;
	var inputUploadFile = null;

	function uploadFile(obj) {
		var uploadPicDlg = b_ueditor.getDialog("insertimage");  //如果是附件，参数为attachment；如果是图片，参数为insertimage
		uploadPicDlg.render();
		uploadPicDlg.open();

		inputUploadFile = $(obj);
	}
	
	$(function() {
		b_ueditor = UE.getEditor('hideRender', {
			toolbars: [['insertimage', 'insertvideo', 'attachment']]
		});
		b_ueditor.ready(function() {
			b_ueditor.setDisabled();
			b_ueditor.setHide(); //隐藏UE框体
			b_ueditor.addListener('beforeInsertImage', function(t, arg) {
				var str = "";
				if (arg != null && arg.length > 0) {
					str = arg[0].src;
				}
				inputUploadFile.val(str);
			});
		});
	});
</script>
<script type="text/javascript">
	var myEditor = UE.getEditor('txtContent',{initialFrameWidth:680, initialFrameHeight:260});
</script>
<%End sub%>