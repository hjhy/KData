<% Sub oldNewsTemplate(rs, itemIndex) %>
<div style="color:<%If itemIndex = 0 Then Response.write("red") Else Response.write("blue") End if%>">
	第<%=itemIndex%>条：【ID】<%=rs("ID")%> 【标题】<%=rs("Title")%>
</div>
<div>
	链接地址：<%=AF.Helper.Url("/index/show.asp?id="&rs("ID"))%>
</div>
<% End Sub %>

<div>
	<h2>往期产品</h2>
	<div>
		<%
			sql = "select top 5 ID, Title from News order by id asc"
			'此语句意思是：执行sql语句，然后执行oldNewsTemplate的方法
			'第二个参数，可随意命名，但要求有两个参数，而且参数名必须是rs和itemIndex
			'rs			为数据记录
			'itemIndex	为数据的索引（0,1,2,3......），在实际应用中，可能会根据索引来对第一行、偶数行、奇数行做特别处理，此时可使用此参数进行判断
			Call AF.DataRender_Simple(sql, "oldNewsTemplate")
		%>
	</div>
</div>