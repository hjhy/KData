<%
'--管理员密码可以填写非16位的明文密码，也可以填写密码Md5加密后的16位值
AdminPwdMd5 = "admin"

'--禁止查看，修改和下载的系统文件名称，格式如|a.asp|b.asp|
SysFile = "|cf_config.asp|cf_downcookie.asp|cf_filemanager.asp|cf_include.asp|cf_index.asp|cf_md5.asp|cf_upload.asp|cf_uploadclass.asp|cf_readme.txt|"

'--不防盗链的文件类型，格式如|.jpg|.gif|
DirectDownFileType = "|.jpg|.gif|.png|.txt|.htm|.html|.asp|.php|.aspx|.js|.css|"

'--普通用户有权限的路径,格式如|/myftp/|,前后必须带/且小写
UserPowerPath = "|/myftp/|"

'--系统安全码
SysCode = "cf1234"

'--定义Fso的名称
FsoName = "Scripting.FileSystemObject"
%>