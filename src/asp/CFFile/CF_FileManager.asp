<%
'乘风文件管理系统
'作者QQ：178575
'作者E-Mail：yliangcf@163.com
'作者网站：http://www.qqcf.com
'上面有程序在线演示，安装演示，使用疑难解答，最新版本下载等内容
'因为这些内容可能时常更新，就没有放在程序里，请自己上网站查看
%>
<% 
Dim fs, sAction, sFile, sPath, sFolder, sFileType, scriptname, dbfile, ReadStream, WriteStream, WriteFile, fileobject, filecollection, file, startpath, lineid, bgcolor, bgcolor_on, bgcolor_off, foldercollection, folder, errornum, errorcode

' Reset our errorcode values 
errornum = 0
errorcode = ""



scriptname=Request.ServerVariables("Script_Name")
sAction = Request.Querystring("action")
sFileType = Request.Querystring("filetype")

If Request.Querystring("path") = "" Then


''setting the base path or rootfolder
''*****************************************************
 sPath = "/"''the root folder for this sytem.
''*****************************************************
''setting the base path or rootfolder

Else
 sPath = Request.Querystring("path")
 If InStr(sPath,"../") Then
	 errornum = errornum+1
	 errorcode = errorcode & "<li><b>Invalid use of ""../""</b>. 你只能编辑自己的文件夹.</li>"
 End If
End If

If sPath="/" Then
 If Request.Querystring("file") = "" Then
  sFile = sPath & Request.Form("file")
 Else
  sFile = sPath & Request.Querystring("file")
 End If 
 If Request.Querystring("folder") = "" Then
  sFolder = sPath & Request.Form("folder")
 Else
  sFolder = sPath & Request.Querystring("folder")
 End If
Else
 If Request.Querystring("file") = "" Then
  sFile = sPath & "/" & Request.Form("file")
 Else
  sFile = sPath & "/" & Request.Querystring("file")
 End If
 If Request.Querystring("folder") = "" Then
  sFolder = sPath & "/" & Request.Form("folder")
 Else
  sFolder = sPath & "/" & Request.Querystring("folder")
 End If
End If
session("foldername")=spath
' Make sure that no errors have occurred and no illegal actions have been taken before doing our stuff... 
If errornum < 1 Then
  Set fs = Server.CreateObject(FsoName)

  Select Case sAction
    Case "login"
      login

    Case "loginsave"
      loginsave

    Case "logout"
      logout

    Case "pwdmodify"
      pwdmodify

    Case "pwdmodifysave"
      pwdmodifysave

    Case "editfile"
	  '--写入防盗链cookie
	  Response.write "<script src='CF_DownCookie.asp'></script>"

      Select Case sFileType
        Case "htm", "asp", "txt", "inc", "html", "shtml", "shtm", "js", "css"
          EditFile
        Case "mdb", "dat"
          EditDb
        Case else
          FileTypeUnsupported
      End Select

    Case "savefile"
      If Session("CFFileAdmin")<>"" Then SaveFile

    Case "viewfolder"
	 If Session("CFFileAdmin")<>"" Then
	  Showlist
	 Else
	  MyArray=Split(UserPowerPath,"|")
	  I = 0
	  For I = 0 To UBound(MyArray)
	   If MyArray(I)<>"" And UserPowerPath<>"" Then
	    If Left(LCase(spath)&"/",Len(MyArray(I)))=MyArray(I) Then
		 Showlist
		 Exit For
		End If
	   End If
	  Next
	 End If

    Case "newfile"
      If Session("CFFileAdmin")<>"" Then CreateFile

    Case "newfolder"
      If Session("CFFileAdmin")<>"" Then CreateFolder

    Case "deletefile"
      If Session("CFFileAdmin")<>"" Then DeleteFile

    Case "deletefolder"
      If Session("CFFileAdmin")<>"" Then DeleteFolder
	
	Case "CreateNewFolder"
	  If Session("CFFileAdmin")<>"" Then CreateNewFolder
	
	Case "UploadFiles"
		If Session("CFFileAdmin")<>"" Then UploadFiles
		
	Case "RenameFolder"
		If Session("CFFileAdmin")<>"" Then RenameFolder

	Case "RenameFile"
		If Session("CFFileAdmin")<>"" Then RenameFile

    Case Else
	 If Session("CFFileAdmin")<>"" Then
	  Showlist
	 Else
	  MyArray=Split(UserPowerPath,"|")
	  I = 0
	  For I = 0 To UBound(MyArray)
	   If MyArray(I)<>"" And UserPowerPath<>"" Then
	    If Left(LCase(spath)&"/",Len(MyArray(I)))=MyArray(I) Then
		 Showlist
		 PathList=1
		 Exit For
		End If
	   End If
	  Next

	  If PathList<>1 Then login
	 End If

       
  End Select
  Set fs = Nothing
Else
  DisplayErrors
End If
%>


