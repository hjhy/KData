<%
'乘风文件管理系统
'作者QQ：178575
'作者E-Mail：yliangcf@163.com
'作者网站：http://www.qqcf.com
'上面有程序在线演示，安装演示，使用疑难解答，最新版本下载等内容
'因为这些内容可能时常更新，就没有放在程序里，请自己上网站查看
%>
<%
Sub EditDb
On error resume next
  If fs.FileExists(server.mappath(dbfile)) Then
    Response.Redirect dbfile & "?db=" & sFile
  Else
	  Response.Write "No database found" &vbCrLf
	End If
End Sub

Sub CreateNewFolder
On error resume next
%>

<br><b>创建文件夹</b><br><br>
当前文件夹: <%=spath%>
  <form method="POST" action="?action=newfolder&path=<%=sPath%>">
        
  <input name="folder" type="text" value="文件夹名" size="30">

        <input type="submit" value="在当前文件夹里创建新文件夹" name="submit"> 
</form>
<br>
<%

End Sub

Sub EditFile
On error resume next
  Set ReadStream = fs.OpenTextFile(server.mappath(sFile))
  filename=request.querystring("file")
  response.write ("<br>")  
  If Session("CFFileAdmin")<>"" Then
   Response.write "您将要修改文件: "
  Else
   Response.write "您正在查看文件: "
  End If
  response.write "<b>"&filename&"</b>"
  
  
  If (Request("path")="" Or Request("path")="/") And InStr(SysFile,"|"&LCase(Trim(filename))&"|")>0 Then
   Response.write "<br><br>系统文件无法查看和修改"
   Response.write "<br><br><a href='?action=viewfolder&path=" & Request("path") & "'>返回</a>"
   Response.End
  Else
   '--对exe.zip,rar文件启用下载防盗链链接
   If InStr(DirectDownFileType,"|" & LCase(Mid(sfile,InStrRev(sfile,"."))) & "|") = 0 Then sfile=Replace(LCase(sfile),"/myftp/","/uuauth/")
   Response.Write "<br><br><a href='" & sfile & "' target='_blank'>下载</a> (可点右键选择目标另存为或迅雷等工具下载)"
   Response.write "<br><br><a href='?action=viewfolder&path=" & Request("path") & "'>返回</a>"
  End If

  Response.Write "<form method=""POST"" action=""?action=savefile&path=" & sPath & "&file=" & Request.Querystring("File") & "&overwrite=yes&reurl="&Server.URLEncode(Request.ServerVariables("HTTP_REFERER"))&""">" &vbCrLf
  Response.Write "<p><textarea rows=""20"" cols=""95"" name=""filestuff"""

  If Session("CFFileAdmin")<>"" Then
   Response.Write " style='BACKGROUND-color:#FFFFFF;'"
  Else
   Response.Write " style='BACKGROUND-color:#EEEEEE;' readonly"
  End If

  Response.Write ">" & Server.HTMLEncode(ReadStream.ReadAll) & "</textarea></p>" &vbCrLf
  If Session("CFFileAdmin")<>"" Then response.write "另存为: <input name=NewFileName type=text size=30>"
  If Session("CFFileAdmin")<>"" Then Response.Write "<p><input type=""submit"" value=""保 存"" name=""submit"">&nbsp;<input type=""reset"" value=""恢复数据"" name=""reset""></p>" &vbCrLf
  Response.Write "</form>" &vbCrLf 

  
End Sub

Sub UploadFiles
On error resume next
%>
<br>当前文件夹:<b> <%=spath%></b><br>
<form action="cf_upload.asp" method="post" enctype="multipart/form-data">
文件1 <input type="file" name="file1" /><br />
文件2 <input type="file" name="file2" /><br />
文件3 <input type="file" name="file3" /><br />
文件4 <input type="file" name="file4" /><br />
文件5 <input type="file" name="file5" /><br />
文件6 <input type="file" name="file6" /><br />
文件7 <input type="file" name="file7" /><br />
文件8 <input type="file" name="file8" /><br />
文件9 <input type="file" name="file9" /><br />
<input type="submit" value="上传" />
</form>
      </form>
<%
End Sub



Sub CreateFile
On error resume next
  response.write "<br><b>创建新文件</b><br>"
  Response.Write "<form method=""POST"" action=""?action=savefile&path=" & sPath & "&reurl="&Server.URLEncode(Request.ServerVariables("HTTP_REFERER"))&""">" &vbCrLf
  Response.Write "<p><input type=""text"" name=""file""></input><font face=""Tahoma"">给文件取名（含扩展名）</p>" &vbCrLf
  Response.Write "<p>资料内容<br><textarea rows=""20"" cols=""80"" name=""newfilestuff""></textarea></p>" &vbCrLf
  Response.Write "<p><input type=""submit"" value=""创建文件"" name=""submit""></p>" &vbCrLf
  Response.Write "</form></font>" &vbCrLf
End Sub

Sub SaveFile
On error resume next
  '' the save as a new file routine
  if request.form("NewFileName")<>"" then
  NewFileName=request.form("NewFileName")
  spath=request("path")
    if spath="/" then slashvalue="" else slashvalue="/" end if
  filestuff=request.form("filestuff")
  NewPathFileName= spath&slashvalue&newfilename''creating the right path and filename
    Set fso = CreateObject(FsoName)
	Set textStreamObject = fso.CreateTextFile(server.mappath(NewPathFileName),true,false)
	textStreamObject.write filestuff
	Response.Redirect("" & Request("reurl") & "")
  else
  '' done saving as routine

  If Request.Querystring("overwrite") = "yes" Then
    set fs=Server.CreateObject(FsoName)
	Set WriteFile = fs.CreateTextFile(server.mappath(sFile), true)
	WriteFile.Write Request.Form("filestuff")''filestuff''Session("filestuff")
    WriteFile.Close
	Response.Redirect("" & Request("reurl") & "")
	
  Else
    If fs.FileExists(server.mappath(sFile)) Then
      Session("sFile") = sFile
	  spath=request.querystring("path")
      Session("newfilestuff") = Request.Form("newfilestuff")
      Response.Write "<br><p>该文件已经存在:  <b>" & sFile & "</b> </p>"
      Response.Write "<UL>"
      Response.Write "<LI><a href=""?action=savenewfile&overwrite=yes&reurl="&Server.URLEncode(Request.ServerVariables("HTTP_REFERER"))&""">覆盖现有的文件?</a>"
      ' We don't want to lose the information that the typed in the previous form if they decide NOT to overwrite the existing file,
	  ' so we provide a javascript link back that works exactly the same as the browser's back button.
			Response.Write "<LI><a href=""javascript:history.back()"">回到前页 </a></LI>"
      Response.Write "</UL>"
    Else 
      Set WriteFile = fs.CreateTextFile(server.mappath(sFile), false)
      WriteFile.Write Request.Form("newfilestuff")
      WriteFile.Close
      Response.Redirect("?action=viewfolder&path="&spath&"")  
    End If
  End If
  end if
End Sub

Sub CreateFolder
On error resume next
 
 If fs.FolderExists(server.mappath(sFolder)) Then 
   response.write "A folder with the name <b>" & sFolder & "</b> 已经存在<br>"  
 Else
   fs.CreateFolder(server.mappath(sFolder))

response.redirect("?action=viewfolder&path="&request.querystring("path")&"")


 End If
End Sub

Sub DeleteFile
On error resume next
	response.write"<br>"
  If (Request("path")="" Or Request("path")="/") And InStr(SysFile,"|"&LCase(Trim(request.querystring("file")))&"|")>0 Then
   Response.write "系统文件无法查看和修改"
   Response.End
  End If
  If Request.Querystring("commit") <> "yes" Then
    Session("sFile") = sFile
    Response.Write "<p>你即将删除的文件是:  " & sFile & ". " 
    If sFileType = "jpg" OR sFileType = "gif" Then
      Response.Write "<p><img src=" & sfile & "?" & Md5(Request.ServerVariables("REMOTE_ADDR") & SysCode,2) & """></p>"
    End If
    Response.Write "<b>注意：删除后将不可恢复！</b></p>"
    Response.Write "<UL>"
    Response.Write "<LI><a href=""?action=deletefile&path=" & sPath & "&file=" & sFile & "&commit=yes&reurl="&Server.URLEncode(Request.ServerVariables("HTTP_REFERER"))&""">继续?</a></LI>"
    Response.Write "<LI><a href=" & Request.ServerVariables("HTTP_REFERER") & ">停止</a></LI>"
    Response.Write "</UL>"
  Else
    fs.DeleteFile(server.mappath(Session("sFile")))
    Response.Redirect("" & Request("reurl") & "")
  End If
End Sub

Sub DeleteFolder
On error resume next
	response.write"<br>"
  If (Request("path")="" Or Request("path")="/") And (LCase(Trim(request.querystring("folder")))="cf_images") Then
   Response.write "系统目录无法修改"
   Response.End
  End If
  If Request.Querystring("commit") <> "yes" Then 
    Session("sFolder") = sFolder
    Response.Write "<p>您即将删除: " & sFolder & ". " 
    Response.Write "<b>这不能动用!</b></p>"
    Response.Write "<UL>"
    Response.Write "<LI><a href=""?action=deletefolder&path=" & sPath & "&folder=" & sFolder &  "&commit=yes&reurl="&Server.URLEncode(Request.ServerVariables("HTTP_REFERER"))&""">继续?</a></LI>"
    Response.Write "<LI><a href=" & Request.ServerVariables("HTTP_REFERER") & ">停止</a></LI>"
    Response.Write "</UL>"
  Else
    Response.Write sPath & "<br>"
    Response.Write sFile & "<br>"
    fs.DeleteFolder(server.mappath(Session("sFolder")))
    Response.Redirect("" & Request("reurl") & "")
  End If
End Sub

Sub RenameFolder

On error resume next
	response.write"<br>"
  If (Request("path")="" Or Request("path")="/") And (LCase(Trim(request.querystring("folder")))="cf_images") Then
   Response.write "系统目录无法修改"
   Response.End
  End If
	Response.write "<b>修改文件夹</b><br>"
  If Request.querystring("commit") <> "yes" Then 
    Response.Write "<p>你将修改<font color=#ff0000><b>" & request.querystring("folder") & "</b></font>的文件夹名为" 
%>
<form name="form1" method="post" action="?action=RenameFolder&path=<%=spath%>&folder=<%=request.querystring("folder")%>&commit=yes&reurl=<%=Server.URLEncode(Request.ServerVariables("HTTP_REFERER"))%>">
  <input name="NewFolderName" type="text" size="30">
  <input type="submit" name="Submit" value="修改">
  <input type="hidden"name="folder" value="<%=request.querystring("folder")%>">
</form>
<%
  Else
	NewFolderName=request.form("NewFolderName")
	sFolder=request.form("folder")
	if spath="/" then slashvalue="" else slashvalue="/" end if
    Set fso = CreateObject(FsoName)
	Set folderObject = fso.GetFolder(Server.MapPath(spath&slashvalue&sFolder))
	FolderObject.Name=NewFolderName
	 Set folderObject = Nothing
	 Set fso = Nothing
	  Response.Redirect("" & Request("reurl") & "") 
   End If
  
End Sub

Sub RenameFile
On error resume next
  response.write"<br>"
  If (Request("path")="" Or Request("path")="/") And InStr(SysFile,"|"&LCase(Trim(request.querystring("file")))&"|")>0 Then
   Response.write "系统文件无法查看和修改"
   Response.End
  End If
  Response.write "<b>修改文件名</b><br>"
  If Request("commit") <> "yes" Then 
    Response.Write "<p>你将修改<font color=#ff0000><b>" & request.querystring("file") & "</b></font>的文件名为" 
%>
<form name="form1" method="post" action="?action=RenameFile&path=<%=spath%>&folder=<%=request.querystring("folder")%>&commit=yes&reurl=<%=Server.URLEncode(Request.ServerVariables("HTTP_REFERER"))%>">
  <input name="NewFileName" type="text" size="30">
  <input type="submit" name="Submit" value="修改">
  <input type="hidden" name="filename" value="<%=request.querystring("file") %>">
</form>
<p>
  <%
  Else
  	NewFileName=request.form("NewFileName")
	Sfile=request.form("filename")
	if spath="/" then slashvalue="" else slashvalue="/"
    Set fso = CreateObject(FsoName)
	Set FileObject = fso.GetFile(Server.MapPath(spath&slashvalue&sfile))
	FileObject.Name = NewFileName
	Set FilObject = Nothing
	Set fso = Nothing
    Response.Redirect("" & Request("reurl") & "")  
  End If
  
End Sub

Sub FileTypeUnsupported
On error resume next
  filename=request.querystring("file")
  response.write ("<br>")  
  Response.write "文件: "
  response.write "<b>"&filename&"</b>"
  response.write "<br>"

  If LCase(sFileType) = "jpg" OR LCase(sFileType) = "gif" OR LCase(sFileType) = "png" Then
    Response.Write "<br><a href='" & sfile & "' target='_blank'>下载</a> (可点右键选择目标另存为或迅雷等工具下载)<br><br>"
    Response.Write "<a href='?action=viewfolder&path=" & Request("path") & "'>返回</a>"
    Response.Write "<p><img src=""" & sfile & "?" & Md5(Request.ServerVariables("REMOTE_ADDR") & SysCode,2) & """></p>"
  Else
    '--对exe.zip,rar文件启用下载防盗链链接
    If InStr(DirectDownFileType,"|" & LCase(Mid(sfile,InStrRev(sfile,"."))) & "|") = 0 Then sfile=Replace(LCase(sfile),"/myftp/","/uuauth/")
    Response.Write "<br><a href='" & sfile & "' target='_blank'>下载</a> (可点右键选择目标另存为或迅雷等工具下载)<br><br>"
    Response.Write "<a href='?action=viewfolder&path=" & Request("path") & "'>返回</a>"
  End If
  
End Sub

Sub Size(itemsize)
  Response.Write "<td bgcolor=""" & bgcolor & """ align=""center"" valign=""bottom"">" &vbCrLf
  Select case Len(itemsize)
  Case "1", "2", "3" 
    Response.Write itemsize & " bytes"
  Case "4", "5", "6"
    Response.Write Round(itemsize/1000) & " Kb"
  Case "7", "8", "9"
    Response.Write Round(itemsize/1000000) & " Mb"
  End Select
  Response.Write "</td>" &vbCrLf
End Sub

Sub ShowList
 	' Start building the table to display the information we retrieve about the files and folders in the current directory.  
  Response.Write "<table cellpadding=""0"" cellspacing=""0"" border=""0"" bordercolor=""#cccccc"" width=""100%"">" &vbCrLf
  %>
    <tr>
    <td background="cf_images/header_bg.gif" height="25">名称</td>
    <td background="cf_images/header_bg.gif" height="25"><div align="center">类型</div></td>
    <td background="cf_images/header_bg.gif" height="25"><div align="center">大小 </div></td>
    <td background="cf_images/header_bg.gif" height="25"><div align="center">上传日期 </div></td>
    <%If Session("CFFileAdmin")<>"" Then%><td background="cf_images/header_bg.gif" height="25"><div align="center">修改</div></td><%End If%>
  </tr>
  
  <%  
    ' Use the GetFolder method of the filesystemobject to get the contents of the directory specified in sPath  
  Set fileobject = fs.GetFolder(server.mappath(sPath))
	' Use the SubFolders property to get the folders contained in the directory specified in sPath
  Set foldercollection = fileobject.SubFolders 
    ' Start the code to alternate line colors - just to make the display a little less visually confusing.
  lineid=0
  bgcolor = ""
    bgcolor_off = "#FFFFFF"
    bgcolor_on = "#f0f0f0"

  ' Loop through the folders contained in the foldercollection and display their information on the page
  For Each folder in foldercollection 
    ' Apply our alternating line coloring
    If lineid = 0 Then
      bgcolor = bgcolor_off
      lineid = 1
    Else
      bgcolor = bgcolor_on
      lineid = 0
    End if		

		Response.Write "<tr bgcolor=""" & bgcolor & """><font face=""verdana"" size=""1"">" &vbCrLf
		If Right(sPath,1)="/" Then
	    Response.Write "<td bgcolor=""" & bgcolor & """ align=""left"" valign=""bottom""><img src=cf_images/folder.gif > <a href=""?action=viewfolder&path=" & sPath & folder.name & """>" & folder.name & "</a></td>" & vbCrLf

		Else
		  Response.Write "<td bgcolor=""" & bgcolor & """ align=""left"" valign=""bottom""><img src=cf_images/folder.gif > <a href=""?action=viewfolder&path=" & sPath &"/" &folder.name & """>" & folder.name & "</a></td>" & vbCrLf

		End If  

	  Response.Write "<td bgcolor=""" & bgcolor & """ align=""center"" valign=""bottom"">folder" 

Call Size(folder.size)
	  Response.Write "<td bgcolor=""" & bgcolor & """ align=""center"" valign=""bottom"">" & folder.datelastmodified & "</td>" &vbCrLf
	  If Session("CFFileAdmin")<>"" And Not((Request("path")="" Or Request("path")="/") And (LCase(Trim(folder.name))="cf_images")) Then Response.Write "<td bgcolor=""" & bgcolor & """ align=""center"" valign=""bottom""><a href=""?action=RenameFolder&path=" & sPath & "&folder=" & folder.name & """><img border=0 alt=修改文件夹 src=cf_images/rename2.jpg></a> <a href=""" & scriptname & "?action=deletefolder&path=" & sPath & "&folder=" & folder.name & """><img border=0 alt=delete src=cf_images/del.gif></a></td>" &vbCrLf
    Response.Write "</tr>" &vbCrLf
  Next 
  Set foldercollection=nothing

  ' Use the Files property to get the files contained in the directory specified in sPath
  Set filecollection = fileobject.Files
  
	' Loop through the files contained in the filescollection and dislay their information on the page
	For Each file in filecollection 
    ' Apply our alternating line coloring
    If lineid = 0 Then
      bgcolor = bgcolor_off
      lineid = 1
    Else
      bgcolor = bgcolor_on
      lineid = 0
    End if	
    Response.Write "<tr>" &vbCrLf  
%>
  <%
	if fs.GetExtensionName(file.name)="gif" then image="<img src=cf_images/gif.gif >"
	if fs.GetExtensionName(file.name)="pdf" then image="<img src=cf_images/pdf.gif >"
	if fs.GetExtensionName(file.name)="css" then image="<img src=cf_images/css.gif >"
	if fs.GetExtensionName(file.name)="doc" then image="<img src=cf_images/word.gif >"
	if fs.GetExtensionName(file.name)="xls" then image="<img src=cf_images/xls.gif >"
	if fs.GetExtensionName(file.name)="exe" then image="<img src=cf_images/exe.gif >"
	if fs.GetExtensionName(file.name)="zip" then image="<img src=cf_images/zip.gif >"
	if fs.GetExtensionName(file.name)="jpg" then image="<img src=cf_images/jpg.gif >"
	if fs.GetExtensionName(file.name)="jpeg" then image="<img src=cf_images/jpg.gif >"
	if fs.GetExtensionName(file.name)="htm" then image="<img src=cf_images/htm.gif >"
	if fs.GetExtensionName(file.name)="html" then image="<img src=cf_images/htm.gif >"
	if fs.GetExtensionName(file.name)="swf" then image="<img src=cf_images/swf.gif >"
	if fs.GetExtensionName(file.name)="asp" then image="<img src=cf_images/file.gif >"
	if fs.GetExtensionName(file.name)="txt" then image="<img src=cf_images/file.gif >"
	if fs.GetExtensionName(file.name)="inc" then image="<img src=cf_images/inc.gif >"
	if fs.GetExtensionName(file.name)="js" then image="<img src=cf_images/js.gif >"
	if fs.GetExtensionName(file.name)="mdb" then image="<img src=cf_images/mdb.gif >"
	
	if image="" then image= "<img src=cf_images/unknown.gif >"
Response.Write "<td bgcolor=""" & bgcolor & """ align=""left"" valign=""bottom"">"&image&" <a href=""?action=editfile&path=" & Server.URLEncode(sPath) & "&file=" & Server.URLEncode(file.name) & "&filetype=" & Lcase(fs.GetExtensionName(file.name)) & """>" & file.name & "</a></td>" &vbCrLf
	image=""
		Response.Write "<td bgcolor=""" & bgcolor & """ align=""center"" valign=""bottom"">" & fs.GetExtensionName(file.name) & "</td>" &vbCrLf
	  Call Size(file.size)
	  Response.Write "<td bgcolor=""" & bgcolor & """ align=""center"" valign=""bottom"">" & file.datelastmodified & "</td>" &vbCrLf
	  If Session("CFFileAdmin")<>"" And Not((Request("path")="" Or Request("path")="/") And InStr(SysFile,"|"&LCase(Trim(file.name))&"|")>0) Then Response.Write "<td bgcolor=""" & bgcolor & """ align=""center"" valign=""bottom""><a href=""?action=RenameFile&path=" & sPath & "&file=" & file.name & "&filetype=" & Lcase(fs.GetExtensionName(file.name)) & """><img border=0 alt=重命名文件 src=cf_images/rename1.jpg></a> <a href=""" & scriptname & "?action=deletefile&path=" & sPath & "&file=" & file.name & "&filetype=" & Lcase(fs.GetExtensionName(file.name)) & """><img border=0 alt=delete src=cf_images/del.gif></a></td>" &vbCrLf
    Response.Write "</tr>" &vbCrLf
  Next

  ' We are done displaying information about files and folders in this directory, so close the table.
  Response.Write "</table>" &vbCrLf


End Sub


Sub DisplayErrors
  Response.Write "U heeft: " & errornum & " niet toegestane akties ondernomen."
	Response.Write "<ul>" & errorcode & "</ul>" & vbCrlf
End Sub



Sub login
On error resume next
  response.write "<br><b>管理员登录</b><br>"
  Response.Write "<form method=""POST"" action=""?action=loginsave&reurl="&Server.URLEncode(Request.ServerVariables("HTTP_REFERER"))&""">" &vbCrLf
  Response.Write "<font face=""Tahoma"">请输入管理员密码<br><input type=""password"" name=""pwd""></input>" &vbCrLf
  Response.Write "<br><input type=""submit"" value=""登录"" name=""submit"">" &vbCrLf
  Response.Write "</form></font>" &vbCrLf
End Sub

Sub loginsave
On error resume Next
If Len(AdminPwdMd5)=16 Then
 If MD5(Trim(Request("pwd")),1)<>AdminPwdMd5 Then
  Response.Write "<script>alert('密码错误');history.go(-1)</script>"
  Response.End
 End If
Else
 If Trim(Request("pwd"))<>AdminPwdMd5 Then
  Response.Write "<script>alert('密码错误');history.go(-1)</script>"
  Response.End
 End If
End If

 Session("CFFileAdmin")="ok"
 Response.Cookies("CFFileAdminCookie")=MD5(AdminPwdMd5&SysCode,1)
 Response.Cookies("CFFileAdminCookie").expires=Dateadd("n",480,Now())

 If Request("reurl")="" Then
  Response.Redirect "cf_index.asp"
 Else
  Response.Redirect Request("reurl")
 End If

End Sub


Sub logout
On error resume next
 Session("CFFileAdmin")=""
 Response.Cookies("CFFileAdminCookie")=""
 Response.Cookies("CFFileAdminCookie").expires=Dateadd("n",-480,Now())

 Response.Redirect Request.ServerVariables("HTTP_REFERER")
End Sub


'--获取当前页的网址信息
Function HttpPath(ByVal Assort)
 Ser=Request.servervariables("SERVER_NAME")
 Scr=Request.servervariables("SCRIPT_NAME")
 Port=Request.Servervariables("SERVER_PORT")

 Scr_2=StrReverse(Mid(StrReverse(Scr),Instr(StrReverse(Scr),"/")))

 ServerIP = Request.ServerVariables("LOCAL_ADDR")
 CliendIP_X = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
 
 If (Left(ServerIP,8) = "192.168." Or Left(ServerIP,5) = "10.0." Or Left(ServerIP,7) = "176.16.") Then
  LanIP = 1
 Else
  LanIP = 0
 End If

 If Assort=1 Then
  HttpPath=Ser 
 ElseIf Assort=2 Then
  If Port="80" Or (LanIP = 1 And CliendIP_X<>"") Then
   HttpPath="http://"&Ser&Scr_2
  Else   
   HttpPath="http://"&Ser&":"&Port&Scr_2
  End If 
 ElseIf Assort=3 Then
  If Port="80" Or (LanIP = 1 And CliendIP_X<>"")Then
   HttpPath="http://"&Ser&Scr
  Else
   HttpPath="http://"&Ser&":"&Port&Scr
  End If
 ElseIf Assort=4 Then
  If Request.ServerVariables("QUERY_STRING")<>"" Then UrlStr="?"&Request.ServerVariables("QUERY_STRING")

  If Port="80" Or (LanIP = 1 And CliendIP_X<>"")Then
   HttpPath="http://"&Ser&Scr&UrlStr
  Else
   HttpPath="http://"&Ser&":"&Port&Scr&UrlStr
  End If
 End If
End Function
%>


