<%
'乘风文件管理系统
'作者QQ：178575
'作者E-Mail：yliangcf@163.com
'作者网站：http://www.qqcf.com
'上面有程序在线演示，安装演示，使用疑难解答，最新版本下载等内容
'因为这些内容可能时常更新，就没有放在程序里，请自己上网站查看
%>
<%
Response.Expires= -1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","no-store"
%>
<!--#include file="cf_config.asp" -->
<!--#include file="cf_include.asp" -->
<!--#include file="cf_md5.asp" -->
<%
CFFileAdminCookie=Request.Cookies("CFFileAdminCookie")

If Session("CFFileAdmin")="" And CFFileAdminCookie<>"" Then  
 If CFFileAdminCookie=MD5(AdminPwdMd5&SysCode,1) Then Session("CFFileAdmin") = "ok"
End If

If Session("CFFileAdmin")<>"" And CFFileAdminCookie="" Then'Session存在但cookie不存在时，重新写Cookie
   Response.Cookies("CFFileAdminCookie")=MD5(AdminPwdMd5&SysCode,1)
   Response.Cookies("CFFileAdminCookie").expires=Dateadd("n",480,Now())
End If
%>

<html>
<head>
<title>乘风文件管理系统</title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<link href="cf_images/style.css" rel="stylesheet" type="text/css">
</head>
<%

Ffile=request.querystring("fileoptions")
if Ffile="" then Ffile=request.cookies("fileoptions")
	if Ffile="" then 
		response.cookies("fileoptions")="max" 
	else 
		response.cookies("fileoptions")=Ffile 
	end if
Ffile=request.cookies("fileoptions")

Ffolder=request.querystring("folderoptions")
if Ffolder="" then Ffolder=request.cookies("folderoptions")
	if Ffolder="" then 
		response.cookies("folderoptions")="max" 
	else 
		response.cookies("folderoptions")=Ffolder 
	end if
Ffolder=request.cookies("folderoptions")


'settings of boxes is now loaded.
%>

<body>
<table width="700" border="2" align="center" cellpadding="0" cellspacing="0" bordercolor="#0060F9" bgcolor="#F3F3EE">
  <tr> 
    <td colspan="8" bordercolor="#0066FF">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr > 
          <td background="cf_images/expl_top_back.jpg" width="4%"><img src="cf_images/expl_top.gif" width="20" height="20"></td>
          <td background="cf_images/expl_top_back.jpg" width="91%" height="27"><font color="#FFFFFF"></font></td>
          <td background="cf_images/expl_top_back.jpg" width="5%"><div align="right"></div></td>
        </tr>
      </table></td>
  </tr>
  <tr>

<td width='40' bordercolor="#FFFFFF"> <div align="center"><a class="knop1" href="<%=HttpPath(2)%>cf_index.asp">首页</a></div></td>
<%If Session("CFFileAdmin")<>"" Then%>
    <td width='60' bordercolor="#FFFFFF"> <div align="center"><a class="knop1" href="?action=newfile&path=<%=request.querystring("path")%>">新建资料</a></div></td>
    <td width='80' bordercolor="#FFFFFF"> <div align="center"><a class="knop1" href="?action=CreateNewFolder&path=<%=request.querystring("path")%>">新建文件夹</a></div></td>
    <td width='130' bordercolor="#FFFFFF"> <div align="center"><a class="knop1" href="?action=UploadFiles&path=<%=request.querystring("path")%>">往此文件夹上传资料</a></div></td>

	<td width='40' bordercolor="#FFFFFF"> <div align="center"><a class="knop1" href="?action=logout" onclick="return confirm('确定要退出吗?')">退出</a></div></td>
<%Else%>
    <td width='40' bordercolor="#FFFFFF"> <div align="center"><a class="knop1" href="?action=login">登录</a></div></td>
<%End If%>
<%
'--列出普通用户有权限的目录
If UserPowerPath<>"" Then

UserPowerPath_Array=Split(UserPowerPath,"|")
For I = 0 To UBound(UserPowerPath_Array)
If UserPowerPath_Array(I)<>"" Then
 Response.write "<td bordercolor='#FFFFFF'> <div align='center'><a class='knop1' href='?action=viewfolder&path="&UserPowerPath_Array(I)&"'>转到"&UserPowerPath_Array(I)&"目录</a></div></td>"
End If
Next

End If
%>

    <td bordercolor="#FFFFFF"><div align="right"></div></td>
  </tr>
  <tr> 
    <td colspan="8" bgcolor="#999999" height="3" ></td>
  </tr>
  <tr> 
    <td colspan="8" bordercolor="#FFFFFF"><div align="left"><a href="javascript:history.go(-1)"><img src="cf_images/expl_back.gif" alt="后退" width="51" height="21" border="0"></a> 
        <a href="javascript:history.go()"><img src="cf_images/expl_reload.gif" alt="刷新" width="26" height="21" border="0"></a><a href="<%=HttpPath(2)%>cf_index.asp"><img src="cf_images/ecpl_home.gif" alt="首页" width="23" height="21" border="0"></a> 
        <a href="?action=viewfolder&path=<%
		If request.querystring("path")="" Or request.querystring("path")="/" Then 
		Response.Write request.querystring("path")
Else
 If InStr(request.querystring("path"),"/")>0 Then Response.Write Left(request.querystring("path"),InstrRev(request.querystring("path"),"/")-1)
End If%>"><img src="cf_images/expl_up.gif" alt="回上一级" width="21" height="21" border="0"></a>
		<%If Session("CFFileAdmin")<>"" Then%>
		<img src="cf_images/expl_devider.gif" width="9" height="21"><a href="?action=newfile&path=<%=request.querystring("path")%>"><img src="cf_images/expl_newfile.gif" alt="创建新文件" width="26" height="21" border="0"></a> 
        <a href="?action=CreateNewFolder&path=<%=request.querystring("path")%>"><img src="cf_images/nfolder.gif" width="20" height="16" border="0"></a>
		<a href="?action=UploadFiles&path=<%=request.querystring("path")%>"><img src="cf_images/upload.gif" width="20" height="16" border="0"></a>
		<%End If%>

		</div>
		</td>
  </tr>
  <td colspan="8" bgcolor="#999999" height="3" ></td>
  <tr> 
    <td colspan="8" bordercolor="#FFFFFF" bgcolor="#F3F3EE">

<div style="float:left;">&nbsp;&nbsp;路径:&nbsp;&nbsp;&nbsp;&nbsp;</div>  
<div style="BACKGROUND-COLOR: #DADADA;float:left;width:640px;height:15px;">
<%
MyPath = request.querystring("path")
MyArray=Split(MyPath,"/")
I = 0
Response.write "<a href='"&HttpPath(2)&"cf_index.asp'>首页</a>"
For I = 0 To UBound(MyArray)
 If I <> UBound(MyArray) Then
  Response.write "<a href='?action=viewfolder&path="
  Response.write Left(MyPath,InStr(MyPath,MyArray(i))+Len(MyArray(i))-1)&"'>"
  Response.write MyArray(i)&"</a>/"
 Else
  Response.write "<a href='?action=viewfolder&path="
  Response.write Left(MyPath,InStr(MyPath,MyArray(i))+Len(MyArray(i))-1)&"'>"
  Response.write MyArray(i)&"</a>"
 End If
Next
%>
</div>     
    </td>
  </tr>
  <tr> 
    <td colspan="8" bgcolor="#999999" height="3" ></td>
  </tr>
  <tr bordercolor="#FFFFFF"> 
    <td colspan="8"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr align="left" valign="top"> 
          <td width="150" bgcolor="#6B86DE"> <br>
		  <% if Ffile="min" then %>
            <table width="135" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr bgcolor="#295DCE"> 
                <td width="10" bgcolor="#295DCE"> <div align="center"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong> 
                    </strong></font></div></td>
                <td bgcolor="#295DCE"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong>资料 
                  / 文件夹</strong></font></td>
                <td width="33"> <div align="right"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"></font><a href="?fileoptions=max&action=viewfolder&path=<%=request.querystring("path")%>"><img src="cf_images/arrows_down.gif" width="21" height="22" border="0"></a></div></td>
              </tr>
            </table>
            <% else %>
          
            <table width="135" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr bgcolor="#295DCE"> 
                <td width="10" bgcolor="#295DCE"> 
                  <div align="center"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong> 
                    </strong></font></div></td>
                <td bgcolor="#295DCE"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong>资料 
                  / 文件夹</strong></font></td>
                <td width="33"> <div align="right"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"></font><a href="?fileoptions=min&action=viewfolder&path=<%=request.querystring("path")%>"><img src="cf_images/arrows_up.gif"  border="0"></a></div></td>
              </tr>
              <tr bgcolor="#EFF3FF"> 
                <td colspan="3">
				<%If Session("CFFileAdmin")<>"" Then%>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="24%"><div align="center"><img src="cf_images/expl_newfile.gif" width="26" height="21"></div></td>
                      <td width="76%"><a href="?action=newfile&path=<%=request.querystring("path")%>">自建文件</a></td>
                    </tr>
                  </table>
				  <br>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="24%"><div align="center"><img src="cf_images/nfolder.gif" width="25" height="16"></div></td>
                      <td width="76%"><a href="?action=CreateNewFolder&path=<%=request.querystring("path")%>">创建文件夹</a></td>
                    </tr>
                  </table>
                  <br>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="24%"><div align="center"><img src="cf_images/upload.gif" ></div></td>
                      <td width="76%"><a href="?action=UploadFiles&path=<%=request.querystring("path")%>">上传本地文件</a></td>
                    </tr>
                  </table>
                  <%Else%>
				  <br>
				  <%End If%>

</td>
              </tr>
            </table> 
			<% end if %><br>

          </td>
          <td width="5">&nbsp;</td>
          <td width="574" height="350" bgcolor="#FFFFFF"> 
            <!--#include file="cf_filemanager.asp" -->
          </td>
        </tr>
      </table> </td>
  </tr>
  <tr > 
    <td height="25" colspan="8" bordercolor="#FFFFFF" background="cf_images/footer_bg.gif"> 
      <div align="right"><a href="http://www.qqcf.com" target="_blank">乘风文件管理系统 v1.0</a></div></td>
  </tr>
</table>

</body>
</html>
