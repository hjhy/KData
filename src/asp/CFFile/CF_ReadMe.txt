乘风文件管理系统 v1.0
作者QQ：178575
作者E-Mail：yliangcf@163.com
作者网站：http://www.qqcf.com
上面有程序在线演示，安装演示，使用疑难解答，最新版本下载等内容
因为这些内容可能时常更新，就没有放在程序里，请自己上网站查看


1.简单高效免费的站点文件管理系统，对您网站内的文件和文件夹进行可视化的管理操作
2.可以对文件进行浏览、编辑、重命名、删除、新建、上传文件等功能
3.可以对文件夹进行浏览、重命名、删除、新建等功能
4.可以自定义匿名用户浏览和下载权限的目录
5.方便的路径切换，在路径栏里点路径目录名可快速切换目录
6.下载可以结合防盗链组件使用
7.程序没有使用数据库，简单方便
8.首页文件是cf_index.asp，避免了和其它站点文件冲突，为了安全系统本身的文件无法查看和修改
9.初次使用时请修改cf_config.asp里的管理密码，默认密码admin

