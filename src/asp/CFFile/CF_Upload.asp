<%
'乘风文件管理系统
'作者QQ：178575
'作者E-Mail：yliangcf@163.com
'作者网站：http://www.qqcf.com
'上面有程序在线演示，安装演示，使用疑难解答，最新版本下载等内容
'因为这些内容可能时常更新，就没有放在程序里，请自己上网站查看
%>
<!--#include file="CF_Config.asp"-->
<!--#include file="CF_Include.asp"-->
<!--#include file="CF_MD5.asp" -->
<!--#include file="CF_UpLoadClass.asp"-->
<%
CFFileAdminCookie=Request.Cookies("CFFileAdminCookie")

If Session("CFFileAdmin")="" And CFFileAdminCookie<>"" Then  
 If CFFileAdminCookie=MD5(AdminPwdMd5&SysCode,1) Then Session("CFFileAdmin") = "ok"
End If

If Session("CFFileAdmin")<>"" And CFFileAdminCookie="" Then'Session存在但cookie不存在时，重新写Cookie
   Response.Cookies("CFFileAdminCookie")=MD5(AdminPwdMd5&SysCode,1)
   Response.Cookies("CFFileAdminCookie").expires=Dateadd("n",480,Now())
End If

If Session("CFFileAdmin")="" Then
 Response.Write "没有权限"
 Response.End
End If

dim savpath
savepath = session("foldername")
If savepath="" Then
 Response.write "上传目录不能为空"
 Response.End
End If

dim upload
set upload = new AnUpLoad
upload.Exe = "*"
upload.MaxSize = 2 * 1024 * 1024 '2M
upload.GetData()
if upload.ErrorID>0 then 
	response.Write upload.Description
else	
	for each f in upload.files(-1)
		dim file
		set file = upload.files(f)
		if not(file is nothing) then
			result = file.saveToFile(savepath,1,true)
			if result then
				response.Write "文件'" & file.LocalName & "'上传成功，保存位置'" & server.MapPath(savepath & "/" & file.filename) & "',文件大小" & file.size & "字节<br />"
			else
				response.Write file.Exception
			end if
		end if
		set file = nothing
	next
end if
set upload = Nothing

Response.redirect(HttpPath(2) & "cf_index.asp?Action=viewfolder&path=" & savepath)
%>