// PicWindow.cpp: implementation of the CPicWindow class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "PicWindow.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPicWindow::CPicWindow()
:m_wZoomMode(ID_VIEW_SHRINK)
{
}

CPicWindow::~CPicWindow()
{
}

//////////////////////////////////////////////////////////////////////
// Message handlers
//////////////////////////////////////////////////////////////////////

LRESULT CPicWindow::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	setScrolls();
	return 0;
}

void CPicWindow::DoPaint(CDCHandle dc)
{
	SIZE picSize = {0, 0};
	m_pix.GetSizeInPixels(picSize);
	RECT rcPaint = {0, 0, m_sizeClient.cx, m_sizeClient.cy};
	if (m_wZoomMode != ID_VIEW_SHRINK ||
		(picSize.cx < m_sizeClient.cx && picSize.cy < m_sizeClient.cy)){
		rcPaint.left = max(0, (m_sizeClient.cx - picSize.cx)/2);
		rcPaint.top = max(0, (m_sizeClient.cy - picSize.cy)/2);
		rcPaint.right = rcPaint.left + picSize.cx;
		rcPaint.bottom = rcPaint.top + picSize.cy; 
	}
	else{	// fit to window
		float xRatio = static_cast<float>(picSize.cx) /
					   static_cast<float>(m_sizeClient.cx);
		float yRatio = static_cast<float>(picSize.cy) /
					   static_cast<float>(m_sizeClient.cy);
		if (xRatio < yRatio){
			int paintWidth = static_cast<int>(
				static_cast<float>(picSize.cx)/yRatio);
			rcPaint.left = (m_sizeClient.cx - paintWidth) / 2;
			rcPaint.right = rcPaint.left + paintWidth;
		}
		else if (yRatio < xRatio){
			int paintHeight = static_cast<int>(
				static_cast<float>(picSize.cy)/xRatio);
			rcPaint.top = (m_sizeClient.cy - paintHeight) / 2;
			rcPaint.bottom = rcPaint.top + paintHeight;
		}
	}
	m_pix.Render(dc, rcPaint);
}

//////////////////////////////////////////////////////////////////////
// Public interface
//////////////////////////////////////////////////////////////////////

// accepts a filename to read a picture from, aborts any previous read by
// resetting m_lPicLoadStatus
DWORD CPicWindow::setPicturePath(TCHAR *pszPicturePath)
{
	if (!lstrcmpi(pszPicturePath, m_szPicturePath))
		return 0;

	BOOL bResult = m_pix.LoadFromFile(pszPicturePath);
	if (bResult)
		// copy the full pathname to the member variable
		lstrcpyn(m_szPicturePath, pszPicturePath, MAX_PATH);
	else
		m_szPicturePath[0] = 0;
	// set the scale, size, scrollbars, etc.
	setScrolls();
	return 0;
}

void CPicWindow::toggleZoomMode()
{
	m_wZoomMode ^= ID_VIEW_SHRINK;
	setScrolls();
}

void CPicWindow::setScrolls(BOOL bRedraw)
{
	SIZE picSize = {0, 0};
	m_pix.GetSizeInPixels(picSize);
	if		(!m_pix.m_pPix ||
			m_wZoomMode == ID_VIEW_SHRINK ||
			(picSize.cx <= m_sizeClient.cx &&
			picSize.cy <= m_sizeClient.cy))
		SetScrollSize(1, 1, bRedraw);
	else
		SetScrollSize(picSize.cx, picSize.cy, bRedraw);
}

bool CPicWindow::isViewShrinkToFit()
{
	return m_wZoomMode == ID_VIEW_SHRINK;
}
