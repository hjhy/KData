// PicWindow.h: interface for the CPicWindow class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PICWINDOW_H__997E15DC_79EB_4035_AABF_520C3A0A7328__INCLUDED_)
#define AFX_PICWINDOW_H__997E15DC_79EB_4035_AABF_520C3A0A7328__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "pix.h"
#include <atlscrl.h>

class CPicWindow :	public CScrollWindowImpl<CPicWindow>
{
public:
	DECLARE_WND_CLASS(TEXT("CPicWindow"))
	CPicWindow();
	~CPicWindow();
	void DoPaint(CDCHandle dc);
	BEGIN_MSG_MAP(CPicWindow)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		CHAIN_MSG_MAP(CScrollWindowImpl<CPicWindow>)
	END_MSG_MAP()

// picture operations
	bool isViewShrinkToFit();
	void toggleZoomMode();
	DWORD setPicturePath(TCHAR* pszPicturePath);

protected:
	void setScrolls(BOOL bRedraw = TRUE);

///////////////////////////////////////////////////////////////////////////////
//	Internal Data	
///////////////////////////////////////////////////////////////////////////////

						CPix					m_pix;
						TCHAR					m_szPicturePath[MAX_PATH];
						WORD					m_wZoomMode;

///////////////////////////////////////////////////////////////////////////////
//	Message Handlers
///////////////////////////////////////////////////////////////////////////////
	LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
};

#endif // !defined(AFX_PICWINDOW_H__997E15DC_79EB_4035_AABF_520C3A0A7328__INCLUDED_)
