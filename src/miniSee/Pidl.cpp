// Pidl.cpp: implementation of the CPidlHelper class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Pidl.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPidlHelper::CPidlHelper()
//:m_pShellMalloc(NULL)
{
	if (FAILED(SHGetMalloc(&m_spMalloc)))
		m_spMalloc = NULL;
}

CPidlHelper::~CPidlHelper()
{
}

void CPidlHelper::freePidl(LPITEMIDLIST& pidl)
{
	ILFree(pidl);
	pidl = NULL;
}

// Splits pidl into a pidl for its parent folder, and a single-SHITEMID
// relative pidl. pParent and/or pChild can be NULL, in which case the
// corresponding pidl isn't created. The caller must free any pidls created.
// No pidls are created if an error occors.
// Return values: true if pidls were successfully created, false otherwise.
bool CPidlHelper::split(LPCITEMIDLIST pidl, LPITEMIDLIST *pParent, LPITEMIDLIST *pChild)
{
	if (pParent){
		*pParent = NULL;
		ILRemoveLastID(*pParent = ILClone(pidl));
		if (!*pParent) return false;
	}
	if (pChild){
		*pChild = NULL;
		*pChild = ILClone(ILFindLastID(pidl));
		if (!*pChild){
			ILFree(*pParent);
			return false;
		}
	}
	return true;
}

// puts up to nMaxLen characters of pidl's filename in sz. Assumes that pidl
// is a single SHITEMID that belongs to pParent.
bool CPidlHelper::getFileName(IShellFolder* pParent,
							  LPCITEMIDLIST pidl,
							  TCHAR *sz,
							  int nMaxLen)
{
	STRRET strret;
	if (FAILED(pParent->GetDisplayNameOf(pidl, SHGDN_FORPARSING, &strret)))
		return false;
	return strret2TCHAR(pidl, &strret, sz, nMaxLen);
}

bool CPidlHelper::strret2TCHAR(LPCITEMIDLIST pidl,
							  STRRET *psr,
							  TCHAR *sz,
							  int nMaxChars,		// = MAX_PATH
							  bool bFreeOleStr)		// = true
{
	USES_CONVERSION;
	if (!m_spMalloc) return false;

	TCHAR* pszTmp = NULL;
	switch (psr->uType){
	case STRRET_WSTR:
		pszTmp = W2T(psr->pOleStr);
		if (bFreeOleStr) m_spMalloc->Free(psr->pOleStr);
		break;
	case STRRET_OFFSET:
		pszTmp = A2T((char*)pidl + psr->uOffset);
		break;
	case STRRET_CSTR:
		pszTmp = A2T(psr->cStr);
		break;
	default:
		break;
	}
	if (pszTmp)
		lstrcpyn(sz, pszTmp, nMaxChars);
	return NULL != pszTmp;
}
