// Pidl.h: interface for the CPidlHelper class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PIDL_H__D64F05F0_1989_404C_BB20_75057F58DB76__INCLUDED_)
#define AFX_PIDL_H__D64F05F0_1989_404C_BB20_75057F58DB76__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CPidlHelper 
{
public:
	CPidlHelper();
	virtual ~CPidlHelper();

// puts up to nMaxLen characters of pidl's filename in sz. Assumes that pidl
// is a single SHITEMID that belongs to pParent.
	bool getFileName(IShellFolder* pParent, LPCITEMIDLIST pidl, TCHAR* sz, int nMaxLen = MAX_PATH);

// Splits pidl into a pidl for its parent folder, and a single-SHITEMID
// relative pidl. pParent and/or pChild can be NULL, in which case the
// corresponding pidl isn't created. The caller must free any pidls created.
// No pidls are created if an error occors.
// Return values: true if pidls were successfully created, false otherwise.
	bool split(LPCITEMIDLIST pidl, LPITEMIDLIST* pParent, LPITEMIDLIST* pChild);

// Converts a STRRET structure to a TCHAR string of nMaxChars length. Assumes
// sz is a buffer large enough to contain the string.
// Returns true of the conversion is successful.
	bool strret2TCHAR(LPCITEMIDLIST pidl, STRRET* psr, TCHAR* sz, int nMaxChars = MAX_PATH, bool bFreeOleStr = true);

// freePidl uses ILFree, thus it assumes the pidl has been allocated using
// ILClone or its equivalent
	void freePidl(LPITEMIDLIST& pidl);

// ****************************************************************************
// data
protected:
	CComPtr<IMalloc>	m_spMalloc;
};

#endif // !defined(AFX_PIDL_H__D64F05F0_1989_404C_BB20_75057F58DB76__INCLUDED_)
