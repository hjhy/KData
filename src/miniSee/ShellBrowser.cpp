// ShellBrowser.cpp: implementation of the CShellBrowser class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ShellBrowser.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CShellBrowser::CShellBrowser()
:m_dwRef(0), m_hWndShellView(NULL), m_psf(NULL),
m_pShellView(NULL), m_pidl(NULL)
{
}

CShellBrowser::~CShellBrowser()
{
	freeMemberPtrs();
}

///////////////////////////////////////////////////////////////////////////////
// IShellBrowser methods

STDMETHODIMP CShellBrowser::QueryActiveShellView(struct IShellView ** ppshv)
{
	m_pShellView->AddRef();
	*ppshv = m_pShellView;
	return NOERROR; 
}

///////////////////////////////////////////////////////////////////////////////
// Utilities

//.............................................................................
// checks if pidl is a subfolder of psfParent.
// pidl must be a single SHITEMID relative to psfParent
bool CShellBrowser::isFolder(LPCITEMIDLIST pidl, IShellFolder *psfParent)
{
	ATLASSERT(NULL != psfParent);
	if (!psfParent) return false;
	DWORD dwAttribs = SFGAO_FOLDER | SFGAO_FILESYSTEM | SFGAO_FILESYSANCESTOR;
	HRESULT hr = psfParent->GetAttributesOf(1, &pidl, &dwAttribs);
	return (NOERROR == hr &&
		(dwAttribs & SFGAO_FOLDER) != 0 &&
		((dwAttribs & SFGAO_FILESYSTEM) != 0 ||
		(dwAttribs & SFGAO_FILESYSANCESTOR) != 0));
}

bool CShellBrowser::isBrowsable(LPCITEMIDLIST pidl)	// = false
{
	return (isFolder(pidl, m_psf) || isFolderLink(pidl));
}

//.............................................................................
// Check if pidl is a link to a folder.
// return:	true if the pidl is a shortcut to a folder
bool CShellBrowser::isFolderLink(LPCITEMIDLIST pidl,
								 LPITEMIDLIST* pOutPidl)	// = NULL
{
	ATLASSERT(m_psf);
	if (!m_psf) return false;
	// is pidl a link?
	DWORD dwAttribs = SFGAO_LINK;
	m_psf->GetAttributesOf(1, &pidl, &dwAttribs);
	if (!(dwAttribs & SFGAO_LINK)) return NULL;
	// get link object
	IShellLink* pLink;
	if (NOERROR != m_psf->GetUIObjectOf(m_hWndShellView,
										1, &pidl, IID_IShellLink, 0,
										(void**)&pLink))
		return false;
	// get link target's pidl
	LPITEMIDLIST rPidl = NULL, pidlParent = NULL, pidlChild = NULL;
	if (NOERROR == pLink->GetIDList(&rPidl)){
		IShellFolder* psf;
		if (NOERROR == SHGetDesktopFolder(&psf)){
			// if pidl is complex (it probably is),
			// split into parent/child pidls first
			if (m_pidlHelp.split(rPidl, &pidlParent, &pidlChild)){
				IShellFolder* psf2;
				if (NOERROR ==
					psf->BindToObject(pidlParent, NULL, IID_IShellFolder, 
						reinterpret_cast<void**>(&psf2))){
					if (!isFolder(pidlChild, psf2))
						m_pidlHelp.freePidl(rPidl);
					psf2->Release();
				}
				m_pidlHelp.freePidl(pidlParent);
				m_pidlHelp.freePidl(pidlChild);
			}
			else if (!isFolder(rPidl, psf)) m_pidlHelp.freePidl(rPidl);
			psf->Release();
		}
	}
	pLink->Release();
	if (rPidl){
		if (pOutPidl)
			*pOutPidl = rPidl;
		else
			m_pidlHelp.freePidl(rPidl);
		return true;
	}
	else return false;
}

void CShellBrowser::freeMemberPtrs()
{
	if (m_pShellView){
		m_pShellView->UIActivate(SVUIA_DEACTIVATE);
		m_pShellView->DestroyViewWindow();
		m_pShellView->Release();
	}
	if (m_psf)
		m_psf->Release();
	m_pShellView = NULL;
	m_psf = NULL;
}
