// ShellBrowser.h: interface for the CShellBrowser class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SHELLBROWSER_H__B105CBD4_6813_46C0_A7DA_DF66B08779BC__INCLUDED_)
#define AFX_SHELLBROWSER_H__B105CBD4_6813_46C0_A7DA_DF66B08779BC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Pidl.h"

class CShellBrowser : public IShellBrowser, public ICommDlgBrowser
{
public:
	CShellBrowser();
	virtual ~CShellBrowser();
protected:
	CPidlHelper m_pidlHelp;
	bool isFolder(LPCITEMIDLIST pidl, IShellFolder* psf);
	// methods
	bool isBrowsable(LPCITEMIDLIST pidl);
	bool strret2TCHAR(LPCITEMIDLIST pidl, STRRET* psr, TCHAR* sz, int nMaxChars = MAX_PATH);
	bool isFolderLink(LPCITEMIDLIST pidl, LPITEMIDLIST* pOutPidl = NULL);
	void freeMemberPtrs();

	// data
	DWORD					m_dwRef;
	HWND					m_hWndShellView;	// handle to shellview window,
												// which encapsulates listview control
	IShellFolder*			m_psf;				// current folder
	IShellView*				m_pShellView;		// current hosted shellview
	LPITEMIDLIST			m_pidl;
public:

	STDMETHODIMP QueryInterface(REFIID iid, void **ppvObject)
	{
		if(ppvObject == NULL)
			return E_POINTER;

		*ppvObject = NULL;

		if(iid == IID_IUnknown)
			*ppvObject = (IUnknown*)(IShellBrowser*) this;
		else if(iid == IID_IOleWindow)
			*ppvObject = (IOleWindow*) this;			
		else if(iid == IID_IShellBrowser)
			*ppvObject = (IShellBrowser*) this;
		else if(iid == IID_ICommDlgBrowser)
			*ppvObject = (ICommDlgBrowser*) this;
		else
			return E_NOINTERFACE;
		((IUnknown*)(*ppvObject))->AddRef();
		return S_OK;
	}

	STDMETHOD_(ULONG, AddRef)() { return ++m_dwRef; }
	STDMETHOD_(ULONG, Release)(){ return --m_dwRef; }  //not heap based	

	// *** IOleWindow methods ***
	STDMETHODIMP ContextSensitiveHelp(BOOL /*fEnterMode*/){return E_NOTIMPL;}	

	// *** ICommDlgBrowser methods ***
	// handle double click and ENTER key if needed
	STDMETHODIMP OnDefaultCommand(THIS_ struct IShellView * /*ppshv*/){return E_NOTIMPL;}
	// handle selection, rename, focus if needed
	STDMETHODIMP OnStateChange(THIS_ struct IShellView * /*ppshv*/,ULONG /*uChange*/){return E_NOTIMPL;}
	// filter files if needed
	STDMETHODIMP IncludeObject(THIS_ struct IShellView * /*ppshv*/,LPCITEMIDLIST /*pidl*/){return S_OK;}

	// *** IShellBrowser methods *** (same as IOleInPlaceFrame)
	STDMETHODIMP QueryActiveShellView(struct IShellView ** ppshv);
	STDMETHODIMP EnableModelessSB(BOOL /*fEnable*/){return E_NOTIMPL;}
	STDMETHODIMP GetControlWindow(UINT /*id*/, HWND* /*lphwnd*/){return E_NOTIMPL;}
	STDMETHODIMP SendControlMsg(UINT /*id*/, UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, LRESULT* /*pret*/){return E_NOTIMPL;};
	STDMETHODIMP InsertMenusSB(HMENU /*hmenuShared*/, LPOLEMENUGROUPWIDTHS /*lpMenuWidths*/){return E_NOTIMPL;}
	STDMETHODIMP SetMenuSB(HMENU /*hmenuShared*/, HOLEMENU /*holemenuReserved*/,HWND /*hwndActiveObject*/){return E_NOTIMPL;}
	STDMETHODIMP RemoveMenusSB(HMENU /*hmenuShared*/){return E_NOTIMPL;}
	STDMETHODIMP SetStatusTextSB(LPCOLESTR /*lpszStatusText*/){return E_NOTIMPL;}
	STDMETHODIMP GetViewStateStream(DWORD /*grfMode*/,LPSTREAM * /*ppStrm*/){return E_NOTIMPL;}
	STDMETHODIMP OnViewWindowActive(struct IShellView* /*ppshv*/){return E_NOTIMPL;}
	STDMETHODIMP SetToolbarItems(LPTBBUTTON /*lpButtons*/, UINT /*nButtons*/,UINT /*uFlags*/){return E_NOTIMPL;}
	STDMETHODIMP TranslateAcceleratorSB(LPMSG /*lpmsg*/, WORD /*wID*/){return E_NOTIMPL;}
};

#endif // !defined(AFX_SHELLBROWSER_H__B105CBD4_6813_46C0_A7DA_DF66B08779BC__INCLUDED_)
