// ShellViewSplitter.cpp: implementation of the CShellViewSplitter class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ShellViewSplitter.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CShellViewSplitter::CShellViewSplitter()
{

}

CShellViewSplitter::~CShellViewSplitter()
{

}

LRESULT CShellViewSplitter::OnGetIShellBrowser(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	return ::SendMessage(GetParent(), WM_GETISHELLBROWSER, wParam, lParam);;
}
