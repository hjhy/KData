// ShellViewSplitter.h: interface for the CShellViewSplitter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SHELLVIEWSPLITTER_H__4E992B86_27AD_4F24_87D7_D5960368E0AE__INCLUDED_)
#define AFX_SHELLVIEWSPLITTER_H__4E992B86_27AD_4F24_87D7_D5960368E0AE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include <atlsplit.h>									// WTL::CSplitterWindow

#ifndef WM_GETISHELLBROWSER
	#define WM_GETISHELLBROWSER		(WM_USER+7)
#endif

class CShellViewSplitter : public CSplitterWindow  
{
public:
	DECLARE_WND_CLASS(TEXT("CShellViewSplitter"))
	CShellViewSplitter();
	virtual ~CShellViewSplitter();
	BEGIN_MSG_MAP(CShellViewSplitter)
		MESSAGE_HANDLER(WM_GETISHELLBROWSER, OnGetIShellBrowser)
		CHAIN_MSG_MAP(CSplitterWindow)
	END_MSG_MAP()

	// Message handler prototypes
	LRESULT OnGetIShellBrowser(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

};

#endif // !defined(AFX_SHELLVIEWSPLITTER_H__4E992B86_27AD_4F24_87D7_D5960368E0AE__INCLUDED_)
