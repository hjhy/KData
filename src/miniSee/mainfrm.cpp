// MainFrm.cpp : implmentation of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

#include "aboutdlg.h"
#include <shlwapi.h>		// IE4 shell utility functions
#include "MainFrm.h"


// TO DO: implement more flexible, ini file based extension management system
const TCHAR* const CMainFrame::m_szExtensions[] = {
	TEXT("bmp"),
	TEXT("gif"),
	TEXT("jpg")
};
const int CMainFrame::m_nExt = sizeof(m_szExtensions) / sizeof(TCHAR*);
const int CMainFrame::ID_TIMER_PICLOAD = 1;

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
	return CFrameWindowImpl<CMainFrame>::PreTranslateMessage(pMsg);
}

///////////////////////////////////////////////////////////////////////////////
// Message handlers
BOOL CMainFrame::OnIdle()
{
	UIUpdateToolBar();
	return FALSE;
}

LRESULT CMainFrame::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	// get system strings
	GetCurrentDirectory(MAX_PATH, m_pszAppDir);

	// create command bar window
	HWND hWndCmdBar = m_CmdBar.Create(m_hWnd, rcDefault, NULL, ATL_SIMPLE_CMDBAR_PANE_STYLE);
	// attach menu
	m_CmdBar.AttachMenu(GetMenu());
	// remove old menu
	SetMenu(NULL);

	// init toolbar button array, including separators, if any
	TBBUTTON tbb[] =
	{
		// File|New
		STBB_STD_FILENEW,
		// File|Save
		STBB_STD_FILESAVE,
		// separator
		STBB_SEPARATOR,
		// View|Parent folder
		STBB_VIEW_PARENTFOLDER(ID_VIEW_PARENTFOLDER),
		// separator
		STBB_SEPARATOR,
		// Help|About
		STBB(STD_HELP, ID_APP_ABOUT, IDB_STD_SMALL_COLOR)
	};

	// create the toolbar control
	HWND hWndToolBar = CreateSysToolbarCtrl(m_hWnd, tbb,
		sizeof(tbb)/sizeof(TBBUTTON), &m_CmdBar);

	CreateSimpleReBar(ATL_SIMPLE_REBAR_NOBORDER_STYLE);
	AddSimpleReBarBand(hWndCmdBar);
	AddSimpleReBarBand(hWndToolBar, NULL, TRUE);

	CreateSimpleStatusBar();

	UIAddToolBar(hWndToolBar);
	UISetCheck(ID_VIEW_TOOLBAR, 1);
	UISetCheck(ID_VIEW_STATUS_BAR, 1);
	UISetCheck(ID_VIEW_SHRINK, m_picWindow.isViewShrinkToFit());

	// register object for message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->AddMessageFilter(this);
	pLoop->AddIdleHandler(this);

	// create the vertical splitter
	RECT rcSplitter;
	::GetClientRect(m_hWnd, &rcSplitter);
	m_hWndClient = m_vSplit.Create(m_hWnd, rcSplitter, NULL, WS_CHILD |
		WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);
	m_vSplit.m_cxyMin = 35; // minimum size
	m_vSplit.SetSplitterPos(150); // from left

	// Initialize folder window
	if (FAILED(BrowseObject(NULL, SBSP_SAMEBROWSER | SBSP_ABSOLUTE)))
		return -1;
	RECT rect = { 0, 0, 1, 1 };
	m_picWindow.Create(m_vSplit, rect, NULL, 
		WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
		WS_EX_CLIENTEDGE);
	m_vSplit.SetSplitterPane(SPLIT_PANE_RIGHT, m_picWindow.m_hWnd);
	return 0;
}

LRESULT CMainFrame::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	freeMemberPtrs();
	::PostMessage(m_hWnd, WM_QUIT, 0, 0);
	return 0;
}

LRESULT CMainFrame::OnFileExit(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	PostMessage(WM_CLOSE);
	return 0;
}

LRESULT CMainFrame::OnFileNew(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CMainFrame::OnViewParentfolder(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BrowseObject(NULL, SBSP_SAMEBROWSER | SBSP_PARENT);
	return 0;
}

LRESULT CMainFrame::OnViewToolBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	static BOOL bVisible = TRUE;	// initially visible
	bVisible = !bVisible;
	CReBarCtrl rebar = m_hWndToolBar;
	int nBandIndex = rebar.IdToIndex(ATL_IDW_BAND_FIRST + 1);	// toolbar is 2nd added band
	rebar.ShowBand(nBandIndex, bVisible);
	UISetCheck(ID_VIEW_TOOLBAR, bVisible);
	UpdateLayout();
	return 0;
}

LRESULT CMainFrame::OnViewStatusBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL bVisible = !::IsWindowVisible(m_hWndStatusBar);
	::ShowWindow(m_hWndStatusBar, bVisible ? SW_SHOWNOACTIVATE : SW_HIDE);
	UISetCheck(ID_VIEW_STATUS_BAR, bVisible);
	UpdateLayout();
	return 0;
}

LRESULT CMainFrame::OnViewShrink(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	m_picWindow.toggleZoomMode();
	UISetCheck(ID_VIEW_SHRINK, m_picWindow.isViewShrinkToFit());
	UpdateLayout();
	return 0;
}

LRESULT CMainFrame::OnAppAbout(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CAboutDlg dlg;
	dlg.DoModal();
	return 0;
}

LRESULT CMainFrame::OnGetIShellBrowser(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	bHandled = TRUE;
	return (LRESULT)(IShellBrowser*)this;
}

LRESULT CMainFrame::OnTimer(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	if (wParam == ID_TIMER_PICLOAD){	// timer ID == ID_TIMER_PICLOAD
		KillTimer(ID_TIMER_PICLOAD);
		LPCITEMIDLIST pidl;
		int iFocus = getNextShellViewWindowItem(NULL, &pidl, LVNI_FOCUSED);
		ATLTRACE(TEXT("OnTimer focusItem (old): %d (%d)\n"), iFocus, m_iFocusItem);
		if (iFocus < 0 || iFocus != m_iFocusItem) return 0;
		TCHAR szPath[MAX_PATH];
		if (!isPicture(pidl, szPath))
			szPath[0] = TEXT('\0');
		ATLTRACE(TEXT("**********************\n\t%s\n"), szPath);
		m_picWindow.setPicturePath(szPath);
		SetWindowText(PathFindFileName(szPath));
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// IOleWindow methods

STDMETHODIMP CMainFrame::GetWindow(HWND * lphwnd)
{ 
	*lphwnd = m_vSplit; 
	return S_OK; 
}

///////////////////////////////////////////////////////////////////////////////
// IShellBrowser methods

STDMETHODIMP CMainFrame::BrowseObject(LPCITEMIDLIST pidl, UINT wFlags)
{
	if (wFlags & SBSP_PARENT && !m_pidl)
		return E_FAIL;
	if (wFlags & SBSP_RELATIVE && !m_psf)
		return E_FAIL;

	IShellFolder* psf = m_psf;
	LPITEMIDLIST pidlCopy = NULL;
	HRESULT hr = E_FAIL;
	// get IShellFolder, FULLY QUALIFIED pidl...
	if (NULL == pidl && !(wFlags & SBSP_PARENT)){
		freeMemberPtrs();						// ...for the desktop...
		if (SUCCEEDED(SHGetDesktopFolder(&m_psf)))
			hr = SHGetSpecialFolderLocation(m_hWnd, CSIDL_DESKTOP, &pidlCopy);
	}
	else{										// ...or for the selected folder...
		if (wFlags & SBSP_RELATIVE){			// ...using a relative pidl...
			// Browse child folder
			TCHAR szPath[MAX_PATH];
			m_pidlHelp.getFileName(m_psf, pidl, szPath);
			pidlCopy = ILCombine(m_pidl, pidl);
			hr = psf->BindToObject(pidl,NULL, IID_IShellFolder,
				reinterpret_cast<void**>(&m_psf));
		}
		else{									// ...or an absolute pidl.
			if (wFlags & SBSP_PARENT){
				// Browse parent: pidlCopy = parent folder's PIDL
				if (!m_pidlHelp.split(m_pidl, &pidlCopy, NULL)) return E_FAIL;
			}
			else{
				// Browse absolute PIDL: pidlCopy = clone(pidl)
				pidlCopy = ILClone(pidl);
			}
			// Create IShellFolder for target PIDL through Desktop folder
			IShellFolder* psf2;
			if (SUCCEEDED(hr = SHGetDesktopFolder(&psf2))){
				if (SUCCEEDED(psf2->BindToObject(pidlCopy, NULL,
					IID_IShellFolder, reinterpret_cast<void**>(&m_psf)))){
						psf2->Release();}
				else m_psf = psf2;
			}
		}
	}
	if (FAILED(hr)){
		if (pidlCopy) m_pidlHelp.freePidl(pidlCopy);
		if (m_psf){ 
			m_psf->Release();}
		m_psf = psf;
		return hr;
	}
	ATLASSERT(m_psf && pidlCopy);
	if (!m_psf || !pidlCopy) return E_FAIL;

	// use the IShellFolder to create a view window
	FOLDERSETTINGS fs = {FVM_DETAILS, FWF_SNAPTOGRID | FWF_NOICONS};
	if (m_pShellView) m_pShellView->GetCurrentInfo(&fs);
	IShellView* psv = m_pShellView;
	m_pShellView = NULL;
	HWND hWndShellView = NULL;

	if (SUCCEEDED(hr = m_psf->CreateViewObject(m_vSplit, IID_IShellView,
		reinterpret_cast<void**>(&m_pShellView)))){
		hr = m_pShellView->CreateViewWindow(psv, &fs,
			static_cast<IShellBrowser*>(this), &CWindow::rcDefault,
			&hWndShellView);
	}
	if (FAILED(hr)){
		if (m_pShellView){
			m_pShellView->UIActivate(SVUIA_DEACTIVATE);
			m_pShellView->DestroyViewWindow();
			m_pShellView->Release();
			m_pShellView = psv;
		}
		m_pidlHelp.freePidl(pidlCopy);
		m_psf->Release();
		m_psf = psf;
		return hr;
	}
	m_hWndShellView = hWndShellView;
	m_iFocusItem = -1;
	if (m_pidl)
		m_pidlHelp.freePidl(m_pidl);
	m_pidl = pidlCopy;
	UIEnable(ID_VIEW_PARENTFOLDER, m_pidl->mkid.cb != 0);
	if(psv != NULL)
	{
		psv->UIActivate(SVUIA_DEACTIVATE);
		psv->DestroyViewWindow();
		psv->Release();
	}
	if (psf){
		psf->Release();}
	m_vSplit.SetSplitterPane(SPLIT_PANE_LEFT, m_hWndShellView);
	m_pShellView->UIActivate(SVUIA_ACTIVATE_FOCUS);
	m_dwLastSelChangeTicks = GetTickCount();

	// Select first item in list view
	HWND hWndListView = FindWindowEx(m_hWndShellView, NULL, WC_LISTVIEW, NULL);
	ListView_SetItemState(hWndListView, 0, LVIS_FOCUSED | LVIS_SELECTED,
		0x000F);
	::ShowWindow(hWndListView,SW_NORMAL);
	return NOERROR;
}

///////////////////////////////////////////////////////////////////////////////
// ICommDlgBrowser methods
STDMETHODIMP CMainFrame::OnDefaultCommand(THIS_ struct IShellView* /*ppshv*/)
{	//handle double click and ENTER key if needed
	// get item selected in list view
	LPCITEMIDLIST pidl;
	if (-1 == getNextShellViewWindowItem(NULL, &pidl))
		return E_FAIL;
	LPITEMIDLIST pidlTo = NULL;

	if (isFolder(pidl, m_psf))
	// if it's a folder, jump to it
		BrowseObject(pidl, SBSP_SAMEBROWSER | SBSP_RELATIVE);
	else if (isFolderLink(pidl, &pidlTo))
	// if it's a folder link, follow the link
		BrowseObject(pidlTo, SBSP_SAMEBROWSER | SBSP_ABSOLUTE);
	else return E_NOTIMPL;
	if (pidlTo) m_pidlHelp.freePidl(pidlTo);
	return NOERROR;
}

STDMETHODIMP CMainFrame::OnStateChange(THIS_ struct IShellView* /*ppshv*/,
									   ULONG uChange)
{	//handle selection, rename, focus if needed
	if (uChange == CDBOSC_SELCHANGE){
		// get the index of the focused listview item
		int iFocus = getNextShellViewWindowItem(NULL, NULL, LVNI_FOCUSED);
		// don't bother with any item we've already shown, or can't show
		if (iFocus < 0 || iFocus == m_iFocusItem) return S_OK;

		// selection really has changed.
		KillTimer(ID_TIMER_PICLOAD);		// stop any pending picture loads
		m_iFocusItem = iFocus;				// save the selected item's index

		// wait a bit, then load the selected picture
		// select the wait interval:
		DWORD dwNow = GetTickCount();
		DWORD dwElapsed = dwNow - m_dwLastSelChangeTicks;
		m_dwLastSelChangeTicks = dwNow;

		// set the timer and wait. WM_TIMER handler will load the selected
		// picture.
		SetTimer(ID_TIMER_PICLOAD, dwElapsed < 100 ? dwElapsed + 20 : 120);
		return  S_OK;
	}
	return E_NOTIMPL; 
}

STDMETHODIMP CMainFrame::IncludeObject(
	THIS_							// .cpp files: expands to nothing (see <basetyps.h>)
	struct IShellView* /*ppshv*/,	// applicable shell view
	LPCITEMIDLIST pidl)				// the object in question (RELATIVE pidl)
{
	if (isBrowsable(pidl) || isPicture(pidl))
		return S_OK;	// show it
	return S_FALSE;		// hide it
}

///////////////////////////////////////////////////////////////////////////////
// Utility functions

// answers "is the item a picture?" true/false. If pszFileName is specified,
// copies filename into it, up to nChars
bool CMainFrame::isPicture(LPCITEMIDLIST pidl,
						   TCHAR* pszFileName,	// = NULL
						   int nChars)			// = MAX_PATH
{
	TCHAR szParseName[MAX_PATH] = {0};
	if (!m_pidlHelp.getFileName(m_psf, pidl, szParseName)) return false;
	if (pszFileName)
		lstrcpyn(pszFileName, szParseName, nChars);
	TCHAR* pe = PathFindExtension(szParseName);
	if (!*pe) return false;
	pe++;
	int j;
	for (int i = 0; i < m_nExt; i++){
		j = lstrcmpi(m_szExtensions[i], pe);
		if (!j) return true;
		else if (j > 0) return false;
	}
	return false;
}

// returns the index of the first item after nPrevious that has the properties
// specified by flags, or -1 if none are found. If nPrevious is -1, returns the
// index of the first item. If pPidl is not NULL and a valid index is returned,
// *pPidl will allow temporary access the item's PIDL. The system should
// eventually delete the PIDL so if it must be used for a prolonged period the
// caller should make a copy of it.
int CMainFrame::getNextShellViewWindowItem(HWND hWnd,				// = NULL
										   LPCITEMIDLIST *pPidl,	// = NULL
										   UINT flags,				// = LVNI_SELECTED
										   int nPrevious)			// = -1
{
	// get list view
	HWND hWndListView = hWnd;
	if (!hWndListView) hWndListView = getShellListViewHwnd();
	if (!hWndListView) return -1;

	// get item selected in list view
	LV_ITEM lvItem = {0};
	lvItem.iItem = SendMessage(hWndListView, LVM_GETNEXTITEM, nPrevious, 
		MAKELPARAM(LVNI_ALL | flags, 0));

	// get pidl
	if (lvItem.iItem >= 0 && pPidl){
		ATLTRACE(TEXT("Getting pidl\n"));
		getShellListViewItemPidl(hWndListView, lvItem, pPidl);
	}

	return lvItem.iItem;
}

HWND CMainFrame::getShellListViewHwnd()
{
	if (!m_hWndShellView) return NULL;
	// get item selected in list view
	HWND hWndListView = FindWindowEx(m_hWndShellView, NULL, WC_LISTVIEW, NULL);
	if (!::IsWindowVisible(hWndListView)){
		hWndListView = FindWindowEx(m_hWndShellView, NULL,
			TEXT("ThumbnailVwExtWnd32"), NULL);
		hWndListView = FindWindowEx(hWndListView, NULL, WC_LISTVIEW, NULL);
	}
	return hWndListView;
}

void CMainFrame::getShellListViewItemPidl(HWND hWnd, int iItem, LPCITEMIDLIST* pPidl)
{
	if (pPidl && iItem >= 0){
		LV_ITEM lvItem = {0};
		lvItem.iItem = iItem;
		getShellListViewItemPidl(hWnd, lvItem, pPidl);
	}
}

void CMainFrame::getShellListViewItemPidl(HWND hWnd, LV_ITEM& lvItem, LPCITEMIDLIST *pPidl)
{
	// get pidl (hack!)
	lvItem.mask = LVIF_PARAM;
	lvItem.iSubItem = 0;
	lvItem.lParam = 0;
	SendMessage(hWnd, LVM_GETITEM, 0,
		reinterpret_cast<LPARAM>(&lvItem));
	*pPidl = reinterpret_cast<LPCITEMIDLIST>(lvItem.lParam);
}
