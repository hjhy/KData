// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__5C712340_92AD_4CB6_A2D6_68DAEFE520D3__INCLUDED_)
#define AFX_MAINFRM_H__5C712340_92AD_4CB6_A2D6_68DAEFE520D3__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ShellBrowser.h"								// CShellBrowser
#include "ShellViewSplitter.h"							// CShellViewSplitter
#include "SysToolbarCtrl.h"								// CSysToolbarCtrl
#include "PicWindow.h"									// CPicWindow

#ifndef WM_GETISHELLBROWSER
	#define WM_GETISHELLBROWSER		(WM_USER+7)
#endif

class CMainFrame : public CFrameWindowImpl<CMainFrame>,
	public CUpdateUI<CMainFrame>, public CSysToolbarCtrl<CMainFrame>,
	public CMessageFilter, public CIdleHandler, public CShellBrowser
{
public:
	DECLARE_FRAME_WND_CLASS(NULL, IDR_MAINFRAME)

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnIdle();
	// *** IOleWindow methods ***
	STDMETHOD (GetWindow)(HWND * lphwnd);
	// *** IShellBrowser methods ***
	STDMETHOD (BrowseObject)(LPCITEMIDLIST pidl, UINT wFlags);
	// *** ICommDlgBrowser methods ***
	STDMETHOD (OnDefaultCommand)(THIS_ struct IShellView* /*ppshv*/);
	STDMETHOD (IncludeObject)(THIS_ struct IShellView* /*ppshv*/, LPCITEMIDLIST pidl);
    STDMETHOD(OnStateChange) (THIS_ struct IShellView* /*ppshv*/,ULONG uChange);

	BEGIN_MSG_MAP(CMainFrame)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_GETISHELLBROWSER, OnGetIShellBrowser)
		MESSAGE_HANDLER(WM_TIMER, OnTimer)
		COMMAND_ID_HANDLER(ID_APP_EXIT, OnFileExit)
		COMMAND_ID_HANDLER(ID_FILE_NEW, OnFileNew)
		COMMAND_ID_HANDLER(ID_VIEW_PARENTFOLDER, OnViewParentfolder)
		COMMAND_ID_HANDLER(ID_VIEW_TOOLBAR, OnViewToolBar)
		COMMAND_ID_HANDLER(ID_VIEW_STATUS_BAR, OnViewStatusBar)
		COMMAND_ID_HANDLER(ID_VIEW_SHRINK, OnViewShrink)
		COMMAND_ID_HANDLER(ID_APP_ABOUT, OnAppAbout)
		CHAIN_MSG_MAP(CUpdateUI<CMainFrame>)
		CHAIN_MSG_MAP(CFrameWindowImpl<CMainFrame>)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
	END_MSG_MAP()

	BEGIN_UPDATE_UI_MAP(CMainFrame)
		UPDATE_ELEMENT(ID_VIEW_TOOLBAR, UPDUI_MENUPOPUP)
		UPDATE_ELEMENT(ID_VIEW_STATUS_BAR, UPDUI_MENUPOPUP)
		UPDATE_ELEMENT(ID_VIEW_SHRINK, UPDUI_MENUPOPUP)
		UPDATE_ELEMENT(ID_VIEW_PARENTFOLDER, UPDUI_MENUPOPUP | UPDUI_TOOLBAR)
	END_UPDATE_UI_MAP()

// Handler prototypes (uncomment arguments if needed):
	LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnGetIShellBrowser(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled);
	LRESULT OnFileExit(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnFileNew(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnViewParentfolder(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnViewToolBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnViewStatusBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnViewShrink(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnAppAbout(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnTimer(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/);

protected:
	// utility methods
	bool isPicture(LPCITEMIDLIST pidl, TCHAR* pszFileName = NULL, int nChars = MAX_PATH);
	HWND getShellListViewHwnd();
	int getNextShellViewWindowItem(HWND hWnd = NULL, LPCITEMIDLIST* pPidl = NULL, UINT flags = LVNI_SELECTED, int nPrevious = -1);
	void getShellListViewItemPidl(HWND hWnd, int iItem, LPCITEMIDLIST* pPidl);
	void getShellListViewItemPidl(HWND hWnd, LV_ITEM& lvItem, LPCITEMIDLIST* pPidl);

///////////////////////////////////////////////////////////////////////////////
// data

public:
protected:
					CCommandBarCtrl		m_CmdBar;
					CPicWindow			m_picWindow;
					CShellViewSplitter	m_vSplit;
	static const	int					m_nExt;
					int					m_iFocusItem;
	static const	int					ID_TIMER_PICLOAD;
					DWORD				m_dwLastSelChangeTicks;
					TCHAR				m_pszAppDir[MAX_PATH];
	static const	TCHAR* const		m_szExtensions[];
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__5C712340_92AD_4CB6_A2D6_68DAEFE520D3__INCLUDED_)
