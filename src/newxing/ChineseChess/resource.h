//Download by http://www.NewXing.com
//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Chess.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CHESS_DIALOG                102
#define IDR_MAINFRAME                   128
#define IDD_SET                         129
#define IDB_CHESSMAN                    130
#define IDB_CHESSBOARD                  131
#define IDR_MENU_SETCHESSBOARDING       135
#define IDR_MENU_MAIN                   138
#define IDR_MENU_CHESSING               140
#define IDI_UNDO                        146
#define IDI_REDO                        147
#define IDI_STOP                        149
#define IDI_COMPUTER                    150
#define IDI_SET                         151
#define IDI_HELP                        152
#define IDB_OK                          154
#define IDB_CANCEL                      155
#define IDD_HELP                        155
#define IDC_LISTCHESSRECORD             1000
#define IDC_LIST_ENGINE                 1000
#define IDC_EDTDEPTH                    1001
#define IDC_PLY                         1002
#define IDC_OUTPUTINFO                  1003
#define IDC_BTNCOMPUTER                 1004
#define IDC_BTN_STOP                    1005
#define IDC_PROGRESSTHINK               1006
#define IDC_RADIOREDCHESS               1007
#define IDC_BTNUNDO                     1007
#define IDC_RADIOBLACKCHESS             1008
#define IDC_BTNREDO                     1008
#define IDC_RADIOPC                     1009
#define IDC_RADIOPP                     1010
#define IDC_RADIOCC                     1011
#define IDC_EDIT_HELP                   1011
#define IDC_RADIOHASH                   1012
#define IDC_RADIO_USERDEFINE            1013
#define IDC_RADIO_DEFAULTSET            1014
#define IDM_NEWGAME                     32771
#define IDM_SETCHESSBOARD               32772
#define IDM_OPENFILE                    32773
#define IDM_SAVEFILE                    32774
#define IDM_ABOUT                       32775
#define IDM_HELP                        32776
#define IDM_SET                         32777
#define ID_BAI_DELETE                   32778
#define IDM_DELETE                      32778
#define IDM_EXIT                        32778
#define ID_BAI_CANCEL                   32779
#define IDM_SCBOVER                     32779
#define ID_RED_B                        32780
#define IDM_RPAWN                       32780
#define ID_RED_P                        32781
#define IDM_RCANON                      32781
#define ID_RED_J                        32782
#define IDM_RCAR                        32782
#define ID_RED_K                        32783
#define IDM_RKING                       32783
#define IDM_PREVIEW                     32783
#define ID_RED_M                        32784
#define IDM_RHORSE                      32784
#define IDM_PREVIEWOVER                 32784
#define ID_RED_X                        32785
#define IDM_RELEPHANT                   32785
#define IDM_INVERSECB                   32785
#define ID_RED_S                        32786
#define IDM_RBISHOP                     32786
#define ID_BLACK_B                      32787
#define IDM_BPAWN                       32787
#define ID_BLACK_P                      32788
#define IDM_BCANON                      32788
#define ID_BLACK_J                      32789
#define IDM_BCAR                        32789
#define ID_BLACK_M                      32790
#define IDM_BHORSE                      32790
#define ID_BLACK_X                      32791
#define IDM_BELEPHANT                   32791
#define ID_BLACK_S                      32792
#define IDM_BBISHOP                     32792
#define ID_BLACK_K                      32793
#define IDM_BKING                       32793
#define IDM_CLEARCB                     32794

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        156
#define _APS_NEXT_COMMAND_VALUE         32786
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
