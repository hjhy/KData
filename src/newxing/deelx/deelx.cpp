// deelx.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "deelx.h"

int main(int argc, char* argv[])
{
	// text
    char * text = "12.5, a1.1, 0.123, 178";

    // declare
    static CRegexpT <char> regexp("\\b\\d+\\.\\d+");

    // loop
    MatchResult result = regexp.Match(text);

    while( result.IsMatched() )
    {
        printf("%.*s\n", result.GetEnd() - result.GetStart(), text + result.GetStart());

        // get next
        result = regexp.Match(text, result.GetEnd());
    }

    return 0;
}

