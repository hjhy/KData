; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CChessClientDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "chessclient.h"
LastPage=0

ClassCount=5
Class1=CChessClientApp
Class2=CAboutDlg
Class3=CChessClientDlg
Class4=CClientSock
Class5=CSetDlg

ResourceCount=5
Resource1=IDD_CHESSCLIENT_DIALOG
Resource2=IDR_MENUSET
Resource3=IDD_SET
Resource4=IDD_ABOUTBOX
Resource5=IDR_MAIN_MENU

[CLS:CChessClientApp]
Type=0
BaseClass=CWinApp
HeaderFile=ChessClient.h
ImplementationFile=ChessClient.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=ChessClientDlg.cpp
ImplementationFile=ChessClientDlg.cpp

[CLS:CChessClientDlg]
Type=0
BaseClass=CDialog
HeaderFile=ChessClientDlg.h
ImplementationFile=ChessClientDlg.cpp
Filter=D
VirtualFilter=dWC
LastObject=IDC_BTN_GIVEUP

[CLS:CClientSock]
Type=0
BaseClass=CSocket
HeaderFile=ClientSock.h
ImplementationFile=ClientSock.cpp

[CLS:CSetDlg]
Type=0
BaseClass=CDialog
HeaderFile=SetDlg.h
ImplementationFile=SetDlg.cpp
Filter=D
VirtualFilter=dWC
LastObject=IDC_EDIT_IPADDR

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_CHESSCLIENT_DIALOG]
Type=1
Class=CChessClientDlg
ControlCount=20
Control1=IDC_LISTCHESSRECORD,listbox,1352728833
Control2=IDC_OUTPUTINFO,static,1342312960
Control3=IDC_EDIT_MSGSEND,edit,1484853380
Control4=IDC_BTN_SEND,button,1476460545
Control5=IDC_EDIT_MSGACCEPT,edit,1352730628
Control6=IDC_BTN_UNDO,button,1476460544
Control7=IDC_BTN_PEACE,button,1476460544
Control8=IDC_BTN_GIVEUP,button,1476460544
Control9=IDC_STATIC,button,1342177287
Control10=IDC_STATIC,button,1342177287
Control11=IDC_STATIC,static,1342308352
Control12=IDC_STATIC,static,1342308352
Control13=IDC_EDIT_PLAYER1,edit,1484849280
Control14=IDC_EDIT_GROUP1,edit,1484849280
Control15=IDC_STATIC,static,1342308352
Control16=IDC_STATIC,static,1342308352
Control17=IDC_STATIC,static,1342308352
Control18=IDC_EDIT_PLAYER2,edit,1484849280
Control19=IDC_EDIT_GROUP2,edit,1484849280
Control20=IDC_STATIC,static,1342308352

[DLG:IDD_SET]
Type=1
Class=CSetDlg
ControlCount=8
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_EDIT_PORT,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=IDC_EDIT_IPADDR,edit,1350631552
Control7=IDC_STATIC,static,1342308352
Control8=IDC_EDIT_PLAYER,edit,1350631552

[MNU:IDR_MENUSET]
Type=1
Class=?
Command1=IDM_SET
CommandCount=1

[MNU:IDR_MAIN_MENU]
Type=1
Class=?
Command1=IDC_BTN_CONNECT
Command2=IDC_BTN_DISCONNECT
Command3=IDC_BTN_GIVEUP
Command4=IDC_BTN_PEACE
Command5=IDC_BTNUNDO
Command6=IDM_SET
Command7=IDM_ABOUT
Command8=IDM_HELP
CommandCount=8

