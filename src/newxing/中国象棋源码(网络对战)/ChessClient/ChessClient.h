//Download by http://www.NewXing.com
// ChessClient.h : main header file for the CHESSCLIENT application
//

#if !defined(AFX_CHESSCLIENT_H__C691175A_2DCC_4A14_A1E1_C17FC30F6F9A__INCLUDED_)
#define AFX_CHESSCLIENT_H__C691175A_2DCC_4A14_A1E1_C17FC30F6F9A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CChessClientApp:
// See ChessClient.cpp for the implementation of this class
//

class CChessClientApp : public CWinApp
{
public:
	CChessClientApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChessClientApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CChessClientApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHESSCLIENT_H__C691175A_2DCC_4A14_A1E1_C17FC30F6F9A__INCLUDED_)
