//Download by http://www.NewXing.com
// ChessClientDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ChessClient.h"
#include "ChessClientDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
#define GRILLEWIDTH  39//棋盘上每个格子的宽度
#define GRILLEHEIGHT 39//棋盘上每个格子的高度

const BYTE InitChessBoard[10][9]=
{
	{B_CAR,B_HORSE,B_ELEPHANT,B_BISHOP,B_KING,B_BISHOP,B_ELEPHANT,B_HORSE,B_CAR},
	{NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS},
	{NOCHESS,B_CANON,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,B_CANON,NOCHESS},
	{B_PAWN,NOCHESS,B_PAWN,NOCHESS,B_PAWN,NOCHESS,B_PAWN,NOCHESS,B_PAWN},
	{NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS},
	               //楚河                       汉界//
	{NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS},
	{R_PAWN,NOCHESS,R_PAWN,NOCHESS,R_PAWN,NOCHESS,R_PAWN,NOCHESS,R_PAWN},
	{NOCHESS,R_CANON,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,R_CANON,NOCHESS},
	{NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS,NOCHESS},
	{R_CAR,R_HORSE,R_ELEPHANT,R_BISHOP,R_KING,R_BISHOP,R_ELEPHANT,R_HORSE,R_CAR}
};

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChessClientDlg dialog

CChessClientDlg::CChessClientDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChessClientDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CChessClientDlg)
	m_strMsgSend = _T("");
	m_strMsgAccept = _T("");
	m_group1 = _T("");
	m_group2 = _T("");
	m_player1 = _T("");
	m_player2 = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32

}

void CChessClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChessClientDlg)
	DDX_Control(pDX, IDC_LISTCHESSRECORD, m_lstChessRecord);
	DDX_Control(pDX, IDC_OUTPUTINFO, m_staticOutputInfo);
	DDX_Text(pDX, IDC_EDIT_MSGSEND, m_strMsgSend);
	DDX_Text(pDX, IDC_EDIT_MSGACCEPT, m_strMsgAccept);
	DDX_Text(pDX, IDC_EDIT_GROUP1, m_group1);
	DDX_Text(pDX, IDC_EDIT_GROUP2, m_group2);
	DDX_Text(pDX, IDC_EDIT_PLAYER1, m_player1);
	DDX_Text(pDX, IDC_EDIT_PLAYER2, m_player2);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CChessClientDlg, CDialog)
	//{{AFX_MSG_MAP(CChessClientDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_COMMAND(IDM_SET, OnSet)
	ON_COMMAND(IDC_BTN_CONNECT, OnBtnconnect)
	ON_COMMAND(IDC_BTN_DISCONNECT, OnBtnDisconnect)
	ON_BN_CLICKED(IDC_BTN_SEND, OnBtnSend)
	ON_WM_RBUTTONUP()
	ON_BN_CLICKED(IDC_BTNUNDO, OnBtnundo)
	ON_BN_CLICKED(IDC_BTN_PEACE, OnBtnPeace)
	ON_BN_CLICKED(IDC_BTN_GIVEUP, OnBtnGiveup)
	ON_BN_CLICKED(IDC_BTNREDO, OnBtnredo)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_NEWMSG,OnReceiveNewMsg)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChessClientDlg message handlers

BOOL CChessClientDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

		//加入菜单

	m_menu.LoadMenu(IDR_MAIN_MENU);
	SetMenu(&m_menu);
	m_menu.EnableMenuItem(IDC_BTN_DISCONNECT,TRUE);

//	GetDlgItem(IDC_BTN_CONNECT)->DestroyWindow();
//	GetDlgItem(IDC_BTN_DISCONNECT)->DestroyWindow();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);// Set small icon
	

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_nChessColor=BLACKCHESS;
	m_nBout=0;
	m_nPort=8000;
	m_strIPAddr="127.0.0.1";
	
	m_ClientSock=new CClientSock();
	m_bIsConnect=false;
	m_nWhoChess=REDCHESS;
	m_bIsAccept=false;
	// TODO: Add extra initialization here
	m_Chessman.Create(IDB_CHESSMAN,36,14,RGB(0,255,0));//创建含有棋子图形的ImgList，用于绘制棋子

	//下面这段代码取棋盘图形的宽，高
	BITMAP BitMap;
	m_BoardBmp.LoadBitmap(IDB_CHESSBOARD);
	m_BoardBmp.GetBitmap(&BitMap); //取BitMap 对象
	m_nBoardWidth=BitMap.bmWidth;  //棋盘宽度
	m_nBoardHeight=BitMap.bmHeight;//棋盘高度

	m_BoardBmp.DeleteObject();
	memcpy(m_ChessBoard,InitChessBoard,90);//初始化棋盘
	m_MoveChess.nChessID=NOCHESS;//将移动的棋子清空
	InvertChessBoard(m_ChessBoard);

	m_group1="黑方";
	m_group2="红方";
	m_player1="Player1";

	UpdateData(FALSE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CChessClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CChessClientDlg::OnPaint() 
{
	CPaintDC dc(this);
	CDC MemDC;
	int i,j;
	POINT pt;
	CBitmap* pOldBmp;

	MemDC.CreateCompatibleDC(&dc);
	m_BoardBmp.LoadBitmap(IDB_CHESSBOARD);
	pOldBmp=MemDC.SelectObject(&m_BoardBmp);
	
	//绘制棋盘上的棋子
	for(i=0;i<10;i++)
		for(j=0;j<9;j++)
		{			
			if(m_ChessBoard[i][j]==NOCHESS)
				continue;

			pt.x=j*GRILLEHEIGHT+14;
			pt.y=i*GRILLEWIDTH+15;
			m_Chessman.Draw(&MemDC,m_ChessBoard[i][j]-1,pt,ILD_TRANSPARENT);
		}

	//绘制用户正在拖动的棋子
	if(m_MoveChess.nChessID!=NOCHESS)
		m_Chessman.Draw(&MemDC,m_MoveChess.nChessID-1,m_MoveChess.ptMovePoint,ILD_TRANSPARENT);
	dc.BitBlt(0,0,m_nBoardWidth,m_nBoardHeight,&MemDC,0,0,SRCCOPY);

	CBrush brush;
	brush.CreateSolidBrush(RGB(212,208,200));
	dc.SelectObject(&brush);
	if(m_nWhoChess==1)
	{
			dc.Rectangle(450,100,488,137);
			pt.x=450;
			pt.y=295;
			m_Chessman.Draw(&dc,m_ChessBoard[9][4]-1,pt,ILD_TRANSPARENT);			
	}
	if(m_nWhoChess==2)
	{
			dc.Rectangle(450,295,488,332);
			pt.x=450;
			pt.y=100;		
			m_Chessman.Draw(&dc,m_ChessBoard[0][4]-1,pt,ILD_TRANSPARENT);		
	}
		
	//将绘制的内容刷新到屏幕
	MemDC.SelectObject(&pOldBmp);//恢复内存Dc的原位图	
	MemDC.DeleteDC();            //释放内存	 
	m_BoardBmp.DeleteObject();   //删除棋盘位图对象
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CChessClientDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CChessClientDlg::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if(!m_bIsConnect || m_nWhoChess!=m_nChessColor)
		return;

	if(m_MoveChess.nChessID)
	{
		//防止将棋子拖出棋盘
		if(point.x<15)//左边
			point.x=15;
		if(point.y<15)//上边
			point.y=15;
		if(point.x>m_nBoardWidth-15)//右边				
			point.x=m_nBoardWidth-15;
		if(point.y>m_nBoardHeight-15)//下边				
			point.y=m_nBoardHeight-15;
		
		//让棋子中心位于鼠标所在处
		point.x-=18;
		point.y-=18;
		
		m_MoveChess.ptMovePoint=point;//保存移动棋子的坐标	
		
		InvalidateRect(NULL,FALSE);//刷新窗口
		UpdateWindow();//立即执行刷新
	}

	CDialog::OnMouseMove(nFlags, point);
}

void CChessClientDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if(!m_bIsConnect)
	{
		m_staticOutputInfo.SetWindowText("老兄，你一个人下啊，这可不行");
		return;
	}

	if(m_nWhoChess!=m_nChessColor)
	{
		m_staticOutputInfo.SetWindowText("老兄，人家还没下呢，你急什么啊");
		return;
	}

	int x,y;
	
	//将坐标换算成棋盘上的格子
	y=(point.y-14)/GRILLEHEIGHT;
	x=(point.x-15)/GRILLEWIDTH;
	
	memcpy(m_BackupChessBoard,m_ChessBoard,90);//备份当前棋盘
	
	//判断鼠标是否在棋盘内，并且点中了用户棋子
	if(point.x>0 && point.y>0 && point.x<m_nBoardWidth && point.y<m_nBoardHeight && (m_nChessColor==REDCHESS?IsRed(m_ChessBoard[y][x]):IsBlack(m_ChessBoard[y][x])))
	{
		memcpy(m_BackupChessBoard,m_ChessBoard,90);//备份棋盘
		
		//将当前棋子的信息装入，记录移动棋子的结构中
		m_ptMoveChess.x=x;
		m_ptMoveChess.y=y;
		m_MoveChess.nChessID=m_ChessBoard[y][x];
		
		//将该棋子原位置棋子去掉
		m_ChessBoard[y][x]=NOCHESS;
		
		//让棋子中点坐标位于鼠标所在点
		point.x-=18;
		point.y-=18;
		m_MoveChess.ptMovePoint=point;
		//重绘屏幕
		InvalidateRect(NULL,FALSE);
		UpdateWindow();
		//SetCapture();//独占鼠标焦点
	}
	else
		if(point.x>0 && point.y>0 && point.x<m_nBoardWidth && point.y<m_nBoardHeight && (m_nChessColor!=REDCHESS?IsRed(m_ChessBoard[y][x]):IsBlack(m_ChessBoard[y][x])))
			m_staticOutputInfo.SetWindowText("不好意思，这是我的棋子，请你不要乱动");
		else
			m_staticOutputInfo.SetWindowText("老兄，那又没有棋子，你瞎点什么啊");
	
	CDialog::OnLButtonDown(nFlags, point);
}

void CChessClientDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if(!m_bIsConnect || m_nWhoChess!=m_nChessColor)
		return;

	int x,y;

    //将坐标换算成棋盘上的格子 
	y=(point.y-14)/GRILLEHEIGHT;
	x=(point.x-15)/GRILLEWIDTH;
	
	//判断是否有移动棋子，并且该棋子的移动是一个合法走法
//	if(m_MoveChess.nChessID && CMoveGenerator::IsValidMove(m_BackupChessBoard,m_ptMoveChess.x,m_ptMoveChess.y,x,y,m_nChessColor))
	if(m_MoveChess.nChessID && m_pMG->IsValidMove(m_BackupChessBoard,m_ptMoveChess.x,m_ptMoveChess.y,x,y,m_nChessColor))
	{
		//---------将客户端走法压栈---------
		m_cmMove.From.x=m_ptMoveChess.x;
		m_cmMove.From.y=m_ptMoveChess.y;
		m_cmMove.To.x=x;
		m_cmMove.To.y=y;
		m_cmMove.nChessID=m_MoveChess.nChessID;
		m_umUndoMove.cmChessMove=m_cmMove;
		m_umUndoMove.nChessID=m_ChessBoard[y][x];
		m_stackUndoMove.push(m_umUndoMove);
		//----------------------------------
		
		if(m_nChessColor==REDCHESS)
			m_nBout++;
		this->AddChessRecord(m_ptMoveChess.x+1,m_ptMoveChess.y+1,x+1,y+1,m_nChessColor,m_MoveChess.nChessID);
		m_ChessBoard[y][x]=m_MoveChess.nChessID;
		m_nWhoChess=m_nWhoChess%2+1;

		//发送走法
		CString str;
		str.Format("%d%d%d%d",m_cmMove.From.x,m_cmMove.From.y,m_cmMove.To.x,m_cmMove.To.y);
		if(m_ClientSock->Send(str,str.GetLength())<0)
		{
			memcpy(m_ChessBoard,m_BackupChessBoard,90);
			m_staticOutputInfo.SetWindowText("数据发送失败,请重新走一步");
			m_nWhoChess=m_nWhoChess%2+1;
		}
		else
			GetDlgItem(IDC_BTNUNDO)->EnableWindow(true);
	}
	else//否则恢复移动前的棋盘状态
		memcpy(m_ChessBoard,m_BackupChessBoard,90);
    
	m_MoveChess.nChessID=NOCHESS;//将移动的棋子清空

	//重绘屏幕  
	InvalidateRect(NULL,FALSE);
	UpdateWindow();
		
	switch(IsGameOver(m_ChessBoard))
	{
	case -1:
		m_staticOutputInfo.SetWindowText("老兄,你好强哦");
		break;

	case 1:
		m_staticOutputInfo.SetWindowText("老兄,看来你不是我的对手啊");
		break;

	default:
		return;
	}
	
	CDialog::OnLButtonUp(nFlags, point);
}

int CChessClientDlg::IsGameOver(BYTE position[][9])
{
	int i,j;
	BOOL RedLive=FALSE,BlackLive=FALSE;

	//检查红方九宫是否有帅
	for(i=7;i<10;i++)
		for(j=3;j<6;j++)
		{
			if(position[i][j]==B_KING)
				BlackLive=TRUE;
			if(position[i][j]==R_KING)
				RedLive=TRUE;
		}

	//检查黑方九宫是否有将
	for(i=0;i<3;i++)
		for(j=3;j<6;j++)
		{
			if(position[i][j]==B_KING)
				BlackLive=TRUE;
			if(position[i][j]==R_KING)
				RedLive=TRUE;
		}

	if(m_nChessColor==REDCHESS)
	{
		if(!RedLive)
			return 1;
		
		if(!BlackLive)
			return -1;
	}
	else
	{
		if(!RedLive)
			return -1;
		
		if(!BlackLive)
			return 1;
	}

	return 0;
}

void CChessClientDlg::AddChessRecord(int nFromX,int nFromY,int nToX,int nToY,int nChessColor,int nSourceID)
{
	CString strChess;
	CString str;

	if(nChessColor==REDCHESS)//用户执红棋
		str=this->GetMoveStr(nFromX,nFromY,nToX,nToY,nSourceID);
	else
	{
		InvertChessBoard(m_ChessBoard);
		str=this->GetMoveStr(10-nFromX,11-nFromY,10-nToX,11-nToY,nSourceID);
		InvertChessBoard(m_ChessBoard);
	}
	str.Format("%d--"+str,m_nBout);

	m_lstChessRecord.AddString(str);
	m_lstChessRecord.PostMessage(WM_VSCROLL,SB_BOTTOM,0);
}

CString CChessClientDlg::ConvertDigit2Chinese(int nNum)
{
	switch(nNum)
	{
	case 1:
		return "一";
		
	case 2:
		return "二";
		
	case 3:
		return "三";
		
	case 4:
		return "四";
		
	case 5:
		return "五";
		
	case 6:
		return "六";
		
	case 7:
		return "七";
		
	case 8:
		return "八";
		
	case 9:
		return "九";

	default:
		return "";
	}
}

void CChessClientDlg::InvertChessBoard(BYTE cb[][9])
{
	int i,j;
	BYTE btTemp;

	for(i=0;i<5;i++)
		for(j=0;j<9;j++)
		{
			btTemp=cb[i][j];
			cb[i][j]=cb[9-i][8-j];
			cb[9-i][8-j]=btTemp;
		}

	//刷新屏幕
	InvalidateRect(NULL,FALSE);
	UpdateWindow();
}

void CChessClientDlg::OnSet() 
{
	// TODO: Add your command handler code here
	m_SetDlg.m_strIPAddr=m_strIPAddr;
	if(m_SetDlg.DoModal()==IDOK)
	{
		m_nPort=m_SetDlg.GetPort();
		//m_strIPAddr=_T(m_SetDlg.GetIPAddr());
		m_strIPAddr.Replace(m_strIPAddr,m_SetDlg.GetIPAddr());
		m_player1=m_SetDlg.m_player;
		UpdateData(FALSE);
	}
}

void CChessClientDlg::OnBtnconnect() 
{
	// TODO: Add your control notification handler code here
	m_ClientSock->Create();
	m_staticOutputInfo.SetWindowText("正与主机"+m_strIPAddr+"建立连接，请稍等...");
	if(!m_ClientSock->Connect((LPCTSTR)m_strIPAddr,m_nPort))
	{
		m_ClientSock->Close();
		m_staticOutputInfo.SetWindowText("未与任何主机建立连接");
	}
	else
	{
		m_bIsConnect=true;
		m_staticOutputInfo.SetWindowText("已与主机"+m_strIPAddr+"建立连接");

		if(m_ClientSock->Send("UserName"+m_player1,m_player1.GetLength()+8)<0)
			MessageBox("发送命令时出现错误！你不妨重新试试。","错误提示",MB_ICONSTOP);
		
		m_menu.EnableMenuItem(IDC_BTN_CONNECT,TRUE);
		m_menu.EnableMenuItem(IDC_BTN_DISCONNECT,FALSE);
		m_menu.EnableMenuItem(IDM_SET,TRUE);

		GetDlgItem(IDC_EDIT_MSGSEND)->EnableWindow(true);
		GetDlgItem(IDC_BTN_SEND)->EnableWindow(true);
		GetDlgItem(IDC_BTN_GIVEUP)->EnableWindow(true);
		GetDlgItem(IDC_BTN_PEACE)->EnableWindow(true);
		//GetDlgItem(IDC_BTNUNDO)->EnableWindow(true);
	}
}

void CChessClientDlg::OnBtnDisconnect() 
{
	// TODO: Add your control notification handler code here
	m_ClientSock->Send("QUIT",4);

	m_ClientSock->Close();
	m_staticOutputInfo.SetWindowText("已与主机"+m_strIPAddr+"断开连接");

	m_menu.EnableMenuItem(IDC_BTN_CONNECT,FALSE);
	m_menu.EnableMenuItem(IDC_BTN_DISCONNECT,TRUE);
	m_menu.EnableMenuItem(IDM_SET,FALSE);

	GetDlgItem(IDC_EDIT_MSGSEND)->EnableWindow(false);
	GetDlgItem(IDC_BTN_SEND)->EnableWindow(false);
	GetDlgItem(IDC_BTN_GIVEUP)->EnableWindow(false);
	GetDlgItem(IDC_BTN_PEACE)->EnableWindow(false);
	GetDlgItem(IDC_BTNUNDO)->EnableWindow(false);
}

void CChessClientDlg::OnBtnSend() 
{
	// TODO: Add your control notification handler code here
	UpdateData(true);
	int nLen=m_ClientSock->Send("MSG"+m_strMsgSend,3+m_strMsgSend.GetLength());
	if(nLen<=0)
		MessageBox("发送时出现错误！你不妨重新试试。","错误提示",MB_ICONINFORMATION);
	else
	{
		m_bIsAccept?m_strMsgAccept+="\r\n\r\n客户端说：\r\n"+m_strMsgSend:
	m_strMsgAccept+="客户端说：\r\n"+m_strMsgSend,m_bIsAccept=true;

		m_strMsgSend="";
		UpdateData(false);
	}

	GetDlgItem(IDC_EDIT_MSGACCEPT)->SendMessage(WM_VSCROLL,SB_BOTTOM,0);
}

void CChessClientDlg::OnReceiveNewMsg(WPARAM wParam,LPARAM lParam)
{
	CString str;
	char* buf;
	CHESSMOVE cm;

	buf=(char*)wParam;
	str.Format("%s",buf);

//	if(AnalyzeCmd(buf))
//		return;

	if(this->AnalyzeStr(buf,cm))
	{
		//---------将客户端走法压栈---------
		m_cmMove.From.x=cm.From.x;
		m_cmMove.From.y=cm.From.y;
		m_cmMove.To.x=cm.To.x;
		m_cmMove.To.y=cm.To.y;
//		m_cmMove.nChessID=cm.nChessID;
//		m_umUndoMove.cmChessMove=cm;
//		m_umUndoMove.nChessID=m_ChessBoard[y][x];
//		m_stackUndoMove.push(m_umUndoMove);
		//----------------------------------

		if(m_nChessColor==BLACKCHESS)
			m_nBout++;
		AddChessRecord(9-cm.From.x,10-cm.From.y,9-cm.To.x,10-cm.To.y,m_nChessColor,m_ChessBoard[9-cm.From.y][8-cm.From.x]);
		
		InvertChessBoard(m_ChessBoard);
		m_umUndoMove.nChessID=this->MakeMove(m_ChessBoard,&cm);
		InvertChessBoard(m_ChessBoard);

		m_nWhoChess=m_nWhoChess%2+1;

		GetDlgItem(IDC_BTNUNDO)->EnableWindow(false);

		InvalidateRect(NULL,FALSE);
		UpdateWindow();
		::FlashWindow(this->GetSafeHwnd(),TRUE);

		return;
	}

	if(str.Find("UserName")==0)
	{
		m_player2=str.Mid(8,str.GetLength()-1);
		UpdateData(false);
		return;
	}

	if(strcmp(str,"GiveUp")==0)
	{
		MessageBox("兄弟，俺认输了，你确实比我强那么一点点！","认输");
		Restart();
		return;
	}

	if(strcmp(str,"PeaceChess")==0)
	{
		if(MessageBox("兄弟，这盘棋真痛快，咱们和棋吧","和棋",MB_YESNO|MB_ICONQUESTION)==IDYES)
		{
			m_ClientSock->Send("PeaceOK",7);
			Restart();
		}
		else
			m_ClientSock->Send("PeaceNO",7);
			
		return;
	}

	if(strcmp(str,"PeaceOK")==0)
	{
		MessageBox("同意和棋");
		Restart();
		return;
	}

	if(strcmp(str,"PeaceNO")==0)
	{
		MessageBox("对方不同意和棋","提示",MB_ICONINFORMATION);
		
		return;
	}

	if(strcmp(str,"UndoChess")==0)
	{
		if(MessageBox("兄弟，我看错了，认我悔一步吧！","悔棋",MB_YESNO|MB_ICONQUESTION)==IDYES)
		{
			m_ClientSock->Send("UNOK",4);

	//		m_ptMoveChess.x=cm.From.x;
	//		m_ptMoveChess.y=cm.From.y;

			m_ChessBoard[9-m_cmMove.From.y][8-m_cmMove.From.x]=m_ChessBoard[9-m_cmMove.To.y][8-m_cmMove.To.x];
			m_ChessBoard[9-m_cmMove.To.y][8-m_cmMove.To.x]=m_umUndoMove.nChessID;
			m_nBout--;
			m_nWhoChess=m_nWhoChess%2+1;

			

			InvalidateRect(NULL,FALSE);
			UpdateWindow();
		}
		else
			m_ClientSock->Send("UndoNo",6);
		return;
	}

	if(strcmp(str,"UNOK")==0)
	{
		MessageBox("对方同意悔棋");

		m_umUndoMove=m_stackUndoMove.top();
		m_stackUndoMove.pop();
		m_cmMove=m_umUndoMove.cmChessMove;
//		m_ptMoveChess.x=m_cmMove.From.x;
//		m_ptMoveChess.y=m_cmMove.From.y;

		m_ChessBoard[m_cmMove.To.y][m_cmMove.To.x]=m_umUndoMove.nChessID;
		m_ChessBoard[m_cmMove.From.y][m_cmMove.From.x]=m_cmMove.nChessID;


		m_nBout--;
		m_nWhoChess=m_nWhoChess%2+1;

		GetDlgItem(IDC_BTNUNDO)->EnableWindow(false);

		InvalidateRect(NULL,FALSE);
		UpdateWindow();
		return;
	}

	if(strcmp(str,"UndoNo")==0)
	{
		MessageBox("兄弟，落子无悔才是真英雄啊！");
		return;
	}

	if(str.Find("MSG")==0)
	{
		if(m_bIsAccept)
			m_strMsgAccept+="\r\n\r\n服务端说：\r\n"+str.Mid(3,str.GetLength()-1);
		else
		{
			m_bIsAccept=true;
			m_strMsgAccept+="服务端说：\r\n"+str.Mid(3,str.GetLength()-1);
		}
	}	

	if(strcmp(str,"QUIT")==0)
	{
		MessageBox("对方已经退出游戏");
		Restart();
		return;
	}

	UpdateData(false);

	GetDlgItem(IDC_EDIT_MSGACCEPT)->SendMessage(WM_VSCROLL,SB_BOTTOM,0);
}

bool CChessClientDlg::AnalyzeStr(char* p,CHESSMOVE& cm)
{
	//长度不为4,不是象棋走法
	if(strlen(p)!=4)
		return false;

	//中间含有非数字,不是象棋走法
	for(int i=0;i<4;i++)
		if(p[i]>'9' || p[i]<'0')
			return false;

	int n=atoi(p);

	cm.From.x=n/1000;
	cm.From.y=n/100%10;
	cm.To.x=n/10%100%10;
	cm.To.y=n%1000%100%10;

	return true;
}

void CChessClientDlg::RedoChessMove(BYTE position[][9],CHESSMOVE* move)
{
    position[move->To.y][move->To.x]=position[move->From.y][move->From.x];
	position[move->From.y][move->From.x]=NOCHESS;
}

void CChessClientDlg::UndoChessMove(BYTE position[][9],CHESSMOVE* move, BYTE nChessID)
{
	position[move->From.y][move->From.x]=position[move->To.y][move->To.x];//将目标位置棋子拷回原位  	
	position[move->To.y][move->To.x]=nChessID;							  //恢复目标位置的棋子
}

BYTE CChessClientDlg::MakeMove(BYTE position[][9],CHESSMOVE* move)
{
	BYTE nChessID;

	nChessID=position[move->To.y][move->To.x];   //取目标位置棋子
    position[move->To.y][move->To.x]=position[move->From.y][move->From.x];
											     //把棋子移动到目标位置	
	position[move->From.y][move->From.x]=NOCHESS;//将原位置清空
	
	return nChessID;//返回被吃掉的棋子
}

void CChessClientDlg::UnMakeMove(BYTE position[][9],CHESSMOVE* move, BYTE nChessID)
{
	position[move->From.y][move->From.x]=position[move->To.y][move->To.x];//将目标位置棋子拷回原位  	
	position[move->To.y][move->To.x]=nChessID;								//恢复目标位置的棋子
}

CString CChessClientDlg::GetMoveStr(int nFromX,int nFromY,int nToX,int nToY,int nSourceID)
{
	CString str;
	bool bIsAgain;
	int i;
	int nCount;
	int nPos[5];
	int j=0;

	switch(nSourceID)
	{
	case B_KING://黑将
		if(nFromY==nToY)
		{
			str.Format("黑：将%d平%d",nFromX,nToX);
			break;
		}
		
		if(nFromY>nToY)
			str.Format("黑：将%d退%d",nFromX,nFromY-nToY);
		else
			str.Format("黑：将%d进%d",nFromX,nToY-nFromY);
		
		break;
		
	case B_CAR://黑车
		bIsAgain=false;
		for(i=0;i<10;i++)
			if(m_ChessBoard[i][nFromX-1]==B_CAR && i!=nFromY-1 && i!=nToY-1)
			{
				bIsAgain=true;
				break;
			}
			
			if(nFromY>nToY)
			{
				if(bIsAgain)
				{
					if(i>nFromY-1)
						str.Format("黑：后车进%d",nFromY-nToY);
					else
						str.Format("黑：前车进%d",nFromY-nToY);
				}
				else
					str.Format("黑：车%d退%d",nFromX,nFromY-nToY);
			}
			else
				if(nFromY<nToY)
				{						
					if(bIsAgain)
					{
						if(i>nFromY-1)
							str.Format("黑：后车进%d",nToY-nFromY);
						else
							str.Format("黑：前车进%d",nToY-nFromY);
					}
					else
						str.Format("黑：车%d进%d",nFromX,nToY-nFromY);
				}
				else
				{
					if(bIsAgain)
					{
						if(i>nFromY-1)
							str.Format("黑：后车平%d",nToX);
						else
							str.Format("黑：前车平%d",nToX);
					}
					else
						str.Format("黑：车%d平%d",nFromX,nToX);
					
					break;
				}
				
				break;
				
	case B_HORSE://黑马
		bIsAgain=false;
		for(i=0;i<10;i++)
			if(m_ChessBoard[i][nFromX-1]==B_HORSE && i!=nFromY-1 && i!=nToY-1)
			{
				bIsAgain=true;
				break;
			}
			
			if(bIsAgain)
			{
				if(i>nFromY-1)
				{
					if(nFromY>nToY)
						str.Format("黑：后马退%d",nToX);
					else
						str.Format("黑：后马进%d",nToX);
				}
				else
				{
					if(nFromY>nToY)
						str.Format("黑：前马退%d",nToX);
					else
						str.Format("黑：前马进%d",nToX);
				}
			}
			else
			{
				if(nFromY>nToY)
					str.Format("黑：马%d退%d",nFromX,nToX);
				else
					str.Format("黑：马%d进%d",nFromX,nToX);
			}
			
			break;
			
	case B_CANON://黑炮	
		bIsAgain=false;
		for(i=0;i<10;i++)
			if(m_ChessBoard[i][nFromX-1]==B_CANON && i!=nFromY-1 && i!=nToY-1)
			{
				bIsAgain=true;
				break;
			}
			
			if(nFromY>nToY)
			{
				if(bIsAgain)
				{
					if(i>nFromY-1)
						str.Format("黑：后炮进%d",nFromY-nToY);
					else
						str.Format("黑：前炮进%d",nFromY-nToY);
				}
				else
					str.Format("黑：炮%d退%d",nFromX,nFromY-nToY);
			}
			else
				if(nFromY<nToY)
				{
					bIsAgain=false;
					for(i=0;i<10;i++)
						if(m_ChessBoard[i][nFromX-1]==B_CANON && i!=nFromY-1 && i!=nToY-1)
						{
							bIsAgain=true;
							break;
						}
						
						if(bIsAgain)
						{
							if(i>nFromY-1)
								str.Format("黑：后炮进%d",nToY-nFromY);
							else
								str.Format("黑：前炮进%d",nToY-nFromY);
						}
						else
							str.Format("黑：炮%d进%d",nFromX,nToY-nFromY);
				}
				else
				{
					if(bIsAgain)
					{
						if(i>nFromY-1)
							str.Format("黑：后炮平%d",nToX);
						else
							str.Format("黑：前炮平%d",nToX);
					}
					else
						str.Format("黑：炮%d平%d",nFromX,nToX);
					break;
				}
				
				break;
				
	case B_BISHOP://黑士
		if(nFromY>nToY)
			str.Format("黑：士%d退%d",nFromX,nToX);
		else
			str.Format("黑：士%d进%d",nFromX,nToX);
		
		break;
		
	case B_ELEPHANT://黑象
		bIsAgain=false;
		for(i=0;i<5;i++)
			if(m_ChessBoard[i][nFromX-1]==B_ELEPHANT && i!=nFromY-1 && i!=nToY-1)
			{
				bIsAgain=true;
				break;
			}
			
			if(bIsAgain)
			{
				if(i>nFromY-1)
				{
					if(nFromY>nToY)
						str.Format("黑：后象退%d",nToX);
					else
						str.Format("黑：后象进%d",nToX);
				}
				else
				{
					if(nFromY>nToY)
						str.Format("黑：前象退%d",nToX);
					else
						str.Format("黑：前象进%d",nToX);
				}
			}
			else
			{
				if(nFromY>nToY)
					str.Format("黑：象%d退%d",nFromX,nToX);
				else
					str.Format("黑：象%d进%d",nFromX,nToX);
			}
			
			break;
			
	case B_PAWN://黑卒
		nCount=0;
		j=0;
		for(i=0;i<5;i++)
			nPos[i]=-1;
		
		for(i=0;i<10;i++)
			if(m_ChessBoard[i][nFromX-1]==B_PAWN && i!=nFromY-1 && i!=nToY-1)
			{
				nPos[j]=i;
				nCount++;
			}
			
			if(nCount==0)
			{
				if(nFromY==nToY)
					str.Format("黑：卒%d平%d",nFromX,nToX);
				else
					str.Format("黑：卒%d进%d",nFromX,1);
				
				break;
			}
			if(nCount==1)
			{
				if(nFromY>nPos[0])
				{
					if(nFromY==nToY)
						str.Format("黑：前卒平%d",nToX);
					else
						str.Format("黑：前卒进%d",1);
				}
				else
				{
					if(nFromY==nToY)
						str.Format("黑：后卒平%d",nToX);
					else
						str.Format("黑：后卒进%d",1);
				}
				
				break;
			}
			j=0;
			if(nCount>1)
			{
				for(i=0;i<5;i++)
					if(nPos[i]==-1)
						break;
					else
						if(nPos[i]>nFromY)
							break;
						else
							j++;
						
						if(nFromY==nToY)
							str.Format("黑：%d卒平%d",j+1,1);
						else
							str.Format("黑：%d卒进%d",j+1,1);
						
						break;
			}
			
	case R_KING://红帅
		if(nFromX==nToX)
		{
			if(nFromY>nToY)
				str="红：帅"+ConvertDigit2Chinese(10-nFromX)+"进"+ConvertDigit2Chinese(nFromY-nToY);
			else
				str="红：帅"+ConvertDigit2Chinese(10-nFromX)+"退"+ConvertDigit2Chinese(nToY-nFromY);
		}
		else
			str="红：帅"+ConvertDigit2Chinese(10-nFromX)+"平"+ConvertDigit2Chinese(10-nToX);
		
		break;
		
	case R_CAR://红车
		bIsAgain=false;
		for(i=0;i<10;i++)
			if(m_ChessBoard[i][nFromX-1]==R_CAR && i!=nFromY-1 && i!=nToY-1)
			{
				bIsAgain=true;
				break;
			}
			
			if(nFromY>nToY)
			{
				if(bIsAgain)
				{
					if(i>nFromY-1)
						str="红：前车进"+ConvertDigit2Chinese(nFromY-nToY);
					else
						str="红：后车进"+ConvertDigit2Chinese(nFromY-nToY);
				}
				else
					str="红：车"+ConvertDigit2Chinese(10-nFromX)+"进"+ConvertDigit2Chinese(nFromY-nToY);
			}
			else
				if(nFromY<nToY)
				{
					if(bIsAgain)
					{
						if(i>nFromY-1)
							str="红：后车退"+ConvertDigit2Chinese(nToY-nFromY);
						else
							str="红：前车退"+ConvertDigit2Chinese(nToY-nFromY);
					}
					else
						str="红：车"+ConvertDigit2Chinese(10-nFromX)+"退"+ConvertDigit2Chinese(nToY-nFromY);
				}
				else
				{
					if(bIsAgain)
					{
						if(i>nFromY-1)
							str="红：后车平"+ConvertDigit2Chinese(nToX);
						else
							str="红：前车平"+ConvertDigit2Chinese(nToX);
					}
					else
						str="红：车"+ConvertDigit2Chinese(10-nFromX)+"平"+ConvertDigit2Chinese(10-nToX);
					
					break;
				}
				
				break;
				
	case R_HORSE://红马
		bIsAgain=false;
		for(i=0;i<10;i++)
			if(m_ChessBoard[i][nFromX-1]==R_HORSE && i!=nFromY-1 && i!=nToY-1)
			{
				bIsAgain=true;
				break;
			}
			
			if(bIsAgain)
			{
				if(i>nFromY-1)
				{
					if(nFromY>nToY)
						str="红：前马进"+ConvertDigit2Chinese(10-nToX);
					else
						str="红：前马退"+ConvertDigit2Chinese(10-nToX);
				}
				else
				{
					if(nFromY>nToY)
						str="红：前马进"+ConvertDigit2Chinese(10-nToX);
					else
						str="红：前马退"+ConvertDigit2Chinese(10-nToX);
				}
			}
			else
			{
				if(nFromY>nToY)
					str="红：马"+ConvertDigit2Chinese(10-nFromX)+"进"+ConvertDigit2Chinese(10-nToX);
				else
					str="红：马"+ConvertDigit2Chinese(10-nFromX)+"退"+ConvertDigit2Chinese(10-nToX);
			}
			
			break;
			
	case R_CANON://红炮
		bIsAgain=false;
		for(i=0;i<10;i++)
			if(m_ChessBoard[i][nFromX-1]==R_CANON && i!=nFromY-1 && i!=nToY-1)
			{
				bIsAgain=true;
				break;
			}				
			
			if(nFromY>nToY)
			{
				if(bIsAgain)
				{
					if(i>nFromY-1)
						str="红：前炮进"+ConvertDigit2Chinese(nFromY-nToY);
					else
						str="红：后炮进"+ConvertDigit2Chinese(nFromY-nToY);
				}
				else
					str="红：炮"+ConvertDigit2Chinese(10-nFromX)+"进"+ConvertDigit2Chinese(nFromY-nToY);
			}
			else
				if(nFromY<nToY)
				{
					if(bIsAgain)
					{
						if(i>nFromY-1)
							str="红：前炮退"+ConvertDigit2Chinese(nToY-nFromY);
						else
							str="红：后炮退"+ConvertDigit2Chinese(nToY-nFromY);
					}
					else
						str="红：炮"+ConvertDigit2Chinese(nFromX)+"退"+ConvertDigit2Chinese(nToY-nFromY);
				}
				else
				{
					if(bIsAgain)
					{
						if(i>nFromY-1)
							str="红：前炮平"+ConvertDigit2Chinese(10-nToX);
						else
							str="红：后炮平"+ConvertDigit2Chinese(10-nToX);
					}
					else
						str="红：炮"+ConvertDigit2Chinese(10-nFromX)+"平"+ConvertDigit2Chinese(10-nToX);
				}
				
				break;
				
	case R_BISHOP://红士
		if(nFromY>nToY)
			str="红：士"+ConvertDigit2Chinese(10-nFromX)+"进"+ConvertDigit2Chinese(10-nToX);
		else
			str="红：士"+ConvertDigit2Chinese(10-nFromX)+"退"+ConvertDigit2Chinese(10-nToX);
		
		break;
		
	case R_ELEPHANT://红相
		bIsAgain=false;
		for(i=0;i<5;i++)
			if(m_ChessBoard[i][nFromX-1]==R_ELEPHANT && i!=nFromY-1 && i!=nToY-1)
			{
				bIsAgain=true;
				break;
			}
			
			if(bIsAgain)
			{
				if(i>nFromY-1)
				{
					if(nFromY>nToY)
						str="红：前相退"+ConvertDigit2Chinese(10-nToX);
					else
						str="红：前相进"+ConvertDigit2Chinese(10-nToX);
				}
				else
				{
					if(nFromY>nToY)
						str="红：后相退"+ConvertDigit2Chinese(10-nToX);
					else
						str="红：后相进"+ConvertDigit2Chinese(10-nToX);
				}
			}
			else
			{
				if(nFromY>nToY)
					str="红：相"+ConvertDigit2Chinese(10-nFromX)+"进"+ConvertDigit2Chinese(10-nToX);
				else
					str="红：相"+ConvertDigit2Chinese(10-nFromX)+"退"+ConvertDigit2Chinese(10-nToX);
			}
			
			break;
			
	case R_PAWN://红兵
		nCount=0;
		j=0;
		for(i=0;i<5;i++)
			nPos[i]=-1;
		
		for(i=0;i<10;i++)
			if(m_ChessBoard[i][nFromX-1]==R_PAWN && i!=nFromY-1 && i!=nToY-1)
			{
				nPos[j]=i;
				nCount++;
			}
			
			if(nCount==0)
			{
				if(nFromY==nToY)
					str="红：兵"+ConvertDigit2Chinese(10-nFromX)+"平"+ConvertDigit2Chinese(10-nToX);
				else
					str="红：兵"+ConvertDigit2Chinese(10-nFromX)+"进"+ConvertDigit2Chinese(1);
				
				break;
			}
			if(nCount==1)
			{
				if(nFromY-1>nPos[0])
				{
					if(nFromY==nToY)
						str="红：前兵平"+ConvertDigit2Chinese(10-nToX);
					else
						str="红：前兵进"+ConvertDigit2Chinese(1);
				}
				else
				{
					if(nFromY==nToY)
						str="红：后兵平"+ConvertDigit2Chinese(10-nToX);
					else
						str="红：后兵进"+ConvertDigit2Chinese(1);
				}
				
				break;
			}
			j=0;
			if(nCount>1)
			{
				for(i=0;i<5;i++)
					if(nPos[i]==-1)
						break;
					else
						if(nPos[i]>nFromY-1)
							break;
						else
							j++;
						if(nFromY==nToY)
							str="红："+ConvertDigit2Chinese(j+1)+"兵平"+ConvertDigit2Chinese(nToX);
						else
							str="红："+ConvertDigit2Chinese(j+1)+"兵进"+ConvertDigit2Chinese(1);	
			}
			
			break;
			
	default:
		break;
	}

	return str;
}

void CChessClientDlg::OnRButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
}

void CChessClientDlg::OnBtnundo() 
{
	// TODO: Add your control notification handler code here
	if(m_ClientSock->Send("UndoChess",9)<0)
		MessageBox("发送命令时出现错误！你不妨重新试试。","错误提示",MB_ICONSTOP);
}

void CChessClientDlg::OnBtnredo() 
{
	// TODO: Add your control notification handler code here
	if(m_ClientSock->Send("RedoChess",4)<0)
		MessageBox("发送命令时出现错误！你不妨重新试试。","错误提示",MB_ICONSTOP);
}

void CChessClientDlg::OnBtnPeace() 
{
	// TODO: Add your control notification handler code here
	if(m_ClientSock->Send("PeaceChess",10)<0)
		MessageBox("发送命令时出现错误！你不妨重新试试。","错误提示",MB_ICONSTOP);
}

void CChessClientDlg::OnBtnGiveup() 
{
	// TODO: Add your control notification handler code here
	if(m_ClientSock->Send("GiveUp",6)<0)
		MessageBox("发送命令时出现错误！你不妨重新试试。","错误提示",MB_ICONSTOP);
	Restart();
}

bool CChessClientDlg::AnalyzeCmd(char* p)
{
	//---------和棋---------
	if(p=="PeaceChess")
	{
		if(MessageBox("兄弟,咱们讲和吧","求和",MB_YESNO | MB_DEFBUTTON2)==IDYES)
		{
			while(m_ClientSock->Send("PeaceChess-y",12)<0);
			InitVar();
		}
		else
			while(m_ClientSock->Send("PeaceChess-n",12)<0);

		return 1;
	}
	//----------------------


	//---------认输---------
	if(p=="GiveUp")
	{
		MessageBox("兄弟,你强,俺投降","认输",MB_ICONINFORMATION);
		return 1;
	}
	//----------------------


	//---------悔棋---------
	if(p=="UndoChess")
	{
		if(MessageBox("兄弟,实在不好意思,俺刚才不小心走错了一着,你大人有大量,就让我悔一步吧","悔棋",MB_YESNO | MB_DEFBUTTON2)==IDYES)
		{
			while(m_ClientSock->Send("UndoChess-y",11)<0);
			InitVar();
		}
		else
			while(m_ClientSock->Send("UndoChess-n",11)<0);

		return 1;
	}
	//----------------------


	//---------还原---------
	if(p=="RedoChess")
	{
		if(MessageBox("兄弟,实在不好意思,俺不想悔了,还是还原吧","悔棋",MB_YESNO | MB_DEFBUTTON2)==IDYES)
		{
			while(m_ClientSock->Send("RedoChess-y",11)<0);
			InitVar();
		}
		else
			while(m_ClientSock->Send("RedoChess-n",11)<0);

		return 1;
	}
	//----------------------


	//---------和棋成---------
	if(p=="PeaceChess-y")
	{
		MessageBox("兄弟,以后咱们都一家人了","已和",MB_ICONINFORMATION);
		InitVar();
		
		return 1;
	}	
	//------------------------


	//---------悔棋成---------
	if(p=="UndoChess-y")
	{
		return 1;
	}
	//------------------------


	//---------还原成---------
	if(p=="RedoChess-y")
	{
		return 1;
	}
	//------------------------


	//---------和棋不成---------
	if(p=="PeaceChess-n")
	{
		MessageBox("兄弟,你还不够资格和我讲和吧","求和",MB_ICONINFORMATION);
		return 1;
	}
	//--------------------------


	//---------悔棋不成---------
	if(p=="UndoChess-n")
	{
		MessageBox("兄弟,你想悔就悔啊,那我岂不是太没有面子啦","悔棋不成",MB_ICONINFORMATION);
		return 1;
	}
	//--------------------------


	//---------还原不成---------
	if(p=="RedoChess")
	{
		MessageBox("兄弟,你想还原就还原啊,那我岂不是太没有面子啦","还原不成",MB_ICONINFORMATION);
		return 1;
	}
	//--------------------------

	return 0;
}

void CChessClientDlg::InitVar()
{
}

void CChessClientDlg::Restart()
{
//	m_Chessman.Create(IDB_CHESSMAN,36,14,RGB(0,255,0));//创建含有棋子图形的ImgList，用于绘制棋子
	
	m_nChessColor=BLACKCHESS;
	m_nBout=0;
	

	m_bIsConnect=false;
	m_nWhoChess=REDCHESS;
	m_bIsAccept=false;


	//下面这段代码取棋盘图形的宽，高
	BITMAP BitMap;
	m_BoardBmp.LoadBitmap(IDB_CHESSBOARD);
	m_BoardBmp.GetBitmap(&BitMap); //取BitMap 对象
	m_nBoardWidth=BitMap.bmWidth;  //棋盘宽度
	m_nBoardHeight=BitMap.bmHeight;//棋盘高度

	m_BoardBmp.DeleteObject();
	memcpy(m_ChessBoard,InitChessBoard,90);//初始化棋盘
	m_MoveChess.nChessID=NOCHESS;//将移动的棋子清空
	InvertChessBoard(m_ChessBoard);

	m_group1="黑方";
	m_group2="红方";

	UpdateData(FALSE);

	m_ClientSock->Close();
	m_staticOutputInfo.SetWindowText("已与主机"+m_strIPAddr+"断开连接");

	m_menu.EnableMenuItem(IDC_BTN_CONNECT,FALSE);
	m_menu.EnableMenuItem(IDC_BTN_DISCONNECT,TRUE);
	m_menu.EnableMenuItem(IDM_SET,FALSE);

	GetDlgItem(IDC_EDIT_MSGSEND)->EnableWindow(false);
	GetDlgItem(IDC_BTN_SEND)->EnableWindow(false);
	GetDlgItem(IDC_BTN_GIVEUP)->EnableWindow(false);
	GetDlgItem(IDC_BTN_PEACE)->EnableWindow(false);
	GetDlgItem(IDC_BTNUNDO)->EnableWindow(false);
	
	//重绘屏幕  
	InvalidateRect(NULL,FALSE);
	UpdateWindow();
}

BOOL CChessClientDlg::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	m_ClientSock->Send("QUIT",4);
	return CDialog::DestroyWindow();
}
