//Download by http://www.NewXing.com
// ChessClientDlg.h : header file
//

#if !defined(AFX_CHESSCLIENTDLG_H__F2153AC0_23AC_44E7_BAF7_FF232EEBC013__INCLUDED_)
#define AFX_CHESSCLIENTDLG_H__F2153AC0_23AC_44E7_BAF7_FF232EEBC013__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CChessClientDlg dialog
#include "Define.h"
#include "MoveGenerator.h"
#include "SetDlg.h"
#include "ClientSock.h"
#include <stack>

typedef struct _movechess
{
	BYTE nChessID;
	POINT ptMovePoint;
}MOVECHESS;

using namespace std;

class CChessClientDlg : public CDialog
{
// Construction
public:
	void Restart();
	CChessClientDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CChessClientDlg)
	enum { IDD = IDD_CHESSCLIENT_DIALOG };
	CListBox	m_lstChessRecord;
	CStatic	m_staticOutputInfo;
	CString	m_strMsgSend;
	CString	m_strMsgAccept;
	CString	m_group1;
	CString	m_group2;
	CString	m_player1;
	CString	m_player2;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChessClientDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
private:
	BYTE MakeMove(BYTE position[][9],CHESSMOVE* move);				       //根据某一走法产生走了之后的棋盘
	void UnMakeMove(BYTE position[][9],CHESSMOVE* move,BYTE nChessID);     //恢复为走过之前的棋盘	
	void UndoChessMove(BYTE position[10][9],CHESSMOVE* move,BYTE nChessID);//悔棋
	void RedoChessMove(BYTE position[10][9],CHESSMOVE* move);              //还原

	void InitVar();                        //初始化变量
	bool AnalyzeCmd(char* p);			   //解析命令
	bool AnalyzeStr(char* p,CHESSMOVE& cm);//解析走法
	CString ConvertDigit2Chinese(int nNum);//转换数字为汉字
	int IsGameOver(BYTE position[][9]);
	void InvertChessBoard(BYTE cb[][9]);   //对换红黑双方棋子
	CString GetMoveStr(int nFromX,int nFromY,int nToX,int nToY,int nSourceID);
	void AddChessRecord(int nFromX,int nFromY,int nToX,int nToY,int nChessColor,int nSourceID);
										   //记录下棋步骤

protected:
	HICON m_hIcon;
	CSetDlg m_SetDlg;
	CMenu m_menu;
	

	BYTE m_ChessBoard[10][9];	   //棋盘数组，用于显示棋盘
	BYTE m_BackupChessBoard[10][9];//备份棋盘数组，用于出错恢复
	MOVECHESS m_MoveChess;		   //用于保存当前被拖拽的棋子的结构
	POINT m_ptMoveChess;		   //用于保存当前被拖拽的棋子的位置
	CBitmap m_BoardBmp;			   //bitmap图用于显示棋盘
	CImageList m_Chessman;		   //用于绘制棋子的ImageList对象
	int m_nBoardWidth;			   //棋盘宽度
	int m_nBoardHeight;			   //棋盘高度

	CMoveGenerator* m_pMG;		   //走法产生器
	int m_nChessColor;		       //用户棋子颜色
	int m_nWhoChess;               //下步该谁下
	int m_nBout;				   //已下的回合数
	CHESSMOVE m_cmMove;            //走法
	UNDOMOVE m_umUndoMove;         //悔棋走法
	stack<UNDOMOVE> m_stackUndoMove;
								   //记录走法的栈，便于悔棋
	stack<CHESSMOVE> m_stackRedoMove;
								   //记录已悔棋的走法的栈，便于还原

	//-----远程连接所需的变量-----
	int m_nPort;
	CString m_strIPAddr;
	bool m_bIsConnect;//是否已建立连接
	bool m_bIsAccept; //是否已接收消息
	CClientSock* m_ClientSock;
	//----------------------------

	// Generated message map functions
	//{{AFX_MSG(CChessClientDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnSet();
	afx_msg void OnBtnconnect();
	afx_msg void OnBtnDisconnect();
	afx_msg void OnBtnSend();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBtnundo();
	afx_msg void OnBtnPeace();
	afx_msg void OnBtnGiveup();
	afx_msg void OnBtnredo();
	//}}AFX_MSG
	afx_msg void OnReceiveNewMsg(WPARAM, LPARAM);      //自定义消息
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHESSCLIENTDLG_H__F2153AC0_23AC_44E7_BAF7_FF232EEBC013__INCLUDED_)
