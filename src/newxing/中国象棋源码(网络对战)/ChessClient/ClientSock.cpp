//Download by http://www.NewXing.com
// ClientSock.cpp : implementation file
//

#include "stdafx.h"
#include "ClientSock.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClientSock

CClientSock::CClientSock()
{
}

CClientSock::~CClientSock()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CClientSock, CSocket)
	//{{AFX_MSG_MAP(CClientSock)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CClientSock member functions

void CClientSock::OnReceive(int nErrorCode)
{
	CSocket::OnReceive(nErrorCode);

	char lpBuf[65535];
	CString str;

	memset(lpBuf,0,sizeof(lpBuf));
	Receive(lpBuf,65535,0);
	str.Format("%s",lpBuf);
	::AfxGetMainWnd()->SendMessage(WM_NEWMSG,(WPARAM)lpBuf, 0);
}