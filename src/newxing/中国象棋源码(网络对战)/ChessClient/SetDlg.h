//Download by http://www.NewXing.com
#if !defined(AFX_SETDLG_H__888036ED_9C71_44CD_9FC9_A8E8BC293C60__INCLUDED_)
#define AFX_SETDLG_H__888036ED_9C71_44CD_9FC9_A8E8BC293C60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSetDlg dialog

class CSetDlg : public CDialog
{
// Construction
public:
	CSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSetDlg)
	enum { IDD = IDD_SET };
	CString	m_strIPAddr;
	int		m_nPort;
	CString	m_player;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
public:
	int GetPort(){return m_nPort;};
	CString GetIPAddr(){return m_strIPAddr;};

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSetDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETDLG_H__888036ED_9C71_44CD_9FC9_A8E8BC293C60__INCLUDED_)
