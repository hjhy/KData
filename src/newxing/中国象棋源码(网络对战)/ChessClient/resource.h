//Download by http://www.NewXing.com
//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ChessClient.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CHESSCLIENT_DIALOG          102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDB_CHESSBOARD                  129
#define IDB_CHESSMAN                    130
#define IDR_MENUSET                     131
#define IDD_SET                         132
#define IDR_MAIN_MENU                   134
#define IDC_LISTCHESSRECORD             1000
#define IDC_EDIT_IPADDR                 1001
#define IDC_BTN_CONNECT                 1001
#define IDC_EDIT_PORT                   1002
#define IDC_EDIT_MSGSEND                1002
#define IDC_OUTPUTINFO                  1003
#define IDC_EDIT_PLAYER                 1003
#define IDC_EDIT_MSGACCEPT              1004
#define IDC_EDIT_PLAYER1                1005
#define IDC_BTN_SEND                    1006
#define IDC_RADIOREDCHESS               1007
#define IDC_EDIT_GROUP1                 1007
#define IDC_RADIOBLACKCHESS             1008
#define IDC_EDIT_PLAYER2                1008
#define IDC_BTN_DISCONNECT              1009
#define IDC_BTNUNDO                     1010
#define IDC_BTNREDO                     1011
#define IDC_EDIT_GROUP2                 1011
#define IDC_BUTTON4                     1012
#define IDC_BTN_PEACE                   1012
#define IDC_BUTTON5                     1013
#define IDC_BTN_GIVEUP                  1013
#define IDC_BTN_UNDO                    1014
#define IDM_SET                         32771
#define IDM_ABOUT                       32774
#define IDM_HELP                        32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
