; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CChessServerDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "chessserver.h"
LastPage=0

ClassCount=6
Class1=CChessServerApp
Class2=CAboutDlg
Class3=CChessServerDlg
Class4=CServerSock
Class5=CSetDlg
Class6=CSubServerSock

ResourceCount=5
Resource1=IDD_CHESSSERVER_DIALOG
Resource2=IDR_MENUSET
Resource3=IDD_SET
Resource4=IDD_ABOUTBOX
Resource5=IDR_MAIN_MENU

[CLS:CChessServerApp]
Type=0
BaseClass=CWinApp
HeaderFile=ChessServer.h
ImplementationFile=ChessServer.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=ChessServerDlg.cpp
ImplementationFile=ChessServerDlg.cpp

[CLS:CChessServerDlg]
Type=0
BaseClass=CDialog
HeaderFile=ChessServerDlg.h
ImplementationFile=ChessServerDlg.cpp
LastObject=CChessServerDlg
Filter=D
VirtualFilter=dWC

[CLS:CServerSock]
Type=0
BaseClass=CSocket
HeaderFile=ServerSock.h
ImplementationFile=ServerSock.cpp

[CLS:CSetDlg]
Type=0
BaseClass=CDialog
HeaderFile=SetDlg.h
ImplementationFile=SetDlg.cpp
LastObject=CSetDlg
Filter=D
VirtualFilter=dWC

[CLS:CSubServerSock]
Type=0
BaseClass=CSocket
HeaderFile=SubServerSock.h
ImplementationFile=SubServerSock.cpp

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_CHESSSERVER_DIALOG]
Type=1
Class=CChessServerDlg
ControlCount=20
Control1=IDC_EDIT_MSGSEND,edit,1484853380
Control2=IDC_BTN_SEND,button,1476460545
Control3=IDC_EDIT_MSGACCEPT,edit,1352730628
Control4=IDC_BTNUNDO,button,1476460544
Control5=IDC_BTN_PEACE,button,1476460544
Control6=IDC_BTN_GIVEUP,button,1476460544
Control7=IDC_LISTCHESSRECORD,listbox,1352728833
Control8=IDC_STATIC,button,1342177287
Control9=IDC_STATIC,button,1342177287
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_EDIT_PLAYER1,edit,1484849280
Control13=IDC_EDIT_GROUP1,edit,1484849280
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,static,1342308352
Control16=IDC_STATIC,static,1342308352
Control17=IDC_STATIC,static,1342308352
Control18=IDC_EDIT_PLAYER2,edit,1484849280
Control19=IDC_EDIT_GROUP2,edit,1484849280
Control20=IDC_OUTPUTINFO,static,1342312960

[DLG:IDD_SET]
Type=1
Class=CSetDlg
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_EDIT_PORT,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=IDC_EDIT_NAME,edit,1350631552

[MNU:IDR_MENUSET]
Type=1
Class=?
Command1=IDM_SET
CommandCount=1

[MNU:IDR_MAIN_MENU]
Type=1
Class=?
Command1=IDC_BTN_LISTEN
Command2=IDC_BTN_DISCONNECT
Command3=IDC_BTN_GIVEUP
Command4=IDC_BTN_PEACE
Command5=IDC_BTNUNDO
Command6=IDM_SET
Command7=IDM_ABOUT
Command8=IDM_HELP
CommandCount=8

