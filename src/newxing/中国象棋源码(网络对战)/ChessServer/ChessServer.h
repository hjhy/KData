//Download by http://www.NewXing.com
// ChessServer.h : main header file for the CHESSSERVER application
//

#if !defined(AFX_CHESSSERVER_H__9F28DB61_0A36_46AE_856A_B0ABAE1D6C98__INCLUDED_)
#define AFX_CHESSSERVER_H__9F28DB61_0A36_46AE_856A_B0ABAE1D6C98__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CChessServerApp:
// See ChessServer.cpp for the implementation of this class
//

class CChessServerApp : public CWinApp
{
public:
	CChessServerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChessServerApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CChessServerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHESSSERVER_H__9F28DB61_0A36_46AE_856A_B0ABAE1D6C98__INCLUDED_)
