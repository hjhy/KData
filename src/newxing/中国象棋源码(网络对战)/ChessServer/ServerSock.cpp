//Download by http://www.NewXing.com
// ServerSock.cpp : implementation file
//

#include "stdafx.h"
#include "ServerSock.h"
#include "SubServerSock.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CServerSock

CServerSock::CServerSock()
{
	bIsConnect=false;
	m_subsock=new CSubServerSock();
}

CServerSock::~CServerSock()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CServerSock, CSocket)
	//{{AFX_MSG_MAP(CServerSock)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CServerSock member functions

void CServerSock::OnAccept(int nErrorCode)
{
	::AfxGetMainWnd()->SendMessage(WM_CONNECT,0,0);
	m_subsock=new CSubServerSock();
	bIsConnect=true;
	if(this->Accept(*m_subsock)<0)
		MessageBox(NULL,"����ʧ�ܣ�","������ʾ",MB_ICONINFORMATION);
}


int CServerSock::SendMsg(CString strMsg)
{
	return m_subsock->Send(strMsg,strMsg.GetLength());
}
