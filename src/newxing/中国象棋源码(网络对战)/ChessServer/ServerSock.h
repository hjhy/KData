//Download by http://www.NewXing.com
#if !defined(AFX_SERVERSOCK_H__A777480A_E566_404B_806F_243401E3E5C0__INCLUDED_)
#define AFX_SERVERSOCK_H__A777480A_E566_404B_806F_243401E3E5C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ServerSock.h : header file
//

#include "SubServerSock.h"


/////////////////////////////////////////////////////////////////////////////
// CServerSock command target

class CServerSock : public CSocket
{
// Attributes
public:

// Operations
public:
	CServerSock();
	virtual ~CServerSock();

// Overrides
public:
	int SendMsg(CString strMsg);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServerSock)
	virtual void OnAccept(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CServerSock)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
private:
	bool bIsConnect;
	CSubServerSock* m_subsock;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVERSOCK_H__A777480A_E566_404B_806F_243401E3E5C0__INCLUDED_)
