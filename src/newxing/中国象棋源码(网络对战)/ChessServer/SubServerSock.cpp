//Download by http://www.NewXing.com
// SubServerSock.cpp : implementation file
//

#include "stdafx.h"
#include "SubServerSock.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSubServerSock

CSubServerSock::CSubServerSock()
{
}

CSubServerSock::~CSubServerSock()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CSubServerSock, CSocket)
	//{{AFX_MSG_MAP(CSubServerSock)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CSubServerSock member functions

void CSubServerSock::OnReceive(int nErrorCode)
{
	CSocket::OnReceive(nErrorCode);

	char lpBuf[65535];
	CString str;

	memset(lpBuf,0,sizeof(lpBuf));
	Receive(lpBuf,65535,0);
	str.Format("%s",lpBuf);
	::AfxGetMainWnd()->SendMessage(WM_NEWMSG,(WPARAM)lpBuf, 0);
}