//Download by http://www.NewXing.com
#if !defined(AFX_SUBSERVERSOCK_H__516A413B_C9EE_4F8C_A1E3_D4F70D4ECEC6__INCLUDED_)
#define AFX_SUBSERVERSOCK_H__516A413B_C9EE_4F8C_A1E3_D4F70D4ECEC6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SubServerSock.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CSubServerSock command target

class CSubServerSock : public CSocket
{
// Attributes
public:

// Operations
public:
	CSubServerSock();
	virtual ~CSubServerSock();

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSubServerSock)
	virtual void OnReceive(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CSubServerSock)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SUBSERVERSOCK_H__516A413B_C9EE_4F8C_A1E3_D4F70D4ECEC6__INCLUDED_)
