//Download by http://www.NewXing.com
//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ChessServer.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CHESSSERVER_DIALOG          102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDB_CHESSBOARD                  129
#define IDB_CHESSMAN                    130
#define IDR_MENUSET                     131
#define IDD_SET                         132
#define IDR_MAIN_MENU                   132
#define IDB_BITMAP1                     133
#define IDC_LISTCHESSRECORD             1000
#define IDC_EDIT_IPADDR                 1001
#define IDC_BTN_LISTEN                  1001
#define IDC_EDIT_MSGSEND                1002
#define IDC_EDIT_PORT                   1002
#define IDC_OUTPUTINFO                  1003
#define IDC_EDIT_NAME                   1003
#define IDC_EDIT_MSGACCEPT              1004
#define IDC_EDIT_PLAYER1                1005
#define IDC_BTN_SEND                    1006
#define IDC_RADIOREDCHESS               1007
#define IDC_EDIT_GROUP1                 1007
#define IDC_RADIOBLACKCHESS             1008
#define IDC_BTN_DISCONNECT              1009
#define IDC_BTNUNDO                     1010
#define IDC_BTNREDO                     1011
#define IDC_BTN_PEACE                   1012
#define IDC_BTN_GIVEUP                  1013
#define IDC_EDIT_PLAYER2                1015
#define IDC_EDIT_GROUP2                 1016
#define IDC_BUTTON1                     1019
#define IDC_BTN_UNDO                    1020
#define IDM_SET                         32771
#define IDM_ABOUT                       32777
#define IDM_HELP                        32778

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1021
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
