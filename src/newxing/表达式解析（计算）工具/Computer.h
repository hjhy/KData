//Download by http://www.NewXing.com
// Computer.h : main header file for the COMPUTER application
//

#if !defined(AFX_COMPUTER_H__7942E27F_F01E_4881_818C_0D12D85E66ED__INCLUDED_)
#define AFX_COMPUTER_H__7942E27F_F01E_4881_818C_0D12D85E66ED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CComputerApp:
// See Computer.cpp for the implementation of this class
//

class CComputerApp : public CWinApp
{
public:
	CComputerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CComputerApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CComputerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMPUTER_H__7942E27F_F01E_4881_818C_0D12D85E66ED__INCLUDED_)
