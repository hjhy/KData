// ComputerDlg.cpp : implementation file
// Download by http://www.NewXing.com

#include "stdafx.h"
#include "Computer.h"
#include "ComputerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CBitmap m_OK;
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CComputerDlg dialog

CComputerDlg::CComputerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CComputerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CComputerDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_upTimes=0;
}

void CComputerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CComputerDlg)
	DDX_Control(pDX, IDC_GET_RESULT, m_cnComputer);
	DDX_Control(pDX, IDC_RESULT, m_cnResult);
	DDX_Control(pDX, IDC_FORMULA, m_cnFormula);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CComputerDlg, CDialog)
	//{{AFX_MSG_MAP(CComputerDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_GET_VARIANT_TABLE, OnGetVariantTable)
	ON_BN_CLICKED(IDC_GET_RESULT, OnGetResult)
	ON_EN_CHANGE(IDC_FORMULA, OnChangeFormula)
	ON_BN_CLICKED(IDC_BUTTON_UP, OnButtonUp)
	ON_BN_CLICKED(IDC_BUTTON_DOWN, OnButtonDown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CComputerDlg message handlers

BOOL CComputerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	m_OK.LoadBitmap(IDB_OK);
	CButton* bt=(CButton*)GetDlgItem(IDOK);
	HBITMAP hBitmap=(HBITMAP)m_OK.GetSafeHandle();
	bt->SetBitmap(hBitmap);

	m_CANCEL.LoadBitmap(IDB_CANCEL);
	bt=(CButton*)GetDlgItem(IDCANCEL);
	hBitmap=(HBITMAP)m_CANCEL.GetSafeHandle();
	bt->SetBitmap(hBitmap);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	VERIFY(m_up.AutoLoad(IDC_BUTTON_UP,this));
	VERIFY(m_down.AutoLoad(IDC_BUTTON_DOWN,this));
	
	// TODO: Add extra initialization here
	CString str;
	str.LoadString(IDS_INSTRUCT);
	CEdit* edit=(CEdit*)GetDlgItem(IDC_EDIT_INSTRUCT);
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight=18;
	strcmp(lf.lfFaceName,"宋体");
	CFont font;
	font.CreateFontIndirect(&lf);
	edit->SetWindowText(str);

	CStatic *ps=(CStatic*)GetDlgItem(IDC_BUTTON_INSRUCTION);
	ps->SetFont(&font);
	ps=(CStatic*)GetDlgItem(IDC_BUTTON_INSRUCTION2);
	ps->SetFont(&font);	
	m_cnFormula.SetFont(&font);
	m_cnResult.SetFont(&font);

	ps=(CStatic*)GetDlgItem(IDC_FIELD_INSTRUCT);
	ps->SetFont(&font);
	ps->SetWindowText("变量赋值区");
	font.DeleteObject();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CComputerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CComputerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
		/////////////////////////////

		///////////////////////////////
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CComputerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CComputerDlg::OnGetVariantTable() 
{
	CString str;
	m_cnFormula.GetWindowText(str);
	if(str.GetLength()==0)
		return;
	m_formula.SetFormula(str.GetBuffer(str.GetLength()));
	if(m_formula.GetVariantTableSize()==0)
	{
		double value=m_formula.computer(NULL,0);
		CString str;
		if(m_formula.GetErrorNumber())
			str=m_formula.GetErrorInformation();
		else
			str.Format("%g",value);
		m_cnResult.SetWindowText(str);
		return;
	}
	m_staticArray.desroy();
	m_editDataArray.desroy();
	m_staticArray.Create(CPoint(321,0),this,100,20,&m_formula,ES_RIGHT);
	m_staticArray.draw();
	m_editDataArray.Create(CPoint(419,0),this,80,20,m_formula.GetVariantTableSize());
	m_editDataArray.draw();
	CRect rect;
	GetClientRect(rect);
	if(rect.Height()<m_editDataArray.getHeightA())
	{
		CButton*button=(CButton*)GetDlgItem(IDC_BUTTON_UP);
		button->EnableWindow();
	}
	m_cnComputer.EnableWindow();
	CStatic* pStatic=(CStatic*)GetDlgItem(IDC_FIELD_INSTRUCT);
	pStatic->SetWindowText("");
}

void CComputerDlg::OnGetResult() 
{
	// TODO: Add your control notification handler code here
	int size=m_formula.GetVariantTableSize();
	if(size==0)
		return;
	double* data;//=new double[size];
	int index=m_editDataArray.GetWindowData(data);
	if(index>0)
	{
		const COperand* pOd=m_formula.GetVariantTable();
		CString str;
		str.Format("变量 %s 没有赋值，将以0代替",pOd[index-1].m_name);
		AfxMessageBox(str);
	}
	CString str;
	double result=m_formula.computer(data,size);
	if(m_formula.GetErrorNumber())
		str=m_formula.GetErrorInformation();
	else
		str.Format("%g",result);
	m_cnResult.SetWindowText(str);
	CStatic*pStatic=(CStatic*)GetDlgItem(IDC_STATIC0);
	str=m_formula.GetDigitalString(data,size)+"=";
	pStatic->SetWindowText(str);
	delete[]data;
}

void CComputerDlg::OnChangeFormula() 
{
	CStatic*pStatic=(CStatic*)GetDlgItem(IDC_STATIC0);
	pStatic->SetWindowText("结果:");
	pStatic=(CStatic*)GetDlgItem(IDC_FIELD_INSTRUCT);
	pStatic->SetWindowText("变量赋值区");

	m_cnResult.SetWindowText("");
	
	m_cnComputer.EnableWindow(FALSE);
	m_staticArray.desroy();m_editDataArray.desroy();
	CButton*button=(CButton*)GetDlgItem(IDC_BUTTON_UP);
	button->EnableWindow(FALSE);
	button=(CButton*)GetDlgItem(IDC_BUTTON_DOWN);
	button->EnableWindow(FALSE);
	m_upTimes=0;
	m_formula.Destroy();
}

void CComputerDlg::OnButtonUp() 
{
	// TODO: Add your control notification handler code here
	m_editDataArray.Move(0,-2*m_editDataArray.getHeight());
	m_staticArray.Move(0,-2*m_staticArray.getHeight());
	m_upTimes++;
	CRect rect;
	GetClientRect(rect);
	CButton*button;
	if(m_editDataArray.getHeightA()<=rect.Height()+m_upTimes*2*m_editDataArray.getHeight())
	{
		button=(CButton*)GetDlgItem(IDC_BUTTON_UP);
		button->EnableWindow(FALSE);
	}
	button=(CButton*)GetDlgItem(IDC_BUTTON_DOWN);
	button->EnableWindow();
	Invalidate();
}

void CComputerDlg::OnButtonDown() 
{
	// TODO: Add your control notification handler code here
	m_editDataArray.Move(0,2*m_editDataArray.getHeight());
	m_staticArray.Move(0,2*m_staticArray.getHeight());
	m_upTimes--;
	CButton*button;
	if(m_upTimes<=0)
	{
		button=(CButton*)GetDlgItem(IDC_BUTTON_DOWN);
		button->EnableWindow(FALSE);
	}
	button=(CButton*)GetDlgItem(IDC_BUTTON_UP);
	button->EnableWindow();
	Invalidate();
}

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here
	m_OK.LoadBitmap(IDB_OK);
	CButton* bt=(CButton*)GetDlgItem(IDOK);
	HBITMAP hBitmap=(HBITMAP)m_OK.GetSafeHandle();
	bt->SetBitmap(hBitmap);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CComputerDlg::OnOK() 
{
	UINT id=2000;
	CWnd* edit=GetDlgItem(IDC_FORMULA);
	if(edit==GetFocus())
		OnGetVariantTable();
	else
		OnGetResult();//CDialog::OnOK();
}
