//Download by http://www.NewXing.com
// ComputerDlg.h : header file
//

#if !defined(AFX_COMPUTERDLG_H__2130510B_485D_43AF_928F_AF3922796F4B__INCLUDED_)
#define AFX_COMPUTERDLG_H__2130510B_485D_43AF_928F_AF3922796F4B__INCLUDED_
#include "StringResolution.h"
#include "EditArray.h"
#include "StaticArray.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CComputerDlg dialog

class CComputerDlg : public CDialog
{
// Construction
public:
	CComputerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CComputerDlg)
	enum { IDD = IDD_COMPUTER_DIALOG };
	CButton	m_cnComputer;
	CEdit	m_cnResult;
	CEdit	m_cnFormula;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CComputerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CComputerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnGetVariantTable();
	afx_msg void OnGetResult();
	afx_msg void OnChangeFormula();
	afx_msg void OnButtonUp();
	afx_msg void OnButtonDown();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CStringResolution	m_formula;
	CEditArray			m_editDataArray;
	CStaticArray		m_staticArray;
	CBitmapButton		m_up;
	CBitmapButton		m_down;
	int					m_upTimes;
	CBitmap				m_OK;
	CBitmap				m_CANCEL;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMPUTERDLG_H__2130510B_485D_43AF_928F_AF3922796F4B__INCLUDED_)
