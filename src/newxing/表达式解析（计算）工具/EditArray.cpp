// EditArray.cpp: implementation of the CEditArray class.
// Download by http://www.NewXing.com
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Computer.h"
#include "EditArray.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEditArray::CEditArray():m_width(100),m_height(20)
{m_Topleft=CPoint(0,0);}
BOOL CEditArray::Create(CPoint point,CWnd* pParentWnd,int width,int height,CStringResolution*formula,DWORD dwStyle)
{
	m_Topleft=point;
	m_width=width,m_height=height;
	int i;
	int num=formula->GetVariantTableSize();
	CPoint p=point;
	CSize size(width,height);
	const COperand* pOd=0;
	if(formula)
		pOd=formula->GetVariantTable();
	for(i=0;i<num;i++)
	{
		CEdit* pEdit=new CEdit;
		pEdit->Create(dwStyle,CRect(p,size),pParentWnd,2000+i);
		if(pOd)
			pEdit->SetWindowText(pOd[i].m_name+"=");
		m_EditArray.Add(pEdit);
		p.y+=height-1;
	}
	return TRUE;
}
BOOL CEditArray::Create(CPoint point,CWnd* pParentWnd,int width,int height,int num,DWORD dwStyle)
{
	m_Topleft=point;
	m_width=width,m_height=height;
	int i;
	CPoint p=point;
	CSize size(width,height);
	for(i=0;i<num;i++)
	{
		CEdit* pEdit=new CEdit;
		pEdit->Create(WS_BORDER|dwStyle,CRect(p,size),pParentWnd,2000+i);
		pEdit->SetWindowText("0");
		m_EditArray.Add(pEdit);
		p.y+=height-1;
	}
	return TRUE;
}
CEditArray::~CEditArray()
{
	for(int i=0;i<m_EditArray.GetSize();i++)
	{
		CEdit* pEdit=(CEdit*)m_EditArray.GetAt(i);
		delete pEdit;
	}
	m_EditArray.RemoveAll();
}

BOOL CEditArray::draw()
{
	for(int i=0;i<m_EditArray.GetSize();i++)
	{
		CEdit*pEdit=(CEdit*)m_EditArray.GetAt(i);
		pEdit->ShowWindow(SW_SHOW);
	}
	return TRUE;
}

BOOL CEditArray::desroy()
{
	for(int i=0;i<m_EditArray.GetSize();i++)
	{
		CEdit* pEdit=(CEdit*)m_EditArray.GetAt(i);
		delete pEdit;
	}
	m_EditArray.RemoveAll();
	return TRUE;
}

BOOL CEditArray::EnableWindow(BOOL b)
{
	for(int i=0;i<m_EditArray.GetSize();i++)
	{
		CEdit* pEdit=(CEdit*)m_EditArray.GetAt(i);
		pEdit->EnableWindow(b);
	}
	return b;
}

int CEditArray::GetWindowData(double *&data)
{
	int value=0;
	int num=m_EditArray.GetSize();
	if(num==0)
		return -1;
	data=new double[num];
	CString str;
	for(int i=0;i<num;i++)
	{
		CEdit*pEdit=(CEdit*)m_EditArray.GetAt(i);
		pEdit->GetWindowText(str);
		if(str.GetLength()==0&&value==0)
			value=i+1;
		data[i]=atof(str);
	}
	return value;
}

void CEditArray::Move(int x, int y)
{
	int num=m_EditArray.GetSize();
	m_Topleft.x+=x;
	m_Topleft.y+=y;
	x=m_Topleft.x;
	y=m_Topleft.y;
	CString str;
	for(int i=0;i<num;i++)
	{
		CEdit*pEdit=(CEdit*)m_EditArray.GetAt(i);
		pEdit->GetWindowText(str);
		pEdit->MoveWindow(x,y,m_width,m_height);
		pEdit->SetWindowText(str);
		y+=m_height-1;
	}
	draw();
}

int CEditArray::getHeight()
{
	return m_height;
}

int CEditArray::getHeightA()
{
	return (m_height-1)*m_EditArray.GetSize();
}