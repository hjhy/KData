//Download by http://www.NewXing.com
// EditArray.h: interface for the CEditArray class.
//
//////////////////////////////////////////////////////////////////////
#include "StringResolution.h"
#if !defined(AFX_EDITARRAY_H__0D483F26_9614_486D_BA9C_733EE608079E__INCLUDED_)
#define AFX_EDITARRAY_H__0D483F26_9614_486D_BA9C_733EE608079E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CEditArray
{
public:
	int getHeight();
	int getHeightA();
	void Move(int x,int y);
	int GetWindowData(double*&data);
	BOOL EnableWindow(BOOL b=TRUE);
	BOOL desroy();
	BOOL draw();
	CEditArray();
	BOOL Create(CPoint point,CWnd* pParentWnd,int width,int height,CStringResolution* formula,DWORD dwStyle=ES_LEFT);
	BOOL Create(CPoint point,CWnd* pParentWnd,int width,int height,int num,DWORD dwStyle=ES_LEFT);
	virtual ~CEditArray();
protected:
	CPtrArray m_EditArray;
	CPoint m_Topleft;
	int m_width,m_height;
};

#endif // !defined(AFX_EDITARRAY_H__0D483F26_9614_486D_BA9C_733EE608079E__INCLUDED_)
