//Download by http://www.NewXing.com
//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Computer.rc
//
#define IDS_INSTRUCT                    1
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_COMPUTER_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDB_OK                          137
#define IDB_CANCEL                      138
#define IDC_FORMULA                     1024
#define IDC_RESULT                      1025
#define IDC_GET_VARIANT_TABLE           1026
#define IDC_GET_RESULT                  1027
#define IDC_STATIC0                     1040
#define IDC_EDIT_INSTRUCT               1041
#define IDC_BUTTON_UP                   1045
#define IDC_BUTTON_DOWN                 1046
#define IDC_BUTTON_INSRUCTION           1048
#define IDC_BUTTON_INSRUCTION2          1049
#define IDC_FIELD_INSTRUCT              1052

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1053
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
