// StaticArray.cpp: implementation of the CStaticArray class.
// Download by http://www.NewXing.com
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Computer.h"
#include "StaticArray.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStaticArray::CStaticArray()
{
	m_Topleft=CPoint(0,0);
	m_width=100,m_height=20;
}

CStaticArray::~CStaticArray()
{
	for(int i=0;i<m_StaticArray.GetSize();i++)
	{
		CEdit* pEdit=(CEdit*)m_StaticArray.GetAt(i);
		delete pEdit;
	}
	m_StaticArray.RemoveAll();
}

BOOL CStaticArray::Create(CPoint point,CWnd* pParentWnd,int width,int height,CStringResolution*formula,DWORD dwStyle)
{
	m_Topleft=point;
	m_width=width,m_height=height;
	int i;
	int num=formula->GetVariantTableSize();
	CPoint p=point;
	CSize size(width,height);
	const COperand* pOd=0;
	if(formula)
		pOd=formula->GetVariantTable();
	for(i=0;i<num;i++)
	{
		CStatic* pStatic=new CStatic;
		pStatic->Create("",dwStyle,CRect(p,size),pParentWnd,2000+i);
		if(pOd)
			pStatic->SetWindowText(pOd[i].m_name+" = ");
		m_StaticArray.Add(pStatic);
		p.y+=height-1;
	}
	return TRUE;
}


BOOL CStaticArray::draw()
{
	for(int i=0;i<m_StaticArray.GetSize();i++)
	{
		CStatic*pStatic=(CStatic*)m_StaticArray.GetAt(i);
		pStatic->ShowWindow(SW_SHOW);
	}
	return TRUE;
}

BOOL CStaticArray::desroy()
{
	for(int i=0;i<m_StaticArray.GetSize();i++)
	{
		CStatic* pEdit=(CStatic*)m_StaticArray.GetAt(i);
		delete pEdit;
	}
	m_StaticArray.RemoveAll();
	return TRUE;
}


void CStaticArray::Move(int x, int y)
{
	int num=m_StaticArray.GetSize();
	m_Topleft.x+=x;
	m_Topleft.y+=y;
	x=m_Topleft.x;
	y=m_Topleft.y;
	CString str;
	for(int i=0;i<num;i++)
	{
		CStatic*pEdit=(CStatic*)m_StaticArray.GetAt(i);
		pEdit->GetWindowText(str);
		pEdit->MoveWindow(x,y,m_width,m_height);
		pEdit->SetWindowText(str);
		y+=m_height-1;
	}
	draw();
}

int CStaticArray::getHeight()
{
	return m_height;
}

int CStaticArray::getHeightA()
{
	return (m_height-1)*m_StaticArray.GetSize();
}
