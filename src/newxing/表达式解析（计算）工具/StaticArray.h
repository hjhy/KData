//Download by http://www.NewXing.com
// StaticArray.h: interface for the CStaticArray class.
//
//////////////////////////////////////////////////////////////////////
#include "StringResolution.h"

#if !defined(AFX_STATICARRAY_H__29D4BCD3_35CC_476E_A5A6_B2107D8C6E75__INCLUDED_)
#define AFX_STATICARRAY_H__29D4BCD3_35CC_476E_A5A6_B2107D8C6E75__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CStaticArray  
{
public:
	int getHeight();
	int getHeightA();
	void Move(int x,int y);
	CStaticArray();
	virtual ~CStaticArray();
	BOOL desroy();
	BOOL draw();
	BOOL Create(CPoint point,CWnd* pParentWnd,int width,int height,CStringResolution* formula,DWORD dwStyle=SS_RIGHT);
protected:
	CPtrArray m_StaticArray;
	CPoint 	m_Topleft;
	int m_width,m_height;
};

#endif // !defined(AFX_STATICARRAY_H__29D4BCD3_35CC_476E_A5A6_B2107D8C6E75__INCLUDED_)
